/******************************************************************************
 * Copyright (c) 2012 Texas Instruments Incorporated - http://www.ti.com
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

/******************************************************************************
 *
 * File Name:  	evmc66x_bmc.c
 *
 * Description:	This file contains the function definitions for accessing
 * 				board management controller
 *
 ******************************************************************************/

/************************
 * Include Files
 ************************/
#include "platform_internal.h"


/******************************************************************************
 *
 * Function:	getBoardVersion
 *
 * Description:	Gets the board version
 *
 * Parameters:	void
 *
 * Return Value: Board version
 ******************************************************************************/
uint32_t getBoardVersion(void)
{
    uint8_t uchValue_lo = 0;
    uint8_t uchValue_hi = 0;

    /* TBD: read the board ID from BMC via I2C */
    return (uchValue_hi << 8 | uchValue_lo);
}

/******************************************************************************
 *
 * Function:	bmcGetUserSwitch
 *
 * Description:	This function gets the status of user switch
 *
 * Parameters:	uint8_t* - address of byte location
 *
 * Return Value: status 0 = switch off, 1 = switch on
 *
 ******************************************************************************/
uint32_t bmcGetUserSwitch(uint32_t uiSwitchNum)
{
    /* TBD: read the user switch status from BMC via I2C */
    return 0;
}

