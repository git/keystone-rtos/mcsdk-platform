/******************************************************************************
 * Copyright (c) 2010-2011 Texas Instruments Incorporated - http://www.ti.com
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *****************************************************************************/
/******************************************************************************
 *
 * File Name:  	evm66x_phy.c
 *
 * Description:	This file contains the phy related APIs
 *
 ******************************************************************************/
/* Chip Level definitions include */
#include "platform_internal.h"
#if 0
unsigned int pa_serdes_cfg_base =  0x0232A000;
#define PASS_CFG_BASE			0x02000000
#define SGMII0_OFFSET			0x00090100
#define SGMII1_OFFSET			0x00090200
#define SGMII2_OFFSET			0x00090400
#define SGMII3_OFFSET			0x00090500
#define SGMII0_STATUS			*((volatile unsigned int *)(PASS_CFG_BASE + SGMII0_OFFSET + 0x14))
#define SGMII1_STATUS			*((volatile unsigned int *)(PASS_CFG_BASE + SGMII1_OFFSET + 0x14))
#define SGMII2_STATUS			*((volatile unsigned int *)(PASS_CFG_BASE + SGMII2_OFFSET + 0x14))
#define SGMII3_STATUS			*((volatile unsigned int *)(PASS_CFG_BASE + SGMII3_OFFSET + 0x14))


#define mkptr(base,offset)	((volatile unsigned int *)((base)+(offset)))
unsigned int reg_poll_to(unsigned int wdth, 
                         unsigned int addr, 
                         unsigned int poll_val, 
                         unsigned int masked_bits, 
                         unsigned int to_cycles)
{
	unsigned int read_data, read_data_masked;
	unsigned int loop_count = 0;
	read_data = (*(unsigned int *) (addr));
	read_data_masked = read_data & masked_bits;


	while (read_data_masked != (poll_val & masked_bits))
	{

		read_data = (*(volatile unsigned int *) (addr));
		read_data_masked = read_data & masked_bits;

		loop_count++;

		if( loop_count == to_cycles )
		{
            printf("reg_poll_to max Limit reached \n");
			return 1;
		}
	}

	return 0;
}
#endif
#define NUM_MAC_PORTS	2
void CSL_SgmiiDefSerdesSetup()
{
    uint32_t i;
    CSL_SERDES_RESULT   csl_retval;
    CSL_SERDES_LANE_ENABLE_STATUS lane_retval = CSL_SERDES_LANE_ENABLE_NO_ERR;
    uint32_t serdes_mux_ethernet_sel;
    CSL_SERDES_LANE_ENABLE_PARAMS_T serdes_lane_enable_params1, serdes_lane_enable_params2;
    int numPort1 = (NUM_MAC_PORTS > 2)?2:NUM_MAC_PORTS;
    int numPort2 = (NUM_MAC_PORTS > 2)?NUM_MAC_PORTS - 2:0;

    /* Check CSISC2_3_MUXSEL bit */
    if (CSL_FEXTR(*(volatile uint32_t *)(CSL_BOOT_CFG_REGS + 0x20), 28, 28) == 0)
        serdes_mux_ethernet_sel = 1;

    memset(&serdes_lane_enable_params1, 0, sizeof(serdes_lane_enable_params1));
    memset(&serdes_lane_enable_params2, 0, sizeof(serdes_lane_enable_params2));

    serdes_lane_enable_params1.base_addr = CSL_CSISC2_2_SERDES_CFG_REGS;
    serdes_lane_enable_params1.ref_clock = CSL_SERDES_REF_CLOCK_156p25M;
    serdes_lane_enable_params1.linkrate = CSL_SERDES_LINK_RATE_1p25G;
    serdes_lane_enable_params1.num_lanes = numPort1;
    serdes_lane_enable_params1.phy_type = SERDES_SGMII;
    serdes_lane_enable_params1.forceattboost = CSL_SERDES_FORCE_ATT_BOOST_DISABLED;

    for(i=0; i< serdes_lane_enable_params1.num_lanes; i++)
    {
        serdes_lane_enable_params1.lane_ctrl_rate[i] = CSL_SERDES_LANE_QUARTER_RATE;
        serdes_lane_enable_params1.loopback_mode[i] = CSL_SERDES_LOOPBACK_DISABLED;

        /* When RX auto adaptation is on, these are the starting values used for att, boost adaptation */
        serdes_lane_enable_params1.rx_coeff.att_start[i] = 7;
        serdes_lane_enable_params1.rx_coeff.boost_start[i] = 5;

        /* For higher speeds PHY-A, force attenuation and boost values  */
        serdes_lane_enable_params1.rx_coeff.force_att_val[i] = 1;
        serdes_lane_enable_params1.rx_coeff.force_boost_val[i] = 1;

        /* CM, C1, C2 are obtained through Serdes Diagnostic BER test */
        serdes_lane_enable_params1.tx_coeff.cm_coeff[i] = 0;
        serdes_lane_enable_params1.tx_coeff.c1_coeff[i] = 0;
        serdes_lane_enable_params1.tx_coeff.c2_coeff[i] = 0;
        serdes_lane_enable_params1.tx_coeff.tx_att[i] = 12;
        serdes_lane_enable_params1.tx_coeff.tx_vreg[i] = 4;
    }
    serdes_lane_enable_params1.lane_mask = (1 << serdes_lane_enable_params1.num_lanes) - 1;;
    serdes_lane_enable_params1.operating_mode = CSL_SERDES_FUNCTIONAL_MODE;

    serdes_lane_enable_params2.base_addr = CSL_CSISC2_3_SERDES_CFG_REGS;
    serdes_lane_enable_params2.ref_clock = CSL_SERDES_REF_CLOCK_156p25M;
    serdes_lane_enable_params2.linkrate = CSL_SERDES_LINK_RATE_1p25G;
    serdes_lane_enable_params2.num_lanes = numPort2;
    serdes_lane_enable_params2.phy_type = SERDES_SGMII;
    serdes_lane_enable_params2.forceattboost = CSL_SERDES_FORCE_ATT_BOOST_DISABLED;

    for(i=0; i< serdes_lane_enable_params2.num_lanes; i++)
    {
        serdes_lane_enable_params2.lane_ctrl_rate[i] = CSL_SERDES_LANE_QUARTER_RATE;
        serdes_lane_enable_params2.loopback_mode[i] = CSL_SERDES_LOOPBACK_DISABLED;

        /* When RX auto adaptation is on, these are the starting values used for att, boost adaptation */
        serdes_lane_enable_params2.rx_coeff.att_start[i] = 7;
        serdes_lane_enable_params2.rx_coeff.boost_start[i] = 5;

        /* For higher speeds PHY-A, force attenuation and boost values  */
        serdes_lane_enable_params2.rx_coeff.force_att_val[i] = 1;
        serdes_lane_enable_params2.rx_coeff.force_boost_val[i] = 1;

        /* CM, C1, C2 are obtained through Serdes Diagnostic BER test */
        serdes_lane_enable_params2.tx_coeff.cm_coeff[i] = 0;
        serdes_lane_enable_params2.tx_coeff.c1_coeff[i] = 0;
        serdes_lane_enable_params2.tx_coeff.c2_coeff[i] = 0;
        serdes_lane_enable_params2.tx_coeff.tx_att[i] = 12;
        serdes_lane_enable_params2.tx_coeff.tx_vreg[i] = 4;
    }
    serdes_lane_enable_params2.lane_mask = (1 << serdes_lane_enable_params2.num_lanes) - 1;;
    serdes_lane_enable_params2.operating_mode = CSL_SERDES_FUNCTIONAL_MODE;


    /* Check CSISC2_3_MUXSEL bit */
    if (CSL_FEXTR(*(volatile uint32_t *)(CSL_BOOT_CFG_REGS + 0x20), 28, 28) == 0)
        serdes_mux_ethernet_sel = 1;

    /* SB CMU and COMLANE and Lane Setup */
    csl_retval = CSL_EthernetSerdesInit(serdes_lane_enable_params1.base_addr,
            serdes_lane_enable_params1.ref_clock,
            serdes_lane_enable_params1.linkrate); /* SGMII Lane 0 and Lane 1 */

    if (serdes_mux_ethernet_sel && numPort2)
    {
        csl_retval |= CSL_EthernetSerdesInit(serdes_lane_enable_params2.base_addr,
                serdes_lane_enable_params2.ref_clock,
                serdes_lane_enable_params2.linkrate); /* SGMII Lane 2 and Lane 3 */
    }

    /* Common Init Mode */
    /* Iteration Mode needs to be set to Common Init Mode first with a lane_mask value equal to the total number of lanes being configured */
    /* The lane_mask is a don't care for Common Init as it operates on all lanes. It always sets it to 0xF internally in the API */
    serdes_lane_enable_params1.iteration_mode = CSL_SERDES_LANE_ENABLE_COMMON_INIT;
    serdes_lane_enable_params1.lane_mask = 0xF;
    lane_retval = CSL_SerdesLaneEnable(&serdes_lane_enable_params1);

    /* Lane Init Mode */
    /* Once CSL_SerdesLaneEnable is called with iteration_mode = CSL_SERDES_LANE_ENABLE_COMMON_INIT, the lanes needs to be enabled by setting
       iteration_mode =  CSL_SERDES_LANE_ENABLE_LANE_INIT with the lane_mask equal to the specific lane being configured */
    /* For example, if lane 0 is being configured, lane mask needs to be set to 0x1. if lane 1 is being configured, lane mask needs to be 0x2 etc */
    serdes_lane_enable_params1.iteration_mode = CSL_SERDES_LANE_ENABLE_LANE_INIT;
    for(i=0; i< serdes_lane_enable_params1.num_lanes; i++)
    {
        serdes_lane_enable_params1.lane_mask = 1<<i;
        lane_retval = CSL_SerdesLaneEnable(&serdes_lane_enable_params1);
    }

    if(serdes_mux_ethernet_sel && numPort2)
    {
         /* Common Init Mode */
         /* Iteration Mode needs to be set to Common Init Mode first with a lane_mask value equal to the total number of lanes being configured */
         /* The lane_mask is a don't care for Common Init as it operates on all lanes. It always sets it to 0xF internally in the API */
         serdes_lane_enable_params2.iteration_mode = CSL_SERDES_LANE_ENABLE_COMMON_INIT;
         serdes_lane_enable_params2.lane_mask = 0xF;
         lane_retval = CSL_SerdesLaneEnable(&serdes_lane_enable_params2);
    
         /* Lane Init Mode */
         /* Once CSL_SerdesLaneEnable is called with iteration_mode = CSL_SERDES_LANE_ENABLE_COMMON_INIT, the lanes needs to be enabled by setting
            iteration_mode =  CSL_SERDES_LANE_ENABLE_LANE_INIT with the lane_mask equal to the specific lane being configured */
         /* For example, if lane 0 is being configured, lane mask needs to be set to 0x1. if lane 1 is being configured, lane mask needs to be 0x2 etc */
         serdes_lane_enable_params2.iteration_mode = CSL_SERDES_LANE_ENABLE_LANE_INIT;
         for(i=0; i< serdes_lane_enable_params2.num_lanes; i++)
         {
             serdes_lane_enable_params2.lane_mask = 1<<i;
             lane_retval = CSL_SerdesLaneEnable(&serdes_lane_enable_params2);
         }
    }

}


/**
 *  @brief
 *      Configure the sgmii Serdes on devices using the initialization sequence
 */
void configSerdes()
{
	CSL_SgmiiDefSerdesSetup();
    /* All done with configuration. Return Now. */
    return;
}

/** ============================================================================
 *   @n@b Init_SGMII
 *
 *   @b Description
 *   @n SGMII peripheral initialization code.
 *
 *   @param[in]
 *   @n macPortNum      MAC port number for which the SGMII port setup must
 *                      be performed.
 *
 *   @return
 *   @n None
 * =============================================================================
 */
#ifdef SIMULATOR_SUPPORT
void Init_SGMII (uint32_t macPortNum)
{
    CSL_SGMII_ADVABILITY    sgmiiCfg;
	CSL_SGMII_STATUS        sgmiiStatus;

    /* Reset the port before configuring it */
    CSL_SGMII_doSoftReset (macPortNum);
    while (CSL_SGMII_getSoftResetStatus (macPortNum) != 0);

    /* Hold the port in soft reset and set up
     * the SGMII control register:
     *      (1) Enable Master Mode
     *      (2) Enable Auto-negotiation
     */
    CSL_SGMII_startRxTxSoftReset (macPortNum);
    CSL_SGMII_enableMasterMode (macPortNum);
    CSL_SGMII_enableAutoNegotiation (macPortNum);
    CSL_SGMII_endRxTxSoftReset (macPortNum);

    /* Setup the Advertised Ability register for this port:
     *      (1) Enable Full duplex mode
     *      (2) Enable Auto Negotiation
     *      (3) Enable the Link
     */
    sgmiiCfg.linkSpeed      =   CSL_SGMII_1000_MBPS;
    sgmiiCfg.duplexMode     =   CSL_SGMII_FULL_DUPLEX;
    CSL_SGMII_setAdvAbility (macPortNum, &sgmiiCfg);

    /* All done with configuration. Return Now. */
    return;
}
#else
void Init_SGMII (uint32_t macPortNum)
{
    CSL_SGMII_ADVABILITY    sgmiiCfg;
    CSL_SGMII_STATUS        sgmiiStatus;

    /* Reset the port before configuring it */
    CSL_SGMII_doSoftReset (macPortNum);
    while (CSL_SGMII_getSoftResetStatus (macPortNum) != 0);

     if (macPortNum < 2) {
        /* Hold the port in soft reset and set up
         * the SGMII control register:
         *      (1) Enable Master Mode
         *      (2) Enable Auto-negotiation
         */
        CSL_SGMII_startRxTxSoftReset (macPortNum);
        CSL_SGMII_disableMasterMode (macPortNum);


        /* Setup the Advertised Ability register for this port:
         *      (1) Enable Full duplex mode
         *      (2) Enable Auto Negotiation
         *      (3) Enable the Link
         */
        sgmiiCfg.linkSpeed      =   CSL_SGMII_1000_MBPS;
        sgmiiCfg.duplexMode     =   CSL_SGMII_FULL_DUPLEX;
        CSL_SGMII_setAdvAbility (macPortNum, &sgmiiCfg);

        CSL_SGMII_enableAutoNegotiation (macPortNum);
        CSL_SGMII_endRxTxSoftReset (macPortNum);
        do
        {
            CSL_SGMII_getStatus(macPortNum, &sgmiiStatus);
        } while (sgmiiStatus.bIsLinkUp != 1);

        /* Wait for SGMII Autonegotiation to complete without error */
        do
        {
            CSL_SGMII_getStatus(macPortNum, &sgmiiStatus);
            if (sgmiiStatus.bIsAutoNegError != 0)
                return; /* This is an error condition */
        } while (sgmiiStatus.bIsAutoNegComplete != 1);
    }

    if (macPortNum >= 2) {
        /* Hold the port in soft reset and set up
         * the SGMII control register:
         *      (1) Disable Master Mode
         *      (2) Enable Auto-negotiation
         */
        CSL_SGMII_startRxTxSoftReset (macPortNum);
        CSL_SGMII_disableMasterMode (macPortNum);

        /* Setup the Advertised Ability register for this port:
         *      (1) Enable Full duplex mode
         *      (2) Enable Auto Negotiation
         *      (3) Enable the Link
         */
        sgmiiCfg.linkSpeed      =   CSL_SGMII_1000_MBPS;
        sgmiiCfg.duplexMode     =   CSL_SGMII_FULL_DUPLEX;
        sgmiiCfg.bLinkUp        =   1;
        CSL_SGMII_setAdvAbility (macPortNum, &sgmiiCfg);

        CSL_SGMII_enableAutoNegotiation (macPortNum);
        CSL_SGMII_endRxTxSoftReset (macPortNum);

        do
        {
            CSL_SGMII_getStatus(macPortNum, &sgmiiStatus);
        } while (sgmiiStatus.bIsLinkUp != 1);
     }

    /* All done with configuration. Return Now. */
    return;
}
#endif


