################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
src/evm66x_i2c.obj: ../src/evm66x_i2c.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evm66x_i2c.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evm66x_nand.obj: ../src/evm66x_nand.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evm66x_nand.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evm66x_sodimm.obj: ../src/evm66x_sodimm.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evm66x_sodimm.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evmc66x.obj: ../src/evmc66x.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evmc66x.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evmc66x_bmc.obj: ../src/evmc66x_bmc.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evmc66x_bmc.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evmc66x_ecc.obj: ../src/evmc66x_ecc.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evmc66x_ecc.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evmc66x_fpga.obj: ../src/evmc66x_fpga.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evmc66x_fpga.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evmc66x_gpio.obj: ../src/evmc66x_gpio.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evmc66x_gpio.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evmc66x_i2c_eeprom.obj: ../src/evmc66x_i2c_eeprom.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evmc66x_i2c_eeprom.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evmc66x_nor.obj: ../src/evmc66x_nor.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evmc66x_nor.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evmc66x_phy.obj: ../src/evmc66x_phy.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evmc66x_phy.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evmc66x_spi.obj: ../src/evmc66x_spi.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evmc66x_spi.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/evmc66x_uart.obj: ../src/evmc66x_uart.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/evmc66x_uart.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/platform.obj: ../src/platform.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"$(CG_TOOL_ROOT)/bin/cl6x" -mv6600 --abi=eabi -g --include_path="$(CG_TOOL_ROOT)/include" --include_path="$(PDK_INSTALL_PATH)/ti/platform/evmk2l/platform_lib/include" --include_path="$(PDK_INSTALL_PATH)" --include_path="$(PDK_INSTALL_PATH)/ti/csl" --include_path="$(PDK_INSTALL_PATH)/ti/platform" --include_path="../include" --include_path="../../.." --define=DEVICE_K2L --display_error_number --diag_warning=225 --preproc_with_compile --preproc_dependency="src/platform.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


