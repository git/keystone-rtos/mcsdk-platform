/******************************************************************************
 * Copyright (c) 2010-2011 Texas Instruments Incorporated - http://www.ti.com
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

/******************************************************************************
 *
 * File    Name:       evmc6670.c
 *
 * Description: This contains   TMS320C6670 specific functions.
 *
 ******************************************************************************/

/************************
 * Include Files
 ************************/
#include "platform_internal.h"

static void pll_delay(uint32_t ix)
{
    while (ix--) {
        asm("   NOP");
    }
}

int prog_pll1_values(PllcHwSetup *hwSetup)
{
    uint32_t temp;

    /* Check the Range for Multiplier and Divider (error > 63) here */
    if (hwSetup->pllM > 4095)
    {
        return -1;
    }

    if (hwSetup->preDiv > 63)
    {
        return -1;
    }

    /* Set the PLL Multiplier, Divider, BWADJ                                    *
     * The PLLM[5:0] bits of the multiplier are controlled by the PLLM Register  *
     * inside the PLL Controller and the PLLM[12:6] bits are controlled by the   *
     * chip-level MAINPLLCTL0 Register.                                          *
     * PLL Control Register (PLLM)  Layout                                       *
     * |31...6   |5...0        |                                                 *
     * |Reserved |PLLM         |                                                 *
     *                                                                           *
     * Main PLL Control Register (MAINPLLCTL0)                                   *
     * |31...24   |23...19   |18...12    | 11...6   |5...0 |                     *
     * |BWADJ[7:0]| Reserved |PLLM[12:6] | Reserved | PLLD |                     */

    /* Set pll multipler (13 bit field) */
    PLLM_REG     = (hwSetup->pllM & 0x0000003F); /* bits[5:0]  */
    temp          = (hwSetup->pllM & 0x1FC0) >> 6;       /* bits[12:6] */
    MAINPLLCTL0_REG  &=~(0x0007F000);                /*Clear PLLM field */
    MAINPLLCTL0_REG  |=((temp << 12) & 0x0007F000);

    /* Set the BWADJ     (12 bit field)                                          *
     * BWADJ[11:8] and BWADJ[7:0] are located in MAINPLLCTL0 and MAINPLLCTL1     *
     * registers. BWADJ[11:0] should be programmed to a value equal to half of   *
     * PLLM[12:0] value (round down if PLLM has an odd value)                    *
     * Example: If PLLM = 15, then BWADJ = 7                                     */
    temp = ((hwSetup->pllM + 1)>> 1) - 1; /* Divide the pllm by 2 */
    MAINPLLCTL0_REG &=~(0xFF000000);  /* Clear the BWADJ Field */
    MAINPLLCTL0_REG |=  ((temp << 24) & 0xFF000000);
    MAINPLLCTL1_REG &=~(0x0000000F);   /* Clear the BWADJ field */
    MAINPLLCTL1_REG |= ((temp >> 8) & 0x0000000F);

    /* Set the pll divider (6 bit field)                                         *
     * PLLD[5:0] is located in MAINPLLCTL0                                       */
    MAINPLLCTL0_REG   &= ~(0x0000003F);    /* Clear the Field */
    MAINPLLCTL0_REG   |= (hwSetup->preDiv & 0x0000003F);

    /* Set the OUTPUT DIVIDE (4 bit field) in SECCTL */
    SECCTL_REG    &= ~(0x00780000);     /* Clear the field       */
    SECCTL_REG   |= ((1 << 19) & 0x00780000) ;

    return(0);
}

CSL_Status CorePllcHwSetup (
        PllcHwSetup          *hwSetup
        )
{
    CSL_Status       status = CSL_SOK;
    volatile uint32_t i, loopCount;
    uint32_t temp;

    /* Unlock the Boot Config */
    CSL_BootCfgUnlockKicker();


    /* Wait for Stabilization time (min 100 us)                                *
     * The below loop is good enough for the Gel file to get minimum of        *
     * 100 micro seconds, this should be appropriately modified for port       *
     * to a C function                                                         *
     * Minimum delay in GEL can be 1 milli seconds, so program to 1ms=1000us,  *
     * more than required, but should be Okay                                  */
    pll_delay(140056);

    /* If PLL previously configured in RBL, avoid di/dt supply excursion by    *
     * matching PLL output to RefClk rate                                      *
     * if RBL configures the PLL, the BYPASS bit would be set to '0'           */
    temp = SECCTL_REG &  0x00800000; /* Check the Bit 23 value */

    if (temp != 0) {
        /* PLL BYPASS is enabled, we assume if not in Bypass ENSAT = 1 */

        /* Usage Note 9: For optimal PLL operation, the ENSAT bit in the PLL control *
         * registers for the Main PLL, DDR3 PLL, and PA PLL should be set to 1.      *
         * The PLL initialization sequence in the boot ROM sets this bit to 0 and    *
         * could lead to non-optimal PLL operation. Software can set the bit to the  *
         * optimal value of 1 after boot                                             *
         * Ref: http://www.ti.com/lit/er/sprz334b/sprz334b.pdf                       *
         * |31...7   |6     |5 4       |3...0      |                                 *
         * |Reserved |ENSAT |Reserved  |BWADJ[11:8]|                                 */

        MAINPLLCTL1_REG = MAINPLLCTL1_REG | 0x00000040;

        /* Clear PLLENSRC bit */
        PLLCTL_REG &= ~(1 << 5);

        /* Clear PLLEN bit */
        PLLCTL_REG &= ~(1 << 0);

        /* Wait for 4 RefClks   *
         * Assuming slowest Ref clock of 25MHz, min: 160 ns delay */
        pll_delay(225);

        /* Bypass needed to perform PWRDN cycle for C6670 and C6678                  *
         * Needed on all devices when in NOBOOT, I2C or SPI boot modes               *
         * Ref: Figure 4-2 of http://www.ti.com/lit/ug/sprugv2a/sprugv2a.pdf         *
         * PLL Secondary Control Register (SECCTL)  Layout                           *
         * |31...24  |23     |22...19       |18...0   |                              *
         * |Reserved |BYPASS |OUTPUT DIVIDE |Reserved |                              */
        SECCTL_REG |= 0x00800000; /* Set the Bit 23 */

        /* Advisory 8: Multiple PLLs May Not Lock After Power-on Reset Issue         *
         * In order to ensure proper PLL startup, the PLL power_down pin needs to be *
         * toggled. This is accomplished by toggling the PLLPWRDN bit in the PLLCTL  *
         * register. This needs to be done before the main PLL initialization        *
         * sequence                                                                  *
         * Ref: Figure 4-1 of http://www.ti.com/lit/ug/sprugv2a/sprugv2a.pdf         *
         * PLL Control Register (PLLCTL)  Layout                                     *
         * |31...4   |3      |2        |1        |0        |                         *
         * |Reserved |PLLRST |Reserved |PLLPWRDN |Reserved |                         */

        PLLCTL_REG   |= 0x00000002; /*Power Down the PLL */

        /* Stay in a loop such that the bit is set for 5 �s (minimum) and           *
         * then clear the bit.                                                      */

        pll_delay(14005);

        /* Power up the PLL */
        PLLCTL_REG   &= ~(0x00000002);

    }

    /* Place PLL in Reset, In PLLCTL, write PLLRST = 1 (PLL is reset)         */
    PLLCTL_REG |= 0x00000008;

    /* Wait for PLL Reset assertion Time (min: 50 us)                          *
     * Minimum delay in GEL can be 1 milli seconds, so program to 1ms=1000us,  *
     * more than required, but should be Okay                                  */
    /*pll_delay(140050);*/

    /* Program the necessary multipliers/dividers and BW adjustments             */
    prog_pll1_values(hwSetup);

    /* go stat bit needs to be zero here    */
    /* Read the GOSTAT bit in PLLSTAT to make sure the bit returns to 0 to      *
     * indicate that the GO operation has completed                             */
    /* wait for the GOSTAT, but don't trap if lock is never read */
    for (i = 0; i < 100; i++) {
        pll_delay(300);
        if ( (PLL1_STAT & 0x00000001) == 0 ) {
            break;
        }
    }
    if (i == 100) {
        return CSL_ESYS_FAIL;
    }

    /* Set PLL dividers if needed */
    PLL1_DIV2 = (0x8000) | (hwSetup->pllDiv2);
    PLL1_DIV5 = (0x8000) | (hwSetup->pllDiv5);
    PLL1_DIV8 = (0x8000) | (hwSetup->pllDiv8);

    /* Program ALNCTLn */
    /* Set bit 1, 4 and 7 */
    PLLALNCTL_REG |= ( (1 << 1) | (1 << 4) | (1 << 7));

    /* Set GOSET bit in PLLCMD to initiate the GO operation to change the divide *
     * values and align the SYSCLKs as programmed                                */
    PLLCMD_REG     |= 0x00000001;

    /* wait for the phase adj */
    pll_delay(1000);

    /* Read the GOSTAT bit in PLLSTAT to make sure the bit returns to 0 to      *
     * indicate that the GO operation has completed                             */

    /* wait for the GOSTAT, but don't trap if lock is never read */
    for (i = 0; i < 100; i++) {
        pll_delay(300);
        if ( (PLL1_STAT & 0x00000001) == 0 ) {
            break;
        }
    }
    if (i == 100) {
        return CSL_ESYS_FAIL;
    }
    /* Wait for a minimum of 7 us*/
    pll_delay (14006);

    /*In PLLCTL, write PLLRST = 0 to bring PLL out of reset */
    PLLCTL_REG &= ~(0x00000008);

    /* Wait for PLL Lock time (min 50 us) */
    pll_delay (140056 >> 1);
    PLL1_SECCTL &= ~(0x00800000); /* Release Bypass */

    /* Set the PLLEN */
    PLLCTL_REG |= (1 << 0);

    return status;
}

CSL_Status CorePllcGetHwSetup (
        PllcHwSetup             *hwSetup
        )
{
    CSL_Status       status   = CSL_SOK;
    volatile uint32_t i, loopCount;

    /* Unlock the Boot Config */
    CSL_BootCfgUnlockKicker();

    hwSetup->divEnable = 0;

    hwSetup->pllM       = (PLLM_REG & 0x3F);
    hwSetup->preDiv     = PREDIV_REG;
    hwSetup->pllDiv2    = PLLDIV2_REG ;
    hwSetup->pllDiv5    = PLLDIV5_REG;
    hwSetup->pllDiv8    = PLLDIV8_REG;

    /* wait for the GOSTAT, but don't trap if lock is never read */
    for (i = 0; i < 100; i++) {
        pll_delay(300);
        if ( (PLL1_STAT & 0x00000001) == 0 ) {
            break;
        }
    }
    if (i == 100) {
        return CSL_ESYS_FAIL;
    }

    return status;
}

CSL_Status
SetPaPllConfig
(
 void
 )
{
    uint32_t passclksel = (DEVSTAT_REG & PASSCLKSEL_MASK);
    uint32_t papllctl0val = PAPLLCTL0_REG;
    uint32_t obsclkval = OBSCLCTL_REG;
    uint32_t pa_pllm = PLLM_PASS;
    uint32_t pa_plld = PLLD_PASS;
    uint32_t temp;
    int pass_freq;
    int pass_freM,pass_freD;

    if (passclksel == 0)
    {
        IFPRINT(platform_write("SYSCLK/ALTCORECLK is the input to the PA PLL...\n"));
    }

    /* Unlock the Boot Config */
    CSL_BootCfgUnlockKicker();

    if (DNUM == 0)
    {
        /* Usage Note 9: For optimal PLL operation, the ENSAT bit in the PLL control *
         * registers for the Main PLL, DDR3 PLL, and PA PLL should be set to 1.      *
         * The PLL initialization sequence in the boot ROM sets this bit to 0 and    *
         * could lead to non-optimal PLL operation. Software can set the bit to the  *
         * optimal value of 1 after boot                                             *
         * PAPLLCTL1_REG Bit map                                                     *
         * |31...7   |6     |5 4       |3...0      |                                 *
         * |Reserved |ENSAT |Reserved  |BWADJ[11:8]|                                 */

        PAPLLCTL1_REG |= 0x00000040;

        /* Wait for the PLL Reset time (min: 1000 ns)                                */
        /*pll_delay(1400);*/

        /* Put the PLL in Bypass Mode                                                *
         * PAPLLCTL0_REG Bit map                                                     *
         * |31...24    |23     |22...19       |18...6   |5...0 |                     *
         * |BWADJ[7:0] |BYPASS |Reserved      |PLLM     |PLLD  |                     */

        PAPLLCTL0_REG |= 0x00800000; /* Set the Bit 23 */

        /*Wait 4 cycles for the slowest of PLLOUT or reference clock source (CLKIN)*/
        /*pll_delay(4);*/

        /* In PLL Controller, reset the PLL (bit 14), set PLLSEL bit (bit 13) in PAPLLCTL1_REG register       */
        PAPLLCTL1_REG |= 0x00006000;

        /* Wait for the PLL Reset time (min: 1000 ns)                                */
        /*pll_delay(1400);*/

        /* Program the necessary multipliers/dividers and BW adjustments             */
        /* Set the divider values */
        PAPLLCTL0_REG &= ~(0x0000003F);
        PAPLLCTL0_REG |= (pa_plld & 0x0000003F);

        /* Set the Multipler values */
        PAPLLCTL0_REG &= ~(0x0007FFC0);
        PAPLLCTL0_REG |= ((pa_pllm << 6) & 0x0007FFC0 );

        /* Set the BWADJ */
        temp = ((pa_pllm + 1) >> 1) - 1;
        PAPLLCTL0_REG &= ~(0xFF000000);
        PAPLLCTL0_REG |= ((temp << 24) & 0xFF000000);
        PAPLLCTL1_REG &= ~(0x0000000F);
        PAPLLCTL1_REG |= ((temp >> 8) & 0x0000000F);

        /*Wait for PLL to lock min 5 micro seconds*/
        pll_delay(7000);

        /*In PAPLLCTL1_REG, write PLLRST = 0 to bring PLL out of reset */
        PAPLLCTL1_REG &= ~(0x00004000);

        /*Wait for PLL to lock min 50 micro seconds*/
        pll_delay(70000);

        /* Put the PLL in PLL Mode                                                   *
         * PAPLLCTL0_REG Bit map                                                     *
         * |31...24    |23     |22...19       |18...6   |5...0 |                     *
         * |BWADJ[7:0] |BYPASS |Reserved      |PLLM     |PLLD  |                     */
        PAPLLCTL0_REG &= ~(0x00800000); /* ReSet the Bit 23 */

        papllctl0val = PAPLLCTL0_REG;

        /* Tells the multiplier value for the PA PLL */
        pa_pllm = (((papllctl0val & PA_PLL_CLKF_MASK) >> 6) + 1);
        IFPRINT(platform_write("PA PLL programmable multiplier = %d\n", pa_pllm));

        /* Tells the divider value for the PA PLL */
        pa_plld = (((papllctl0val & PA_PLL_CLKR_MASK) >> 0) +1);
        IFPRINT(platform_write("PA PLL programmable divider = %d\n", pa_plld));

        // Compute the real pass clk freq (*100)
        //pass_freq = (12288 * (pa_pllm) / (pa_plld) / (1)); //Logic wrong in k2h ,Pass_clk=122.88 Mhz
        pass_freq =  ((10000 * pa_pllm) / (2 * pa_plld));	 //bug fixed ,Pass_clk=100 Mhz
        // Displayed frequency in MHz
        pass_freM = pass_freq / 100;

        // passclk freq first decimal if freq expressed in MHz
        pass_freD = ((pass_freq - pass_freM * 100) + 5) / 10;

        // Add roundup unit to MHz displayed and reajust decimal value if necessary...
        if (pass_freD > 9)
        {
            pass_freD = pass_freD - 10;
            pass_freM = pass_freM + 1;
        }

        IFPRINT(platform_write("PLL3 Setup for PASSCLK @ %d.%d MHz... \n", pass_freM, pass_freD ));
        IFPRINT(platform_write("PLL3 Setup... Done.\n" ));

        return CSL_SOK;

    }
    else
    {
        IFPRINT(platform_write("DSP core #%d cannot set PA PLL \n",DNUM));
        return CSL_ESYS_FAIL;
    }

}

/***************************************************************************************
 * FUNCTION PURPOSE: Power up all the power domains
 ***************************************************************************************
 * DESCRIPTION: this function powers up the PA subsystem domains
 ***************************************************************************************/
void PowerUpDomains (void)
{
    /* PASS power domain is turned OFF by default. It needs to be turned on before doing any
     * PASS device register access. This not required for the simulator. */

    /* Set PASS Power domain to ON */
    CSL_PSC_enablePowerDomain (CSL_PSC_PD_ALWAYSON);

    /* Enable the clocks for PASS modules */
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_AEMIF_SPI, PSC_MODSTATE_ENABLE);

    /* Start the state transition */
    CSL_PSC_startStateTransition (CSL_PSC_PD_ALWAYSON);

    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone (CSL_PSC_PD_ALWAYSON));


    /* PASS power domain is turned OFF by default. It needs to be turned on before doing any
     * PASS device register access. This not required for the simulator. */

    /* Set PASS Power domain to ON */
    CSL_PSC_enablePowerDomain (CSL_PSC_PD_NETCP);

    /* Enable the clocks for PASS modules */
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_PA, PSC_MODSTATE_ENABLE);
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_CPGMAC,  PSC_MODSTATE_ENABLE);
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_SA,  PSC_MODSTATE_ENABLE);

    /* Start the state transition */
    CSL_PSC_startStateTransition (CSL_PSC_PD_NETCP);

    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone (CSL_PSC_PD_NETCP));
}

CSL_Status SetDDR3PllConfig(pll_init_data *data)
{

    uint32_t ddr3pllctl0val = DDR3PLLCTL0_REG(data->pll);
    uint32_t ddr3_pllm = data->pll_m - 1;
    uint32_t ddr3_plld = (data->pll_d - 1) & 0x3f;
    uint32_t ddr3_pllod = (data->pll_od - 1) & 0xf;
    uint32_t bwadj, temp;

    /* Unlock the Boot Config */
    CSL_BootCfgUnlockKicker();

    if (DNUM == 0)
    {
       /* Usage Note 9: For optimal PLL operation, the ENSAT bit in the PLL control *
         * registers for the Main PLL, DDR3 PLL, and PA PLL should be set to 1.      *
         * The PLL initialization sequence in the boot ROM sets this bit to 0 and    *
         * could lead to non-optimal PLL operation. Software can set the bit to the  *
         * optimal value of 1 after boot                                             *
         * DDR3PLLCTL1_REG Bit map                                                     *
         * |31...7   |6     |5 4       |3...0      |                                 *
         * |Reserved |ENSAT |Reserved  |BWADJ[11:8]|                                 */

        DDR3PLLCTL1_REG(data->pll) |= 0x00000040;

        /* Wait for the PLL Reset time (min: 1000 ns)                                */
        /*pll_delay(1400);*/

        /* Put the PLL in Bypass Mode                                                *
         * DDR3PLLCTL0_REG Bit map                                                     *
         * |31...24    |23     |22...19       |18...6   |5...0 |                     *
         * |BWADJ[7:0] |BYPASS |Reserved      |PLLM     |PLLD  |                     */
		temp = DDR3PLLCTL0_REG(data->pll);
        temp &= 0x00800000; /* clear everything except Bypass */

        /* Program the necessary multipliers/dividers and BW adjustments             */
		bwadj = ddr3_pllm >> 1;
		temp |= ((bwadj & 0xff) << 24) | (ddr3_pllm << 6) | (ddr3_plld & 0x3f) | (ddr3_pllod<<19);
		DDR3PLLCTL0_REG(data->pll) = temp;

        /*Wait 4 cycles for the slowest of PLLOUT or reference clock source (CLKIN)*/
        /*pll_delay(4);*/

		/* Set BWADJ[11:8] bits */
		temp = DDR3PLLCTL1_REG(data->pll);
		temp &= ~(0xf);
		temp |= ((bwadj>>8) & 0xf);
		DDR3PLLCTL1_REG(data->pll) = temp;

        /* In PLL Controller, reset the PLL (bit 13) in DDR3PLLCTL1_REG register       */
        DDR3PLLCTL1_REG(data->pll) |= 0x00002000;

        /* Wait for the PLL Reset time (min: 1000 ns)                                */
        pll_delay(7000);

        /*In DDR3PLLCTL1_REG, write PLLRST = 0 to bring PLL out of reset */
        DDR3PLLCTL1_REG(data->pll) &= ~(0x00002000);

        /*Wait for PLL to lock min 50 micro seconds*/
        pll_delay(70000);

        /* Put the PLL in PLL Mode                                                   *
         * DDR3PLLCTL0_REG Bit map                                                   *
         * |31...24    |23     |22...19       |18...6   |5...0 |                     *
         * |BWADJ[7:0] |BYPASS |Reserved      |PLLM     |PLLD  |                     */
        DDR3PLLCTL0_REG(data->pll) &= ~(0x00800000); /* clear bypass, enable pll */

    	pll_delay (0x20000);

        ddr3pllctl0val = DDR3PLLCTL0_REG(data->pll);

        /* Tells the multiplier value for the DDR3 PLL */
        ddr3_pllm = (((ddr3pllctl0val & 0x0007FFC0) >> 6) + 1);
        IFPRINT(platform_write("PA PLL programmable multiplier = %d\n", ddr3_pllm));

        /* Tells the divider value for the DDR3 PLL */
        ddr3_plld = (((ddr3pllctl0val & 0x0000003F) >> 0) +1);
        IFPRINT(platform_write("PA PLL programmable divider = %d\n", ddr3_plld));

        IFPRINT(platform_write("PLL2 Setup... Done.\n" ));

        return CSL_SOK;

    }
    else
    {
        IFPRINT(platform_write("DSP core #%d cannot set DDR3 PLL \n",DNUM));
        return CSL_ESYS_FAIL;
    }
}

#define XMC_BASE_ADDR (0x08000000)
#define XMPAX2_L     (*(volatile unsigned int*)(XMC_BASE_ADDR + 0x00000010))
#define XMPAX2_H     (*(volatile unsigned int*)(XMC_BASE_ADDR + 0x00000014))
/*--------------------------------------------------------------*/
/* xmc_setup()                                                  */
/* XMC MPAX register setting to access DDR3 config space        */
/*--------------------------------------------------------------*/
void xmc_setup()
{
    /* mapping for ddr emif registers XMPAX*2 */

    XMPAX2_L =  0x121010FF;  /* replacement addr + perm */
    XMPAX2_H =  0x2101000B;    /* base addr + seg size (64KB)*/	//"1B"-->"B" by xj
#if 0
    /* mapping for ddr emif registers XMPAX*2 */
    CSL_XMC_XMPAXL    mpaxl;
    CSL_XMC_XMPAXH    mpaxh;

    /* base addr + seg size (64KB)*/    //"1B"-->"B" by xj */
    mpaxh.bAddr     = (0x2100000B >> 12);
    mpaxh.segSize   = (0x2100000B & 0x0000001F);

    /* replacement addr + perm*/
    mpaxl.rAddr     = 0x100000;
    mpaxl.sr        = 1;
    mpaxl.sw        = 1;
    mpaxl.sx        = 1;
    mpaxl.ur        = 1;
    mpaxl.uw        = 1;
    mpaxl.ux        = 1;

    /* set the xmpax for index2 */
    CSL_XMC_setXMPAXH(2, &mpaxh);
    CSL_XMC_setXMPAXL(2, &mpaxl);
#endif
}

static ddr3_emif_config ddr3_1600 =
{
	0x6200DE62,
	0x166C9455,
	0x00001D4A,
	0x321DFF53,
	0x543F07FF,
	0x70073200,
	0x00001869,
};

static ddr3_phy_config ddr3phy_1600 =
{
	0x1C000,
	(IODDRM_MASK | ZCKSEL_MASK | ZCKSEL_MASK),
	((1 << 2) | (1 << 7) | (1 << 23)),
	0x426213CF,
	0xCFC712B3,
	0, /* not set in gel */
	0x08861A80,
	0x0C827100,
	(PDQ_MASK | MPRDQ_MASK | BYTEMASK_MASK | NOSRA_MASK),
	((1 << 10) | (1 << 27)),
	0x011CBB66,
	0x12840300,
	0x5002CE00,
	0x00001C70,
	0x00000006,
	0x00000058,
	0x710035C7,
	0x00F07A12,
	0x0000007B,
	0x0000007B,
	0x0000007B,
	0x00000033,
	0x0000FF81,
};

void init_ddrphy(uint32_t base, ddr3_phy_config *phy_cfg)
{
	uint32_t tmp;

	while((read_reg(base + TCI6638_DDRPHY_PGSR0_OFFSET)
		 & 0x00000001) != 0x00000001);

	write_reg(phy_cfg->pllcr, base + TCI6638_DDRPHY_PLLCR_OFFSET);
	platform_delay (DDR_CFG_DELAY);

	tmp = read_reg(base + TCI6638_DDRPHY_PGCR1_OFFSET);
	tmp &= ~(phy_cfg->pgcr1_mask);
	tmp |= phy_cfg->pgcr1_val;
        write_reg(tmp, TCI6638_DDRPHY_PGCR1_OFFSET);
	platform_delay (DDR_CFG_DELAY);

	write_reg(phy_cfg->ptr0,   base + TCI6638_DDRPHY_PTR0_OFFSET);
	write_reg(phy_cfg->ptr1,   base + TCI6638_DDRPHY_PTR1_OFFSET);
	write_reg(phy_cfg->ptr3,  base + TCI6638_DDRPHY_PTR3_OFFSET);
	write_reg(phy_cfg->ptr4,  base + TCI6638_DDRPHY_PTR4_OFFSET);

	tmp =  read_reg(base + TCI6638_DDRPHY_DCR_OFFSET);
	tmp &= ~(phy_cfg->dcr_mask);
	tmp |= phy_cfg->dcr_val;
	write_reg(tmp, base + TCI6638_DDRPHY_DCR_OFFSET);
	platform_delay (DDR_CFG_DELAY);

	write_reg(phy_cfg->dtpr0, base + TCI6638_DDRPHY_DTPR0_OFFSET);
	write_reg(phy_cfg->dtpr1, base + TCI6638_DDRPHY_DTPR1_OFFSET);
	write_reg(phy_cfg->dtpr2, base + TCI6638_DDRPHY_DTPR2_OFFSET);

	write_reg(phy_cfg->mr0,   base + TCI6638_DDRPHY_MR0_OFFSET);
	write_reg(phy_cfg->mr1,   base + TCI6638_DDRPHY_MR1_OFFSET);
	write_reg(phy_cfg->mr2,   base + TCI6638_DDRPHY_MR2_OFFSET);

	write_reg(phy_cfg->dtcr,  base + TCI6638_DDRPHY_DTCR_OFFSET);
	platform_delay (DDR_CFG_DELAY);
	write_reg(phy_cfg->pgcr2, base + TCI6638_DDRPHY_PGCR2_OFFSET);
	platform_delay (DDR_CFG_DELAY);

	write_reg(phy_cfg->zq0cr1, base + TCI6638_DDRPHY_ZQ0CR1_OFFSET);
	write_reg(phy_cfg->zq1cr1, base + TCI6638_DDRPHY_ZQ1CR1_OFFSET);
	write_reg(phy_cfg->zq2cr1, base + TCI6638_DDRPHY_ZQ2CR1_OFFSET);

	write_reg(phy_cfg->pir_v1, base + TCI6638_DDRPHY_PIR_OFFSET);
	platform_delay (DDR_CFG_DELAY);
	while((read_reg(base + TCI6638_DDRPHY_PGSR0_OFFSET) & 0x1) != 0x1);

	write_reg(phy_cfg->pir_v2, base + TCI6638_DDRPHY_PIR_OFFSET);
	platform_delay (DDR_CFG_DELAY);
	while((read_reg(base + TCI6638_DDRPHY_PGSR0_OFFSET) & 0x1) != 0x1);
}

void init_ddremif(uint32_t base, ddr3_emif_config *emif_cfg)
{
	write_reg(emif_cfg->sdcfg,  base + TCI6638_DDR3_SDCFG_OFFSET );
	write_reg(emif_cfg->sdtim1, base + TCI6638_DDR3_SDTIM1_OFFSET);
	write_reg(emif_cfg->sdtim2, base + TCI6638_DDR3_SDTIM2_OFFSET);
	write_reg(emif_cfg->sdtim3, base + TCI6638_DDR3_SDTIM3_OFFSET);
	write_reg(emif_cfg->sdtim4, base + TCI6638_DDR3_SDTIM4_OFFSET);
	write_reg(emif_cfg->zqcfg,  base + TCI6638_DDR3_ZQCFG_OFFSET );
	write_reg(emif_cfg->sdrfc,  base + TCI6638_DDR3_SDRFC_OFFSET );
}


/* Set the desired DDR3 configuration -- assumes 66.67 MHz DDR3 clock input */
CSL_Status DDR3Init()
{
    CSL_Status status = CSL_SOK;

    CSL_BootCfgUnlockKicker();
    
	init_ddrphy(TCI6638_DDR3A_DDRPHYC, &ddr3phy_1600);
	init_ddremif(TCI6638_DDR3A_EMIF_CTRL_BASE, &ddr3_1600);

    return (status);
}

