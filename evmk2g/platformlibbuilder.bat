@REM ******************************************************************************
@REM * FILE PURPOSE: Platform Library Builder
@REM ******************************************************************************
@REM * FILE NAME: platformlibbuilder.bat
@REM *
@REM * DESCRIPTION: 
@REM *  The batch file cleans and builds the platform library components. Please
@REM *  make sure that the PDK Build Environment is setup correctly before calling
@REM *  this file. Following environment variables will be pre-requisite before 
@REM *  running the batch file
@REM *  C6X_GEN_INSTALL_PATH: Installation directory for Code Generation Tool
@REM *
@REM * Copyright (C) 2015, Texas Instruments, Inc.
@REM *****************************************************************************

@echo OFF

REM *****************************************************************************
REM Clean all Platform library components 
REM *****************************************************************************
gmake -C . LIBDIR=./platform_lib/lib clean

REM *****************************************************************************
REM Build all Platform library components 
REM *****************************************************************************
gmake -C . LIBDIR=./platform_lib/lib all