#*******************************************************************************
#* FILE PURPOSE: Lower level makefile for Creating Platform Libraries
#*******************************************************************************
#* FILE NAME: platform_lib_release.mk
#*
#* DESCRIPTION: Defines Source Files, Compilers flags and build rules
#*
#*
#*******************************************************************************
#

#
# Macro definitions referenced below
#
empty =
space =$(empty) $(empty)
CC = $(C6X_GEN_INSTALL_PATH)/bin/cl6x -c -o3 -ms2 -q -eo.o -mv6600 --abi=eabi
ARIN = $(C6X_GEN_INSTALL_PATH)/bin/ar6x rq
LD = $(C6X_GEN_INSTALL_PATH)/bin/lnk6x --abi=eabi
RTSLIB = -l $(C6X_GEN_INSTALL_PATH)/lib/undefined
INCS = -I. -I$(strip $(subst ;, -I,$(subst $(space),\$(space),$(LIB_INCDIR))))
OBJEXT = oe66
AOBJEXT = se66
INTERNALDEFS = -Dti_targets_elf_C66  -DMAKEFILE_BUILD -eo.$(OBJEXT) -ea.$(AOBJEXT) -fr=$(@D) -fs=$(@D) -ppa -ppd=$@.dep
INTERNALLINKDEFS = -o $@ -m $@.map
# Output for prebuilt generated libraries
OBJDIR = $(LIBDIR)/release/obj

#List the COMMONSRC Files
COMMONSRCC= \
    platform_lib/src/platform.c\
    platform_lib/src/evmc66x.c\
    platform_lib/src/evm66x_i2c.c\
    platform_lib/src/evm66x_nand.c\
    platform_lib/src/evmc66x_audio_dc_adc.c\
    platform_lib/src/evmc66x_audio_dc_dac.c\
    platform_lib/src/evmc66x_bmc.c\
    platform_lib/src/evmc66x_dcan.c\
    platform_lib/src/evmc66x_dss.c\
    platform_lib/src/evmc66x_dss_panel.c\
    platform_lib/src/evmc66x_elm.c\
    platform_lib/src/evmc66x_gpio.c\
    platform_lib/src/evmc66x_gpmc.c\
    platform_lib/src/evmc66x_hdmi.c\
    platform_lib/src/evmc66x_hdmi_panel.c\
    platform_lib/src/evmc66x_i2c_eeprom.c\
    platform_lib/src/evmc66x_i2c_ioexpander.c\
    platform_lib/src/evmc66x_mmchs.c\
    platform_lib/src/evmc66x_mmchs_card.c\
    platform_lib/src/evmc66x_nand_gpmc.c\
    platform_lib/src/evmc66x_nor.c\
    platform_lib/src/evmc66x_pinmux.c\
    platform_lib/src/evmc66x_pwm.c\
    platform_lib/src/evmc66x_qspi.c\
    platform_lib/src/evmc66x_qspi_norflash.c\
    platform_lib/src/evmc66x_spi.c\
    platform_lib/src/evmc66x_touch.c\
    platform_lib/src/evmc66x_uart.c\
    platform_lib/src/platform_audio.c
    
# FLAGS for the COMMONSRC Files
COMMONSRCCFLAGS =   -DDEVICE_K2G

# Make Rule for the COMMONSRC Files
COMMONSRCCOBJS = $(patsubst %.c, $(OBJDIR)/%.$(OBJEXT), $(COMMONSRCC))

$(COMMONSRCCOBJS): $(OBJDIR)/%.$(OBJEXT): %.c
	
	-@echo cle66 $< ...
	if [ ! -d $(@D) ]; then $(MKDIR) $(@D) ; fi;
	$(RM) $@.dep
	$(CC) $(COMMONSRCCFLAGS) $(INTERNALDEFS) $(INCS) -fc $< 
	-@$(CP) $@.dep $@.pp; \
         $(SED) -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
             -e '/^$$/ d' -e 's/$$/ :/' < $@.pp >> $@.dep; \
         $(RM) $@.pp 

#Create Empty rule for dependency
$(COMMONSRCCOBJS):./platform_lib/build/platform_lib_release.mk
./platform_lib/build/platform_lib_release.mk:

#Include Depedency for COMMONSRC Files
ifneq (clean,$(MAKECMDGOALS))
 -include $(COMMONSRCCOBJS:%.$(OBJEXT)=%.$(OBJEXT).dep)
endif

# FLAGS for the COMMONSRC Files

$(LIBDIR)/release/ti.platform.evmk2g.ae66 : $(COMMONSRCCOBJS)
	@echo archiving $? into $@ ...
	if [ ! -d $(LIBDIR) ]; then $(MKDIR) $(LIBDIR) ; fi;
	$(ARIN) $@ $?
	$(RMDIR) $(OBJDIR)
