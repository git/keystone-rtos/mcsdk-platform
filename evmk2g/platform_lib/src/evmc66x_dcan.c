/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 *  \file  evmc66x_dcan.c
 *
 *  \brief This file contains C66x DCAN specific functions.
 *
 ******************************************************************************/

#include "platform_internal.h"

#if (PLATFORM_DCAN_IN)

/**
 * \brief This function initializes DCAN module.
 *
 * This function should be called before using any other DCAN functions
 *
 */
void dcanInit(void)
{
	int32_t ret;

	/* Configure DCAN0 Tx as GPIO pin */
	ret = pinMuxSetMode(PIN_NUM_127, PADCONFIG_MUX_MODE_QUATERNARY);
	if(ret)
	{
		IFPRINT(platform_write("ERROR:In configuring the %d pin\n",
		                       PIN_NUM_127));
	}

    /* Configure DCAN0 Rx as DCAN pin */
	ret = pinMuxSetMode(PIN_NUM_128, PADCONFIG_MUX_MODE_PRIMARY);
	if(ret)
	{
		IFPRINT(platform_write("ERROR:In configuring the %d pin\n",
		                       PIN_NUM_128));
	}

    /* Configure DCAN1 Tx as GPIO pin */
	ret = pinMuxSetMode(PIN_NUM_137, PADCONFIG_MUX_MODE_QUATERNARY);
	if(ret)
	{
    	IFPRINT(platform_write("ERROR:In configuring the %d pin\n",
    	                       PIN_NUM_137));
	}

    /* Configure DCAN1 Rx as DCAN pin */
	ret = pinMuxSetMode(PIN_NUM_138, PADCONFIG_MUX_MODE_SECONDARY);
	if(ret)
	{
    	IFPRINT(platform_write("ERROR:In configuring the %d pin\n",
    	                        PIN_NUM_138));
	}

	/* Set the direction for Tx pins */
	ret = gpioSetDirection(GPIO_PORT_1, GPIO_PIN_56, GPIO_OUT);
	if(ret)
	{
    	IFPRINT(platform_write("Failed to set the direction of %d pin\n",
    	                       GPIO_PIN_56));
	}

	ret = gpioSetDirection(GPIO_PORT_1, GPIO_PIN_66, GPIO_OUT);
	if(ret)
	{
    	IFPRINT(platform_write("Failed to set the direction of %d pin\n",
    	                       GPIO_PIN_66));
    }

#if 0
    /* Set the direction for Rx pins */
	ret = gpioSetDirection(GPIO_PORT_1, GPIO_PIN_57, GPIO_IN);
	if(ret)
	{
		IFPRINT(platform_write("Failed to set the direction of %d pin\n",
							   GPIO_PIN_57));
	}

	ret = gpioSetDirection(GPIO_PORT_1, GPIO_PIN_67, GPIO_IN);
	if(ret)
	{
    	IFPRINT(platform_write("Failed to set the direction of %d pin\n",
    	                       GPIO_PIN_67));
	}
#endif
}

/**
 *
 * \brief  This Function toggle the pins between low and high
 * \n      states with some delay
 *
 * \param  timeDelay  [IN]   Time delay between high and low states in usec
 *
 * \return
 * \n      GPIO_RET_OK          - Requested operation is successful
 * \n      INVALID_GPIO_PORT    - Invalid GPIO port number
 * \n      INVALID_GPIO_NUMBER  - Invalid GPIO pin number
 *
 */
int16_t dcanTogglePins(uint32_t timeDealy)
{
	uint16_t ret;

    /* Set the pins to high */
	ret = gpioSetOutput(GPIO_PORT_1, GPIO_PIN_56);
    if(ret)
    {
    	IFPRINT(platform_write("Error in setting the output of %d pin\n",
    	                       GPIO_PIN_56));
    	goto err;
    }

#if 0
    ret = gpioSetOutput(GPIO_PORT_1, GPIO_PIN_57);
    if(ret)
    {
    	IFPRINT(platform_write("Error in setting the output of %d pin\n",
    	                       GPIO_PIN_57));
    	goto err;
    }
#endif

    ret = gpioSetOutput(GPIO_PORT_1, GPIO_PIN_66);
    if(ret)
    {
    	IFPRINT(platform_write("Error in setting the output of %d pin\n",
    	                       GPIO_PIN_66));
    	goto err;
    }

#if 0
    ret = gpioSetOutput(GPIO_PORT_1, GPIO_PIN_67);
    if(ret)
    {
    	IFPRINT(platform_write("Error in setting the output of %d pin\n",
    	                       GPIO_PIN_67));
    	goto err;
    }
#endif

    platform_delay(timeDealy);

    /* set the pins to low */
    ret = gpioClearOutput( GPIO_PORT_1, GPIO_PIN_56);
    if(ret)
    {

    	IFPRINT(platform_write("Error in clearing the output of %d pin\n",
    	                       GPIO_PIN_56));
    	goto err;
    }

#if 0
    ret = gpioClearOutput( GPIO_PORT_1, GPIO_PIN_57);
    if(ret)
    {
    	IFPRINT(platform_write("Error in clearing the output of %d pin\n",
    	                       GPIO_PIN_57));
    	goto err;
    }
#endif

    ret = gpioClearOutput( GPIO_PORT_1, GPIO_PIN_66);
    if(ret)
    {
    	IFPRINT(platform_write("Error in clearing the output of %d pin\n",
    	                       GPIO_PIN_66));
    	goto err;
    }

#if 0
    ret = gpioClearOutput( GPIO_PORT_1, GPIO_PIN_67);
    if(ret)
    {
    	IFPRINT(platform_write("Error in clearing the output of %d pin\n",
    	                       GPIO_PIN_67));
    	goto err;
    }
#endif

    platform_delay(timeDealy);

err : return (int16_t)ret;

}

#endif /* #if (PLATFORM_DCAN_IN) */

/* Nothing past this point */
