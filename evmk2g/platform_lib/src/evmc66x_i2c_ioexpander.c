/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * \file   evmc66x_i2c_ioexpander.c
 *
 * \brief  This contains C66x specific i2c io expander functions.
 *
 ******************************************************************************/

#include "platform_internal.h"

#if (PLATFORM_I2C_IO_EXP_IN)

/**
 *  \brief    Reads the value of an input pin/port from I/O Expander
 *
 *  Pin/Port from which the value to be read should be configured
 *  as input before calling this function
 *
 *  Set the value of 'pin' to 0xFF to read all the pins in the port
 *
 *  \param    i2cPort   [IN]   I2C controller number
 *  \param    slaveAddr [IN]   Slave address for IO expander
 *  \param    port      [IN]   Port number of the pin
 *  \param    pin       [IN]   Pin number of selected port
 *                           0 - 7 for reading a pin value
 *                           0xFF for reading all 8 pins on the port
 *  \param    pValue    [OUT]  Pointer to store the data read
 *
 *  \return    - I2C_RET
 */
static I2C_RET io_exp_read(uint8_t i2cPort, uint8_t slaveAddr,
                           uint8_t port, uint8_t pin, uint8_t *pValue)
{
    I2C_RET     retVal;
    uint32_t    readOffset;

    /* Read pin */
    if(0 == port)
    {
        /* Port0 Input Cmd */
        readOffset = I2C_IO_EXP_PORT0_IN;
    }
    else if(1 == port)
    {
        /* Port1 Input Cmd */
        readOffset = I2C_IO_EXP_PORT1_IN;
    }

    retVal = i2cRead (i2cPort, slaveAddr, pValue, readOffset, 1, 1);
    if(retVal != I2C_RET_OK)
    {
        IFPRINT(platform_write("I2C Read Failed!!\n"));
        return (retVal);
    }

    /* Check if pin read request and extract the value of the pin */
    if(pin != 0xFF)
    {
        *pValue &= (1 << pin);
    }

    return (retVal);
}

/**
 *  \brief    Writes the given value to an output pin/port of I/O Expander
 *
 *  Pin/Port to which the value to be written should be configured
 *  as output before calling this function
 *
 *  Set the value of 'pin' to 0xFF to write all the pins in the port
 *
 *  \param    i2cPort   [IN]   I2C controller number
 *  \param    slaveAddr [IN]   Slave address for IO expander
 *  \param    port      [IN]   Port number of the pin
 *  \param    pin       [IN]   Pin number of selected port
 *                           0 - 7 for writing a pin value
 *                           0xFF for writing all 8 pins on the port
 *  \param    value     [IN]   Value to write to the pin
 *
 *  \return    - I2C_RET
 */
static I2C_RET io_exp_write(uint8_t i2cPort, uint8_t slaveAddr,
                            uint8_t port, uint8_t pin, uint8_t value)
{
    uint32_t    readOffset;
    I2C_RET     retVal;
    uint8_t     oldValue;
    uint8_t     i2cWriteBuf[2];

    /* Read value of the port */
    if(0 == port)
    {
        /* Port0 Output Cmd */
        readOffset = I2C_IO_EXP_PORT0_OUT;
    }
    else if(1 == port)
    {
        /* Port1 Output Cmd */
        readOffset = I2C_IO_EXP_PORT1_OUT;
    }

    /* Read the current value of the pins if request is to write
       a single pin */
    if(pin != 0xFF)
    {
        /* TODO: Read of output port may not return actual value of pin.
           Need to verify on HW and confirm */
        retVal = i2cRead (i2cPort, slaveAddr, &oldValue, readOffset, 1, 1);
        if(retVal != I2C_RET_OK)
        {
            IFPRINT(platform_write("I2C Read Failed!!\n"));
            return (retVal);
        }

        i2cWriteBuf[1] = oldValue;

        if(value)
        {
            i2cWriteBuf[1] |= ((uint8_t)0x1 << pin);
        }
        else
        {
            i2cWriteBuf[1] &= (~((uint8_t)0x1 << pin));
        }

        i2cDelay(DELAY_CONST);
    }
    else
    {
        i2cWriteBuf[1] = value;
    }

    i2cWriteBuf[0] = readOffset;

    retVal = i2cWrite(i2cPort, slaveAddr, i2cWriteBuf, 2, I2C_RELEASE_BUS);
    if(retVal != I2C_RET_OK)
    {
        IFPRINT(platform_write("I2C Write Failed!!\n"));
    }

    return (retVal);
}

/**
 *  \brief    Configures direction of the IO expander pin/port as IN/OUT
 *
 *  \param    i2cPort   [IN]   I2C controller number
 *  \param    slaveAddr [IN]   Slave address for IO expander
 *  \param    port      [IN]   Port number of the pin
 *  \param    pin       [IN]   Pin number of selected port
 *                           0 - 7 for configuring a pin direction
 *                           0xFF for configuring all 8 pins on the port
 *  \param    pinDir    [IN]   Direction of the pin/port
 *                           I2C_IO_EXP_PIN_IN  - Configures pin as input
 *                           I2C_IO_EXP_PIN_OUT - Configures pin as output
 *                           Value may vary when configuring whole port
 *
 *  \return    - I2C_RET
 */
static I2C_RET io_exp_config(uint8_t i2cPort, uint8_t slaveAddr,
                             uint8_t port, uint8_t pin, uint8_t pinDir)
{
    uint32_t    readOffset;
    I2C_RET     retVal;
    uint8_t     cfgValue;
    uint8_t     i2cWriteBuf[2];

    /* Read the value of current configuration of the port */
    if(0 == port)
    {
        /* Port0 config register */
        readOffset = I2C_IO_EXP_PORT0_CFG;
    }
    else if(1 == port)
    {
        /* Port1 config register */
        readOffset = I2C_IO_EXP_PORT1_CFG;
    }

    /* Read the current configuration of the pins if request is to
       configure a single pin */
    if(pin != 0xFF)
    {
        retVal = i2cRead (i2cPort, slaveAddr, &cfgValue, readOffset, 1, 1);
        if(retVal != I2C_RET_OK)
        {
            IFPRINT(platform_write("I2C Read Failed!!\n"));
            return (retVal);
        }

        i2cWriteBuf[1] = cfgValue;

        if(pinDir == I2C_IO_EXP_PIN_IN)
        {
            i2cWriteBuf[1] |= ((uint8_t)0x1 << pin);
        }
        else
        {
            i2cWriteBuf[1] &= (~((uint8_t)0x1 << pin));
        }

        i2cDelay(DELAY_CONST);
    }
    else
    {
        i2cWriteBuf[1] = pinDir;
    }

    i2cWriteBuf[0] = readOffset;

    retVal = i2cWrite(i2cPort, slaveAddr, i2cWriteBuf, 2, I2C_RELEASE_BUS);
    if(retVal != I2C_RET_OK)
    {
        IFPRINT(platform_write("I2C Write Failed!!\n"));
    }

    return (retVal);
}

/**
 *  \brief    Initializes IO expander
 *
 *  This function configures default direction and value of the IO
 *  expander pins. I2C module should be initialized before calling
 *  this function.
 *
 *  \return    - I2C_RET
 */
I2C_RET i2cIoExpanderInit(void)
{
    I2C_RET    retVal;

    /* Configure default direction of each pin on port 0 */
    retVal = io_exp_config(IO_EXP_I2C_PORT, I2C_IO_EXP_ADDR,
                           0, 0xFF, I2C_IO_EXP_P0_DEF_CFG);
    if(retVal != I2C_RET_OK)
    {
        IFPRINT(platform_write("IO Expander Configuration Failed!\n"));
        return (retVal);
    }

    /* Configure default direction of each pin on port 1 */
    retVal = io_exp_config(IO_EXP_I2C_PORT, I2C_IO_EXP_ADDR,
                           1, 0xFF, I2C_IO_EXP_P1_DEF_CFG);
    if(retVal != I2C_RET_OK)
    {
        IFPRINT(platform_write("IO Expander Configuration Failed!\n"));
        return (retVal);
    }

    /* Set the default value of each pin on port 0 */
    retVal = io_exp_write(IO_EXP_I2C_PORT, I2C_IO_EXP_ADDR,
                          0, 0xFF, I2C_IO_EXP_P0_DEF_VAL);
    if(retVal != I2C_RET_OK)
    {
        IFPRINT(platform_write("IO Expander write Failed!\n"));
        return (retVal);
    }

    /* Set the default value of each pin on port 1 */
    retVal = io_exp_write(IO_EXP_I2C_PORT, I2C_IO_EXP_ADDR,
                          1, 0xFF, I2C_IO_EXP_P1_DEF_VAL);
    if(retVal != I2C_RET_OK)
    {
        IFPRINT(platform_write("IO Expander write Failed!\n"));
        return (retVal);
    }

    return (retVal);
}

/**
 *  \brief    Reads the value of an input pin from I/O Expander
 *
 *  Pin from which the value to be read should be configured
 *  as input before calling this function
 *
 *  \param    pin    [IN]   Pin ID
 *
 *  \return    - Pin value or 0xFF in case of error
 */
uint8_t i2cIoExpanderReadPin(I2cIoExpPins pin)
{
    I2C_RET    retVal;
    uint8_t    port;
    uint8_t    pinNum;
    uint8_t    pinVal;

    pinVal = 0xFF;

    if((pin >= IO_EXP_P00) && (pin < IO_EXP_MAX))
    {
        port   = IO_EXP_GET_PORT(pin);
        pinNum = pin % IO_EXP_P10;

        retVal = io_exp_read(IO_EXP_I2C_PORT, I2C_IO_EXP_ADDR,
                             port, pinNum, &pinVal);
        if(retVal != I2C_RET_OK)
        {
            pinVal = 0xFF;
        }
    }

    return (pinVal);
}

/**
 *  \brief    Writes the give value to an output pin of I/O Expander
 *
 *  Pin to which the value to be written should be configured
 *  as output before calling this function
 *
 *  \param  pin    [IN]   Pin number of selected port
 *  \param  value  [IN]   Value to write
 *
 *  \return    - I2C_RET
 */
I2C_RET i2cIoExpanderWritePin(I2cIoExpPins pin, uint8_t value)
{
    I2C_RET    retVal;
    uint8_t    port;
    uint8_t    pinNum;

    if((pin >= IO_EXP_P00) && (pin < IO_EXP_MAX))
    {
        port   = IO_EXP_GET_PORT(pin);
        pinNum = pin % IO_EXP_P10;

        retVal = io_exp_write(IO_EXP_I2C_PORT, I2C_IO_EXP_ADDR,
                              port, pinNum, value);
    }
    else
    {
        retVal = I2C_RET_INVALID_PARAM;
    }

    return (retVal);
}

/**
 *  \brief    Configures direction of the IO expander pin as IN/OUT
 *
 *  \param  pin     [IN]   Pin number of selected port
 *  \param  pinDir  [IN]   Direction of the pin
 *                           I2C_IO_EXP_PIN_IN  - Configures pin as input
 *                           I2C_IO_EXP_PIN_OUT - Configures pin as output
 *
 *  \return    - I2C_RET
 */
I2C_RET i2cIoExpanderConfigPin(I2cIoExpPins pin, uint8_t pinDir)
{
    I2C_RET    retVal;
    uint8_t    port;
    uint8_t    pinNum;

    if(((pin >= IO_EXP_P00) && (pin < IO_EXP_MAX)) &&
       ((pinDir == I2C_IO_EXP_PIN_IN) || (pinDir == I2C_IO_EXP_PIN_OUT)))
    {
        port   = IO_EXP_GET_PORT(pin);
        pinNum = pin % IO_EXP_P10;

        retVal = io_exp_config(IO_EXP_I2C_PORT, I2C_IO_EXP_ADDR,
                               port, pinNum, pinDir);
    }
    else
    {
        retVal = I2C_RET_INVALID_PARAM;
    }

    return (retVal);
}

#endif  /* #if (PLATFORM_I2C_IO_EXP_IN) */

/* Nothing past this point */
