/******************************************************************************
 * Copyright (c) 2010-2011 Texas Instruments Incorporated - http://www.ti.com
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *****************************************************************************/
/******************************************************************************
 *
 * File Name:  	evm66x_phy.c
 *
 * Description:	This file contains the phy related APIs
 *
 ******************************************************************************/
/* Chip Level definitions include */
#include "platform_internal.h"
#define NUM_MAC_PORTS	3
//#define SERDES_INTERNAL_LOOPBACK
void CSL_SgmiiDefSerdesSetup()
{
	uint32_t i;
	CSL_SERDES_RESULT   csl_retval;
	CSL_SERDES_LANE_ENABLE_STATUS lane_retval;
	CSL_SERDES_STATUS   pllstat;
   int numPort1 = (NUM_MAC_PORTS > 2)?2:NUM_MAC_PORTS;
   int numPort2 = (NUM_MAC_PORTS > 2)?NUM_MAC_PORTS - 2:0;


	/* SB CMU and COMLANE and Lane Setup */
	csl_retval = CSL_EthernetSerdesInit(CSL_NETCP_SERDES_0_CFG_REGS,
										CSL_SERDES_REF_CLOCK_156p25M,
										CSL_SERDES_LINK_RATE_1p25G); /* 4 port switch1 */

	   if (numPort2)
	   {
		csl_retval |= CSL_EthernetSerdesInit(CSL_NETCP_SERDES_1_CFG_REGS,
											 CSL_SERDES_REF_CLOCK_156p25M,
											 CSL_SERDES_LINK_RATE_1p25G); /* 4 port switch2 */
	   }

	//SB Lane Enable
	for(i=0; i < numPort1; i++)
	{
		lane_retval |= CSL_EthernetSerdesLaneEnable(CSL_NETCP_SERDES_0_CFG_REGS,
													i,
													CSL_SERDES_LOOPBACK_DISABLED,
													CSL_SERDES_LANE_QUARTER_RATE); /* 4 port switch1 */
	}

	for(i=0; i < numPort2; i++)
	{
		lane_retval |= CSL_EthernetSerdesLaneEnable(CSL_NETCP_SERDES_1_CFG_REGS,
													i,
													CSL_SERDES_LOOPBACK_DISABLED,
													CSL_SERDES_LANE_QUARTER_RATE); /* 4 port switch2 */
	}

	/* SB PLL Enable */
	CSL_EthernetSerdesPllEnable(CSL_NETCP_SERDES_0_CFG_REGS); /* 4 port switch1 */
	if(numPort2)
		CSL_EthernetSerdesPllEnable(CSL_NETCP_SERDES_1_CFG_REGS); /* 4 port switch2 */

	/* SB PLL Status Poll */
	do
	{
		pllstat = CSL_EthernetSerdesGetStatus(CSL_NETCP_SERDES_0_CFG_REGS, numPort1); /* 4 port switch1 */
		   if(numPort2)
			pllstat &= CSL_EthernetSerdesGetStatus(CSL_NETCP_SERDES_1_CFG_REGS, numPort2); /* 4 port switch2 */
	}while(pllstat == CSL_SERDES_STATUS_PLL_NOT_LOCKED);

}


/**
 *  @brief
 *      Configure the sgmii Serdes on devices using the initialization sequence
 */
void configSerdes()
{
	CSL_SgmiiDefSerdesSetup();
    /* All done with configuration. Return Now. */
    return;
}

/** ============================================================================
 *   @n@b Init_SGMII
 *
 *   @b Description
 *   @n SGMII peripheral initialization code.
 *
 *   @param[in]
 *   @n macPortNum      MAC port number for which the SGMII port setup must
 *                      be performed.
 *
 *   @return
 *   @n None
 * =============================================================================
 */
#ifdef SIMULATOR_SUPPORT
void Init_SGMII (uint32_t macPortNum)
{
    CSL_SGMII_ADVABILITY    sgmiiCfg;
	CSL_SGMII_STATUS        sgmiiStatus;

    /* Reset the port before configuring it */
    CSL_SGMII_doSoftReset (macPortNum);
    while (CSL_SGMII_getSoftResetStatus (macPortNum) != 0);

    /* Hold the port in soft reset and set up
     * the SGMII control register:
     *      (1) Enable Master Mode
     *      (2) Enable Auto-negotiation
     */
    CSL_SGMII_startRxTxSoftReset (macPortNum);
    CSL_SGMII_enableMasterMode (macPortNum);
    CSL_SGMII_enableAutoNegotiation (macPortNum);
    CSL_SGMII_endRxTxSoftReset (macPortNum);

    /* Setup the Advertised Ability register for this port:
     *      (1) Enable Full duplex mode
     *      (2) Enable Auto Negotiation
     *      (3) Enable the Link
     */
    sgmiiCfg.linkSpeed      =   CSL_SGMII_1000_MBPS;
    sgmiiCfg.duplexMode     =   CSL_SGMII_FULL_DUPLEX;
    CSL_SGMII_setAdvAbility (macPortNum, &sgmiiCfg);

    /* All done with configuration. Return Now. */
    return;
}
#else
void Init_SGMII (uint32_t macPortNum)
{
    CSL_SGMII_ADVABILITY    sgmiiCfg;
    CSL_SGMII_STATUS        sgmiiStatus;

    /* Reset the port before configuring it */
    CSL_SGMII_doSoftReset (macPortNum);
    while (CSL_SGMII_getSoftResetStatus (macPortNum) != 0);

     if (macPortNum < 2) {
        /* Hold the port in soft reset and set up
         * the SGMII control register:
         *      (1) Enable Master Mode
         *      (2) Enable Auto-negotiation
         */
        CSL_SGMII_startRxTxSoftReset (macPortNum);
        CSL_SGMII_disableMasterMode (macPortNum);
        CSL_SGMII_enableAutoNegotiation (macPortNum);
        CSL_SGMII_endRxTxSoftReset (macPortNum);

        /* Setup the Advertised Ability register for this port:
         *      (1) Enable Full duplex mode
         *      (2) Enable Auto Negotiation
         *      (3) Enable the Link
         */
        sgmiiCfg.linkSpeed      =   CSL_SGMII_1000_MBPS;
        sgmiiCfg.duplexMode     =   CSL_SGMII_FULL_DUPLEX;
        CSL_SGMII_setAdvAbility (macPortNum, &sgmiiCfg);

        do
        {
            CSL_SGMII_getStatus(macPortNum, &sgmiiStatus);
        } while (sgmiiStatus.bIsLinkUp != 1);

        /* Wait for SGMII Autonegotiation to complete without error */
        do
        {
            CSL_SGMII_getStatus(macPortNum, &sgmiiStatus);
            if (sgmiiStatus.bIsAutoNegError != 0)
                return; /* This is an error condition */
        } while (sgmiiStatus.bIsAutoNegComplete != 1);
    }

    if (macPortNum >= 2) {
        /* Hold the port in soft reset and set up
         * the SGMII control register:
         *      (1) Disable Master Mode
         *      (2) Enable Auto-negotiation
         */
        CSL_SGMII_startRxTxSoftReset (macPortNum);
        CSL_SGMII_disableMasterMode (macPortNum);
        CSL_SGMII_enableAutoNegotiation (macPortNum);
        CSL_SGMII_endRxTxSoftReset (macPortNum);

        /* Setup the Advertised Ability register for this port:
         *      (1) Enable Full duplex mode
         *      (2) Enable Auto Negotiation
         *      (3) Enable the Link
         */
        sgmiiCfg.linkSpeed      =   CSL_SGMII_1000_MBPS;
        sgmiiCfg.duplexMode     =   CSL_SGMII_FULL_DUPLEX;
        sgmiiCfg.bLinkUp        =   1;
        CSL_SGMII_setAdvAbility (macPortNum, &sgmiiCfg);

        do
        {
            CSL_SGMII_getStatus(macPortNum, &sgmiiStatus);
        } while (sgmiiStatus.bIsLinkUp != 1);
     }

    /* All done with configuration. Return Now. */
    return;
}
#endif


