/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *  \file      evmc66x_audio_dc_dac.c
 *
 *  \brief     Implementation of low level functions for PCM169x DAC.
 *
 *  This file contains low level library implementation for PCM169x DAC.
 *  This library is designed to work together with a high-level API layer
 *  which takes care of application interfacing. Input argument selection
 *  is done for ease of programming. Input argument boundary check and
 *  verification is expected to be done at the upper layers.
 *
 */

/******************************************************************************
 **                          INCLUDE FILE
 ******************************************************************************/
#include "platform_internal.h"


/******************************************************************************
 **                          GLOBAL DEFINITIONS
 ******************************************************************************/

#if (PLATFORM_AUDIO_DAC)

/**
 * \brief     Array with the default values of DAC registers.
 *
 * {"Reg addr", "Value"}
 *
 * Array to be used when we call pcm169xDACInit from app layer.
 *
 **/
static const DacRegDefConfig pcm169xReg[] = {
	{0x40, 0xC0},  /** Normal Operation, Disable analog mute circuit &
			           select 'AUTO' as sampling mode selection */
	{0x41, 0x00},  /** Power save enable mode & I2S format */
	{0x42, 0x00},  /** DAC1-DAC8 are in normal operation mode &
			           sharp rolloff filter is selected */
	{0x43, 0x00},  /** DAC1-DAC8 Phase of analog signal is normal output */
	{0x44, 0x00},  /** DAC1-DAC8 soft mute is disabled */
	{0x46, 0x10},  /** Attenuation mode : 0.5dB, digital de-empahasis :
			           48kHz & ZERO1 = DATA1 left, ZERO2 = DATA1 right
			           and high for zero detect*/
	{0x48, 0xFF},  /** VOUT1 0dB, no digital attenuation */
	{0x49, 0xFF},  /** VOUT2 0dB, no digital attenuation */
	{0x4A, 0xFF},  /** VOUT3 0dB, no digital attenuation */
	{0x4B, 0xFF},  /** VOUT4 0dB, no digital attenuation */
	{0x4C, 0xFF},  /** VOUT5 0dB, no digital attenuation */
	{0x4D, 0xFF},  /** VOUT6 0dB, no digital attenuation */
	{0x4E, 0xFF},  /** VOUT7 0dB, no digital attenuation */
	{0x4F, 0xFF}   /** VOUT8 0dB, no digital attenuation */
};

/******************************************************************************
 **                          FUNCTION DEFINITIONS
 ******************************************************************************/

/**
 * \brief     Reads DAC register using I2C interface.
 *
 * \param     addr  [IN]  DAC HW instance I2C slave address.
 *
 * \param     reg   [IN]  Register to be written.
 *
 * \param     data  [OUT] Pointer to store data read
 *
 *
 * \return    0 for success.
 *
 **/
static DAC_RET pcm169x_read_reg(Uint8 addr, Uint8 reg, Uint8 *data)
{
	DAC_RET ret;
	I2C_RET i2cRet;

	ret = DAC_RET_OK;

	DBG_PCM169x (platform_write("pcm169x_read_reg() : addr = 0x%x "
				                "reg = 0x%x\n", addr, reg));

	i2cRet = i2cRead(PCM169x_I2C_PORT_NUM, addr, data, reg, 1, 1);
	if(i2cRet)
	{
		ret = DAC_RET_I2C_ERR;
		IFPRINT(platform_write("pcm169x_read_reg() : i2cRead error : ret = %d\n",
		                       ret));
	}

    /* Small delay to get the I2C bus ready and allow multiple reads */
	platform_delay(DAC_I2C_DELAY);

	return (ret);
}

/**
 * \brief     Writes DAC register using I2C interface.
 *
 * Note: Disable ENABLE_DAC_REG_ECHO to stop register echo by write function
 *
 * \param     addr  [IN] DAC HW instance I2C slave address.
 *
 * \param     reg   [IN] Register to be written.
 *
 * \param     data  [IN] Data to be written
 *
 *
 * \return    0 for success.
 *
 **/
static DAC_RET pcm169x_write_reg(Uint8 addr, Uint8 reg, Uint8 data)
{
	DAC_RET  ret;
	I2C_RET  i2cRet;
	Uint8    slaveData[2];
	Uint8    value;

    ret = DAC_RET_OK;

	slaveData[0] = reg;
	slaveData[1] = data;

	DBG_PCM169x (platform_write("pcm169x_write_reg() : addr = 0x%x reg = 0x%x"
				                " data = 0x%x\n", addr, reg, data));

	i2cRet = i2cWrite(PCM169x_I2C_PORT_NUM, addr, slaveData, 2, I2C_RELEASE_BUS);
	if(i2cRet)
	{
		ret = DAC_RET_I2C_ERR;
		IFPRINT(platform_write("pcm169x_write_reg() : i2cWrite Error : ret = %d\n",
		                        ret));
	}

    /* Small delay to get the I2C bus ready and allow multiple writes */
	platform_delay(DAC_I2C_DELAY);

#ifdef ENABLE_DAC_REG_ECHO
	ret = pcm169x_read_reg(addr, reg, &value);
	if(ret)
	{
		IFPRINT (platform_write("pcm186x_write_reg(): ADC Read for Reg Echo Failed\n"));
	}
#endif

	return (ret);
}


/**
 * \brief     Configures all the DAC registers to default value.
 *
 * \param     addr [IN] DAC HW instance I2C slave address.
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xDacInit (Uint8 addr)
{
	DAC_RET ret = -1;
	Uint8   count;

	DBG_PCM169x (platform_write("pcm169xDacInit() : addr = 0x%x\n", addr));

	for (count = 0; count < ARRAY_SIZE(pcm169xReg); count++)
	{
		ret = pcm169x_write_reg(addr, pcm169xReg[count].reg, pcm169xReg[count].def);
		if(ret)
		{
			IFPRINT (platform_write("pcm169xDacInit() : Error in Writing Register = 0x%x\n", pcm169xReg[count].reg));
			return (ret);
		}
	}

	return (ret);
}

/**
 * \brief     Resets the PCM169x Codec
 *
 * \param     addr   [IN] DAC HW instance I2C slave address.
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xReset(Uint8 addr)
{
	DAC_RET ret;

	DBG_PCM169x (platform_write("pcm169xReset() : addr = 0x%x\n", addr));

	ret = pcm169x_write_reg(addr, PCM169x_RESET_MODE_CTRL, PCM169x_SRST_CTRL_VAL);

	return (ret);
}

/**
 * \brief     Sets the DAC Volume
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     vol      [IN] Volume in percentage; 0 to 100
 *
 * \param     chanId   [IN] Channel ID to set volume
 *                          0 to 7 for individual channel volume configuration
 *                          0xF for volume configuration all the 8 channels
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xSetVolume(Uint8 addr, Uint8 vol, Uint8 chanId)
{
	Uint8   count;
	Uint8   value;
	Int8    attnMode;
	DAC_RET ret;

	DBG_PCM169x (platform_write("pcm169xSetVolume() : addr = 0x%x "
				                "vol = %d  chanId = %d\n", addr, vol, chanId));

	/* Read the configured attenuation mode */
	attnMode = pcm169xGetAttnMode(addr);
	if(attnMode == DAC_RET_FAIL)
	{
		IFPRINT(platform_write("pcm169xSetVolume() : Reading Attenuation Mode Failed\n"));
		ret = DAC_RET_FAIL;
		return (ret);
	}

	if(attnMode == 0)
	{
		/* Fine step attenuation mode */
		if(vol == 0)
		{
			value = vol;
			IFPRINT(platform_write("pcm169xSetVolume() : Volume Muted\n"));
		}
		else
		{
			/* DAC volume ranges from 0dB to -63dB in steps of 0.5 (127 values)
			   which needs to be mapped from 1 to 100% by a factor of 1.27.
			   Volume step value may not be linear at some points due to
			   fractional parts and is ignored to simplify the computation */
			value = ((vol * 1.27) + 128);
			IFPRINT(platform_write("pcm169xSetVolume() : Setting the Volume to %ddB\n",
			                       ((value - 255) * 0.5)));
		}
	}
	else
	{
		/* Wide range attenuation mode */
		if(vol == 0)
		{
			value = vol;
			IFPRINT(platform_write("pcm169xSetVolume() : Volume Muted\n"));
		}
		else
		{
			/* DAC volume ranges from 0dB to -100dB in steps of 1 (101 values)
			   which needs to be mapped from 1 to 100%.
			   Only the values 0dB to -99dB are supported to simplify the
			   computation */
			value = (vol + 155);
			IFPRINT(platform_write("pcm169xSetVolume() : Setting the Volume to %ddB\n",
			                       (value - 255)));
		}
	}

	if(chanId == 0xF)
	{
		/* Set volume for all the 8 channels */
		for (count = 0; count < 8; count++)
		{
			ret = pcm169x_write_reg(addr, PCM169x_ATT_CONTROL(count), value);
			if(ret)
			{
				IFPRINT(platform_write("pcm169xSetVolume() : Setting the Volume to Channel %d Failed\n",
									   chanId));
				return (ret);
			}
		}
	}
	else
	{
		/* Check the channel Id as invalid value can cause wrong
		   register access */
		if(chanId < 8)
		{
			ret = pcm169x_write_reg(addr, PCM169x_ATT_CONTROL(chanId), value);
			if(ret)
			{
				IFPRINT(platform_write("pcm169xSetVolume() : Setting the Volume to Channel %d Failed\n",
									   chanId));
				return (ret);
			}
		}
		else
		{
			ret = DAC_RET_INV_PARAM;
		}
	}

	return (ret);
}

/**
 * \brief     Unmute or Mute DAC Outputs.
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     chanId   [IN] Channel ID offset for mute control
 *                          0x01 - DAC1
 *                          0x02 - DAC2
 *                          0x04 - DAC3
 *                          0x08 - DAC4
 *                          0x10 - DAC5
 *                          0x20 - DAC6
 *                          0x40 - DAC7
 *                          0x80 - DAC8
 *                          0xFF - All channels
 *
 * \param     mute     [IN] Mute configuration control
 *                          1 : Mute DAC Output.
 *                          0 : Unmute DAC Output.
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xSoftMuteCtrl(Uint8 addr, Uint8 chanId, Uint8 mute)
{
	DAC_RET ret;
	Uint8   value;

	DBG_PCM169x (platform_write("pcm169xSoftMuteCtrl() : addr = 0x%x "
				                "chanId = %d " "mute = %d\n" , addr, chanId, mute));

	ret = pcm169x_read_reg(addr, PCM169x_SOFT_MUTE, &value);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xSoftMuteCtrl() : Error in Reading Register = 0x%x\n",
		                        PCM169x_SOFT_MUTE));
		return (ret);
	}

	if(mute == 0)
	{
		value = (value & ~(chanId));
	}
	else
	{
		value = (value | chanId);
	}

	DBG_PCM169x (platform_write("pcm169xSoftMuteCtrl() : Value = 0x%x\n", value));

	ret = pcm169x_write_reg(addr, PCM169x_SOFT_MUTE, value);

	return (ret);
}

/**
 * \brief     Configures output phase
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     chanId   [IN] Channel ID offset for phase control
 *                          0x01 - DAC1
 *                          0x02 - DAC2
 *                          0x04 - DAC3
 *                          0x08 - DAC4
 *                          0x10 - DAC5
 *                          0x20 - DAC6
 *                          0x40 - DAC7
 *                          0x80 - DAC8
 *                          0xFF - All channels
 *
 * \param     phase    [IN] DAC output phase
 *                          0 - Normal Output.
 *                          1 - Inverted Output.
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xSetOutputPhase(Uint8 addr, Uint8 chanId, Uint8 phase)
{
	DAC_RET ret;
	Uint8   value;

	DBG_PCM169x (platform_write("pcm169xSetOutputPhase() : addr = 0x%x "
	                            "chanId = %d\n" "phase = %d\n",
	                            addr, chanId, phase));

	ret = pcm169x_read_reg(addr, PCM169x_REVDA_SEL, &value);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xSetOutputPhase() : Error in Reading Register = 0x%x\n",
		                        PCM169x_REVDA_SEL));
		return (ret);
	}

	if(phase == 0)
	{
		value = (value & ~(chanId));
	}
	else
	{
		value = (value | chanId);
	}

	DBG_PCM169x (platform_write("pcm169xSetOutputPhase() : Value = 0x%x\n", value));

	ret = pcm169x_write_reg(addr, PCM169x_REVDA_SEL, value);

	return (ret);
}

/**
 * \brief     Configures the power-save mode
 *
 * \param     addr  [IN] DAC HW instance I2C slave address.
 *
 * \param     mode  [IN] Power mode configuration
 *                       0 - Enable power-save mode
 *                       1 - Disable power-save mode
 *
 * \return    0 if success.
 *
 **/
DAC_RET pcm169xSetPowerMode(Uint8 addr, Uint8 mode)
{
	Uint8   read = 0;
	DAC_RET ret;

	DBG_PCM169x (platform_write("pcm169xSetPowerMode() : addr = 0x%x mode = 0x%x\n",
	                            addr, mode));

	ret = pcm169x_read_reg(addr, PCM169x_AUDIO_FMT, &read);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xSetPowerMode() : Error in Reading Register = 0x%x\n",
								 PCM169x_AUDIO_FMT));
		return (ret);
	}

	ret = pcm169x_write_reg(addr, PCM169x_AUDIO_FMT, ((read & 0x7F) | mode));

	return (ret);
}

/**
 * \brief     Configures the data format and slot width
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     dataFmt    Data type for the codec operation
 *
 *    DAC_DATA_FORMAT_I2S - 16-/20-/24-/32-bit I2S format (default)
 *    DAC_DATA_FORMAT_LEFTJ - 16-/20-/24-/32-bit left-justified format
 *    DAC_DATA_FORMAT_24BIT_RIGHTJ - 24-bit right-justified format
 *    DAC_DATA_FORMAT_16BIT_RIGHTJ - 16-bit right-justified format
 *    DAC_DATA_FORMAT_24BIT_I2S_DSP - 24-bit I2S mode DSP format
 *    DAC_DATA_FORMAT_24BIT_LEFTJ_DSP - 24-bit left-justified mode DSP format
 *    DAC_DATA_FORMAT_24BIT_I2S_TDM - 24-bit I2S mode TDM format
 *    DAC_DATA_FORMAT_24BIT_LEFTJ_TDM - 24-bit left-justified mode TDM format
 *    DAC_DATA_FORMAT_24BIT_HS_I2S_TDM - 24-bit high-speed I2S mode TDM format
 *    DAC_DATA_FORMAT_24BIT_HS_LEFTJ_TDM - 24-bit high-speed left-justified
 *                                         mode TDM format
 *
 * \return    0 if success.
 *
 **/
DAC_RET pcm169xDataConfig(Uint8 addr, Uint8 dataFmt)
{
	Uint8   read = 0;
	DAC_RET ret;

	DBG_PCM169x (platform_write("pcm169xDataConfig() : addr = 0x%x dataFmt = 0x%x\n",
	                            addr, dataFmt));

	ret = pcm169x_read_reg(addr, PCM169x_AUDIO_FMT, &read);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xDataConfig() : Error in Reading Register = 0x%x\n",
								 PCM169x_AUDIO_FMT));
		return (ret);
	}

	DBG_PCM169x (platform_write("read = 0x%x \n", read));

	ret = pcm169x_write_reg(addr, PCM169x_AUDIO_FMT, ((read & 0xF0) | dataFmt));

	return (ret);
}

/**
 * \brief     Sets attenuation mode
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     mode     [IN] Attenuation mode
 *                          0 - Fine step: 0.5-dB step for 0 dB to �63 dB range
 *                          1 - Wide range: 1-dB step for 0 dB to �100 dB range
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xSetAttnMode(Uint8 addr, Uint8 mode)
{
	DAC_RET ret;
	Uint8   read = 0;

	DBG_PCM169x (platform_write("pcm169xSetAttnMode() : addr = 0x%x "
				                "mode = %d\n", addr, mode));

	ret = pcm169x_read_reg(addr, PCM169x_DEMPH_CTRL, &read);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xSetAttnMode() : Error in Reading Register = 0x%x\n",
		                        PCM169x_DEMPH_CTRL));
		return (ret);
	}

	ret = pcm169x_write_reg(addr, PCM169x_DEMPH_CTRL, ((read & 0x7F) | (mode << 7)));

	return (ret);
}

/**
 * \brief     Reads attenuation mode value
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \return
 *          0 - Fine step: 0.5-dB step for 0 dB to �63 dB range
 *          1 - Wide range: 1-dB step for 0 dB to �100 dB range
 *          DAC_RET_FAIL - Error in reading attenuation mode value
 *
 **/
Int8 pcm169xGetAttnMode(Uint8 addr)
{
	Int8  ret = DAC_RET_FAIL;
	Uint8 read = 0;

	DBG_PCM169x (platform_write("pcm169xGetAttnMode() : addr = 0x%x ", addr));

	ret = pcm169x_read_reg(addr, PCM169x_DEMPH_CTRL, &read);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xGetAttnMode() : Error in Reading Register = 0x%x\n",
		                        PCM169x_DEMPH_CTRL));
		return (ret);
	}

	ret = ((read & 0x7F) >> 7);

	return (ret);
}

/**
 * \brief     Configures digital de-emphasis functionality
 *
 * \param     addr   [IN] DAC HW instance I2C slave address.
 *
 * \param     rate   [IN] De-emphasis selection
 *                      DAC_DEEMP_DISABLE  -  Digital de-emphasis disabled
 *                      DAC_DEEMP_48KHZ    -  Digital de-emphasis 48KHz enabled
 *	                    DAC_DEEMP_44KHZ    -  Digital de-emphasis 44KHz enabled
 *	                    DAC_DEEMP_32KHZ    -  Digital de-emphasis 32KHz enabled
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xDeempCtrl(Uint8 addr, Uint8 rate)
{
	DAC_RET ret;
	Uint8   read = 0;

	DBG_PCM169x (platform_write("pcm169xDeempCtrl() : addr = 0x%x "
				                "rate = %d\n", addr, rate));

	ret = pcm169x_read_reg(addr, PCM169x_DEMPH_CTRL, &read);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xDeempCtrl() : Error in Reading Register = 0x%x\n",
		                        PCM169x_DEMPH_CTRL));
		return (ret);
	}

	ret = pcm169x_write_reg(addr, PCM169x_DEMPH_CTRL, ((read & 0xCF) | (rate << 4)));

	return (ret);
}

/**
 * \brief     Reads DAC register values and displays.
 *
 * \param     addr   [IN] DAC HW instance I2C slave address.
 *
 * \return    0 if success.
 *
 **/
DAC_RET pcm169xRegDump(Uint8 addr)
{
	DAC_RET ret;
	Uint8   count;
	Uint8   read = 0;

	DBG_PCM169x (platform_write("pcm186xRegDump() : addr = 0x%x\n", addr));

	for (count = PCM169x_REG_START; count <= PCM169x_REG_END; count++)
	{
		ret = pcm169x_read_reg(addr, count, &read);
		if(ret)
		{
		    IFPRINT (platform_write("pcm186xRegDump() : Error in Reading Register = 0x%x\n",
		                             count));
			return (ret);
		}

		IFPRINT (platform_write("Register 0x%x = 0x%x\n", count, read));

		read = 0;
	}

	return (ret);
}

/**
 * \brief     Selects the sampling mode for PCM169x Codec.
 *
 * \param     addr   [IN] DAC HW instance I2C slave address.
 *
 * \param     mode :
 *            0  - Auto
 *            1  - Single Rate
 *            2  - Dual Rate
 *            3  - Quad Rate
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xSetSamplingMode(Uint8 addr, Uint8 mode)
{
	Uint8   read = 0;
	DAC_RET ret = DAC_RET_INV_PARAM;

	DBG_PCM169x (platform_write("pcm169xSetSamplingMode() : addr = 0x%x " "mode = %d\n",
	                            addr, mode));

	if(mode > 3) {
		IFPRINT(platform_write("pcm169xSetSamplingMode() : Invalid Sampling Mode - %d\n",
		                       mode));
		return (ret);
	}

	ret = pcm169x_read_reg(addr, PCM169x_RESET_MODE_CTRL, &read);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xSetSamplingMode() : Error in Reading Register = 0x%x\n",
								 PCM169x_RESET_MODE_CTRL));
		return (ret);
	}

	DBG_PCM169x (platform_write("pcm169xSetSamplingMode() : Sampling Mode Register Value Read = 0x%x\n",
	                            read));

	ret = pcm169x_write_reg(addr, PCM169x_RESET_MODE_CTRL,
	                      ((read & 0xFC) | mode));

	return (ret);
}

/**
 * \brief     Enables DAC operation by configuring opmode.
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     modeCtrl [IN] Mode control for DAC channel pair
 *                          0x1 - DAC1/2
 *                          0x2 - DAC3/4
 *                          0x4 - DAC5/6
 *                          0x8 - DAC7/8
 *                          0xF - All channels
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xEnableDacOpeartion(Uint8 addr, Uint8 modeCtrl)
{
	DAC_RET ret;
	Uint8   read = 0;

	DBG_PCM169x (platform_write("pcm169xEnableDacOpeartion() : addr = 0x%x " "modeCtrl = %d\n",
	                             addr, modeCtrl));

	ret = pcm169x_read_reg(addr, PCM169x_OPEDA_CTRL, &read);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xEnableDacOpeartion() : Error in Reading Register = 0x%x\n",
		                        PCM169x_OPEDA_CTRL));
		return (ret);
	}

	ret = pcm169x_write_reg(addr, PCM169x_OPEDA_CTRL, (read & ~(modeCtrl << 4)));

	return (ret);
}

/**
 * \brief     Disables DAC operation by configuring opmode.
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     modeCtrl [IN] Mode control for DAC channel pair
 *                          0x1 - DAC1/2
 *                          0x2 - DAC3/4
 *                          0x4 - DAC5/6
 *                          0x8 - DAC7/8
 *                          0xF - All channels
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xDisableDacOpeartion(Uint8 addr, Uint8 modeCtrl)
{
	DAC_RET ret;
	Uint8   read = 0;

	DBG_PCM169x (platform_write("pcm169xDisableDacOpeartion() : addr = 0x%x " "modeCtrl = %d\n",
	                             addr, modeCtrl));

	ret = pcm169x_read_reg(addr, PCM169x_OPEDA_CTRL, &read);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xDisableDacOpeartion() : Error in Reading Register = 0x%x\n",
		                        PCM169x_OPEDA_CTRL));
		return (ret);
	}

	ret = pcm169x_write_reg(addr, PCM169x_OPEDA_CTRL, (read | (modeCtrl << 4)));

	return (ret);
}

/**
 * \brief     Configures filter roll-off
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     chanId   [IN] DAC channel pair ID
 *                          0x1 - DAC1/2
 *                          0x2 - DAC3/4
 *                          0x4 - DAC5/6
 *                          0x8 - DAC7/8
 *                          0xF - All channels
 *
 * \param     rolloff  [IN] Filter roll-off value
 *                          0 - Sharp roll-off
 *                          1 - Slow roll-off
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xSetFilterRolloff(Uint8 addr, Uint8 chanId, Uint8 rolloff)
{
	DAC_RET ret;
	Uint8   read = 0;

	DBG_PCM169x (platform_write("pcm169xSetFilterRolloff() : addr = 0x%x " "chanId = %d\n" "rolloff = %d\n",
	                             addr, chanId, rolloff));

	ret = pcm169x_read_reg(addr, PCM169x_OPEDA_CTRL, &read);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xSetFilterRolloff() : Error in Reading Register = 0x%x\n",
		                        PCM169x_OPEDA_CTRL));
		return (ret);
	}

	if(rolloff == 0)
	{
		read = (read & ~(chanId));
	}
	else
	{
		read = (read | chanId);
	}

	ret = pcm169x_write_reg(addr, PCM169x_OPEDA_CTRL, read);

	return (ret);
}

/**
 * \brief     Enables an AMUTE control event
 *
 * \param     addr    [IN] DAC HW instance I2C slave address.
 *
 * \param     amuteId [IN] Amute control event Id.
 *                         1 - SCKI lost
 *                         2 - Asynchronous detect
 *                         4 - ZERO1 and ZERO2 detect
 *                         8 - DAC disable command
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xEnableAmute (Uint8 addr, Uint8 amuteId)
{
	DAC_RET ret;
	Uint8   read = 0;

	DBG_PCM169x (platform_write("pcm169xEnableAmute() : DAC Address = 0x%x " "AMUTE ID = %d\n",
	                            addr, amuteId));

	ret = pcm169x_read_reg(addr, PCM169x_RESET_MODE_CTRL, &read);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xEnableAmute() : Error in Reading Register = 0x%x\n",
								 PCM169x_RESET_MODE_CTRL));
		return (ret);
	}

	read = ((read & ~(PCM169x_AMUTE_MASK)) | (amuteId << PCM169x_AMUTE_SHIFT));

	DBG_PCM169x (platform_write("pcm169xEnableAmute() : Writing Amute Control Value = 0x%x\n",
	             ((read & PCM169x_AMUTE_MASK) >> PCM169x_AMUTE_SHIFT)));

	ret = pcm169x_write_reg(addr, PCM169x_RESET_MODE_CTRL, read);

	return (ret);
}

/**
 * \brief     Disables an AMUTE control event
 *
 * \param     addr    [IN] DAC HW instance I2C slave address.
 *
 * \param     amuteId [IN] Amute control event Id.
 *                         1 - SCKI lost
 *                         2 - Asynchronous detect
 *                         4 - ZERO1 and ZERO2 detect
 *                         8 - DAC disable command
 *
 * \return    0 for success.
 *
 **/
DAC_RET pcm169xDisableAmute (Uint8 addr, Uint8 amuteId)
{
	DAC_RET ret;
	Uint8   read = 0;

	DBG_PCM169x (platform_write("pcm169xDisableAmute() : DAC Address = 0x%x " "AMUTE ID = %d\n",
	                            addr, amuteId));

	ret = pcm169x_read_reg(addr, PCM169x_RESET_MODE_CTRL, &read);
	if(ret)
	{
		IFPRINT (platform_write("pcm169xDisableAmute() : Error in Reading Register = 0x%x\n",
								 PCM169x_RESET_MODE_CTRL));
		return (ret);
	}

	read = (read & ~(amuteId << PCM169x_AMUTE_SHIFT));

	DBG_PCM169x (platform_write("pcm169xDisableAmute() : Writing Amute Control Value = 0x%x\n",
	             ((read & PCM169x_AMUTE_MASK) >> PCM169x_AMUTE_SHIFT)));

	ret = pcm169x_write_reg(addr, PCM169x_RESET_MODE_CTRL, read);

	return (ret);
}

#endif  /* #if (PLATFORM_AUDIO_DAC) */

/***************************** End Of File ***********************************/
