/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * \file  evmc66x_mmchs_card.c
 *
 * \brief This file contains mmchs device specific functions.
 *
 ******************************************************************************/

#include "platform_internal.h"

#if (PLATFORM_MMCHS_IN)

#define CMD6_READ_DATA_LEN  0x40
#define BLK_DATA_LEN        0x200

/* Global variables */
mmchsInfo           gMmchsInfo;

static const unsigned int tran_exp[] = {
        10000,          100000,         1000000,        10000000,
        0,              0,              0,              0
};

static const unsigned char tran_mant[] = {
        0,      10,     12,     13,     15,     20,     25,     30,
        35,     40,     45,     50,     55,     60,     70,     80,
};

static const unsigned char tran_mant_mmc[] = {
        0,      10,     12,     13,     15,     20,     26,     30,
        35,     40,     45,     52,     55,     60,     70,     80,
};

static const unsigned int tacc_exp[] = {
        1,      10,     100,    1000,   10000,  100000, 1000000, 10000000,
};

static const unsigned int tacc_mant[] = {
        0,      10,     12,     13,     15,     20,     25,     30,
        35,     40,     45,     50,     55,     60,     70,     80,
};

/** ===========================================================================
   *   @n@b mmchs_check_card_type
   *
   *   @n This function determines the type of MMCSD card.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
   *   @li        MMCHS_RET_OK if successful
   *   @li        MMCHS_RET_CARD_DETECT_ERROR if Card detect fails
   *
   *   @b Modifies
   *   @n  None
   *  ===========================================================================
   */
static MMC_RET mmchs_check_card_type(mmchsCardInfo *mmchsCard)
{
    uint32_t status = 0;
    uint32_t xferStat = 0;
    CSL_MmchsRegs *mmcHsReg;

    if (mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCard->mmcHsReg;

    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD8, 0,0);
    if (status == MMCHS_RET_OK)
    {
        status = mmchsGetStatus(mmcHsReg, &xferStat);

        if (status == MMCHS_RET_OK)
        {
            mmchsCard->cardType = MMCHS_SDIO;
        }
        else if ((xferStat & CSL_MMCHS_STAT_CTO_MASK) == 0)
        {
            return MMCHS_RET_CARD_DETECT_ERROR;
        }
    }

    status = mmchsSendAppCmd(mmcHsReg, SDMMC_CMD55,0, 0);
    if (status == MMCHS_RET_OK)
    {
        mmchsCard->cardType = MMCHS_SD_CARD;
        status = MMCHS_RET_OK;
    }
    else if (status == MMCHS_RET_CMD_TIMEOUT)
    {
        status = 0;
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD5,0, 0);
        status = mmchsGetStatus(mmcHsReg, &xferStat);

        if (status == MMCHS_RET_OK)
        {
            mmchsCard->cardType = MMCHS_SDIO;
            status = MMCHS_RET_OK;
            return status;
        }
        mmchsCard->cardType = MMCHS_MMC;
        status = MMCHS_RET_OK;

    }
    else
    {
        IFPRINT_MMC(platform_write ("MMCHS: Error while sending app command \n"));
        status = MMCHS_RET_CARD_DETECT_ERROR;
    }
    return status;
}

/** ===========================================================================
   *   @n@b mmchs_unstuff_bits
   *
   *   @b Description
   *   @n This function unstuffs the 128 bit data in response location and
   *      returns the data of the given size.
   *
   *   @b Arguments
   *   @verbatim
              resp   - Address of the response data
              start  - start bit location
              size   - size of data (in bits) to be fetched
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li    Value of the given location
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
static uint32_t mmchs_unstuff_bits(uint32_t resp[4], uint8_t start, uint32_t size)
{
    const uint32_t mask = (size < 32 ? 1 << size : 0) - 1;
    const uint32_t offset = 3 - (start / 32);
    const uint32_t shift = start & 31;
    uint32_t res;

    res = resp[offset] >> shift;
    if (size + shift > 32)
    {
        res |= resp[offset - 1] << ((32 - shift) % 32);
    }
    return res & mask;
}

/** ===========================================================================
   *   @n@b mmchs_decode_cid
   *
   *   @b Description
   *   @n This function decodes the Card IDentification Register (CID). The
   *      information is received as a 16 bytes (128-bit) response to CMD2
   *      from the SD Card to fetch the Card Identification details like,
   *      Manufacture id, OEM ID, Product name, serial number and the date
   *      of manufacturing.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
static void mmchs_decode_cid(mmchsCardInfo *mmchsCard)
{
    uint32_t *resp;

    if (mmchsCard == NULL)
    {
        return;
    }

    memset(&mmchsCard->cid, 0, sizeof(mmc_cid));
    resp = mmchsCard->raw_cid;

    /*
    * SD doesn't currently have a version field so we will
    * have to assume we can parse this.
    */
    mmchsCard->cid.manfid                = mmchs_unstuff_bits(resp, 120, 8);
    if (mmchsCard->cardType == MMCHS_MMC)
    {
        mmchsCard->cid.cardtype              = mmchs_unstuff_bits(resp, 112, 2);
        mmchsCard->cid.oemid                 = mmchs_unstuff_bits(resp, 104, 8);
        mmchsCard->cid.prod_name[0]          = mmchs_unstuff_bits(resp, 96, 8);
        mmchsCard->cid.prod_name[1]          = mmchs_unstuff_bits(resp, 88, 8);
        mmchsCard->cid.prod_name[2]          = mmchs_unstuff_bits(resp, 80, 8);
        mmchsCard->cid.prod_name[3]          = mmchs_unstuff_bits(resp, 72, 8);
        mmchsCard->cid.prod_name[4]          = mmchs_unstuff_bits(resp, 64, 8);
        mmchsCard->cid.prod_name[5]          = mmchs_unstuff_bits(resp, 56, 8);
        mmchsCard->cid.hwrev                 = mmchs_unstuff_bits(resp, 48, 4);
        mmchsCard->cid.fwrev                 = mmchs_unstuff_bits(resp, 52, 4);
        mmchsCard->cid.serial                = mmchs_unstuff_bits(resp, 16, 32);
        mmchsCard->cid.year                  = mmchs_unstuff_bits(resp, 8, 4);
        mmchsCard->cid.month                 = mmchs_unstuff_bits(resp, 12, 4);
    }
    else
    {
        mmchsCard->cid.oemid                 = mmchs_unstuff_bits(resp, 104, 16);
        mmchsCard->cid.prod_name[0]          = mmchs_unstuff_bits(resp, 96, 8);
        mmchsCard->cid.prod_name[1]          = mmchs_unstuff_bits(resp, 88, 8);
        mmchsCard->cid.prod_name[2]          = mmchs_unstuff_bits(resp, 80, 8);
        mmchsCard->cid.prod_name[3]          = mmchs_unstuff_bits(resp, 72, 8);
        mmchsCard->cid.prod_name[4]          = mmchs_unstuff_bits(resp, 64, 8);
        mmchsCard->cid.hwrev                 = mmchs_unstuff_bits(resp, 56, 4);
        mmchsCard->cid.fwrev                 = mmchs_unstuff_bits(resp, 60, 4);
        mmchsCard->cid.serial                = mmchs_unstuff_bits(resp, 24, 32);
        mmchsCard->cid.year                  = mmchs_unstuff_bits(resp, 12, 4);
        mmchsCard->cid.month                 = mmchs_unstuff_bits(resp, 8, 4);
    }
}

/** ===========================================================================
   *   @n@b mmchs_decode_csd
   *
   *   @b Description
   *   @n This function decodes the SD Card Specific Data Register (CSD). The
   *      information is received as a 16 bytes (128-bit) response to CMD9
   *      from the SD Card to fetch the Max data transfer, block length,
   *      device size etc.,
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
static void mmchs_decode_csd(mmchsCardInfo *mmchsCard)
{
    mmc_csd *csd;
    unsigned int e, m, a, b, blkLen, blkLen1;
    unsigned long int size, size1, e1;
    uint32_t *resp;

    if (mmchsCard == NULL)
    {
        return;
    }

    memset(&mmchsCard->csd, 0, sizeof(mmc_csd));
    resp = mmchsCard->raw_csd;
    csd = &mmchsCard->csd;
    /*
     * We only understand CSD structure v1.1 and v1.2.
     * v1.2 has extra information in bits 15, 11 and 10.
     * We also support eMMC v4.4 & v4.41.
     */
    csd->structure = mmchs_unstuff_bits(resp, 126, 2);

    if (mmchsCard->cardType == MMCHS_MMC)
    {
        csd->mmca_vsn    = mmchs_unstuff_bits(resp, 122, 4);
    }
    m = mmchs_unstuff_bits(resp, 115, 4);
    e = mmchs_unstuff_bits(resp, 112, 3);
    csd->tacc_ns     = (tacc_exp[e] * tacc_mant[m] + 9) / 10;
    csd->tacc_clks   = mmchs_unstuff_bits(resp, 104, 8) * 100;

    m = mmchs_unstuff_bits(resp, 99, 4);
    e = mmchs_unstuff_bits(resp, 96, 3);
    if (mmchsCard->cardType == MMCHS_MMC)
        csd->max_dtr      = tran_exp[e] * tran_mant_mmc[m];
    else
        csd->max_dtr      = tran_exp[e] * tran_mant[m];
    csd->cmdclass     = mmchs_unstuff_bits(resp, 84, 12);

    if (mmchsCard->cardType == MMCHS_MMC)
    {
        e = mmchs_unstuff_bits(resp, 47, 3);
        csd->c_size_mult = mmchs_unstuff_bits(resp, 47, 3);
        m = mmchs_unstuff_bits(resp, 62, 12);
        csd->c_size = m;
        csd->capacity     = (1 + m) << (e + 2);
    }
    else
    {
        if ((csd->structure == 1) && (mmchsCard->cardType == MMCHS_SD_CARD))
        {
            e1 = mmchs_unstuff_bits(resp, 64, 6);
            size = mmchs_unstuff_bits(resp, 48, 16);
            size1 = (e1 << 16) | size;
            csd->capacity = (size1 + 1) * 512;
            csd->capacity = csd->capacity/1024;
        }
        else
        {
            blkLen = mmchs_unstuff_bits(resp, 80, 4); /* Read the READ_BL_LEN - should be 9 for 512 bytes block size */

            csd->read_blkbits = mmchs_unstuff_bits(resp, 80, 4);
            blkLen1 = 1 << blkLen; /* Block_len = 2 pow read_bl_len */
            e = mmchs_unstuff_bits(resp, 47, 3);
            m = mmchs_unstuff_bits(resp, 62, 12);
            csd->c_size_mult = mmchs_unstuff_bits(resp, 47, 3);

            m = mmchs_unstuff_bits(resp, 62, 12);
            csd->c_size = m;
            csd->capacity     = (1 + m) << (e + 2);
            csd->capacity     = csd->capacity * blkLen1;
            csd->capacity = csd->capacity/(1024 * 1024);
        }
    }
    if (mmchsCard->cardType == MMCHS_MMC)
    {

        e = mmchs_unstuff_bits(resp, 42, 5);
        csd->erase_size = mmchs_unstuff_bits(resp, 42, 5);
        m = mmchs_unstuff_bits(resp, 37, 5);
        csd->erase_size_mult = mmchs_unstuff_bits(resp, 37, 5);
        csd->erase_blocks     = (1 + e) * (m + 1);
    }
    else if (mmchsCard->cardType == MMCHS_SD_CARD)
    {
        csd->erase_size =  mmchs_unstuff_bits(resp, 46, 1);
        csd->erase_blocks = mmchs_unstuff_bits(resp, 39, 7);
        csd->erase_blocks += 1;
    }
    csd->read_blkbits = mmchs_unstuff_bits(resp, 80, 4);
    csd->read_partial = mmchs_unstuff_bits(resp, 79, 1);
    csd->write_misalign = mmchs_unstuff_bits(resp, 78, 1);
    csd->read_misalign = mmchs_unstuff_bits(resp, 77, 1);
    csd->dsr_imp = mmchs_unstuff_bits(resp, 76, 1);
    csd->r2w_factor = mmchs_unstuff_bits(resp, 26, 3);
    csd->write_blkbits = mmchs_unstuff_bits(resp, 22, 4);
    csd->write_partial = mmchs_unstuff_bits(resp, 21, 1);

    if (csd->write_blkbits >= 9)
    {
        a = mmchs_unstuff_bits(resp, 42, 5);
        b = mmchs_unstuff_bits(resp, 37, 5);
        csd->erase_size = (a + 1) * (b + 1);
        csd->erase_size <<= csd->write_blkbits - 9;
    }

    mmchsCard->tranSpeed = mmchsCard->csd.max_dtr;
    mmchsCard->blkLen = (1 << mmchsCard->csd.read_blkbits);
}

/** ===========================================================================
   *   @n@b mmchs_decode_scr
   *
   *   @b Description
   *   @n This function decodes the SD Card Configuration Register (SCR). The
   *      information is received after sending ACMD51 requesting 8 bytes
   *      (64-bit) of information from the SD Card to fetch the SDA Version,
   *      bus width, SD Security specification version.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
static void mmchs_decode_scr(mmchsCardInfo *mmchsCard)
{
    if (mmchsCard == NULL)
    {
        return;
    }
    mmc_scr *scr = &mmchsCard->scr;

    scr->scr_struct = SD_CARD_SCR_VER(mmchsCard);
    scr->sda_vsn = SD_CARD_VERSION(mmchsCard);
    scr->bus_widths = SD_CARD_BUSWIDTH(mmchsCard);
    scr->sd_sec = SD_CARD_SEC(mmchsCard);
    scr->data_stat = SD_CARD_DATSTAT(mmchsCard);
}

/** ===========================================================================
   *   @n@b mmchs_decode_ecsd
   *
   *   @b Description
   *   @n This function decodes Extended Card Specific Data (ECSD)
   *      information read from the Card/MMC.
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
    *  =========================================================================
   */
static void mmchs_decode_ecsd(mmchsCardInfo *mmchsCard)
{
    if (mmchsCard == NULL)
    {
        return;
    }
    memcpy(&mmchsCard->ecsd, mmchsCard->raw_ecsd, MMCHS_BLK_LEN);

    mmchsCard->nBlks = (mmchsCard->raw_ecsd[215] << 24) +
                        (mmchsCard->raw_ecsd[214]<< 16) +
                        (mmchsCard->raw_ecsd[213]<<8) +
                        (mmchsCard->raw_ecsd[212]);  /* No. of Sectors */

    mmchsCard->size = mmchsCard->nBlks * mmchsCard->blkLen;
    mmchsCard->sd_ver = mmchsCard->raw_ecsd[192];
    mmchsCard->busWidth = mmchsCard->raw_ecsd[183]; /*SD_BUS_WIDTH_8BIT; */
}

/** ===========================================================================
   *   @n@b mmchs_get_buswidth
   *
   *   @b Description
   *   @n This function read from the ECSD to fetch the high speed supported and
   *      the supported bus width information from the mmc device.
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
    *  =========================================================================
   */
static void mmchs_get_buswidth(mmchsCardInfo *mmchsCard)
{
    if (mmchsCard == NULL)
    {
        return;
    }

    mmchsCard->highSpeed = mmchsCard->raw_ecsd[185]; /*HS_TIMING; */
    mmchsCard->busWidth = mmchsCard->raw_ecsd[183]; /*SD_BUS_WIDTH_8BIT; */
}

/** ===========================================================================
   *   @n@b mmchs_get_ecsd
   *
   *   @b Description
   *   @n This function read from the ECSD information from the MMC device.
   *      the supported bus width information from the mmc device.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_MMC_INIT_FAILED  MMC fails to respond and init
                                              is not complete
       @li        MMCHS_RET_OK            If successful
   *
   *   @b Modifies
   *   @n  None
    *  =========================================================================
   */
MMC_RET mmchs_get_ecsd(mmchsCardInfo *mmchsCard)
{
    uint32_t status;
    uint32_t xferStat;
    uint32_t arg;
    CSL_MmchsRegs *mmcHsReg;

    if (mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCard->mmcHsReg;

    arg = mmchsCard->rca << 16;
    mmchsSetBlockCount(mmcHsReg, 0);
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD8_MMC, arg, MMCHS_BLK_LEN);
    status = mmchsGetStatus(mmcHsReg, &xferStat);
    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_MMC_INIT_FAILED;
    }

     /* delay introduced to allow the transfer complete flag to get updated */
    mmchsDelay(10000);


    status = mmchsReadData(mmcHsReg, mmchsCard->raw_ecsd, MMCHS_BLK_LEN);

    mmchsDelay(1000);
    if (mmchsCard->ecsd.card_type == 0)
        mmchs_decode_ecsd(mmchsCard);
    else
        mmchs_get_buswidth(mmchsCard);

    return MMCHS_RET_OK;
}
/** ===========================================================================
   *   @n@b mmchsPrintCID
   *
   *   @b Description
   *   @n This function prints the CID information read from the Card/MMC
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
void mmchsPrintCID(mmchsCardInfo *mmchsCard)
{
    if (mmchsCard == NULL)
    {
        return;
    }

    IFPRINT(platform_write ("Manf Id = 0x%x\n", mmchsCard->cid.manfid));
    if (mmchsCard->cardType == MMCHS_MMC)
        IFPRINT(platform_write ("Card/BGA =  %s\n",(mmchsCard->cid.cardtype ?
                "BGA (Embedded)" :  "Card (Removable)")));
    IFPRINT(platform_write ("OEM Id = 0x%x\n", mmchsCard->cid.oemid));
    IFPRINT(platform_write ("Product Name = %c%c%c%c%c", mmchsCard->cid.prod_name[0],
                            mmchsCard->cid.prod_name[1],
                            mmchsCard->cid.prod_name[2],
                            mmchsCard->cid.prod_name[3],
                            mmchsCard->cid.prod_name[4]));

    if (mmchsCard->cardType == MMCHS_MMC)
    {
        IFPRINT(platform_write ("%c", mmchsCard->cid.prod_name[5]));
    }
    IFPRINT(platform_write  ("\n"));
    IFPRINT(platform_write ("PRD REV = %d.%d\n", mmchsCard->cid.hwrev,mmchsCard->cid.fwrev));
    IFPRINT(platform_write ("Serial = %d\n", mmchsCard->cid.serial));

    if (mmchsCard->cardType == MMCHS_MMC)
        IFPRINT(platform_write ("Manufacture month: year  = %d:%d\n", mmchsCard->cid.month,
                                                  (mmchsCard->cid.year + 1997)));
    else
        IFPRINT(platform_write ("Manufacture month: year  = %d:%d\n", mmchsCard->cid.month,
                                                  (mmchsCard->cid.year + 2000)));

}

/** ===========================================================================
   *   @n@b mmchsPrintCSD
   *
   *   @b Description
   *   @n This function prints the Card Specific Data (CSD) information read
   *      from the Card/MMC
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
void mmchsPrintCSD(mmchsCardInfo *mmchsCard)
{
    if (mmchsCard == NULL)
    {
        return;
    }

    IFPRINT(platform_write ("CSD Structure =  %d\n",(mmchsCard->csd.structure)));
    if (mmchsCard->cardType == MMCHS_MMC)
    {
        IFPRINT(platform_write ("CSD MMC Version=  %d\n",mmchsCard->csd.mmca_vsn));
    }

    IFPRINT(platform_write ("Cmd class = 0x%x\n", mmchsCard->csd.cmdclass));
    IFPRINT(platform_write ("TAAC = %d clks\n",mmchsCard->csd.tacc_clks));
    IFPRINT(platform_write ("NAAC = %d clks\n",mmchsCard->csd.tacc_ns));
    IFPRINT(platform_write ("Trans speed = %d Hz\n", mmchsCard->csd.max_dtr));
    IFPRINT(platform_write ("Card size multi 0x%x\n",mmchsCard->csd.c_size_mult));
    IFPRINT(platform_write ("Card size %d\n",mmchsCard->csd.capacity));
    if ((mmchsCard->csd.structure == 1) && (mmchsCard->cardType == MMCHS_SD_CARD))
    {
        IFPRINT(platform_write ("Capacity = %d MB\n", mmchsCard->csd.capacity));
    }
    else
    {
        IFPRINT(platform_write ("Capacity = %d MB\n", mmchsCard->csd.capacity));
    }
    if (mmchsCard->cardType == MMCHS_SD_CARD)
    {
        IFPRINT(platform_write ("Erase Block Enable = 0x%x blocks\n", mmchsCard->csd.erase_size));
        IFPRINT(platform_write ("Erase size = 0x%x blocks\n", mmchsCard->csd.erase_blocks));
    }
    else
    {
        IFPRINT(platform_write ("Size of erasable unit = 0x%x blocks\n", mmchsCard->csd.erase_blocks));
    }
    IFPRINT(platform_write ("Read Blkbits 0x%x\n",mmchsCard->csd.read_blkbits));
    IFPRINT(platform_write ("Read partial = 0x%x\n", mmchsCard->csd.read_partial));
    IFPRINT(platform_write ("Read Misalign = 0x%x\n", mmchsCard->csd.read_misalign));
    IFPRINT(platform_write ("Write Blkbits: 0x%x\n",mmchsCard->csd.write_blkbits));
    IFPRINT(platform_write ("Write partial = 0x%x\n", mmchsCard->csd.write_partial));
    IFPRINT(platform_write ("Write Misalign = 0x%x\n", mmchsCard->csd.write_misalign));
    IFPRINT(platform_write ("DSR Impl = 0x%x\n", mmchsCard->csd.dsr_imp));
    IFPRINT(platform_write ("R2W factor = 0x%x\n", mmchsCard->csd.r2w_factor));
    return;
}

/** ===========================================================================
   *   @n@b mmchsPrintECSD
   *
   *   @b Description
   *   @n This function prints the Extended Card Specific Data (CSD)
   *      information read from the Card/MMC.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
void mmchsPrintECSD(mmchsCardInfo *mmchsCard)
{
    if (mmchsCard == NULL)
    {
        return;
    }

    IFPRINT(platform_write ("Sector count =  0x%x\n",(mmchsCard->ecsd.sector_count)));
    IFPRINT(platform_write ("Card Type =  0x%x\n",(mmchsCard->ecsd.card_type & 0x03)));
    IFPRINT(platform_write ("CSD Structure =  0x%x\n",(mmchsCard->ecsd.csd_struct)));
    IFPRINT(platform_write ("Ext CSD Revision =  0x%x\n",(mmchsCard->ecsd.ext_csd_rev)));
    IFPRINT(platform_write ("Bus width =  0x%x\n",(mmchsCard->ecsd.bus_width)));
    return;
}

/** ===========================================================================
   *   @n@b mmchsPrintCardInfo
   *
   *   @b Description
   *   @n This function prints the Generic information about the card and
   *      this function can be called from the test application to print the card details.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
void mmchsPrintCardInfo(mmchsCardInfo *mmchsCard)
{
    if (mmchsCard == NULL)
    {
        return;
    }

    platform_write ("***************************************\n");
    platform_write ("Card Information \n\n");

    if (mmchsCard->cardType == MMCHS_MMC)
    {
        platform_write ("Card Type : MMC :");
    }
    else if (mmchsCard->cardType == MMCHS_SD_CARD)
    {
        platform_write ("Card Type : SD :");
    }
    else if (mmchsCard->cardType == MMCHS_SDIO)
    {
        platform_write ("Card Type : SDIO :");
    }

    if (mmchsCard->cardType == MMCHS_MMC)
    {
        platform_write ("Card/BGA =  %s\n",(mmchsCard->cid.cardtype )?
                            "BGA (Embedded)" : "Card (Removable)");
    }
    platform_write ("SD Version: %d\n",mmchsCard->sd_ver);
    platform_write ("Manf Id = 0x%x\n", mmchsCard->cid.manfid);
    platform_write ("OEM Id = 0x%x\n", mmchsCard->cid.oemid);
    platform_write ("Product Name = %c%c%c%c%c", mmchsCard->cid.prod_name[0],
                            mmchsCard->cid.prod_name[1],
                            mmchsCard->cid.prod_name[2],
                            mmchsCard->cid.prod_name[3],
                            mmchsCard->cid.prod_name[4]);

   if (mmchsCard->cardType == MMCHS_MMC)
   {
    platform_write ("%c", mmchsCard->cid.prod_name[5]);
   }
    platform_write  ("\n");
    platform_write ("PRD REV = %d.%d\n", mmchsCard->cid.hwrev,mmchsCard->cid.fwrev);
    platform_write ("Serial = %lld\n", mmchsCard->cid.serial);
    if (mmchsCard->cardType != MMCHS_MMC)
    {
        platform_write ("Manufacture month: year  = %d:%d\n\n", mmchsCard->cid.month,
                                                      mmchsCard->cid.year + 2000);
    }
    platform_write ("***************************************\n");

    if (mmchsCard->ocr & SD_OCR_DUAL_VOLTAGE_RANGE)
    {
        platform_write ("Dual Voltage Multimedia Card and eMMC Profile: \n");
    }
    else
    {
        platform_write ("High Voltage Multimedia Card Profile: \n");
    }
    if (mmchsCard->ocr & SD_OCR_HIGH_CAPACITY)
    {
        platform_write ("High capacity \nAccess mode : sector mode\n");
    }
    else
    {
        platform_write ("Access mode : byte mode\n");
    }
    platform_write ("***************************************\n");

    platform_write ("Card blkLen = 0x%x\n", mmchsCard->blkLen);
    if (mmchsCard->cardType == MMCHS_MMC)
    {
        platform_write ("eMMC Capacity = %d MB\n",((mmchsCard->nBlks)/(1024 * 2)));

    }
    else
    {
        platform_write ("Capacity = %d MB\n", mmchsCard->csd.capacity);
    }

    platform_write ("***************************************\n");
    if (mmchsCard->cardType == MMCHS_MMC)
    {
        mmchs_get_ecsd(mmchsCard);
        platform_write ("Supported buswidth for eMMC is 1-bit, 4-bit and 8-bit data bus\n");
        if (mmchsCard->busWidth == 0)
        {
            platform_write ("Bus width set to 1-bit data bus\n");
        }
        else if (mmchsCard->busWidth == 1) {
            platform_write ("Bus width set to 4-bit data bus\n");
        }
        else if (mmchsCard->busWidth == 2) {
            platform_write ("Bus width set to 8-bit data bus\n");
        }
    }
    else if (mmchsCard->cardType == MMCHS_SD_CARD)
    {
        platform_write ("SD Card supports bus width ");
        if (mmchsCard->scr.bus_widths & SD_BUS_WIDTH_1BIT)
        {
            platform_write ("1-bit ");
        }
        if (mmchsCard->scr.bus_widths & SD_BUS_WIDTH_4BIT)
        {
            platform_write ("4-bit ");
        }
        platform_write  ("data bus\n");
        platform_write ("SD Card bus width set to %d-bit data bus\n", mmchsCard->busWidth);
    }
    platform_write ("***************************************\n");
    platform_write ("Max bus clock frequency = %d\n",mmchsCard->maxTranSpeed);
    platform_write ("High Speed supported = %d\n",mmchsCard->highSpeed);
    platform_write ("Current clock frequency configured = %d\n",mmchsCard->ctrl->opClk);
    platform_write ("***************************************\n");
    return;
}

/** ===========================================================================
   *   @n@b mmchsPrintSCR
   *
   *   @b Description
   *   @n This function prints the SD Card Configuration Register (SCR)
   *      information read from the Card/MMC
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
    *  =========================================================================
   */
void mmchsPrintSCR(mmchsCardInfo *mmchsCard)
{
    if (mmchsCard == NULL)
    {
        return;
    }
    IFPRINT(platform_write ("SDA Version =  0x%x\n",(mmchsCard->scr.sda_vsn)));
    IFPRINT(platform_write ("Bus width =  0x%x\n",(mmchsCard->scr.bus_widths)));
    IFPRINT(platform_write ("Data status after erase =  0x%x\n",(mmchsCard->scr.data_stat)));
    IFPRINT(platform_write ("SD Security =  0x%x\n",(mmchsCard->scr.sd_sec)));
    return;
}

/** ===========================================================================
   *   @n@b mmchsSetTranSpeed
   *
   *   @b Description
   *   @n This function sets the data transfer speed on the host controller
   *      based on the response from the card.
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg   - Handle to the mmcReg
              speed      - data transfer speed to be set
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_SEND_FAILED   Response to command is error
       @li        MMCHS_RET_NO_MEM        No memory available for allocation
       @li        MMCHS_RET_SEND_NORSP    If no response from the mmc device.
   *   @li        MMCHS_RET_OK            If successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchsSetTranSpeed(mmchsCardInfo *mmchsCard, uint32_t speed)
{
    uint32_t status = 0;
    uint32_t arg = 0;
    uint32_t xferStatus = 0;
    uint8_t buf[40];
    CSL_MmchsRegs *mmcHsReg;
    mmchsControllerInfo *ctrl;

    if(mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCard->mmcHsReg;
    if (mmchsCard->cardType == MMCHS_SD_CARD)
    {

        arg = ((SD_SWITCH_MODE & SD_CMD6_GRP1_SEL) | (SD_CMD6_GRP1_HS));
        mmchsSetBlockCount(mmcHsReg, 0);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD6_SP, arg, CMD6_READ_DATA_LEN);
        if (status != MMCHS_RET_OK)
        {
            return status;
        }
        status = mmchsGetStatus(mmcHsReg, &xferStatus);
        if ((status == MMCHS_RET_OK) && (xferStatus & CSL_MMCHS_STAT_CC_MASK))
        {
            memset (buf, 0, CMD6_READ_DATA_LEN);
            status = mmchsReadData(mmcHsReg, buf, CMD6_READ_DATA_LEN);
            if (status != MMCHS_RET_OK)
            {
                IFPRINT(platform_write  ("MMCHS: CMD6 resp data read failed \n"));
                mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);
                return status;
            }
            mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);
        }
        if (speed > mmchsCard->tranSpeed)
            IFPRINT(platform_write  ("MMCHS: Cannot set higher than %d MHz\n",mmchsCard->tranSpeed));
    }
    else if (mmchsCard->cardType == MMCHS_MMC)
    {
         /* Switching to high-speed mode */
        /* Set HS_TIMING(185) Byte of the EXT_CSD to 1 */
        if (mmchsCard->tranSpeed == 0)
        {
            arg = MMC_SWITCH_TRANSFER_SPEED;
        }
        else
        {
            arg = MMC_SWITCH_HS_TRANSFER_SPEED;
        }

        mmchsSetBlockCount(mmcHsReg, 0);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD6, arg, BLK_DATA_LEN);
        status = mmchsGetStatus(mmcHsReg, &xferStatus);
        if (status != MMCHS_RET_OK)
        {
            return MMCHS_RET_MMC_INIT_FAILED;
        }
    }
    ctrl = mmchsCard->ctrl;

    switch(mmchsCard->tranSpeed)
    {
        case HSMMCSD_TRAN_SPEED_24M_CLK:
        case HSMMCSD_TRAN_SPEED_25M_CLK:
        case HSMMCSD_TRAN_SPEED_26M_CLK:
        case HSMMCSD_TRAN_SPEED_48M_CLK:
        case HSMMCSD_TRAN_SPEED_50M_CLK:
            status = mmchsEnableClock(mmcHsReg, mmchsCard->tranSpeed);
            ctrl->opClk = mmchsCard->tranSpeed;
            break;

        default:
            IFPRINT(platform_write  ("MMCHS: CMD6 response failed xferStat : 0x%x\n", xferStatus));
            break;
    }
    if (mmchsCard->highSpeed)
    {
        mmchsCard->tranSpeed = speed;
        IFPRINT(platform_write  ("Saving tran speed as high speed but currently set to non high speed\n"));
    }

    return status;
}

/** ===========================================================================
   *   @n@b mmchs_set_tran_speed
   *
   *   @b Description
   *   @n This function sets the data transfer speed on the host controller
   *      based on the response from the card.
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg   - Handle to the mmcReg
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_SEND_FAILED   Response to command is error
       @li        MMCHS_RET_SEND_NORSP    If no response from the mmc device.
   *   @li        MMCHS_RET_OK            If successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchs_set_tran_speed(mmchsCardInfo *mmchsCard)
{
    uint32_t status = 0;
    uint32_t arg = 0;
    uint32_t xferStatus = 0;
    uint8_t buf[CMD6_READ_DATA_LEN];
    CSL_MmchsRegs *mmcHsReg;

    if(mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCard->mmcHsReg;
    if (mmchsCard->cardType == MMCHS_SD_CARD)
    {
        mmcHsReg = mmchsCard->mmcHsReg;

        arg = ((SD_CHECK_MODE & SD_CMD6_GRP1_SEL) | (SD_CMD6_GRP1_HS));
        mmchsSetBlockCount(mmcHsReg, 0);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD6_SP, arg, CMD6_READ_DATA_LEN);
        if (status != MMCHS_RET_OK)
        {
            return status;
        }
        status = mmchsGetStatus(mmcHsReg, &xferStatus);
        if ((status == MMCHS_RET_OK) && (xferStatus & CSL_MMCHS_STAT_CC_MASK))
        {
            memset (buf, 0, CMD6_READ_DATA_LEN);
            status = mmchsReadData(mmcHsReg, buf, CMD6_READ_DATA_LEN);
            if (status != MMCHS_RET_OK)
            {
                IFPRINT(platform_write ("MMCHS: CMD6 resp data read failed n"));
                mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);
                return status;
            }
            mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);

            IFPRINT(platform_write  ("CMD6 response on reading 512 bit data \n"));
            int i;
            for (i=0;i<CMD6_READ_DATA_LEN;i++)
            {
                IFPRINT(platform_write  ("0x%x ",buf[i]));
            }
            IFPRINT(platform_write  ("\n"));
            if ((buf[13] & 0xF) & SD_CMD6_GRP1_HS)
            {
                mmchsCard->highSpeed = 1;
                mmchsCard->maxTranSpeed = HSMMCSD_TRAN_SPEED_50M_CLK;
                IFPRINT(platform_write  ("Card supports high speed 25 MB/sec interface speed\n"));
            }
            else
            {
                mmchsCard->maxTranSpeed = HSMMCSD_TRAN_SPEED_25M_CLK;
                IFPRINT(platform_write  ("Card supports default speed 12.5 MB/sec interface speed\n"));
            }
        }
    }
    else if (mmchsCard->cardType == MMCHS_MMC)
    {
         /* Switching to high-speed mode */
        /* Set HS_TIMING(185) Byte of the EXT_CSD to 1 */
        if (mmchsCard->tranSpeed == 0)
        {
            arg = MMC_SWITCH_TRANSFER_SPEED;
        }
        else
        {
            if (mmchsCard->ecsd.card_type & 0x03)
            {
                arg = MMC_SWITCH_HS_TRANSFER_SPEED;
                mmchsCard->highSpeed = 1;
                mmchsCard->maxTranSpeed = HSMMCSD_TRAN_SPEED_52M_CLK;
            }
            else
            {
                arg = MMC_SWITCH_TRANSFER_SPEED;
                mmchsCard->maxTranSpeed = HSMMCSD_TRAN_SPEED_26M_CLK;
            }
        }

        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD6, arg, BLK_DATA_LEN);
        status = mmchsGetStatus(mmcHsReg, &xferStatus);
        if (status != MMCHS_RET_OK)
        {
            return MMCHS_RET_MMC_INIT_FAILED;
        }
    }

    switch(mmchsCard->tranSpeed)
    {
        case HSMMCSD_TRAN_SPEED_24M_CLK:
        case HSMMCSD_TRAN_SPEED_25M_CLK:
        case HSMMCSD_TRAN_SPEED_26M_CLK:
        case HSMMCSD_TRAN_SPEED_48M_CLK:
        case HSMMCSD_TRAN_SPEED_50M_CLK:
            status = mmchsEnableClock(mmcHsReg, mmchsCard->maxTranSpeed);
            mmchsCard->tranSpeed = mmchsCard->maxTranSpeed;
            mmchsCard->ctrl->opClk = mmchsCard->tranSpeed;
            break;

        default:
            IFPRINT(platform_write ("MMCHS: CMD6 response failed xferStat : \
                                    0x%x\n", xferStatus));
            break;
    }
    return status;
}

/** ===========================================================================
   *   @n@b mmchsSetBusWidth
   *
   *   @b Description
   *   @n This function configures the bus width on the mmc device. It sends
   *   CMD6 to get the bus width and mmchsCard capabilities on the mmc device
   *   and then set the allowable bus width on the host controller as well
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg   - Handle to the mmcReg
              busWidth   - Bus width to be set to
   *  buswidth can take the values.\n
   *     SD_BUS_WIDTH_4BIT.\n
   *     SD_BUS_WIDTH_1BIT.\n
   *     SD_BUS_WIDTH_8BIT

       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_SEND_FAILED   Response to command is error
       @li        MMCHS_RET_SEND_NORSP    If no response from the mmc device.
   *   @li        MMCHS_RET_OK            If successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchsSetBusWidth(mmchsCardInfo *mmchsCard, uint8_t busWidth)
{
    uint32_t status = 0;
    uint32_t xferStat = 0;
    uint32_t arg;
    CSL_MmchsRegs *mmcHsReg;

    if ((mmchsCard == NULL) && (busWidth > SD_BUS_WIDTH_8BIT))
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCard->mmcHsReg;

    if (mmchsCard->cardType == MMCHS_SD_CARD)
    {
        switch(busWidth)
        {
        case SD_BUS_WIDTH_4BIT:
        case SD_BUS_WIDTH_8BIT:
            arg = busWidth;
          break;

          default:
            arg = SD_BUS_WIDTH_1BIT_VAL;
            break;
        }

        arg = mmchsCard->rca << 16;
        mmchsSetBlockCount(mmcHsReg, 0);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD55, arg, 0);
        if (status != MMCHS_RET_OK)
        {
            return status;
        }
        status = mmchsGetStatus(mmcHsReg, &xferStat);
        if (status != MMCHS_RET_OK)
        {
            return MMCHS_RET_SDCARD_INIT_FAILED;
        }

        arg = busWidth >> 1;
        mmchsSetBlockCount(mmcHsReg, 0);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD6_BW, arg, 8);
        if (status != MMCHS_RET_OK)
        {
            return status;
        }
        status = mmchsGetStatus(mmcHsReg, &xferStat);
        if (status != MMCHS_RET_OK)
        {
            return MMCHS_RET_SDCARD_INIT_FAILED;
        }
    }
    else if (mmchsCard->cardType == MMCHS_MMC)
    {
        /* Switching to 8 bit bus width */
        /* Set HS_TIMING(183) Byte of the EXT_CSD to 2 */
        arg = SD_BUS_WIDTH_8BIT_VAL;
        mmchsSetBlockCount(mmcHsReg, 0);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD6, arg, 0);
        status = mmchsGetStatus(mmcHsReg, &xferStat);
        if (status != MMCHS_RET_OK)
        {
            return MMCHS_RET_MMC_INIT_FAILED;
        }
        busWidth = SD_BUS_WIDTH_8BIT;
        mmchsDelay(1);
    }

    if (status == MMCHS_RET_OK)
    {
        mmchsCard->busWidth = busWidth;
        status = mmchsBusWidthConfig(mmcHsReg,mmchsCard->busWidth);
    }
    return status;
}

/** ===========================================================================
   *   @n@b mmchs_set_bus_width
   *
   *   @b Description
   *   @n This function configures the bus width on the mmc device or sd card.
   *   This function checks the supported bus width on the device first and then
   *   configure to the maximum supported on both host and the device.
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg   - Handle to the mmcReg
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_SEND_FAILED   Response to command is error
       @li        MMCHS_RET_SEND_NORSP    If no response from the mmc device.
   *   @li        MMCHS_RET_OK            If successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchs_set_bus_width(mmchsCardInfo *mmchsCard)
{
    uint32_t status = 0;
    uint32_t xferStat = 0;
    uint32_t arg;
    CSL_MmchsRegs *mmcHsReg;

    if (mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCard->mmcHsReg;

    if (mmchsCard->cardType == MMCHS_SD_CARD)
    {
        mmchsCard->busWidth = SD_BUS_WIDTH_4BIT;
        arg = mmchsCard->rca << 16;
        mmchsSetBlockCount(mmcHsReg, 0);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD55, arg, 0);
        if (status != MMCHS_RET_OK)
        {
            return status;
        }
        status = mmchsGetStatus(mmcHsReg, &xferStat);
        if (status != MMCHS_RET_OK)
        {
            return MMCHS_RET_SDCARD_INIT_FAILED;
        }

        arg = mmchsCard->busWidth >> 1;
        mmchsSetBlockCount(mmcHsReg, 0);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD6_BW, arg, 8);
        if (status != MMCHS_RET_OK)
        {
            return status;
        }
        status = mmchsGetStatus(mmcHsReg, &xferStat);
        if (status != MMCHS_RET_OK)
        {
            return MMCHS_RET_SDCARD_INIT_FAILED;
        }
    }
    else if (mmchsCard->cardType == MMCHS_MMC)
    {
        /* Switching to 8 bit bus width */
        /* Set HS_TIMING(183) Byte of the EXT_CSD to 2 */
        arg = SD_BUS_WIDTH_8BIT_VAL;
        mmchsSetBlockCount(mmcHsReg, 0);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD6, arg, 0);
        status = mmchsGetStatus(mmcHsReg, &xferStat);
        if (status != MMCHS_RET_OK)
        {
            return MMCHS_RET_MMC_INIT_FAILED;
        }
        mmchsCard->busWidth = SD_BUS_WIDTH_8BIT;
        mmchsDelay(1);
    }

    if (status == MMCHS_RET_OK)
    {
        status = mmchsBusWidthConfig(mmcHsReg,mmchsCard->busWidth);
    }
    return status;
}

/** ===========================================================================
   *   @n@b mmchs_init_sd_card
   *
   *   @b Description
   *   @n This function performs card detection and initializes the SD Card
   *  based on the host and card capability
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the mmchsCard
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_SDCARD_INIT_FAILED  Card fails to respond and init
                                          is not complete
       @li        MMCHS_RET_OK            If successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
static MMC_RET mmchs_init_sd_card(mmchsCardInfo *mmchsCard)
{
    uint32_t status;
    uint32_t retry;
    uint32_t xferStat;
    uint32_t arg;
    uint32_t rsp[4];
    CSL_MmchsRegs *mmcHsReg;
    uint8_t scrBuf[8];

    if (mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCard->mmcHsReg;

    /* CMD0 - reset card */
    status = mmchsSendCmd(mmcHsReg,SDMMC_CMD0,0,0);
    if (status != MMCHS_RET_OK)
    {
        return status;
    }
    mmchsDelay(1000);
    /* Send CMD8 to validate the voltage level of host and the card */
    mmcHsReg->BLK = 0;
    arg = (SD_CHECK_PATTERN | SD_VOLT_2P7_3P6);

    /* CMD8 - send oper voltage */
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD8, arg, 0);

    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_SDCARD_INIT_FAILED;
    }
    status = mmchsGetStatus(mmcHsReg, &xferStat);

    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_SDCARD_INIT_FAILED;
    }

    retry = 0xFFFF;
    arg = (SD_OCR_HIGH_CAPACITY | SD_OCR_VDD_WILDCARD);

    /* Go ahead and send ACMD41, with host capabilities */
    do
    {
        status = mmchsSendAppCmd(mmcHsReg, SDMMC_ACMD41, arg, 0);
        if (status!= MMCHS_RET_OK)
        {
            return MMCHS_RET_SDCARD_INIT_FAILED;
        }
        mmchsGetResponse(mmcHsReg,rsp);
        mmchsDelay(1000);
    /* Poll until we get the card status (BIT31 of OCR) is powered up */
    } while (!(rsp[3] & SD_OCR_CARD_BUSY) && retry--);

    if (retry == 0)
    {
        /* No point in continuing */
        return MMCHS_RET_SDCARD_INIT_FAILED;
    }
    mmchsCard->ocr = rsp[3];
    mmchsCard->highCap = (mmchsCard->ocr &  SD_OCR_HIGH_CAPACITY) ? 1 : 0;

#ifdef DEBUG
    IFPRINT(platform_write ("Card OCR = 0x%x\n",mmchsCard->ocr));
    IFPRINT(platform_write ("Is high capacity ? %s \n",(mmchsCard->highCap) ? "Yes"\
                    : "No"));
#endif

    /* Send CMD2, to get the card identification register */
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD2, 0, 0);
    if (status != MMCHS_RET_OK)
    {
        return status;
    }
    status = mmchsGetStatus(mmcHsReg, &xferStat);
    if (status != MMCHS_RET_OK)
    {
      return MMCHS_RET_SDCARD_INIT_FAILED;
    }

    mmchsGetResponse(mmcHsReg,rsp);
    memcpy(mmchsCard->raw_cid, rsp, 16);

    mmchs_decode_cid(mmchsCard);
#ifdef DEBUG
    IFPRINT(platform_write ("raw_cid[0] = 0x%x\n",mmchsCard->raw_cid[0]));
    IFPRINT(platform_write ("raw_cid[1] = 0x%x\n",mmchsCard->raw_cid[1]));
    IFPRINT(platform_write ("raw_cid[2] = 0x%x\n",mmchsCard->raw_cid[2]));
    IFPRINT(platform_write ("raw_cid[3] = 0x%x\n",mmchsCard->raw_cid[3]));
    mmchsPrintCID(mmchsCard);
#endif

    /* Send CMD3, to get the card relative address */
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD3, 0, 0);
    if (status != MMCHS_RET_OK)
    {
        return status;
    }
    status = mmchsGetStatus(mmcHsReg, &xferStat);

    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_SDCARD_INIT_FAILED;
    }

    mmchsGetResponse(mmcHsReg,rsp);
    mmchsCard->rca =SD_RCA_ADDR(rsp[3]);
#ifdef DEBUG
    IFPRINT(platform_write ("Card RCA = 0x%x\n",mmchsCard->rca));
#endif

    /* Send CMD9, to get the card specific data */
    arg = mmchsCard->rca << 16;
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD9, arg, 0);
    if (status != MMCHS_RET_OK)
    {
        return status;
    }
    status = mmchsGetStatus(mmcHsReg, &xferStat);

    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_SDCARD_INIT_FAILED;
    }

    mmchsGetResponse(mmcHsReg,rsp);
    memcpy(mmchsCard->raw_csd, rsp, 16);

    mmchs_decode_csd(mmchsCard);
#ifdef DEBUG
    IFPRINT(platform_write ("raw_csd[0] = 0x%x\n",mmchsCard->raw_csd[0]));
    IFPRINT(platform_write ("raw_csd[1] = 0x%x\n",mmchsCard->raw_csd[1]));
    IFPRINT(platform_write ("raw_csd[2] = 0x%x\n",mmchsCard->raw_csd[2]));
    IFPRINT(platform_write ("raw_csd[3] = 0x%x\n",mmchsCard->raw_csd[3]));
    mmchsPrintCSD(mmchsCard);
#endif

    if (SD_CARD_CSD_VERSION(mmchsCard))
    {
        mmchsCard->size = SD_CARD1_SIZE(mmchsCard);
        mmchsCard->nBlks = mmchsCard->size / mmchsCard->blkLen;
    }
    else
    {
        mmchsCard->nBlks = SD_CARD0_NUMBLK(mmchsCard);
        mmchsCard->size = SD_CARD0_SIZE(mmchsCard);
    }

#ifdef DEBUG
    IFPRINT(platform_write ("Card tranSpeed = %d\n",mmchsCard->tranSpeed));
    IFPRINT(platform_write ("Card blkLen  = 0x%x\n",mmchsCard->blkLen));
    IFPRINT(platform_write ("Card size = %dMB\n",mmchsCard->csd.capacity));
#endif

    /* Set data block length to 512 (for byte addressing cards) */
    if( !(mmchsCard->highCap) )
    {
        arg = MMCHS_BLK_LEN;
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD16, arg, 0);
        status = mmchsGetStatus(mmcHsReg, &xferStat);
        if (status != MMCHS_RET_OK)
        {
            return MMCHS_RET_SDCARD_INIT_FAILED;
        }
        mmchsCard->blkLen = MMCHS_BLK_LEN;
    }

    /* Select the card */
    arg = mmchsCard->rca << 16;
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD7, arg, 0);
    status = mmchsGetStatus(mmcHsReg, &xferStat);
    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_SDCARD_INIT_FAILED;
    }

    /*
     * Send ACMD51, to get the SD Configuration register details.
     * Note, this needs data transfer (on data lines).
     */
    arg = mmchsCard->rca << 16;
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD55, arg, 0);
    status = mmchsGetStatus(mmcHsReg, &xferStat);
    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_SDCARD_INIT_FAILED;
    }
    status = mmchsSendCmd(mmcHsReg, SDMMC_ACMD51, arg, 8);
    status = mmchsGetStatus(mmcHsReg, &xferStat);
    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_SDCARD_INIT_FAILED;
    }

    status = mmchsReadData(mmcHsReg, scrBuf, 8);
    memcpy(mmchsCard->raw_scr, scrBuf, 8);

    mmchs_decode_scr(mmchsCard);
    mmchsCard->sd_ver = SD_CARD_VERSION(mmchsCard);
    mmchsCard->busWidth = SD_CARD_BUSWIDTH(mmchsCard);

#ifdef DEBUG
     mmchsPrintSCR(mmchsCard);
     IFPRINT(platform_write ("raw_scr0 = 0x%x\n", mmchsCard->raw_scr[0]));
     IFPRINT(platform_write ("raw_scr1 = 0x%x\n", mmchsCard->raw_scr[1]));
     IFPRINT(platform_write ("sd version = 0x%x\n", mmchsCard->sd_ver ));
     IFPRINT(platform_write ("bus width = 0x%x\n", mmchsCard->busWidth));
#endif
    return MMCHS_RET_OK;
}

/** ===========================================================================
   *   @n@b mmchs_init_sdio
   *
   *   @b Description
   *   @n This function performs card detection and initializes the SDIO
   *      device based on the host and sdio device capability
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg   - Handle to the mmcReg
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_SDIO_INIT_FAILED  SDIO fails to respond and init
                                              is not complete
       @li        MMCHS_RET_OK            If successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
static MMC_RET mmchs_init_sdio(mmchsCardInfo *mmchsCard)
{
    uint32_t status;
    uint32_t retry;
    uint32_t xferStat;
    uint32_t arg;
    uint32_t rsp[4];
    CSL_MmchsRegs *mmcHsReg;

    if (mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCard->mmcHsReg;

    /* CMD0 - reset card */
    status = mmchsSendCmd(mmcHsReg,SDMMC_CMD0,0,0);
    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_SDIO_INIT_FAILED;
    }

    /* Send CMD5 to validate the voltage level of host and the card */
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD5, 0, 0);

    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_SDIO_INIT_FAILED;
    }
    status = mmchsGetStatus(mmcHsReg, &xferStat);

    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_SDIO_INIT_FAILED;
    }
    mmchsGetResponse(mmcHsReg,rsp);
    mmchsCard->ocr = rsp[3];
    mmchsCard->numIOFuncs = (mmchsCard->ocr  & 0x70000000) >> 28;

    retry = 0xFFFF;
    arg = (mmchsCard->ocr >> 16) & 0xFFFF;
    /* Go ahead and send CMD5, with host capabilities */
    /* Poll until we get the card status (BIT31 of OCR) is powered up */

    do
    {
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD5, arg, 0);
        if (status!= MMCHS_RET_OK)
        {
            return MMCHS_RET_SDIO_INIT_FAILED;
        }
        mmchsGetResponse(mmcHsReg,rsp);
    } while (!(rsp[3] & SD_OCR_CARD_BUSY) && retry--);

    if (retry == 0)
    {
        /* No point in continuing */
        return MMCHS_RET_SDIO_INIT_FAILED;
    }
    mmchsCard->memPresent = (mmchsCard->ocr  & 0x800000) >> 27;

#ifdef DEBUG
    IFPRINT_MMC(platform_write ("Card OCR = 0x%x\n",mmchsCard->ocr));
    if (mmchsCard->numIOFuncs > 0)
    {
        IFPRINT_MMC(platform_write ("No. of IO Functions = 0x%x\n",mmchsCard->numIOFuncs));
    }
    else if (mmchsCard->memPresent)
    {
        IFPRINT_MMC(platform_write ("SDIO Card also contains memory 0x%x\n"));
    }
    if ((mmchsCard->numIOFuncs > 0) && (mmchsCard->memPresent))
        IFPRINT_MMC(platform_write ("SDIO Card also contains memory 0x%x\n"));
#endif
    return status;
}

 /** ===========================================================================
    *   @n@b mmchs_init_mmc
    *
    *   @b Description
    *   @n This function initializes the MMC device
    *
    *   @b Arguments
    *   @verbatim
               mmchsCard - Handle to the mmchsCard
        @endverbatim
    *
    *   <b> Return Value </b>
    *   @li        MMCHS_RET_INVALID_PARAM
    *   @li       MMCHS_RET_OK if successful
    *   @li       MMCHS_RET_MMC_INIT_FAILED - If failed to initialize
    *
    *   @b Modifies
    *   @n  None
    *  ========================================================================
    */
static MMC_RET mmchs_init_mmc(mmchsCardInfo *mmchsCard)
{
    uint32_t status;
    int32_t retry;
    uint32_t xferStat = 0;
    uint32_t rsp[4];
    uint32_t arg;
    CSL_MmchsRegs *mmcHsReg;

    if (mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCard->mmcHsReg;

    /* CMD0 - reset card */
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD0, 0, 0);

    mmchsDelay(1000);

    /* CMD1 - send oper voltage */
    arg = 0;
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD1, arg, 0);
    status = mmchsGetStatus(mmcHsReg, &xferStat);
    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_MMC_INIT_FAILED;
    }
    mmchsGetResponse(mmcHsReg,rsp);
    mmchsDelay(1000);

    arg = ((rsp[3] & SD_OCR_VOLTAGE_MASK) | (rsp[3] & SD_OCR_ACCESS_MODE) | \
                SD_OCR_HIGH_CAPACITY);
    retry = 0xFFFF;

    /* Go ahead and send CMD1, with host capabilities */
    /* Poll until we get the card status (BIT31 of OCR) is powered up */
    do
    {
        arg = ((rsp[3] & SD_OCR_VOLTAGE_MASK) | (rsp[3] & SD_OCR_ACCESS_MODE) \
               | SD_OCR_HIGH_CAPACITY);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD1, arg, 0);
        status = mmchsGetStatus(mmcHsReg, &xferStat);
        if (status != MMCHS_RET_OK)
        {
            return MMCHS_RET_MMC_INIT_FAILED;
        }
        mmchsGetResponse(mmcHsReg,rsp);
     } while (!(rsp[3] & SD_OCR_CARD_BUSY) && retry--);

    if (retry < 0)
    {
        /* No point in continuing */
        return MMCHS_RET_MMC_INIT_FAILED;
    }

    mmchsCard->ocr = rsp[3];
    mmchsCard->highCap = (mmchsCard->ocr & SD_OCR_HIGH_CAPACITY) ? 1 : 0;

#ifdef DEBUG
    IFPRINT(platform_write ("Card OCR = 0x%x\n",mmchsCard->ocr));
    IFPRINT(platform_write ("Card is High Cap = 0x%x\n",mmchsCard->highCap));
#endif
    /* b. CID retrieval and RCA assignment */
    /* Send CMD2, to get the card identification register */

    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD2, 0, 0);
    status = mmchsGetStatus(mmcHsReg, &xferStat);
    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_MMC_INIT_FAILED;
    }
    mmchsGetResponse(mmcHsReg,rsp);
    memcpy(mmchsCard->raw_cid, rsp, 16);

    mmchs_decode_cid(mmchsCard);
#ifdef DEBUG
    IFPRINT(platform_write ("raw_cid[0] = 0x%x\n",mmchsCard->raw_cid[0]));
    IFPRINT(platform_write ("raw_cid[1] = 0x%x\n",mmchsCard->raw_cid[1]));
    IFPRINT(platform_write ("raw_cid[2] = 0x%x\n",mmchsCard->raw_cid[2]));
    IFPRINT(platform_write ("raw_cid[3] = 0x%x\n",mmchsCard->raw_cid[3]));

    mmchsPrintCID(mmchsCard);
#endif

    /* Send CMD3, to set the mmchsCard relative address */
    mmchsCard->rca = 2;
#ifdef DEBUG
    IFPRINT(platform_write ("Card RCA = 0x%x\n",mmchsCard->rca));
#endif

    arg = mmchsCard->rca << 16;
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD3, arg, 0);
    status = mmchsGetStatus(mmcHsReg, &xferStat);
    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_MMC_INIT_FAILED;
    }

    /* c. CSD retrieval and host adjustment */
    /* Send CMD9, to get the card specific data */
    arg = mmchsCard->rca << 16;
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD9, arg, 0);
    status = mmchsGetStatus(mmcHsReg, &xferStat);
    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_MMC_INIT_FAILED;
    }
    mmchsGetResponse(mmcHsReg,rsp);
    memcpy(mmchsCard->raw_csd, rsp, 16);

    mmchs_decode_csd(mmchsCard);
#ifdef DEBUG
    IFPRINT(platform_write ("raw_csd[0] = 0x%x\n",mmchsCard->raw_csd[0]));
    IFPRINT(platform_write ("raw_csd[1] = 0x%x\n",mmchsCard->raw_csd[1]));
    IFPRINT(platform_write ("raw_csd[2] = 0x%x\n",mmchsCard->raw_csd[2]));
    IFPRINT(platform_write ("raw_csd[3] = 0x%x\n",mmchsCard->raw_csd[3]));
    mmchsPrintCSD(mmchsCard);
#endif

    /* Switching to high-speed mode supported for version 4.0 or higher */
    if (mmchsCard->csd.mmca_vsn != 0x04)
        return MMCHS_RET_MMC_INIT_FAILED;

    /* Select the card */
    arg = mmchsCard->rca << 16;
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD7, arg, 0);
    status = mmchsGetStatus(mmcHsReg, &xferStat);
    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_MMC_INIT_FAILED;
    }

    memset(&mmchsCard->ecsd, 0, sizeof(mmc_ecsd));
    /* Read Extended CSD */
    mmchs_get_ecsd(mmchsCard);

#ifdef DEBUG
    IFPRINT(platform_write ("Card tranSpeed = %d\n",mmchsCard->tranSpeed));
    IFPRINT(platform_write ("Card blkLen  = 0x%x\n",mmchsCard->blkLen));
    IFPRINT(platform_write ("Card nBlks = 0x%x\n",mmchsCard->nBlks));
    IFPRINT(platform_write ("Card SD Version = %d\n",mmchsCard->sd_ver));
    IFPRINT(platform_write ("busWidth = 0x%x\n",mmchsCard->busWidth));
    mmchsPrintECSD(mmchsCard);
#endif

    mmchs_set_tran_speed(mmchsCard);
    mmchs_set_bus_width(mmchsCard);
    mmchsDelay(1);

#if 0  //Diabled for bring-up. Will be enabled later
    /* Enabling the reset pin RST_n by writing ECSD register byte 162,
       bits[1:0] to 0x1 */
    arg = MMC_HS_RESET_PIN_ARG;
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD6, arg, BLK_DATA_LEN);
    status = mmchsGetStatus(mmcHsReg, &xferStat);
    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_MMC_INIT_FAILED;
    }
#endif

    return MMCHS_RET_OK;
}

/** ===========================================================================
   *   @n@b mmchs_reset_card
   *
   *   @b Description
   *   @n This function resets the MMCSD Card
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard - Handle to the mmchsCard
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
   *   @li       MMCHS_RET_OK if successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
static MMC_RET mmchs_reset_card(mmchsCardInfo *mmchsCard)
{
    uint32_t status;
    CSL_MmchsRegs *mmcHsReg;

    if (mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCard->mmcHsReg;
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD0, 0, 0);
    return status;
}

/** ===========================================================================
   *   @n@b mmchsCardInit
   *
   *   @n This function determines the type of MMCSD card.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard - Handle to the mmchsCard
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
   *   @li        MMCHS_RET_CARD_DETECT_ERROR - If failed to detect card
   *   @li       MMCHS_RET_OK if successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
static MMC_RET mmchsCardInit(mmchsCardInfo *mmchsCard)
{
    uint32_t status = 0;

    if (mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    IFPRINT(platform_write ("***************************************\n"));

    platform_delay(10000);
    status = mmchs_reset_card(mmchsCard);
    if (status)
    {
        return status;
    }
    platform_delay(10000);
    status = mmchs_check_card_type(mmchsCard);

    if (status != MMCHS_RET_OK)
    {
        return MMCHS_RET_CARD_DETECT_ERROR;
    }
    else
    {
        if (mmchsCard->cardType == MMCHS_MMC)
        {
            status = mmchs_init_mmc(mmchsCard);
            if (status != MMCHS_RET_OK)
                return status;
            IFPRINT_MMC(platform_write("MMCHS: Card type is eMMC\n\r"));

        }
        else if (mmchsCard->cardType == MMCHS_SD_CARD)
        {
            status = mmchs_init_sd_card(mmchsCard);
            if (status != MMCHS_RET_OK)
                return status;
            IFPRINT_MMC(platform_write("MMCHS: Card type is SD Card\n\r"));
            status = mmchs_set_bus_width(mmchsCard);
            if (status != MMCHS_RET_OK)
                return status;
            status = mmchs_set_tran_speed(mmchsCard);
            if (status != MMCHS_RET_OK)
                return status;
        }
        else if (mmchsCard->cardType  == MMCHS_SDIO)
        {
            status = mmchs_init_sdio(mmchsCard);
            if (status != MMCHS_RET_OK)
                return status;
            IFPRINT_MMC(platform_write("MMCHS: Card type is SDIO\n\r"));
        }
    }
    return status;
}

/** ===========================================================================
   *   @n@b mmchsBlockRead
   *
   *   @b Description
   *   @n This function reads a given block from the mmc device
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard - handle to the mmchsCard
              blkNum - Block Address
              nBlks - size of the block
              buf - buffer to be read into
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li       Returns: The status of read operation
   *   @li       MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li       MMCHS_RET_DATA_RD_FAILED - If failed to read data
   *   @li       MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsBlockRead (mmchsCardInfo *mmchsCard, uint32_t blkNum,\
                uint32_t nBlks, uint8_t *buf)
{
    uint32_t arg;
    uint32_t status;
    uint32_t xferStatus;
    CSL_MmchsRegs *mmcHsReg;

    if(mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }
    mmcHsReg = mmchsCard->mmcHsReg;
    mmchsEnableIntr(mmcHsReg, CSL_MMCHS_IE_BRR_ENABLE_MASK);

    if (mmchsCard->highCap)
        arg = blkNum; /* sector mode */
    else
        arg = blkNum * SECTOR_SIZE; /* byte mode */

    mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);
    platform_delay(10000);
    if (nBlks > 1)
    {
        mmchsSetBlockCount(mmcHsReg, nBlks);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD18, arg, BLK_DATA_LEN);
    }
    else
    {
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD17, arg, BLK_DATA_LEN);
    }
    status = mmchsGetStatus(mmcHsReg, &xferStatus);
    if ((status == MMCHS_RET_OK) &&
        (xferStatus & CSL_MMCHS_STAT_CC_MASK))
    {
        status = mmchsReadData(mmcHsReg, buf, (nBlks * MMCHS_BLK_LEN));
        if (status != MMCHS_RET_OK)
        {
            IFPRINT(platform_write ("MMCHS: Block Read data read failed \n"));
            mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);
            return status;
        }
        mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);
        if (nBlks > 1)
        {
            status = mmchsSendCmd(mmcHsReg, SDMMC_CMD12, 0, 0);
            status = mmchsGetStatus(mmcHsReg, &xferStatus);
            if ((status == MMCHS_RET_OK) &&
                (xferStatus & CSL_MMCHS_STAT_CC_MASK))
            {
                return MMCHS_RET_OK;
            }
        }
    }
    return status;
}

/** ===========================================================================
   *   @n@b mmchsBlockWrite
   *
   *   @b Description
   *   @n This function write to a given block with the data to the mmc device
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard - handle to the mmchsCard
              blkNum - Block Address
              nBlks - size of the block
              buf - data to be written into
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li       Returns: The status of read operation
   *   @li       MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li       MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *  ==========================================================================
   */
MMC_RET mmchsBlockWrite (mmchsCardInfo *mmchsCard, uint32_t blkNum,
             uint32_t nBlks, uint8_t *buf)
{
    uint32_t arg;
    uint32_t status;
    uint32_t xferStatus;
    CSL_MmchsRegs *mmcHsReg;

    if(mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCard->mmcHsReg;
    if (mmchsCard->highCap)
    {
        arg = blkNum; /* sector mode */
    }
    else
    {
        arg = blkNum * SECTOR_SIZE; /* byte mode */
    }

    if (nBlks > 1)
    {
        mmchsSetBlockCount(mmcHsReg, nBlks);
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD25, arg,MMCHS_BLK_LEN);
    }
    else
    {
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD24, arg,MMCHS_BLK_LEN);
    }

    status = mmchsGetStatus(mmcHsReg, &xferStatus);
    if ((status == MMCHS_RET_OK) &&
        (xferStatus & CSL_MMCHS_STAT_CC_MASK))
    {
        status = mmchsWriteData(mmcHsReg, buf, (nBlks * MMCHS_BLK_LEN));
        if (status != MMCHS_RET_OK)
        {
            IFPRINT(platform_write ("MMCHS: Block Read data read failed \n"));
            mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);
            return status;
        }
        mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);
        if (nBlks > 1)
        {
            mmchsDelay(1000);
            status = mmchsSendCmd(mmcHsReg, SDMMC_CMD12, 0,0);
            status = mmchsGetStatus(mmcHsReg, &xferStatus);
            if ((status == MMCHS_RET_OK) &&
                (xferStatus & CSL_MMCHS_STAT_CC_MASK))
            {
                return MMCHS_RET_OK;
            }
        }
    }
    return status;
}


/** ===========================================================================
   *   @n@b mmchsBlockErase
   *
   *   @b Description
   *   @n This function erases a given block
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - handle to the mmcReg
              eraseStart - Erase Start Block Address
              eraseEnd - Erase End Block Address
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li       Returns: The status of read operation
   *   @li       MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li       MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *  ==========================================================================
   */
MMC_RET mmchsBlockErase (mmchsCardInfo *mmchsCard, uint32_t eraseStart,\
                        uint32_t eraseEnd)
{
    uint32_t arg;
    uint32_t status;
    uint32_t xferStatus;
    CSL_MmchsRegs *mmcHsReg;
    uint32_t rsp[4];
    int i;

    if(mmchsCard == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    if (mmchsCard->highCap)
    {
        arg = eraseStart; /* sector mode */
    }
    else
    {
        arg = eraseStart * SECTOR_SIZE; /* byte mode */
    }
    platform_delay(10000);
    mmcHsReg = mmchsCard->mmcHsReg;
    mmchsSetBlockCount(mmcHsReg, 0);
    platform_delay(10000);

    if (mmchsCard->cardType == MMCHS_MMC)
    {
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD35, arg, 0);
    }
    else
    {
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD32, arg, BLK_DATA_LEN);
    }

    status = mmchsGetStatus(mmcHsReg, &xferStatus);
    if (status != MMCHS_RET_OK)
    {
            return status;
    }
    platform_delay(10000);
    mmchsGetResponse(mmcHsReg,rsp);
    IFPRINT(platform_write  ("Response \n"));
     for (i = 3;i>=0;i--)
        IFPRINT(platform_write ("rsp[%d] = 0x%x\n",i,rsp[i]));
    IFPRINT(platform_write  ("\n"));
    mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);
    mmchsDelay(1000);
    platform_delay(10000);
    platform_delay(10000);
    platform_delay(60000);
    if (mmchsCard->highCap)
    {
        arg = eraseEnd; /* sector mode */
    }
    else
    {
        arg = eraseEnd * SECTOR_SIZE; /* byte mode */
    }

    if (mmchsCard->cardType == MMCHS_MMC)
    {
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD36, arg, 0);
    }
    else
    {
        status = mmchsSendCmd(mmcHsReg, SDMMC_CMD33, arg,BLK_DATA_LEN);
    }
    status = mmchsGetStatus(mmcHsReg, &xferStatus);
    if (status != MMCHS_RET_OK)
    {
            return status;
    }
    platform_delay(10000);
    platform_delay(10000);
    mmchsGetResponse(mmcHsReg,rsp);
    IFPRINT(platform_write  ("Response \n"));
         for (i = 3;i>=0;i--)
            IFPRINT(platform_write ("rsp[%d] = 0x%x\n",i,rsp[i]));
        IFPRINT(platform_write  ("\n"));

    mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);
    mmchsDelay(1000);
    platform_delay(10000);
    mmchsSetBlockCount(mmcHsReg, 0);
    arg = 0;
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD38, arg, 0);

    status = mmchsGetStatus(mmcHsReg, &xferStatus);
    if (status != MMCHS_RET_OK)
    {
            return status;
    }
    platform_delay(10000);
    mmchsGetResponse(mmcHsReg,rsp);
    IFPRINT(platform_write  ("Response \n"));
    for (i = 3;i>=0;i--)
        IFPRINT(platform_write ("rsp[%d] = 0x%x\n",i,rsp[i]));
    IFPRINT(platform_write  ("\n"));

    mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);
    mmchsDelay(1000);

    arg = mmchsCard->rca << 16;
    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD13, arg, 0);

    status = mmchsGetStatus(mmcHsReg, &xferStatus);
    if (status != MMCHS_RET_OK)
    {
            return status;
    }
    mmchsGetResponse(mmcHsReg,rsp);
    IFPRINT(platform_write  ("Response \n"));
    for (i = 3;i>=0;i--)
        IFPRINT(platform_write ("rsp[%d] = 0x%x\n",i,rsp[i]));
    IFPRINT(platform_write  ("\n"));

    return status;
}

/** ===========================================================================
   *   @n@b mmchsInit
   *
   *   @b Description
   *   @n This function initializes MMCHS module.
   *
   *   @b Arguments
   *   @verbatim
              mmcInstance - mmcInstance
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        mmchsInfo - handle to the mmchsInfo
   *   @li        NULL - if error
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
mmchsInfo*  mmchsInit(uint8_t mmcInstance)
{
    MMC_RET          retVal;
    mmchsControllerInfo *ctrl;
    mmchsCardInfo *card;

    ctrl = (mmchsControllerInfo *)malloc(sizeof(mmchsControllerInfo));
    card = (mmchsCardInfo *)malloc(sizeof(mmchsCardInfo));

    if ((ctrl == NULL) || (card == NULL))
        return NULL;

    memset(ctrl, 0, sizeof (mmchsControllerInfo));
    memset(card, 0, sizeof (mmchsCardInfo));

    gMmchsInfo.mmcHsReg = mmchsGetRegBaseaddr(mmcInstance);

    gMmchsInfo.mmchsCtrl = ctrl;
    gMmchsInfo.card = card;
    ctrl->mmcHsReg = gMmchsInfo.mmcHsReg;
    card->mmcHsReg = gMmchsInfo.mmcHsReg;
    retVal = mmchsReset(gMmchsInfo.mmchsCtrl);

    if (retVal != MMCHS_RET_OK)
    {
        return NULL;
    }
    retVal = mmchsControllerInit(ctrl);
    if (mmcInstance == 1)
    {
        ctrl->ocr = (SD_OCR_VDD_3P2_3P3|SD_OCR_VDD_3P3_3P4);
    }
    if (retVal == MMCHS_RET_OK)
    {
        retVal = mmchsEnableIntr(gMmchsInfo.mmcHsReg, MMCHS_INTR_ENABLE);
        card->ctrl = ctrl;
        if (mmcInstance == MMCHS_SD_CARD_INST)
        {
            if (mmchsIsCardInserted(gMmchsInfo.mmcHsReg) == FALSE)
            {
                platform_write("SD card is not inserted in the slot \n\r");
                free(ctrl);
                free(card);
                return NULL;
            }
        }
        retVal = mmchsCardInit(gMmchsInfo.card);
        ctrl->card = gMmchsInfo.card;
        if (retVal == MMCHS_RET_OK)
        {
            free(ctrl);
            free(card);
            return &gMmchsInfo;
        }
    }
    free(ctrl);
    free(card);
    return NULL;
}
/* Nothing past this point */

#endif /* #if (PLATFORM_MMCHS_IN) */
