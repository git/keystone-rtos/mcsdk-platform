/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
*
*  \file  evmc66x_mmchs.c
*
*  \brief This file contains C66x mmchs specific functions.
*
******************************************************************************/

#include "platform_internal.h"

#if (PLATFORM_MMCHS_IN)

/** ===========================================================================
   *   @n@b mmchsDelay
   *
   *   @b Description
   *   @n This function creates a delay of given value in usecs .
   *
   *   @b Arguments
   *   @verbatim
              usecs - usecs to give the delay
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @n  None
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
void mmchsDelay (uint32_t usecs)
{
    platform_delay(usecs);
}

 /** ===========================================================================
  *   @n@b mmchsGetRegBaseaddr
  *
  *   @b Description
  *   @n This function is used to get the base address of given MMC Instance.
  *
  *   @b Arguments
  *   @verbatim
             mmcInstance       Instance of MMC: MMC0 - SD Card,
             MMC1 - eMMC/SDIO Card

     mmcInstance can take the values \n
        CSL_MMCSD_0 - SD Card \n
        CSL_MMCSD_1 - eMMC/WLAN \n
      @endverbatim
  *
  *   <b> Return Value </b>  CSL_MmchsRegs*
  *   @li           The base address of the mmcInstance on success
  *  @li            NULL if type is not found
  *
  *   @b Modifies
  *   @n  None
  *
  *  ===========================================================================
  */
CSL_MmchsRegs* mmchsGetRegBaseaddr(uint8_t mmcInstance)
{
    CSL_MmchsRegs    *addr;

    switch(mmcInstance)
    {
        case CSL_MMCSD_0:
            addr = (CSL_MmchsRegs *)CSL_MMCSD_0_S_REGS;
            break;

        case CSL_MMCSD_1:
            addr = (CSL_MmchsRegs *)CSL_MMCSD_1_S_REGS;
            break;

        default :
            addr = NULL;
            break;
    }

    return addr;
}

 /** ===========================================================================
   *   @n@b mmchsEnableClock
   *
   *   @b Description
   *   @n This function is used to enable the clock for the given frequency
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - handle to the mmcReg
              clock - Input frequency


      clock can take the values \n
            MMCHS_CLK_INITSEQ
            MMCHS_CLK_400KHZ
            MMCHS_CLK_12MHZ
            MMCHS_CLK_24MHZ
            MMCHS_CLK_26MHZ
            MMCHS_CLK_48MHZ
            MMCHS_CLK_52MHZ
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *  @li        MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsEnableClock(CSL_MmchsRegs *mmcHsReg, uint32_t clock)
{
    uint32_t val;
    uint32_t regVal;

    if(mmcHsReg == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    /* disables, cen and set data timeout value */
    CSL_FINS(mmcHsReg->SYSCTL, MMCHS_SYSCTL_CEN, DISABLE);
    CSL_FINS(mmcHsReg->SYSCTL, MMCHS_SYSCTL_DTO, MMC_DTO_MAX);

    switch (clock) {
    case HSMMCSD_INIT_FREQ:
    	val = (MMCHS_SDMMC_REF_CLK * 1000 / (400));
        CSL_FINS(mmcHsReg->SYSCTL, MMCHS_SYSCTL_DTO, 0x0);
        break;

    case MMCHS_CLK_12MHZ:
        val = (MMCHS_SDMMC_REF_CLK * 1000 / (12000));
        break;

    case MMCHS_CLK_24MHZ:
    case HSMMCSD_TRAN_SPEED_24M_CLK:
        val = (MMCHS_SDMMC_REF_CLK * 1000 / (24000));
        break;

    case MMCHS_CLK_26MHZ:
    case HSMMCSD_TRAN_SPEED_26M_CLK:
            val = (MMCHS_SDMMC_REF_CLK * 1000 / (26000));
            val = val+1;
            break;
    case MMCHS_CLK_48MHZ:
    case HSMMCSD_TRAN_SPEED_48M_CLK:
            val = (MMCHS_SDMMC_REF_CLK * 1000 / (48000));
            val = val+1;
        break;

    case MMCHS_CLK_50MHZ:
    case HSMMCSD_TRAN_SPEED_50M_CLK:
            val = (MMCHS_SDMMC_REF_CLK * 1000 / (50000));
            val = val+1;
            break;

    case MMCHS_CLK_52MHZ:
    case HSMMCSD_TRAN_SPEED_52M_CLK:
            val = (MMCHS_SDMMC_REF_CLK * 1000 / (52000));
            val = val+1;

            break;

    default:
            val = (MMCHS_SDMMC_REF_CLK * 1000 / (clock/1000));
            val = 0x04;
    }

    IFPRINT(platform_write ("Setting Clock speed to %d\n",clock));
    /* Select clock frequency */

    CSL_FINS(mmcHsReg->SYSCTL, MMCHS_SYSCTL_CLKD, RESETVAL);
    mmcHsReg->SYSCTL |= (val << 6);

    /* enable clock */
    CSL_FINS(mmcHsReg->SYSCTL, MMCHS_SYSCTL_ICE, OSCILLATE);
    mmchsDelay(10);

    /* Wait until stable? */
    do
    {
        regVal = CSL_FEXT(mmcHsReg->SYSCTL, MMCHS_SYSCTL_ICS);
    }while (regVal == 0);

    /* Enable CEN */
    CSL_FINS(mmcHsReg->SYSCTL, MMCHS_SYSCTL_CEN, ENABLE);

    mmchsDelay(1000);

    return MMCHS_RET_OK;
}

/** ===========================================================================
   *   @n@b mmchsReset
   *
   *   @b Description
   *   @n This function Resets the mmc module and the controller.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCtrlInfo - handle to the Controller
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li        MMCHS_RET_IDLE_TIMEOUT - If reset fails and times out.
   *   @li        MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsReset(mmchsControllerInfo *mmchsCtrlInfo)
{
    uint32_t    pollingStatus;
    volatile uint32_t timeout = 0xFFFF;
    CSL_MmchsRegs *mmcHsReg;

    if ((mmchsCtrlInfo == NULL) || (mmchsCtrlInfo->mmcHsReg == NULL))
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCtrlInfo->mmcHsReg;

    /* software reset */
    mmcHsReg->SYSCONFIG |= CSL_MMCHS_SYSCONFIG_SOFTRESET_MASK;

    /* Check if reset done */
    do
    {
        pollingStatus = CSL_FEXT(mmcHsReg->SYSSTATUS, \
                                    MMCHS_SYSSTATUS_RESETDONE);

        if (pollingStatus == CSL_MMCHS_SYSSTATUS_RESETDONE_DONE)
        {
            break;
        }
    } while (timeout--);

    if (0 == timeout)
    {
        return MMCHS_RET_IDLE_TIMEOUT;
    }
    else
    {
        /* Reset is completed; Now reset the host controller */
        CSL_FINS(mmcHsReg->SYSCTL,MMCHS_SYSCTL_SRA, RESET);
        do {
            if (CSL_FEXT(mmcHsReg->SYSCTL, MMCHS_SYSCTL_SRA))
                break;
        } while (timeout--);

        if (0 == timeout)
        {
            return MMCHS_RET_IDLE_TIMEOUT;
        }
    }
    return MMCHS_RET_OK;
}

/** ===========================================================================
   *   @n@b mmchs_send_init_stream
   *
   *   @b Description
   *   @n This function sends the INIT stream to the card
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - handle to the mmcReg
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li         MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
static MMC_RET mmchs_send_init_stream(CSL_MmchsRegs *mmcHsReg)
{
    uint32_t status;
    uint32_t retry;

    if (mmcHsReg == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    /* Enable the command completion status to be set */
    CSL_FINS(mmcHsReg->IE, MMCHS_IE_CC_ENABLE, ENABLE);

    /* Initiate the INIT command */
    CSL_FINS(mmcHsReg->CON, MMCHS_CON_INIT, 1);
    mmcHsReg->CMD = 0;
    mmchsDelay(1);

    retry = 0xFFFF;
    do {
        status = CSL_FEXT(mmcHsReg->STAT, MMCHS_STAT_CC);
        if ( STAT_CC == status)
        {
            mmcHsReg->STAT |= CSL_MMCHS_STAT_CC_MASK;
            break;
        }
    } while (retry--);

    if (retry == 0)
    {
        return MMCHS_RET_IDLE_TIMEOUT;
    }

    CSL_FINS(mmcHsReg->CON, MMCHS_CON_INIT, 0);

    /* Clear all status */
    mmcHsReg->STAT = 0xFFFFFFFF;

    return MMCHS_RET_OK;
}

/** ===========================================================================
   *   @n@b mmchs_bus_power
   *
   *   @b Description
   *   @n This function turn MMC/SD bus power on / off
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - handle to the mmcReg
              pwr           power on / off setting

   * pwr can take the values \n
   *     HS_MMCSD_BUS_POWER_ON \n
   *     HS_MMCSD_BUS_POWER_OFF \n
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li         MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   */
static MMC_RET mmchs_bus_power(CSL_MmchsRegs *mmcHsReg, uint8_t pwr)
{
    volatile uint32_t timeout = 0xFFFFF;

    if ((mmcHsReg == NULL) && (pwr > 1))
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    CSL_FINS(mmcHsReg->HCTL, MMCHS_HCTL_SDBP, pwr);

    do {
        if (pwr == CSL_FEXT(mmcHsReg->HCTL, MMCHS_HCTL_SDBP))
        {
            break;
        }
    } while(timeout--);

    if (timeout == 0)
    {
        return MMCHS_RET_IDLE_TIMEOUT;
    }
    else
    {
        return MMCHS_RET_OK;
    }
}

/** ===========================================================================
   *   @n@b mmchsControllerInit
   *
   *   @b Description
   *   @n This function configures the mmc module and the controller.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCtrlInfo - handle to the Controller
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li         MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsControllerInit(mmchsControllerInfo *mmchsCtrlInfo)
{
    uint32_t status;
    CSL_MmchsRegs *mmcHsReg;

    if ((mmchsCtrlInfo == NULL) || (mmchsCtrlInfo->mmcHsReg == NULL))
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg = mmchsCtrlInfo->mmcHsReg;

    /* Initially configure the host bus width with the possible bus width.
       Then while setting the bus width, based on the card capability set
       the bus width according to the CSR */
    mmchsCtrlInfo->busWidth = (SD_BUS_WIDTH_1BIT | SD_BUS_WIDTH_4BIT);
    mmchsCtrlInfo->highSpeed = 1;
    mmchsCtrlInfo->ocr = (SD_OCR_VDD_3P0_3P1 | SD_OCR_VDD_3P1_3P2);
    mmchsCtrlInfo->opClk = HSMMCSD_INIT_FREQ;

    /* Set supported voltage list - 1.8V and 3.0 V*/
    mmcHsReg->CAPA &= ~(CSL_MMCHS_CAPA_VS18_MASK | \
                              CSL_MMCHS_CAPA_VS33_MASK | \
                              CSL_MMCHS_CAPA_VS30_MASK);

    mmcHsReg->CAPA |= (CSL_MMCHS_CAPA_VS18_MASK | \
                             CSL_MMCHS_CAPA_VS30_MASK);

    /* Set to autoidle enable */

    mmcHsReg->SYSCONFIG &= ~(CSL_MMCHS_SYSCONFIG_CLOCKACTIVITY_MASK | \
                                   CSL_MMCHS_SYSCONFIG_STANDBYMODE_MASK | \
                                   CSL_MMCHS_SYSCONFIG_SIDLEMODE_MASK | \
                                   CSL_MMCHS_SYSCONFIG_ENAWAKEUP_MASK | \
                                   CSL_MMCHS_SYSCONFIG_AUTOIDLE_MASK);
    mmcHsReg->SYSCONFIG |=  CSL_MMCHS_SYSCONFIG_AUTOIDLE_MASK;

    /* 1 Bit Mode, 3V Bus Voltage (SVDS) and Bus Pwr Off */
    CSL_FINS(mmcHsReg->CON, MMCHS_CON_DW8, BITMODE1_4);
    CSL_FINS(mmcHsReg->HCTL, MMCHS_HCTL_DTW, BITMODE_1);

    /* Set block len to 512 */
    mmcHsReg->BLK = MMCHS_BLK_LEN;

    /* Set the bus voltage to 3.0 V*/
    CSL_FINS(mmcHsReg->HCTL, MMCHS_HCTL_SDVS, V3_0);

    /* Switch on Bus Power */
    status = mmchs_bus_power(mmcHsReg, HS_MMCSD_BUS_POWER_ON);

    if (status != 0)
        return status;
    status = mmchsEnableClock(mmcHsReg, HSMMCSD_INIT_FREQ);

    if (status != 0)
            return status;

    /* Send Initialisation Sequence for 80 clock cycles */
    status = mmchs_send_init_stream(mmcHsReg);
    if (status != 0)
            return status;
    return MMCHS_RET_OK;
}

/** ===========================================================================
   *   @n@b mmchsBusWidthConfig
   *
   *   @b Description
   *   @n This function configures the mmc bus width on the host controller
   *
   *   @b Arguments
   *   @verbatim
   *      mmcHsReg - handle to the mmcReg
          buswidth   SD/MMC bus width.\n
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li         MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsBusWidthConfig(CSL_MmchsRegs *mmcHsReg, uint8_t busWidth)
{
    if ((mmcHsReg == NULL) || (busWidth > SD_BUS_WIDTH_8BIT))
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    IFPRINT(platform_write ("Setting bus width to %d\n",busWidth));
    switch(busWidth)
    {
    case SD_BUS_WIDTH_4BIT:
        CSL_FINS(mmcHsReg->CON, MMCHS_CON_DW8, BITMODE1_4);
        CSL_FINS(mmcHsReg->HCTL, MMCHS_HCTL_DTW, BITMODE_4);
        break;

    case SD_BUS_WIDTH_8BIT:
        CSL_FINS(mmcHsReg->CON, MMCHS_CON_DW8, BITMODE_8);
        break;

    default:  // Default will be set to 1-bit mode
        CSL_FINS(mmcHsReg->CON, MMCHS_CON_DW8, BITMODE1_4);
        CSL_FINS(mmcHsReg->HCTL, MMCHS_HCTL_DTW, BITMODE_1);
        break;
    }

    return MMCHS_RET_OK;
}

/** ===========================================================================
   *   @n@b mmchsEnableIntr
   *
   *   @b Description
   *   @nThis function enables the controller events to generate an interrupt
        request
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - handle to the mmcReg
              flags - flag to enable the Interrupt
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li         MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsEnableIntr(CSL_MmchsRegs *mmcHsReg, uint32_t flags)
{
    if(mmcHsReg == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg->ISE |= flags;
    mmcHsReg->IE |= flags;

    return MMCHS_RET_OK;
}

/** ===========================================================================
   *   @n@b mmchsSetBlockCount
   *
   *   @b Description
   *   @n This function sets the block count for multi block data transfer
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - handle to the mmcReg
              nBlks    - number of Blocks
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsSetBlockCount(CSL_MmchsRegs *mmcHsReg, uint32_t nBlks)
{
    uint32_t blkLen = 0;
    if (nBlks == 0) return MMCHS_RET_OK;

    blkLen |= nBlks << MMCHS_BLK_NBLK_SHIFT;
    mmcHsReg->BLK |= blkLen;
    return MMCHS_RET_OK;
}

/** ===========================================================================
   *   @n@b mmchsSendCmd
   *
   *   @b Description
   *   @n This function sends the command to the device over the data bus.
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - handle to the mmcReg
              cmd      - command to send to the controller
              arg      - argument
              blkSize  - block size
              nBlks    - number of Blocks
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li        MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsSendCmd(CSL_MmchsRegs *mmcHsReg, uint32_t cmd, uint32_t arg,
                     uint32_t blkSize)
{
    uint32_t regVal;

    if(mmcHsReg == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }
    /* Software reset cmd and dat lines */
    CSL_FINS(mmcHsReg->SYSCTL, MMCHS_SYSCTL_SRD, RESET);
    CSL_FINS(mmcHsReg->SYSCTL, MMCHS_SYSCTL_SRC, RESET);

    do
    {
        regVal = CSL_FEXTR(mmcHsReg->SYSCTL, \
                            CSL_MMCHS_SYSCTL_SRD_SHIFT, \
                            CSL_MMCHS_SYSCTL_SRC_SHIFT);
        if (regVal == 0)
            break;
    }while (1);

    mmchsClearIntrStatus(mmcHsReg,0xFFFFFFFF);

    /* wait until you are allowed to send cmd */
    do
    {
        regVal = CSL_FEXT(mmcHsReg->PSTATE, MMCHS_PSTATE_DATI);
    if (regVal == CSL_MMCHS_PSTATE_DATI_CMDEN)
            break;
    }while(1);

    /* Set block length to blkLen */
    mmcHsReg->BLK = blkSize;
    mmcHsReg->ARG = arg;
    mmcHsReg->CMD = cmd;
    mmchsDelay(100);

#ifdef DEBUG
    IFPRINT(platform_write ("CMD: %d : ",cmd >> 24));
    IFPRINT(platform_write ("cmd 0x%x; arg: 0x%x blklen: 0x%x  ",cmd,arg,\
                    mmcHsReg->BLK));
    IFPRINT(platform_write ("Status 0x%x\n",mmcHsReg->STAT));
#endif
    platform_delay(100);
    return MMCHS_RET_OK;
}

/** ===========================================================================
   *   @n@b mmchsSendAppCmd
   *
   *   @b Description
   *   @n This function sends the application command to MMCSD.
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - Handle to the mmcReg
              cmd - Command to be sent
              arg - argument for the command
              blkLen - no of bytes in a block
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
   *   @li       MMCHS_RET_OK if successful
   *            MMCHS_RET_CMD_TIMEOUT  if cmd timeout
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchsSendAppCmd(CSL_MmchsRegs *mmcHsReg, uint32_t cmd,
                                    uint32_t arg, uint32_t blkLen)
{
    volatile uint32_t status = 0;
     uint32_t xferStat = 0;

    if (mmcHsReg == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    status = mmchsSendCmd(mmcHsReg, SDMMC_CMD55, 0,0);
    if (status == MMCHS_RET_OK)
    {
        status = mmchsGetStatus(mmcHsReg, &xferStat);
        if ((status == MMCHS_RET_OK) &&
                (xferStat & CSL_MMCHS_STAT_CC_MASK))
        {
            status = mmchsSendCmd(mmcHsReg, cmd, arg, blkLen);
            status = mmchsGetStatus(mmcHsReg, &xferStat);
        }
        else if (xferStat & CSL_MMCHS_STAT_CTO_MASK)
        {
            status = MMCHS_RET_CMD_TIMEOUT;
        }
    }
    return status;
}

/** ===========================================================================
   *   @n@b mmchsClearIntrStatus
   *
   *   @b Description
   *   @n This function clears the interrupt status
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - Handle to the mmcReg
   *          flag     - Specific status to be cleared;
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li        MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsClearIntrStatus(CSL_MmchsRegs *mmcHsReg, uint32_t flag)
{

    if(mmcHsReg == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    mmcHsReg->STAT |= flag;
    return MMCHS_RET_OK;
}

/** ============================================================================
   *   @n@b mmchsGetStatus
   *
   *   @b Description
   *   @nThis function gets status of the controller after sending the command.
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - Handle to the mmcReg
   *          flag     - Specific status to be cleared;
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li        MMCHS_RET_SEND_FAILED - If error flag is set for the command
   *   @li        MMCHS_RET_SEND_NORSP -  If no response for the send command
   *   @li        MMCHS_RET_OK - If send command is responded with
                                 command/transfer complete
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsGetStatus(CSL_MmchsRegs *mmcHsReg, uint32_t *xferStatus)
{
    uint32_t retry = 0xFFFF;
    uint32_t status = 0;

    if ((mmcHsReg == NULL) || (xferStatus == NULL))
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    *xferStatus = 0;
    mmcHsReg->BLK = 0;
    do
    {
        status = mmcHsReg->STAT;
    } while ((status == 0) && retry--);

    *xferStatus = status;
#ifdef DEBUG
    IFPRINT(platform_write (" stat: 0x%x  ",status));
    IFPRINT(platform_write ("Rsp1 : 0x%x\tRsp 2: 0x%x\tRsp3: 0x%x\tRsp4: 0x%x\n",
                    mmcHsReg->RSP10, mmcHsReg->RSP32,
                    mmcHsReg->RSP54, mmcHsReg->RSP76));
#endif
    if (status & CSL_MMCHS_STAT_CC_MASK)
    {
        return MMCHS_RET_OK;
    }
    if (status && ((status & CSL_MMCHS_STAT_ERRI_MASK) == 0))
    {
        return MMCHS_RET_OK;
    }

    if (status & CSL_MMCHS_STAT_ERRI_MASK)
    {
        return MMCHS_RET_SEND_FAILED;
    }
    else
    {
         return MMCHS_RET_SEND_NORSP;
    }
}

/** ============================================================================
   *   @n@b mmchsGetResponse
   *
   *   @b Description
   *   @n This function get the command response from the controller
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - Handle to the mmcReg
   *          rsp        pointer to buffer to be filled with the response
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        status flags
   *
   *   @b Modifies
   *   @n  None
   *
   * \note: that this function shall return the values from all response
   * registers. Hence, rsp, must be a pointer to memory which can hold max
   * response length. It is the responsibility of the caller to use only the
   * required/relevant parts of the response
   *
   *  ==========================================================================
   */
MMC_RET mmchsGetResponse(CSL_MmchsRegs *mmcHsReg, uint32_t *rsp)
{
    if((mmcHsReg == NULL) || (rsp == NULL))
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    rsp[3] = mmcHsReg->RSP10;
    rsp[2] = mmcHsReg->RSP32;
    rsp[1] = mmcHsReg->RSP54;
    rsp[0] = mmcHsReg->RSP76;
    mmchsDelay(10000);
    return MMCHS_RET_OK;
}

/** ============================================================================
   *   @n@b mmchsReadData
   *
   *   @b Description
   *   @n This function reads the data from the mmc device (SD card/eMMC/SDIO)
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg   - Handle to the mmcReg
              dataBuffer - pointer to the data buffer
              blkSize    - block size
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_DATA_RD_FAILED    if read fails
   *   @li        MMCHS_RET_OK if successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchsReadData(CSL_MmchsRegs *mmcHsReg, uint8_t *dataBuffer,
                      uint32_t blkSize)
{
    uint32_t  retry = 0xFFFF;
    uint32_t  status;
    uint32_t blkCnt;

    if((mmcHsReg == NULL) || (dataBuffer == NULL))
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    IFPRINT(platform_write  ("Reading data from mmc device of size 0x%x\n",blkSize));
    for (blkCnt = 0; blkCnt < blkSize/4; blkCnt++)
    {
        do
        {
            status = mmcHsReg->STAT;
            if (status & CSL_MMCHS_STAT_TC_MASK)
                break;
        } while (((status & STAT_BRR) == 0) && retry--); /* ready to read? */

        if (retry == 0)
        {
            break;
        }
        mmchsDelay(50);
        retry = 0xFFFF;
        *(uint32_t *)dataBuffer = mmcHsReg->DATA;
        mmchsDelay(1000);
        IFPRINT(platform_write  ("0x%x ",*(uint32_t *)dataBuffer));
        dataBuffer = dataBuffer + 4;
    }
    platform_write  ("\n");
    if (retry == 0)
    {
        IFPRINT(platform_write ("MMCHS:error read failed\n"));
        return MMCHS_RET_DATA_RD_FAILED;
    }
    else
    {
        return MMCHS_RET_OK;
    }
}

/** ============================================================================
   *   @n@b mmchsWriteData
   *
   *   @b Description
   *   @n This function writes the data to the mmc device (SD card/eMMC/SDIO)
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg   - Handle to the mmcReg
              dataBuffer - pointer to the data buffer
              blkSize    - block size
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_DATA_WR_FAILED    if write fails
   *   @li        MMCHS_RET_OK if successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchsWriteData(CSL_MmchsRegs *mmcHsReg, uint8_t *dataBuffer,
                       uint32_t blkSize)
{
    uint32_t  retry = 0xFFFF;
    uint32_t  status;
    uint32_t blkCnt;

    if((mmcHsReg == NULL) || (dataBuffer == NULL))
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    for (blkCnt = 0; blkCnt < blkSize/4; blkCnt++)
    {
        do
        {
            status = mmcHsReg->STAT;
        } while (((status & STAT_BWR) == 0) && retry--); /* ready to write? */

        if (retry == 0)
        {
            break;
        }
        mmchsDelay(5);
        retry = 0xFFFF;
        mmcHsReg->DATA = *(uint32_t *)dataBuffer;
        dataBuffer = dataBuffer + 4;
    }
    if (retry == 0)
    {
        IFPRINT(platform_write ("MMCHS:error write failed\n"));
        return MMCHS_RET_DATA_WR_FAILED;
    }
    else
    {
        return MMCHS_RET_OK;
    }
}

/** ============================================================================
   *   @n@b mmchsIsCardInserted
   *
   *   @b Description
   *   @n This function checks if the card is inserted in the sdcard slot
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - Handle to the mmcReg
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        0  if the card is not inserted and detected
   *   @li        1  if the card is inserted and detected
   *
   *   @b Modifies
   *   @n  None
   * \note: that this functional may not be available for all instances of the
   * controller. This function, is only useful if the controller has a dedicated
   * card detect pin. If not, the card detection mechanism is application
   * implementation specific
   *  =========================================================================
   */
Bool mmchsIsCardInserted(CSL_MmchsRegs *mmcHsReg)
{
    uint32_t status;
    if (mmcHsReg == NULL)
    {
        return MMCHS_RET_INVALID_PARAM;
    }

    status = mmcHsReg->PSTATE & CSL_MMCHS_PSTATE_CINS_MASK;
    if (status)
        return TRUE;
    else
        return FALSE;
}

#endif  /* #if (PLATFORM_MMCHS_IN) */
