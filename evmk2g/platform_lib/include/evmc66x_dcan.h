/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 *  \file	evmc66x_dcan.h
 *
 *  \brief	This file contains DCAN specific structure, typedefs, function
 *			prototypes.
 *
 ******************************************************************************/

#ifndef	_EVMC66X_DCAN_H_
#define	_EVMC66X_DCAN_H_

/** Padconfig pin number for DCAN0 Tx */
#define PIN_NUM_127  (127)
/** Padconfig pin number for DCAN0 Rx */
#define PIN_NUM_128  (128)
/** Padconfig pin number for DCAN1 Tx */
#define PIN_NUM_137  (137)
/** Padconfig pin number for DCAN1 Rx */
#define PIN_NUM_138  (138)

/**
 * \brief This function initializes DCAN module.
 *
 * This function should be called before using any other DCAN functions
 *
 */
void dcanInit(void);

/**
 *
 * \brief  This Function toggle the pins between low and high
 * \n      states with some delay
 *
 * \param  timeDelay  [IN]   Time delay between high and low states in usec
 *
 * \return
 * \n      GPIO_RET_OK          - Requested operation is successful
 * \n      INVALID_GPIO_PORT    - Invalid GPIO port number
 * \n      INVALID_GPIO_NUMBER  - Invalid GPIO pin number
 *
 */
int16_t dcanTogglePins(uint32_t timeDealy);

#endif // _EVMC66X_DCAN_H_

/* Nothing past this point */
