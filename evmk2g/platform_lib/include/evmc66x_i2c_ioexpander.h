/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * \file       evmc66x_i2c_ioexpander.h
 *
 * \brief This contains I2C IO expander specific structures, typedefs and
 *                function prototypes.
 *
 ******************************************************************************/

#ifndef _EVMC66X_I2C_IOEXPANDER_H_
#define _EVMC66X_I2C_IOEXPANDER_H_

/** I2C IO expander slave address */
#define I2C_IO_EXP_ADDR             (0x20)
/** IO expander I2C port number */
#define IO_EXP_I2C_PORT             (I2C_PORT_1)

/** IO expander pin state high */
#define I2C_IO_EXP_PIN_HIGH         (1)
/** IO expander pin state low */
#define I2C_IO_EXP_PIN_LOW          (0)

/** IO expander input pin direction */
#define I2C_IO_EXP_PIN_IN           (1)
/** IO expander output pin direction */
#define I2C_IO_EXP_PIN_OUT          (0)

/** IO expander port 0 read register */
#define I2C_IO_EXP_PORT0_IN         (0)
/** IO expander port 1 read register */
#define I2C_IO_EXP_PORT1_IN         (1)
/** IO expander port 0 write register */
#define I2C_IO_EXP_PORT0_OUT        (2)
/** IO expander port 1 write register */
#define I2C_IO_EXP_PORT1_OUT        (3)
/** IO expander port 0 polarity inversion register */
#define I2C_IO_EXP_PORT0_POL_INV    (4)
/** IO expander port 1 polarity inversion register */
#define I2C_IO_EXP_PORT1_POL_INV    (5)
/** IO expander port 0 configuration register */
#define I2C_IO_EXP_PORT0_CFG        (6)
/** IO expander port 1 configuration register */
#define I2C_IO_EXP_PORT1_CFG        (7)

/** Macro to get port number of a pin */
#define IO_EXP_GET_PORT(p) ((p >= IO_EXP_P10) ? 1 : 0)

/** IO expander port 0 default configurations. All unused pins
    are configured as input */
#define I2C_IO_EXP_P0_DEF_CFG  (uint8_t)((I2C_IO_EXP_PIN_IN << IO_EXP_P00) | \
                                         (I2C_IO_EXP_PIN_IN << IO_EXP_P01) | \
                                         (I2C_IO_EXP_PIN_OUT << IO_EXP_SOC_LED2) | \
                                         (I2C_IO_EXP_PIN_OUT << IO_EXP_SOC_LED3) | \
                                         (I2C_IO_EXP_PIN_OUT << IO_EXP_SOC_LED4) | \
                                         (I2C_IO_EXP_PIN_OUT << IO_EXP_SOC_WLAN_EN) | \
                                         (I2C_IO_EXP_PIN_OUT << IO_EXP_SOC_BT_EN) | \
                                         (I2C_IO_EXP_PIN_IN << IO_EXP_P07))

/** IO expander port 1 default configurations. All unused pins
    are configured as input */
#define I2C_IO_EXP_P1_DEF_CFG  (uint8_t)((I2C_IO_EXP_PIN_IN << (IO_EXP_P10 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_IN << (IO_EXP_P11 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_IN << (IO_EXP_P12 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_IN << (IO_EXP_P13 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_IN << (IO_EXP_P14 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_IN << (IO_EXP_P15 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_IN << (IO_EXP_SEL_HDMI - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_IN << (IO_EXP_SEL_LCD - IO_EXP_P10)))

/** IO expander port 0 default values */
#define I2C_IO_EXP_P0_DEF_VAL  (uint8_t)((I2C_IO_EXP_PIN_LOW << IO_EXP_P00) | \
                                         (I2C_IO_EXP_PIN_LOW << IO_EXP_P01) | \
                                         (I2C_IO_EXP_PIN_HIGH << IO_EXP_SOC_LED2) | \
                                         (I2C_IO_EXP_PIN_HIGH << IO_EXP_SOC_LED3) | \
                                         (I2C_IO_EXP_PIN_HIGH << IO_EXP_SOC_LED4) | \
                                         (I2C_IO_EXP_PIN_LOW << IO_EXP_SOC_WLAN_EN) | \
                                         (I2C_IO_EXP_PIN_LOW << IO_EXP_SOC_BT_EN) | \
                                         (I2C_IO_EXP_PIN_LOW << IO_EXP_P07))

/** IO expander port 1 default values */
#define I2C_IO_EXP_P1_DEF_VAL  (uint8_t)((I2C_IO_EXP_PIN_LOW << (IO_EXP_P10 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_LOW << (IO_EXP_P11 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_LOW << (IO_EXP_P12 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_LOW << (IO_EXP_P13 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_LOW << (IO_EXP_P14 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_LOW << (IO_EXP_P15 - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_LOW << (IO_EXP_SEL_HDMI - IO_EXP_P10)) | \
                                         (I2C_IO_EXP_PIN_LOW << (IO_EXP_SEL_LCD - IO_EXP_P10)))

/**
 * \brief IO expander pin definitions
 */
typedef enum _I2cIoExpPins
{
    IO_EXP_P00 = 0,     /**< Port 0, pin 0 - Unused                  */
    IO_EXP_P01,         /**< Port 0, pin 1 - Unused                  */
    IO_EXP_SOC_LED4,    /**< Port 0, pin 2 - SOC LED4                */
    IO_EXP_SOC_LED3,    /**< Port 0, pin 3 - SOC LED3                */
    IO_EXP_SOC_LED2,    /**< Port 0, pin 4 - SOC LED2                */
    IO_EXP_SOC_WLAN_EN, /**< Port 0, pin 5 - WLAN enable             */
    IO_EXP_SOC_BT_EN,   /**< Port 0, pin 6 - BT enable               */
    IO_EXP_P07,         /**< Port 0, pin 7 - Unused                  */
    IO_EXP_P10,         /**< Port 1, pin 0 - Unused                  */
    IO_EXP_P11,         /**< Port 1, pin 1 - Unused                  */
    IO_EXP_P12,         /**< Port 1, pin 2 - Unused                  */
    IO_EXP_P13,         /**< Port 1, pin 3 - Unused                  */
    IO_EXP_P14,         /**< Port 1, pin 4 - Unused                  */
    IO_EXP_P15,         /**< Port 1, pin 5 - Unused                  */
    IO_EXP_SEL_HDMI,    /**< Port 1, pin 6 - HDMI select status      */
    IO_EXP_SEL_LCD,     /**< Port 1, pin 7 - LCD select status       */
    IO_EXP_MAX          /**< Maximum pin number - For error checking */

} I2cIoExpPins;

/**
 *  \brief    Initializes IO expander
 *
 *  This function configures default direction and value of the IO
 *  expander pins. I2C module should be initialized before calling
 *  this function.
 *
 *  \return    - I2C_RET
 */
I2C_RET i2cIoExpanderInit(void);

/**
 *  \brief    Reads the value of an input pin from I/O Expander
 *
 *  Pin from which the value to be read should be configured
 *  as input before calling this function
 *
 *  \param    pin    [IN]   Pin ID
 *
 *  \return    - Pin value or 0xFF in case of error
 */
uint8_t i2cIoExpanderReadPin(I2cIoExpPins pin);

/**
 *  \brief    Writes the given value to an output pin of I/O Expander
 *
 *  Pin to which the value to be written should be configured
 *  as output before calling this function
 *
 *  \param  pin    [IN]   Pin number of selected port
 *  \param  value  [IN]   Value to write
 *
 *  \return    - I2C_RET
 */
I2C_RET i2cIoExpanderWritePin(I2cIoExpPins pin, uint8_t value);

/**
 *  \brief    Configures direction of the IO expander pin as IN/OUT
 *
 *  \param  pin     [IN]   Pin number of selected port
 *  \param  pinDir  [IN]   Direction of the pin
 *                           I2C_IO_EXP_PIN_IN  - Configures pin as input
 *                           I2C_IO_EXP_PIN_OUT - Configures pin as output
 *
 *  \return    - I2C_RET
 */
I2C_RET i2cIoExpanderConfigPin(I2cIoExpPins pin, uint8_t pinDir);

#endif // _EVMC66X_I2C_IOEXPANDER_H_

/* Nothing past this point */
