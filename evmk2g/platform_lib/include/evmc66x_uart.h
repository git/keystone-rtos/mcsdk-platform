/*
 * Copyright (c) 2011-2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * \file  evmc66x_uart.h
 *
 * \brief This contains UART specific structure, typedefs, function
 *		  prototypes.
 *
 ******************************************************************************/

#ifndef	_EVM66X_UART_H_
#define	_EVM66X_UART_H_

/************************
 * Defines and Macros
 ************************/
// Mask	to enable DLL and DLM
#define	DLAB		(0x80)			// Way to swap mem banks

// for 19200 baudrate for crystal clock	14.7456	MHz
#define	DLL_VAL		(0x30)
#define	DLM_VAL		(0x00)

// Macros to be	used for setting baudrate
#define	BAUD_RATE_9600		(0x0060)
#define	BAUD_RATE_19200		(0x0030)
#define	BAUD_RATE_56000		(0x0010)
#define	BAUD_RATE_115200	(0x0008)

/* UART port numbers */
#define UART_PORT_0         (CSL_UART_0)
#define UART_PORT_1         (CSL_UART_1)
#define UART_PORT_2         (CSL_UART_2)

// Return values
#define	UART_RET_OK				(0)
#define	UART_RET_INVALID_PARAM  (1)
#define	UART_RET_GEN_ERROR		(99)

/* UART function return status type */
typedef uint16_t  UART_RET;

/************************
 * Function declarations
************************/
/**
 *  \brief	This function initializes the UART.
 *
 *  \param  uartPortNumber [IN] - UART port number
 *
 *  \return UART_RET_OK on success or error code
 *
 */
UART_RET UartInit(uint8_t uartPortNumber);

/**
 *  \brief This function sets the UART baudrate.
 *
 *  \param  uartPortNumber [IN] - UART port number
 *  \param  uiBaudRate     [IN] - Baudrate to set
 *
 *  \return UART_RET_OK on success or error code
 *
 */
UART_RET UartSetBaudRate(uint8_t uartPortNumber, uint16_t uiBaudRate);

/**
 *  \brief	This function reads the UART baudrate.
 *
 *  \param  uartPortNumber [IN] - UART port number
 *
 *  \return uint16_t - 16 bit Baudrate read from UART
 *          Returns 0 for invalid port number
 *
 */
uint16_t UartReadBaudRate(uint8_t uartPortNumber);

/**
 *  \brief This function reads a byte of data from UART device
 *
 *  \param  uartPortNumber [IN] - UART port number
 *
 *  \return uint8_t - 8-bit value read from the RBR register
 *          Returns 0 for invalid port number
 */
uint8_t UartReadData(uint8_t uartPortNumber);

/**
 *  \brief	This function writes a byte of data to UART device
 *
 *  \param  uartPortNumber [IN] - UART port number
 *  \param  uchByte	       [IN] - 8-bit data to write to THR
 *
 *  \return UART_RET_OK on success or error code
 */
UART_RET UartWriteData(uint8_t uartPortNumber, uint8_t uchByte);

/**
 *  \brief	This function gets the status of DR bit
 *
 *  \param  uartPortNumber [IN] - UART port number
 *
 *  \return Status of DR bit
 *
 */
Bool UartIsDataReady(uint8_t uartPortNumber);

#endif // _EVM66X_UART_H_

