/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 *  \file   evmc66x_mmchs_card.h
 *
 *  \brief  This file contains MMCHS device specific structure, typedefs,
 *          function prototypes.
 *
 ******************************************************************************/

#ifndef _EVMC66X_MMCHS_CARD_H_
#define _EVMC66X_MMCHS_CARD_H_

/** MMC Block len for SD Card */
#define MMCHS_BLK_LEN        (0x200)
/** Sector size for Read Write operation */
#define SECTOR_SIZE          (512)
/** Data Response width */
#define DATA_RESPONSE_WIDTH     (128)

/** Type of responses expected from the card after
     while sending the commands */
#define RSP_TYPE_NONE   (0 << 16)
#define RSP_TYPE_R1     (1 << 16)
#define RSP_TYPE_R2     (2 << 16)
#define RSP_TYPE_R3     (3 << 16)

/** Type of command that is sent from the host to the card */
#define CMD_DATA_PRESENT   (1 << 21)
#define CMD_TYPE_ABORT_IO   (3 << 22)
#define CMD_TYPE_FUNC_SEL   (2 << 22)
#define CMD_TYPE_BUS_SUSP   (1 << 22)

/** Data direction in the command */
#define DATA_DIR_RD         (1 << 4)
#define DATA_DIR_WR         (0 << 4)

/** Block Count Enable */
#define CMD_BCE              (1 << 1)
/** Multiblock count */
#define CMD_MSBS             (1 << 5)

/**
 * \brief All supported commands
 */
#define SDMMC_CMD0      (0 << 24  | RSP_TYPE_NONE )
#define SDMMC_CMD1      (1 << 24  | RSP_TYPE_R2)
#define SDMMC_CMD2      (2 << 24  | RSP_TYPE_R1)
#define SDMMC_CMD3      (3 << 24  | RSP_TYPE_R2)
#define SDMMC_CMD4      (4 << 24  | RSP_TYPE_NONE)
#define SDMMC_CMD5      (5 << 24  | RSP_TYPE_R2)
#define SDMMC_CMD6      (6 << 24  | RSP_TYPE_R3)
#define SDMMC_ACMD6     (6  << 24 | RSP_TYPE_R1)
#define SDMMC_CMD6_BW   (6 << 24  | RSP_TYPE_R2 )
#define SDMMC_CMD6_SP   (6 << 24  | CMD_DATA_PRESENT | RSP_TYPE_R3 \
                         | CMD_MSBS | DATA_DIR_RD | CMD_BCE)
#define SDMMC_CMD7      (7 << 24  | RSP_TYPE_R3)
#define SDMMC_CMD8      (8 << 24  | RSP_TYPE_R2)
#define SDMMC_CMD8_MMC  (8 << 24  | CMD_DATA_PRESENT | RSP_TYPE_R3 \
                         | (1 << 5) | (1 << 4) | (1 << 1))
#define SDMMC_CMD9      (9 << 24  | RSP_TYPE_R1)
#define SDMMC_CMD12     (12 << 24 | RSP_TYPE_R3)
#define SDMMC_CMD13     (13 << 24 | RSP_TYPE_R2)
#define SDMMC_CMD16     (16 << 24 | RSP_TYPE_NONE)
#define SDMMC_CMD17     (17 << 24 | CMD_TYPE_ABORT_IO | CMD_DATA_PRESENT | \
                         RSP_TYPE_R2 | DATA_DIR_RD)
#define SDMMC_CMD18     (18 << 24 | CMD_TYPE_ABORT_IO | CMD_DATA_PRESENT | \
                         RSP_TYPE_R2 | CMD_MSBS | DATA_DIR_RD | CMD_BCE)
#define SDMMC_CMD24     (24 << 24 | CMD_TYPE_ABORT_IO | CMD_DATA_PRESENT | \
                         RSP_TYPE_R2 | DATA_DIR_WR)
#define SDMMC_CMD25     (25 << 24 | CMD_TYPE_ABORT_IO | CMD_DATA_PRESENT | \
                         RSP_TYPE_R2 | CMD_MSBS | CMD_BCE)
#define SDMMC_ACMD41    (41 << 24 | RSP_TYPE_R2)
#define SDMMC_ACMD51    (51 << 24 |CMD_DATA_PRESENT | RSP_TYPE_R2 \
                         | CMD_MSBS | DATA_DIR_RD | CMD_BCE)
#define SDMMC_CMD55     (55 << 24 | RSP_TYPE_R2)
#define SDMMC_CMD35     (35 << 24 | RSP_TYPE_R2)
#define SDMMC_CMD36     (36 << 24 | RSP_TYPE_R2)
#define SDMMC_CMD37     (37 << 24 | RSP_TYPE_R2)

#define SDMMC_CMD32     (32 << 24 | RSP_TYPE_R2)
#define SDMMC_CMD33     (33 << 24 | RSP_TYPE_R2)
#define SDMMC_CMD38     (38 << 24 | RSP_TYPE_R2)

/**
 * \brief HSMMC_STAT Defnitions
 */
#define STAT_CC         (1 << 0)
#define STAT_TC         (1 << 1)
#define STAT_BGE        (1 << 2)
#define STAT_BWR        (1 << 4)
#define STAT_BRR        (1 << 5)
#define STAT_CIRQ       (1 << 8)
#define STAT_OBI        (1 << 9)
#define STAT_ERRI       (1 << 15)
#define STAT_CTO        (1 << 16)
#define STAT_CCRC       (1 << 17)
#define STAT_CEB        (1 << 18)
#define STAT_CIE        (1 << 19)
#define STAT_DTO        (1 << 20)
#define STAT_DCRC       (1 << 21)
#define STAT_DEB        (1 << 22)
#define STAT_ACE        (1 << 24)
#define STAT_CERR       (1 << 28)
#define STAT_BADA       (1 << 29)

#define MMCHS_INTR_ENABLE (STAT_CC | STAT_TC | STAT_BWR | STAT_BRR | \
                           STAT_ERRI | STAT_CTO | STAT_DTO)

#define BIT(x) (1 << x)

/** SD voltage enumeration as per VHS field of the interface command */
#define SD_VOLT_2P7_3P6                 (0x000100u)

/** Check RCA/status */
#define SD_RCA_ADDR(rca)             ((rca & 0xFFFF0000) >> 16)
#define SD_RCA_STAT(rca)             (rca & 0x0xFFFF)
#define SD_CHECK_PATTERN             (0xAA)

/** CMD6 Swith mode arguments for High Speed */
#define SD_CHECK_MODE        (0x00FFFFFF)
#define SD_SWITCH_MODE        (0x80FFFFFF)
#define SD_CMD6_GRP1_SEL      (0xFFFFFFF0)
#define SD_CMD6_GRP1_HS       (0x1)


/** Helper macros */
/** Note card registers are big endian */
#define SD_CARD_SCR_VER(sdcard)   (((sdcard)->raw_scr[0] & 0xF0) >> 4)
#define SD_CARD_VERSION(sdcard)   ((sdcard)->raw_scr[0] & 0xF)
#define SD_CARD_BUSWIDTH(sdcard)  (((sdcard)->raw_scr[0] & 0xF00) >> 8)
#define SD_CARD_SEC(sdcard)  (((sdcard)->raw_scr[0] & 0x7000) >> 12)
#define SD_CARD_DATSTAT(sdcard)  (((sdcard)->raw_scr[0] & 0x8000) >> 15)

#define GET_SD_CARD_BUSWIDTH(sdcard)  ((((sdcard.busWidth) & 0x0F) == 0x01) ? \
                                      0x1 : ((((sdcard).busWidth & 0x04) == \
                                      0x04) ? 0x04 : 0xFF))
#define GET_SD_CARD_FRE(sdcard)       (((sdcard.tranSpeed) == 0x5A) ? 50 : \
                                      (((sdcard.tranSpeed) == 0x32) ? 25 : 0))


#define SD_CARD_CSD_VERSION(crd) (((crd)->raw_csd[0] & 0xC0000000) >> 30)

#define SD_CSD0_DEV_SIZE(csd3, csd2, csd1, csd0) (((csd2 & 0x000003FF) << 2) | \
                        ((csd1 & 0xC0000000) >> 30))
#define SD_CSD0_MULT(csd3, csd2, csd1, csd0) ((csd1 & 0x00038000) >> 15)
#define SD_CSD0_RDBLKLEN(csd3, csd2, csd1, csd0) ((csd2 & 0x000F0000) >> 16)
#define SD_CSD0_TRANSPEED(csd3, csd2, csd1, csd0) ((csd3 & 0x000000FF) >> 0)

#define SD_CARD0_DEV_SIZE(crd) SD_CSD0_DEV_SIZE((crd)->raw_csd[0], \
                                                (crd)->raw_csd[1], \
                                                (crd)->raw_csd[2], \
                                                (crd)->raw_csd[3])
#define SD_CARD0_MULT(crd) SD_CSD0_MULT((crd)->raw_csd[0], (crd)->raw_csd[1],\
                                        (crd)->raw_csd[2], (crd)->raw_csd[3])
#define SD_CARD0_RDBLKLEN(crd) SD_CSD0_RDBLKLEN((crd)->raw_csd[0],\
                                                (crd)->raw_csd[1],\
                                                (crd)->raw_csd[2],\
                                                (crd)->raw_csd[3])
#define SD_CARD0_TRANSPEED(crd) SD_CSD0_TRANSPEED((crd)->raw_csd[0],\
                                                  (crd)->raw_csd[1],\
                                                  (crd)->raw_csd[2],\
                                                  (crd)->raw_csd[3])
#define SD_CARD0_NUMBLK(crd) ((SD_CARD0_DEV_SIZE((crd)) + 1) * \
                              (1 << (SD_CARD0_MULT((crd)) + 2)))
#define SD_CARD0_SIZE(crd) ((SD_CARD0_NUMBLK((crd))) * \
                            (1 << (SD_CARD0_RDBLKLEN(crd))))

#define SD_CSD1_DEV_SIZE(csd3, csd2, csd1, csd0) (((csd2 & 0x0000003F) << 16) |\
                                                  ((csd1 & 0xFFFF0000) >> 16))
#define SD_CSD1_RDBLKLEN(csd3, csd2, csd1, csd0) ((csd2 & 0x000F0000) >> 16)
#define SD_CSD1_TRANSPEED(csd3, csd2, csd1, csd0) ((csd3 & 0x000000FF) >> 0)

#define SD_CARD1_DEV_SIZE(crd) SD_CSD1_DEV_SIZE((crd)->raw_csd[0], \
                                                (crd)->raw_csd[1], \
                                                (crd)->raw_csd[2], \
                                                (crd)->raw_csd[3])
#define SD_CARD1_RDBLKLEN(crd) SD_CSD1_RDBLKLEN((crd)->raw_csd[0], \
                                                (crd)->raw_csd[1], \
                                                (crd)->raw_csd[2], \
                                                (crd)->raw_csd[3])
#define SD_CARD1_TRANSPEED(crd) SD_CSD1_TRANSPEED((crd)->raw_csd[0], \
                                                  (crd)->raw_csd[1], \
                                                  (crd)->raw_csd[2], \
                                                  (crd)->raw_csd[3])
#define SD_CARD1_SIZE(crd) ((SD_CARD1_DEV_SIZE((crd)) + 1) * (512 * 1024))


/** SD OCR register definitions */
/** Macros to check the OCR status */
/* Card busy status */
#define SD_OCR_CARD_BUSY  0x80000000
/** High capacity */
#define SD_OCR_HIGH_CAPACITY  BIT(30)
/** Voltage */
#define SD_OCR_VDD_2P7_2P8    BIT(15)
#define SD_OCR_VDD_2P8_2P9    BIT(16)
#define SD_OCR_VDD_2P9_3P0    BIT(17)
#define SD_OCR_VDD_3P0_3P1    BIT(18)
#define SD_OCR_VDD_3P1_3P2    BIT(19)
#define SD_OCR_VDD_3P2_3P3    BIT(20)
#define SD_OCR_VDD_3P3_3P4    BIT(21)
#define SD_OCR_VDD_3P4_3P5    BIT(22)
#define SD_OCR_VDD_3P5_3P6    BIT(23)
#define SD_OCR_DUAL_VOLTAGE_RANGE  BIT(7)
#define SD_OCR_VOLTAGE_MASK     0x00FFFF80
#define SD_OCR_ACCESS_MODE     0x60000000

/** This is for convenience only. Sets all the VDD fields */
#define SD_OCR_VDD_WILDCARD   (0x1FF << 15)

/** SD CSD register definitions */
#define SD_TRANSPEED_25MBPS   (0x32u)
#define SD_TRANSPEED_50MBPS   (0x5Au)

/**
 * Macros that can be used for selecting the bus/data width
 */
#define SD_BUS_WIDTH_8BIT_VAL    (0x03B70200)
#define SD_BUS_WIDTH_4BIT_VAL    (0x03B70100)
#define SD_BUS_WIDTH_1BIT_VAL    (0x03B70000)
#define MMC_SWITCH_HS_TRANSFER_SPEED    (0x03B90100)
#define MMC_SWITCH_TRANSFER_SPEED       (0x03B90000)
#define MMC_HS_RESET_PIN_ARG     (0x03A20100) //taken from the Spec
#define SD_BUS_WIDTH_CMD_DATA_LEN (8)



/**
 * \brief Function declarations
 */
/** ===========================================================================
   *   @n@b mmchsInit
   *
   *   @b Description
   *   @n TThis function initializes MMCHS module.
   *
   *   @b Arguments
   *   @verbatim
              mmcInstance - mmcInstance
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        mmchsInfo - handle to the mmchsInfo
   *   @li        NULL - if error
   *
   *   <b> Pre Condition </b>
   *   @n  None
   *   <b> Post Condition </b>
   *   @n  mmc controller and sd card/emmc/sdio is detected and initialized
   *
   *   @b Modifies
   *   @n  None
   *
   *  ===========================================================================
   */
mmchsInfo*  mmchsInit(uint8_t mmcInstance);

/** ===========================================================================
   *   @n@b mmchsBlockWrite
   *
   *   @b Description
   *   @n This function write to a given block with the data to the mmc device
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard - handle to the mmchsCard
              blkNum - Block Address
              buf - data to be written into
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li       Returns: The status of read operation
   *   @li       MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li       MMCHS_RET_OK - on Success
   *
   *   <b> Pre Condition </b>
   *   @n  mmc should be initialized
   *   <b> Post Condition </b>
   *   @n  None
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsBlockWrite (mmchsCardInfo *mmchsCard, uint32_t blkNum,\
                        uint32_t nBytes, uint8_t *buf);

/** ============================================================================
   *   @n@b mmchsBlockRead
   *
   *   @b Description
   *   @n This function Read a given block from the mmc device
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard - handle to the mmchsCard
              blkNum - Block Address
              buf - buffer to be read into
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li       Returns: The status of read operation
   *   @li       MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li       MMCHS_RET_OK - on Success
   *
   *   <b> Pre Condition </b>
   *   @n  mmc should be initialized
   *   <b> Post Condition </b>
   *   @n  None
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsBlockRead (mmchsCardInfo *mmchsCard, uint32_t blkNum,\
                 uint32_t nBlks, uint8_t *buf);
/** ===========================================================================
   *   @n@b mmchsBlockErase
   *
   *   @b Description
   *   @n This function erases a given block
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard - handle to the mmchsCard
              eraseStart - Erase Start Block Address
              eraseEnd - Erase End Block Address
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li       Returns: The status of read operation
   *   @li       MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li       MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *  ==========================================================================
   */
MMC_RET mmchsBlockErase (mmchsCardInfo *mmchsCard, uint32_t eraseStart,\
                         uint32_t eraseEnd);

/** ============================================================================
   *   @n@b mmchsPrintCID
   *
   *   @b Description
   *   @n This function prints the CID information read from the Card/MMC
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
void mmchsPrintCID(mmchsCardInfo *mmchsCard);

/** ============================================================================
   *   @n@b mmchsPrintCSD
   *
   *   @b Description
   *   @n This function prints the Card Specific Data (CSD) information read
   *      from the Card/MMC
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
void mmchsPrintCSD(mmchsCardInfo *mmchsCard);

/** ============================================================================
   *   @n@b mmchsPrintECSD
   *
   *   @b Description
   *   @n This function prints the Extended Card Specific Data (CSD)
   *      information read from the Card/MMC
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
void mmchsPrintECSD(mmchsCardInfo *mmchsCard);

/** ============================================================================
   *   @n@b mmchsPrintSCR
   *
   *   @b Description
   *   @n This function prints the SD Card Configuration Register (SCR)
   *      information read from the Card/MMC
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
void mmchsPrintSCR(mmchsCardInfo *mmchsCard);

/** ===========================================================================
   *   @n@b mmchsPrintCardInfo
   *
   *   @b Description
   *   @n This function prints the Extended Card Specific Data (CSD)
   *      information read from the Card/MMC.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCard   - Handle to the card
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        None
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
void mmchsPrintCardInfo(mmchsCardInfo *mmchsCard);

/** ===========================================================================
   *   @n@b mmchsSetTranSpeed
   *
   *   @b Description
   *   @n This function sets the data transfer speed on the host controller
   *      based on the response from the card.
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg   - Handle to the mmcReg
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_SEND_FAILED   Response to command is error
       @li        MMCHS_RET_SEND_NORSP    If no response from the mmc device.
   *   @li        MMCHS_RET_OK            If successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchsSetTranSpeed(mmchsCardInfo *mmchsCard, uint32_t speed);

/** ===========================================================================
   *   @n@b mmchsSetBusWidth
   *
   *   @b Description
   *   @n This function configures the bus width on the mmc device. It sends
   *   CMD6 to get the bus width and mmchsCard capabilities on the mmc device
   *   and then set the allowable bus width on the host controller as well
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg   - Handle to the mmcReg
              busWidth   - Bus width to be set to
   *  buswidth can take the values.\n
   *     SD_BUS_WIDTH_4BIT.\n
   *     SD_BUS_WIDTH_1BIT.\n
   *     SD_BUS_WIDTH_8BIT

       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_SEND_FAILED   Response to command is error
       @li        MMCHS_RET_SEND_NORSP    If no response from the mmc device.
   *   @li        MMCHS_RET_OK            If successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchsSetBusWidth(mmchsCardInfo *mmchsCard, uint8_t busWidth);

#endif // _EVMC66X_MMCHS_CARD_H_
