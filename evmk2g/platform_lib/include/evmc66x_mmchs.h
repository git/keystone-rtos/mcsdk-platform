/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * \file   evmc66x_mmchs.h
 *
 * \brief  This file contains MMCHS specific structure, typedefs, function
 *         prototypes.
 *
 ******************************************************************************/

#ifndef _EVMC66X_MMCHS_H_
#define _EVMC66X_MMCHS_H_

/** To enable for debug printing */
#define DEBUG 1

#define PLATFORM_DEBUG_MMC 1

#if (PLATFORM_DEBUG_MMC)
#define IFPRINT_MMC(x)   (x)
#else
#define IFPRINT_MMC(x)
#endif
/**
 * \brief  MMC Error Status
 */
#define MMCHS_RET_OK                 (0)    /**< Return OK on Success */
#define MMCHS_RET_INVALID_PARAM      (1)    /**< Error - Invalid parameter  */
#define MMCHS_RET_IDLE_TIMEOUT       (2)    /**< Error - Idle timeout  */
#define MMCHS_RET_RESET_FAILED       (3)    /**< Error - Reset failed  */
#define MMCHS_RET_SEND_FAILED        (4)    /**< Error - Cmd send fails */
#define MMCHS_RET_SEND_NORSP         (6)    /**< If no response for cmd sent*/
#define MMCHS_RET_DATA_RD_FAILED     (7)    /**< Error - If data read fails  */
#define MMCHS_RET_DATA_WR_FAILED     (8)    /**< Error - If data write fails  */
#define MMCHS_RET_CARD_DETECT_ERROR  (11)   /**< Error- If card detection fails */
#define MMCHS_RET_SDCARD_INIT_FAILED (12)   /**< Error - If SD Card Init fails */
#define MMCHS_RET_MMC_INIT_FAILED    (13)   /**< Error - If MMC Init fails */
#define MMCHS_RET_SDIO_INIT_FAILED   (10)
#define MMCHS_RET_CMD_TIMEOUT        (14)    /**< Error - If CMD timeout fails */
#define MMCHS_RET_NO_MEM             (15)    /**< Error - If insufficient memory for malloc */

typedef uint16_t MMC_RET;

/** Macro to define instance number for each of the MMCSD */
#define CSL_MMCSD_0                  (0U)
#define CSL_MMCSD_1                  (1U)

/** Macro to define instance number for the devices in K2G EVM */
#define MMCHS_SD_CARD_INST          CSL_MMCSD_0
#define MMCHS_MMC_INST              CSL_MMCSD_1
#define MMCHS_SDIO_INST             CSL_MMCSD_1

/** Macro to define the base address of MMCSD for both the instances  */
#define MMCSD_0_S_REGS               (CSL_MMCSD_0_S_REGS)
#define MMCSD_0_S_REGS_SIZE          (CSL_MMCSD_0_S_REGS_SIZE)
#define MMCSD_1_S_REGS               (CSL_MMCSD_1_S_REGS)
#define MMCSD_1_S_REGS_SIZE          (CSL_MMCSD_1_S_REGS_SIZE)

/** MMC Types available */
#define MMCHS_SDIO                   (1)
#define MMCHS_SD_CARD                (2)
#define MMCHS_MMC                    (3)

/** Macro to define ENABLE/DISABLE  */
#define ENABLE                  (1)
#define DISABLE                 (0)

#define OSCILLATE               (1)   /**< To enable the clock*/
#define RESET                   (1)   /**< To reset the command and data lines */
#define RESETVAL                (0) /**< Reset the clock frequency before setting*/
#define V3_0                    (6)   /**< To set the bus voltage to 3.0 Volts */

/** Macro to define the bit modes 1, 4, 8*/
#define BITMODE1_4              (0)
#define BITMODE_8               (1)
#define BITMODE_1               (0)
#define BITMODE_4               (1)

/** Macros to indicate the number of bits form the bus width*/
#define SD_BUS_WIDTH_1BIT       (1)
#define SD_BUS_WIDTH_4BIT       (4)
#define SD_BUS_WIDTH_8BIT       (8)

/** Macro to set the max DTO timeout value */
#define MMC_DTO_MAX             (0x0E)

/** Macro to set the values in CSL format */
#define CSL_MMCHS_MMCHS_SYSCTL_DTO_MMC_MAX     (0x0000000eu)
#define MMCHS_MMCHS_SYSCTL_SRD_SRC_RESET       (0x6000000eu)

/** Clock frequency to be configured based on the stages of initialization*/
#define MMCHS_CLK_INITSEQ       (0)
#define MMCHS_CLK_400KHZ        (1)
#define MMCHS_CLK_12MHZ         (2)
#define MMCHS_CLK_24MHZ         (3)
#define MMCHS_CLK_26MHZ         (4)
#define MMCHS_CLK_48MHZ         (5)
#define MMCHS_CLK_50MHZ         (6)
#define MMCHS_CLK_52MHZ         (7)
#define MMCHS_SDMMC_REF_CLK     (96)

/** Macros to set the block length and number of blocks in the CMD register */
#define MMCHS_BLK_BLEN   (0x00000FFFu)
#define MMCHS_BLK_NBLK   (0xFFFF0000u)
#define MMCHS_BLK_NBLK_SHIFT   (0x00000010u)

/** Macros to set the clock frequency */
#define HSMMCSD_INIT_FREQ                     400000   /* 400kHz */
#define HSMMCSD_TRAN_SPEED_25M_CLK          25000000
#define HSMMCSD_TRAN_SPEED_26M_CLK          26000000
#define HSMMCSD_TRAN_SPEED_52M_CLK          52000000
#define HSMMCSD_TRAN_SPEED_24M_CLK          24000000
#define HSMMCSD_TRAN_SPEED_48M_CLK          48000000
#define HSMMCSD_TRAN_SPEED_50M_CLK          50000000
#define HSMMCSD_TRAN_SPEED_MAX_CLK          50000000

/** Macros to set the bus power on and off */
#define     HS_MMCSD_BUS_POWER_ON   (1)
#define     HS_MMCSD_BUS_POWER_OFF  (0)

/**
 * \brief mmc_cid : Card Identification Data
 *
 * Description: This structure defines the Card Identification Data
 * Register. CID register contains the card identification information used
 * during the card identification phase (MultiMediaCard protocol).
 */
typedef struct _mmc_cid {
    uint8_t     manfid:8;       /**< Manufacturer ID */
    uint8_t     cardtype:2;     /**< Card / BGA */
    uint8_t     oemid:8;        /**< OEMID */
    uint8_t     prod_name[6];   /**< Product name  */
    uint8_t     hwrev:4;        /**< Product revision */
    uint8_t     fwrev:4;        /**< firmware revision */
    uint32_t    serial:32;      /**< Product serial number */
    uint8_t     month:4;        /**< Manufacturing month*/
    uint8_t     year:4;         /**< Manufacturing year */

}mmc_cid;

/**
 * \brief mmc_scr : SD Card Configuration Register
 *
 * Description: This structure defines the Card Configuration Register.
 * SCR provides information on the SD Memory Card's special features that were
 * configured into the given card. The size of SCR register is 64 bits. This
 * register shall be set in the factory by the SD Memory Card manufacturer.
 * This register is supported only by SD Card Specification
 */
 typedef struct _mmc_scr {
    uint8_t     scr_struct:4;   /**< SCR Structure version number */
    uint8_t     sda_vsn:4;      /**< SD Physical layer spec number */
    uint8_t     data_stat:1;    /**< Data status after erase */
    uint8_t     sd_sec:3;       /**< Security specification version */
    uint8_t     bus_widths:4;   /**< Data bus width supported by the card */
    uint8_t     erased_byte;
}mmc_scr;

/**
 * \brief mmc_csd : Card Status Register
 *
 * Description: This structure defines the Card Status Register.
 * The Card Status Data register provides information regarding access to
 * the card contents. The CSD defines the data format, error correction type,
 * maximum data access time, whether the DSR register can be used etc.,
 */
typedef struct _mmc_csd {
    uint8_t     structure:2; /**< CSD Version 2.0 => High capacity */
    uint8_t     mmca_vsn:4;  /**< only on MMC - defines MMC system spec version*/
    uint8_t     reserved:2;
    uint8_t     tacc_clk:8;  /**< Asynchronous part of the data access time */
    uint8_t     tacc_ns_val:8; /**< worst case for clk-dependnt factor of data access time.*/
    uint8_t     trans_speed:8; /**< max data transfer rate per one data line */
    uint16_t    cmdclass:12;  /**< The SD/MMC Card command set */
    uint8_t     read_blkbits:4; /**< Reads block length */
    uint8_t     read_partial:1; /**< partial block sizes in block read commands*/
    uint8_t     write_partial:1; /**< partial block sizes in block write commands */
    uint8_t     write_misalign:1; /**< If writes can spread over 2 physical blks*/
    uint8_t     read_misalign:1; /**< If read can spread over 2 physical blocks*/
    uint8_t     dsr_imp:1;      /**< if configurable driver stage is integrated */
    uint16_t    c_size:12;      /**< Card capacity */
    uint16_t    c_size_mult:3;  /**< Multiplier factor for Card capacity over 2GB*/
    uint8_t     erase_size:5;       /**< erase size */
    uint8_t     erase_size_mult:5;  /**< erase size multiplier */
    uint8_t     r2w_factor:3;       /**< Multiple of read access time */
    uint8_t     write_blkbits:4;    /**< Write block length */

    /** Below definitions are not part of the CSD spec*/
    uint32_t    capacity;       /**< Capacity calculated based on > or < 2 GB */
    uint32_t    erase_blocks;   /**< Erase blocks */
    uint32_t    tacc_clks;      /**< Calcuated data access time */
    uint32_t    tacc_ns;        /**< Calcuated data access time in ns */
    uint32_t    max_dtr;        /**< Maximum bus clock freq /Tranfer speed */
}mmc_csd;

/**
 * \brief mmc_ecsd : Extended Card Status Register
 *
 * Description: This structure defines the Extended Card Status Register.
 * The Extended CSD register defines the card properties and selected modes.
 * It is 512 bytes long. The most significant 320 bytes are the Properties
 * segment, which defines the card capabilities and cannot be modified by the
 * host. The lower 192 bytes are the Modes segment, which defines the
 * configuration the card is working in. These modes can be changed by the
 * host by means of the SWITCH command.
 *
 */
 typedef struct _mmc_ecsd {
    uint8_t     rsvd_19[175];
    uint8_t     erase_grp_def;
    uint8_t     rsvd_18;
    uint8_t     boot_bus_width;
    uint8_t     rsvd_17;
    uint8_t     boot_cfg;
    uint8_t     rsvd_16;
    uint8_t     erase_mem_cont;
    uint8_t     rsvd_15;
    uint8_t     bus_width;
    uint8_t     rsvd_14;
    uint8_t     hs_timing;
    uint8_t     rsvd_13;
    uint8_t     power_class;
    uint8_t     rsvd_12;
    uint8_t     cmd_set_rev;
    uint8_t     rsvd_11;
    uint8_t     cmd_set;
    uint8_t     ext_csd_rev;
    uint8_t     rsvd_9;
    uint8_t     csd_struct;
    uint8_t     rsvd_8;
    uint8_t     card_type;
    uint8_t     rsvd_7[3];
    uint8_t     pwr_cl_52_195;
    uint8_t     pwr_cl_26_195;
    uint8_t     pwr_cl_52_360;
    uint8_t     pwr_cl_26_360;
    uint8_t     rsvd_6;
    uint8_t     min_rd_perf_26;
    uint8_t     min_wr_perf_26;
    uint8_t     min_rd_perf_26_52;
    uint8_t     min_wr_perf_26_52;
    uint8_t     min_rd_perf_52;
    uint8_t     min_wr_perf_52;
    uint8_t     rsvd_5_1;
    uint32_t    sector_count;
    uint8_t     rsvd_5;
    uint8_t     sleep_awake_timeout;
    uint8_t     rsvd_4;
    uint8_t     sleep_current_vccq;
    uint8_t     sleep_current_vcc;
    uint8_t     hc_wp_grp_size;
    uint8_t     rel_wr_sec_cnt;
    uint8_t     erase_timeout_multi;
    uint8_t     hc_erase_grp_size;
    uint8_t     acc_size;
    uint8_t     boot_size_multi;
    uint8_t     rsvd_3;
    uint8_t     boot_info;
    uint8_t     rsvd_2[275];
    uint8_t     s_cmd_set;
    uint8_t     rsvd_1[7];
}mmc_ecsd;

/**
 * \brief mmchsCardInfo : Card Information
 *
 * Description: This structure has the card specific information and the
 * status of the card. This also holds the information during the initialization
 * procedure. The data can be extracted by other APIs to fetch the card
 * specific information.
 *
 */
typedef struct _mmchsCardInfo {
    CSL_MmchsRegs   *mmcHsReg;
    uint8_t         cardType;
    uint32_t        rca;
    uint32_t        raw_scr[2];
    uint32_t        raw_csd[4];
    uint32_t        raw_cid[4];
    uint8_t         raw_ecsd[512];
    uint32_t        ocr;
    uint8_t         sd_ver;
    uint8_t         busWidth;
    uint32_t        tranSpeed;
    uint32_t        maxTranSpeed;
    uint8_t         highCap;
    uint8_t         highSpeed;
    uint32_t        blkLen;
    uint32_t        nBlks;
    uint32_t        size;
    mmc_cid         cid;
    mmc_csd         csd;
    mmc_scr         scr;
    mmc_ecsd        ecsd;
    uint8_t         numIOFuncs;
    uint8_t         memPresent;
    struct _mmchsControllerInfo *ctrl;
}mmchsCardInfo;

/**
 * \brief mmchsControllerInfo : Host Controller Information
 */
typedef struct _mmchsControllerInfo {
    CSL_MmchsRegs   *mmcHsReg;
    uint32_t        opClk;
    uint32_t        busWidth;
    uint32_t        ocr;
    uint32_t        highSpeed;
    mmchsCardInfo   *card;
}mmchsControllerInfo;

/** Structure for MMCHS Information */

/**
 * \brief mmchsInfo : MMCHS Info
 *
 * Description: This structure is the main descriptor for the MMCHS device.
 * This holds the information about the Controller and the Card.
 */
typedef struct _mmchsInfo {
    mmchsControllerInfo     *mmchsCtrl;
    mmchsCardInfo           *card;
    CSL_MmchsRegs           *mmcHsReg;
}mmchsInfo;


/************************
 * Function declarations
 ************************/
/** ===========================================================================
   *   @n@b mmchsDelay
   *
   *   @b Description
   *   @n This function creates a delay of given value in usecs .
   *
   *   @b Arguments
   *   @verbatim
              usecs - usecs to give the delay
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @n  None
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
void mmchsDelay (uint32_t count);

/** ===========================================================================
  *   @n@b mmchsGetRegBaseaddr
  *
  *   @b Description
  *   @n This function is used to get the base address of given MMC Instance.
  *
  *   @b Arguments
  *   @verbatim
             mmcInstance       Instance of MMC: MMC0 - SD Card,
             MMC1 - eMMC/SDIO Card

     mmcInstance can take the values \n
        CSL_MMCSD_0 - SD Card \n
        CSL_MMCSD_1 - eMMC/WLAN \n
      @endverbatim
  *
  *   <b> Return Value </b>  CSL_MmchsRegs*
  *   @li           The base address of the mmcInstance on success
  *   @li            NULL if type is not found
  *
  *   @b Modifies
  *   @n  None
  *
  *  ===========================================================================
  */
CSL_MmchsRegs* mmchsGetRegBaseaddr(uint8_t mmcInstance);

/** ===========================================================================
   *   @n@b mmchsReset
   *
   *   @b Description
   *   @n This function Resets the mmc module and the controller.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCtrlInfo - handle to the Controller
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li        MMCHS_RET_IDLE_TIMEOUT - If reset fails and times out.
   *   @li        MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsReset(mmchsControllerInfo *mmchsCtrlInfo);

/** ===========================================================================
   *   @n@b mmchsEnableClock
   *
   *   @b Description
   *   @n This function is used to enable the clock for the given frequency
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - handle to the mmcReg
              clock - Input frequency


      clock can take the values \n
            MMCHS_CLK_INITSEQ
            MMCHS_CLK_400KHZ
            MMCHS_CLK_12MHZ
            MMCHS_CLK_24MHZ
            MMCHS_CLK_26MHZ
            MMCHS_CLK_48MHZ
            MMCHS_CLK_52MHZ
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li        MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsEnableClock(CSL_MmchsRegs *mmcHsReg, uint32_t clock);

/** ===========================================================================
   *   @n@b mmchsBusWidthConfig
   *
   *   @b Description
   *   @n This function configures the mmc bus width on the host controller
   *
   *   @b Arguments
   *   @verbatim
   *      mmcHsReg - handle to the mmcReg
          buswidth   SD/MMC bus width.\n
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li         MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsBusWidthConfig(CSL_MmchsRegs *mmcHsReg, uint8_t busWidth);

/** ===========================================================================
   *   @n@b mmchsSetBlockCount
   *
   *   @b Description
   *   @n This function sets the block count for multi block data transfer
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - handle to the mmcReg
              nBlks    - number of Blocks
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */

MMC_RET mmchsSetBlockCount(CSL_MmchsRegs *mmcHsReg, uint32_t nBlks);

/** ===========================================================================
   *   @n@b mmchsSendCmd
   *
   *   @b Description
   *   @n This function sends the command to the device over the data bus.
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - handle to the mmcReg
              cmd      - command to send to the controller
              arg      - argument
              blkSize  - block size
              nBlks    - number of Blocks
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li        MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsSendCmd(CSL_MmchsRegs *mmcHsReg, uint32_t cmd, uint32_t arg,
                     uint32_t blkSize);

/** ===========================================================================
   *   @n@b mmchsSendAppCmd
   *
   *   @b Description
   *   @n This function sends the application command to MMCSD.
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - Handle to the mmcReg
              cmd - Command to be sent
              arg - argument for the command
              blkLen - no of bytes in a block
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
   *   @li       MMCHS_RET_OK if successful
   *            MMCHS_RET_CMD_TIMEOUT  if cmd timeout
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchsSendAppCmd(CSL_MmchsRegs *mmcHsReg, uint32_t cmd,
                                    uint32_t arg, uint32_t blkLen);

/** ===========================================================================
   *   @n@b mmchsClearIntrStatus
   *
   *   @b Description
   *   @n This function clears the interrupt status
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - Handle to the mmcReg
   *          flag     - Specific status to be cleared;
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li        MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsClearIntrStatus(CSL_MmchsRegs *mmcHsReg, uint32_t flag);

/** ============================================================================
   *   @n@b mmchsGetStatus
   *
   *   @b Description
   *   @nThis function gets status of the controller after sending the command.
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - Handle to the mmcReg
   *          flag     - Specific status to be cleared;
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li        MMCHS_RET_SEND_FAILED - If error flag is set for the command
   *   @li        MMCHS_RET_SEND_NORSP -  If no response for the send command
   *   @li        MMCHS_RET_OK - If send command is responded with
                                 command/transfer complete
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsGetStatus(CSL_MmchsRegs *mmcHsReg, uint32_t *xferStatus);

/** ============================================================================
   *   @n@b mmchsGetResponse
   *
   *   @b Description
   *   @n This function get the command response from the controller
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - Handle to the mmcReg
   *          rsp        pointer to buffer to be filled with the response
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        status flags
   *
   *   @b Modifies
   *   @n  None
   *
   * \note: that this function shall return the values from all response
   * registers. Hence, rsp, must be a pointer to memory which can hold max
   * response length. It is the responsibility of the caller to use only the
   * required/relevant parts of the response
   *
   *  ==========================================================================
   */
MMC_RET mmchsGetResponse(CSL_MmchsRegs *mmcHsReg, uint32_t *rsp);

/** ============================================================================
   *   @n@b mmchsReadData
   *
   *   @b Description
   *   @n This function reads the data from the mmc device (SD card/eMMC/SDIO)
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg   - Handle to the mmcReg
              dataBuffer - pointer to the data buffer
              blkSize    - block size
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_DATA_RD_FAILED    if read fails
   *   @li        MMCHS_RET_OK if successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchsReadData(CSL_MmchsRegs *mmcHsReg, uint8_t *dataBuffer,
                      uint32_t blkSize);

/** ============================================================================
   *   @n@b mmchsWriteData
   *
   *   @b Description
   *   @n This function writes the data to the mmc device (SD card/eMMC/SDIO)
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg   - Handle to the mmcReg
              dataBuffer - pointer to the data buffer
              blkSize    - block size
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM
       @li        MMCHS_RET_DATA_WR_FAILED    if write fails
   *   @li        MMCHS_RET_OK if successful
   *
   *   @b Modifies
   *   @n  None
   *  =========================================================================
   */
MMC_RET mmchsWriteData(CSL_MmchsRegs *mmcHsReg, uint8_t *dataBuffer,
                        uint32_t blkSize);

/** ============================================================================
   *   @n@b mmchsIsCardInserted
   *
   *   @b Description
   *   @n This function checks if the card is inserted in the sdcard slot
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - Handle to the mmcReg
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        0  if the card is not inserted and detected
   *   @li        1  if the card is inserted and detected
   *
   *   @b Modifies
   *   @n  None
   * \note: that this functional may not be available for all instances of the
   * controller. This function, is only useful if the controller has a dedicated
   * card detect pin. If not, the card detection mechanism is application
   * implementation specific
   *  =========================================================================
   */
Bool mmchsIsCardInserted(CSL_MmchsRegs *mmcHsReg);

/** ===========================================================================
   *   @n@b mmchsEnableIntr
   *
   *   @b Description
   *   @nThis function enables the controller events to generate an interrupt
        request
   *
   *   @b Arguments
   *   @verbatim
              mmcHsReg - handle to the mmcReg
              flags - flag to enable the Interrupt
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li         MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsEnableIntr(CSL_MmchsRegs *mmcHsReg, uint32_t flags);

/** ===========================================================================
   *   @n@b mmchsControllerInit
   *
   *   @b Description
   *   @n This function configures the mmc module and the controller.
   *
   *   @b Arguments
   *   @verbatim
              mmchsCtrlInfo - handle to the Controller
       @endverbatim
   *
   *   <b> Return Value </b>
   *   @li        MMCHS_RET_INVALID_PARAM - If input parameter is invalid
   *   @li         MMCHS_RET_OK - on Success
   *
   *   @b Modifies
   *   @n  None
   *
   *  ==========================================================================
   */
MMC_RET mmchsControllerInit(mmchsControllerInfo *mmchsCtrlInfo);

#endif // _EVMC66X_MMCHS_H_
