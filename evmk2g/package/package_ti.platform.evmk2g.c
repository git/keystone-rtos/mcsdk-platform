/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-z63
 */

#include <xdc/std.h>

__FAR__ char ti_platform_evmk2g__dummy__;

#define __xdc_PKGVERS 1, 0, 0, 0
#define __xdc_PKGNAME ti.platform.evmk2g
#define __xdc_PKGPREFIX ti_platform_evmk2g_

#ifdef __xdc_bld_pkg_c__
#define __stringify(a) #a
#define __local_include(a) __stringify(a)
#include __local_include(__xdc_bld_pkg_c__)
#endif

