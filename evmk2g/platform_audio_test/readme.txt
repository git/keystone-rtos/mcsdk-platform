
-------------------
Audio Analog Test
-------------------

Description
-------------
Audio analog test verifies the functionality of ADC and DAC modules available on
audio daughter card. During this test, data received from ADC will be sent to DAC. 
There are two instances of DAC and ADC available on audio daughter card. 
One ADC supports upto 4 analog channels and one DAC supports 8 analog channels. 
Data received from one DAC shall be sent to both the ADCs making 8 channel Rx/Tx. 
Each DAC should be validated separately.




-------------------
Audio Digital Test
-------------------

Description
-------------
Audio digital test verifies the functionality of DIR and 'S/P DIF' interfaces available on
audio daughter card. During this test, data received from DIR will be sent to 'S/P DIF' interface.
McASP driver will be configured to work in DIT mode for transferring data to 'S/P DIF' interface.




Dependencies
-------------
McASP and eDMA drivers used for the audio test are initial versions delivered by TI. 
McASP and eDMA configurations and usage may change based on final driver version. 
eDMA LLD from TI is supposed to include support for K2G in future releases which requires 
changes to eDMA configurations used in the test.


Note: 
------
Validation of the audio tests with audio daughter card is in progress. This version of audio tests
will be modified during the validation and full functional version will be available in the subsequent
updates of MCSDK

Audio test project requires clean build every time. Remove '.exclude' file and 'sysbios' folder 
in 'src' folder of audio test in case of linking errors during build. This issue will be resolved in
the next version of MCSDK update 

