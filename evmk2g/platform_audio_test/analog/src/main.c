/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      main.c
 *
 * \brief     Audio DC analog test main file
 *
 *  Audio analog test verifies the functionality of ADC and DAC available on
 *  audio daughter card. During this test, data received from ADC will be
 *  sent to DAC. There are two instances of DAC and ADC available on
 *  audio daughter card. One ADC supports 4 analog channels and one DAC
 *  supports 8 analog channels. Data received from one DAC shall be sent
 *  to both the ADCs making 8 channel Rx/Tx. Each DAC should be validated
 *  separately.
 *
 */

#include "analog_test.h"

/* ADC default configuration parameters */
AdcConfig adcCfg =
{
	100,                 /* ADC gain */
	ADC_INL_SE_VINL1,    /* Left input mux for ADC1L */
	ADC_INL_SE_VINL2,    /* Left input mux for ADC2L */
	ADC_INR_SE_VINR1,    /* Right input mux for ADC1R */
	ADC_INR_SE_VINR2,    /* Right input mux for ADC2R */
	ADC_RX_WLEN_16BIT,   /* ADC word length */
	ADC_DATA_FORMAT_I2S, /* ADC data format */
	0
};

/* DAC default configuration parameters */
DacConfig  dacCfg =
{
	DAC_AMUTE_CTRL_SCKI_LOST,   /* Amute event */
	1,                          /* Amute control */
	DAC_SAMPLING_MODE_AUTO,     /* Sampling mode */
	DAC_DATA_FORMAT_I2S,        /* Data format */
	0,                          /* Soft mute control */
	DAC_ATTENUATION_WIDE_RANGE, /* Attenuation mode */
	DAC_DEEMP_44KHZ,            /* De-emph control */
	100                         /* Volume */
};

/**
 *  \brief    Initializes platform specific modules
 *
 *  This function initializes the modules like PLL, DDR, I2C etc
 *  which are required for audio processing. Need to call this function
 *  before doing any HW related operations.
 *
 *  \return    Platform_EOK on Success or error code
 */
Platform_STATUS initPlatform(void)
{
    platform_init_flags  init_flags;
    platform_init_config init_config;
	Platform_STATUS status;

	/* Set request to configure PLL, DDR and Time Stamp Counter */
	init_flags.pll  = 0;
	init_flags.ddr  = 0;
	init_flags.tcsl = 1;
	init_flags.phy  = 0;
	init_flags.ecc  = 0;

	/* PLL configuration shall be done based on default values */
    init_config.pllm       = 0;
    init_config.plld       = 0;
    init_config.postdiv    = 0;
    init_config.mastercore = 1;

	status = platform_init(&init_flags, &init_config);
    if (status != Platform_EOK)
    {
    	printf("Error in Platform Initialization\n");
    	return(status);
    }

	/* Configure platform log messages to standard printf */
    platform_write_configure(PLATFORM_WRITE_PRINTF);

    /* Initialize UART */
    platform_uart_init();
    platform_uart_set_baudrate(115200);

    return(status);
}

/**
 *  \brief    Audio analog test main function
 *
 *  \return    none
 */
void main (void)
{
	Platform_STATUS status;

	printf("Audio Daughter Card Analog Interface Test!\n");

	status = initPlatform();
	if(status != Platform_EOK)
	{
		platform_write("Platform Init Failed!\n");
		exit(1);
	}

	/* Configure eDMA module */
	status = eDmaConfig();
	if(status != Platform_EOK)
	{
		platform_write("eDMA Configuration Failed!\n");
		exit(1);
	}

	/* Initialize common audio configurations */
	status = platformAudioInit();
	if(status != Platform_EOK)
	{
		platform_write("Audio Init Failed!\n");
		exit(1);
	}

	/* Initialize Audio DIR module */
	status = platformAudioDirInit();
	if(status != Platform_EOK)
	{
		platform_write("Audio DIR Configuration Failed!\n");
		exit(1);
	}

	/* Initialize Audio ADC module */
	status = audioAdcConfig(ADC_DEVICE_ALL, &adcCfg);
	if(status != Platform_EOK)
	{
		platform_write("Audio ADC Configuration Failed!\n");
		exit(1);
	}

	/* Initialize McASP module */  //TODO: Check if eDMA and McASP init should be in task context
	status = mcaspConfig();
	if(status != Platform_EOK)
	{
		platform_write("McASP Configuration Failed!\n");
		exit(1);
	}

	/* Initialize Audio DAC module */
	status = audioDacConfig(DAC_DEVICE_ALL, &dacCfg);
	if(status != Platform_EOK)
	{
		platform_write("Audio DAC Configuration Failed!\n");
		exit(1);
	}

	/* Start BIOS execution */
	BIOS_start();
}

/* Nothing past this point */
