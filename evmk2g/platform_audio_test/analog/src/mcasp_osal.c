#if 1

#include <ti/sysbios/knl/Swi.h>
#include <mcasp_drv.h>
#include <include/McaspLocal.h>


    #include <ti/csl/cslr_device.h>

extern Void Mcasp_localIsrSwiFxn(Arg arg0,Arg arg1);
extern Mcasp_Module_State Mcasp_module;
extern Void Mcasp_swiTxFifo(Arg arg0,Arg arg1);
#if 0 // no swi
void osal_swi1(Mcasp_Object   *instHandle)
{
	Swi_Params      swiParams  = {0};
	int count;
Swi_Params_init(&swiParams);

           /* modify the default parameters with the required params         */
           swiParams.priority = Mcasp_SWI_PRIORITY;

           /* create the swi that will be used for ISR handling              */
           Swi_construct((Swi_Struct   * )&(instHandle->isrSwiObject),
               (Swi_FuncPtr)&Mcasp_localIsrSwiFxn, &swiParams, NULL);

           /* copy the swi Handle information to all the module objects      */
           for (count = 0; count < CSL_MCASP_PER_CNT; count++)
           {
               Mcasp_module.isrObject[count].isrSwiTaskHandle = \
                   Swi_handle((Swi_Struct    *)&instHandle->isrSwiObject);
           }

}
void osal_swi2(Mcasp_Object   *instHandle)
{
	Swi_Params      swiParams  = {0};
Swi_Params_init(&swiParams);

            /* create the swi to handle the TX FIFO empty case handling       */
            swiParams.priority = Mcasp_SWI_PRIORITY;
            swiParams.arg0 = (Arg)&instHandle->XmtObj;
            swiParams.arg1 = (Arg)instHandle;

            Swi_construct((Swi_Struct   * )&(instHandle->fifoSwiObject),
                (Swi_FuncPtr)&Mcasp_swiTxFifo, &swiParams, NULL);
}
#endif
/**
 * \fn      static Void mcaspIsr()
 *
 * \brief   This function is the interrupt service routine for the mcasp driver.
 *
 * \param   arg  [IN]  The instance number of the first instance to register
 *                     the ISR.
 *
 * \return  None
 *
 * \enter   arg     is a valid non null pointer
 *
 * \leave   Not implemented
 */
#if 0
Void mcaspIsr(Arg arg)
{
    /* we have got the interrupt for the Mcasp. we need to check which        *
     * instance has generated this interrupt and which section TX or RX. We   *
     * defer the procesing to the Swi thread for the mcasp driver             */

    /* Trigger the SWI thread to handle the interrupts                        *
     * Note : The "arg" here is the instance number of the instance which has *
     * first registered the isr. please note that any valid instance is OK as *
     * all the instances have the same SWI task handle.                       */
    Swi_post((Swi_Handle)arg);
}
#endif
#include <ti/sysbios/knl/Queue.h>

void osal_Queue_construct(void * ptr, int arg)
{
Queue_construct((Queue_Struct*)ptr, NULL);
        }
#include <ti/sysbios/hal/Cache.h>
#include <ti/sysbios/family/c64p/EventCombiner.h>
#include <ti/sysbios/knl/Queue.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/BIOS.h>

void osal_Cache_wb(void * pktAddr, int size)
{
	Cache_wb(pktAddr, size, Cache_Type_ALL, TRUE);
}
void osal_Cache_wbInv(void * pktAddr, int size)
{
	Cache_wbInv(pktAddr, size, Cache_Type_ALL, TRUE);
}
void osal_Task_sleep(int msec)
{
	int i,j;
	//Task_sleep(msec);
	 for (i=0; i < msec; i++) {
	        for (j=0; j < 1000; j++) {
	            asm("* Comment to maintain loops through compiler optimization");
	        }}
}
int osal_getThreadType()
{
	return BIOS_getThreadType() == BIOS_ThreadType_Task;
	
}
#include <ti/sysbios/family/c66/tci66xx/CpIntc.h>
int hostintArr[6]={33,34,35,36,37,38};
int eventIdArr[6]={0x33,0x34,0x35,0x36,0x37,0x38};
void osal_EventCombiner_disableEvent(int cpuEventNum)
{
	EventCombiner_disableEvent(eventIdArr[cpuEventNum- CSL_CIC_McASP_0_XINT]);
}
void osal_EventCombiner_enableEvent(int cpuEventNum)
{
	EventCombiner_enableEvent(eventIdArr[cpuEventNum- CSL_CIC_McASP_0_XINT]);
}

void osal_EventCombiner_dispatchPlug(Uint32 evt, void * intIsr, UArg arg, int flag )
{
	int hostint,eventId;
	  CpIntc_dispatchPlug(evt, (CpIntc_FuncPtr)intIsr, 0, TRUE);
	  hostint=hostintArr[evt- CSL_CIC_McASP_0_XINT];
	                           /* The configuration is for CPINTC0. We map system interrupt 0x88 to Host Interrupt 32. */
	                           CpIntc_mapSysIntToHostInt(0, evt, hostint);

	                           /* Enable the Host Interrupt. */
	                           CpIntc_enableHostInt(0, hostint);

	                           /* Enable the System Interrupt */
	                           CpIntc_enableSysInt(0, evt);

	                           /* Get the event id associated with the host interrupt. */
	                           eventId = eventIdArr[evt- CSL_CIC_McASP_0_XINT];
	                           /* enable the 'global' switch */

	                          /* Enable the Xfer Completion Event Interrupt */
	                               EventCombiner_dispatchPlug(eventId,
	                               						(EventCombiner_FuncPtr)(&CpIntc_dispatch),
	                                                   	hostint, 1);
	                              
	
	//EventCombiner_dispatchPlug(evt, (EventCombiner_FuncPtr)intIsr, arg, flag);
}
#include <ti/sysbios/family/c64p/Hwi.h>
void osal_Hwi_enableIER(int pos)
{
	Hwi_enableIER(pos);
}
int gDebug=0;
void assert(int in)
{
if(in <=0)
	gDebug++;
}
#include <ti/sysbios/knl/Swi.h>
void osal_Swi_post(void * obj)
{
	Swi_post((Swi_Handle)obj);

}
#include <ti/sysbios/knl/Queue.h>
int osal_Queue_empty(void * handle)
{
return Queue_empty(Queue_handle(handle));
//	return ((Mcasp_QueueElem*)handle->next == &((Mcasp_QueueElem*)handle));

}
#if 1
MCASP_Packet * osal_Queue_get(void * handle)
{
return Queue_get(Queue_handle(handle));

}
#endif
void osal_Queue_put(void * handle,Ptr ptr)
		{
	Queue_put(Queue_handle(handle),(Ptr)ptr);
		}

unsigned int osal_disable_interrupts(void)
{
	return _disable_interrupts();
}
unsigned int osal_enable_interrupts(void)
{
	return _enable_interrupts();
}
void osal_restore_interrupts(unsigned int key)
{
	_restore_interrupts( key);
}

#endif
