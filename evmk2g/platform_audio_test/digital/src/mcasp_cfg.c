/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      mcasp_config.c
 *
 * \brief     Configures McASP module
 *
 */

#include "mcasp_cfg.h"

/* Frame index for Rx and Tx buffers */
uint8_t rxFrameIndex = 1;
uint8_t txFrameIndex = 1;
uint32_t gtxFrameIndexCount = 0;
uint32_t grxFrameIndexCount = 0;

/* Flags for counting Rx and Tx interrupts */
volatile uint32_t rxFlag = 0;
volatile uint32_t txFlag = 0;

/* Semaphore handle for Tx and Rx */
Semaphore_Handle semR;
Semaphore_Handle semT;
Semaphore_Params params;

/* McASP device handles */
Ptr hMcaspDevTx;
Ptr hMcaspDevRx;

/* McASP channel handles */
Ptr hMcaspTxChan;
Ptr hMcaspRxChan;

/* McASP channel parameters */
Mcasp_Params mcaspTxParams;
Mcasp_Params mcaspRxParams;

/* McASP Callback function argument */
uint32_t txChanMode;
uint32_t rxChanMode;
uint32_t mcaspRxChanArg = 1;
uint32_t mcaspTxChanArg = 2;

/* McASP Tx and Rx frame buffers */
MCASP_Packet rxFrame[NUM_BUFS];
MCASP_Packet txFrame[NUM_BUFS];

/* Data payload structures for DIT mode */
Mcasp_PktAddrPayload  ditPayload;  /* TODO: Need to check if we need array of payload */
Mcasp_PktAddrPayload  *pDitPayload;
Mcasp_UserDataRam     ditUserData;
Mcasp_ChStatusRam     ditChStatus;

/* McASP Tx and Rx frame buffer pointers */
Ptr txBuf[NUM_BUFS];
Ptr rxBuf[NUM_BUFS];

/* Error flag */
uint32_t gblErrFlag = 0;
uint32_t dirErrFlag = 0;
Error_Block eb;

/* Handle to heap object */
extern HeapMem_Handle heapHandle;

/* Mcasp IOM function object */
extern IOM_Fxns Mcasp_IOMFXNS;

/* External function declarations */
void McaspDevice_init(void);
void GblErr(int arg);

/* McASP HW setup for receive */
Mcasp_HwSetupData mcaspRcvSetup = {
	MCASP_DIR_RMASK,   /* .rmask    */
	MCASP_DIR_RFMT,    /* .rfmt     */
	MCASP_DIR_AFSRCTL, /* .afsrctl  */
	MCASP_DIR_RTDM,    /* .rtdm     */
	MCASP_DIR_RINTCTL, /* .rintctl  */
	MCASP_DIR_RSTAT,   /* .rstat    */
	MCASP_DIR_REVTCTL, /* .revtctl  */
        {
			MCASP_DIR_ACLKRCTL,  /* .aclkrctl  */
			MCASP_DIR_AHCLKRCTL, /* .ahclkrctl */
			MCASP_DIR_RCLKCHK    /* .rclkchk   */
        }
};

/* McASP HW setup for transmit */
Mcasp_HwSetupData mcaspXmtSetup = {
	MCASP_DIT_XMASK,	/* .xmask    */
	MCASP_DIT_XFMT,		/* .xfmt     */
	MCASP_DIT_AFSXCTL,	/* .afsxctl  */
	MCASP_DIT_XTDM, 	/* .xtdm     */
	MCASP_DIT_XINTCTL, 	/* .xintctl  */
	MCASP_DIT_XSTAT, 	/* .xstat    */
	MCASP_DIT_XEVTCTL, 	/* .xevtctl  */
		{
			MCASP_DIT_ACLKXCTL, 	 /* .aclkxctl  */
			MCASP_DIT_AHCLKXCTL,	 /* .ahclkxctl */
			MCASP_DIT_XCLKCHK		 /* .xclkchk   */
        },
};

/* McAsp channel parameters for receive - DIR                */
Mcasp_ChanParams  mcaspRxChanParam =
{
	0x0001,                    /* Number of serializers      */
	{Mcasp_SerializerNum_5},   /* Serializer index           */
	&mcaspRcvSetup,
	TRUE,
	Mcasp_OpMode_TDM,          /* Mode (TDM/DIT)             */
	Mcasp_WordLength_32,
	NULL,
	0,
	NULL,
	GblErr,
	1,                        /* number of TDM channels      */
	Mcasp_BufferFormat_1SER_MULTISLOT_INTERLEAVED,  /* TODO: Need to confirm during testing */
	TRUE,
	TRUE
};

/* McAsp channel parameters for transmit - DIT              */
Mcasp_ChanParams  mcaspTxChanParam =
{
	0x0001,                   /* number of serializers       */
	{Mcasp_SerializerNum_9},   /* Serializer index            */
	&mcaspXmtSetup,
	TRUE,
	Mcasp_OpMode_DIT,         /* Mode (TDM/DIT)              */
	Mcasp_WordLength_32,      /* Word width                  */
	NULL,
	0,
	NULL,
	GblErr,
	1,                        /* Number of TDM channels      */
	Mcasp_BufferFormat_1SER_MULTISLOT_INTERLEAVED,  /* TODO: Need to confirm during testing */
	TRUE,
	TRUE
};

/* Handle to eDMA */
extern EDMA3_DRV_Handle hEdma;

/**
 *  \brief    Function called by McASP driver in case of error
 *
 *  \return    None
 */
void GblErr(int arg)
{
	gblErrFlag = 1;
}

/**
 *  \brief    Function called when DIR ERROR pin is high
 *  TODO: Need to configure for GPIO interrupt
 *  \return    None
 */
void DirErr(int arg)
{
	dirErrFlag = 1;
}

/**
 *  \brief   McASP callback function called up on the data transfer completion
 *
 *  \param  arg   [IN]  - Application specific callback argument
 *  \param  ioBuf [IN]  - McASP IO buffer
 *
 *  \return    None
 */
void mcaspAppCallback(void *arg, MCASP_Packet *ioBuf)
{
	uint32_t mode;

	mode = *(uint32_t*)arg;

	/* Callback is triggered by Rx completion */
	if(mode == mcaspRxChanArg)
	{
		rxFlag++;

		if(rxFrameIndex == 0)
		{
			rxFrameIndex = 1;
		}
		else
		{
			rxFrameIndex = 0;
		}

		/* Post semaphore */
		Semaphore_post(semR);
	}

	/* Callback is triggered by Tx completion */
	if(mode == mcaspTxChanArg)
	{
		if(txFrameIndex == 0)
		{
			txFrameIndex = 1;
		}
		else
		{
			txFrameIndex = 0;
		}

		txFlag++;

		/* Post semaphore */
		Semaphore_post(semT);
	}
}

/**
 *  \brief   Initializes McASP data buffers and submits to McASP driver
 *
 *  \return    Platform_EOK on Success or error code
 */
Platform_STATUS initBuffers(void)
{
	Error_Block  eb;
    uint32_t     count = 0;
    IHeap_Handle iheap;
    Int          status;

    iheap = HeapMem_Handle_to_xdc_runtime_IHeap(heapHandle);
    Error_init(&eb);

    /* Allocate buffers for the McASP data exchanges */
    for(count = 0; count < NUM_BUFS; count++)
    {
        rxBuf[count] = Memory_calloc(iheap, (BUFSIZE * RX_NUM_SERIALIZER),
        						     BUFALIGN, &eb);
        if(NULL == rxBuf[count])
        {
            platform_write("\r\nMEM_calloc failed for Rx\n");
        }
    }

    /* Allocate buffers for the McASP data exchanges */
    for(count = 0; count < NUM_BUFS; count++)
    {
        txBuf[count] = Memory_calloc(iheap, (BUFSIZE * TX_NUM_SERIALIZER),
        							 BUFALIGN, &eb);
        if(NULL == txBuf[count])
        {
            platform_write("\r\nMEM_calloc failed for Tx\n");
        }
    }

    for(count = 0; count < NUM_BUFS; count++)
    {
        /* Issue the first & second empty buffers to the input stream */
    	memset((uint8_t *)rxBuf[count], 0xFF, (BUFSIZE * RX_NUM_SERIALIZER));

		/* RX frame processing */
		rxFrame[count].cmd    = 0;
		rxFrame[count].addr   = (void*)rxBuf[count];
		rxFrame[count].size   = (BUFSIZE * RX_NUM_SERIALIZER);
		rxFrame[count].arg    = (uint32_t) mcaspRxChanArg;
		rxFrame[count].status = 0;
		rxFrame[count].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

		/* Submit McASP packet for Rx */
		status = mcaspSubmitChan(hMcaspRxChan, &rxFrame[count]);
		if((status != MCASP_COMPLETED) && (status != MCASP_PENDING))
		{
			platform_write("mcaspSubmitChan for Rx Failed\n");
			return (Platform_EFAIL);
		}
    }

    for(count = 0; count < NUM_BUFS; count++)
    {
    	memset((uint8_t *)txBuf[count], 0xCC, (BUFSIZE * TX_NUM_SERIALIZER));

    	pDitPayload->addr = (uint32_t*)txBuf[count];

		/* TX frame processing */
		txFrame[count].cmd    = 0;
		txFrame[count].addr   = (void*)pDitPayload;
		txFrame[count].size   = (BUFSIZE * TX_NUM_SERIALIZER);
		txFrame[count].arg    = (uint32_t) mcaspTxChanArg;
		txFrame[count].status = 0;
		txFrame[count].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

		/* Submit McASP packet for Tx */
		status = mcaspSubmitChan(hMcaspTxChan, &txFrame[count]);
		if((status != MCASP_COMPLETED) && (status != MCASP_PENDING))
		{
			platform_write("mcaspSubmitChan for Tx Failed\n");
			return (Platform_EFAIL);
		}
    }

    return (Platform_EOK);
}

/**
 *  \brief   Configures McASP module and creates the channel
 *           for audio Tx and Rx
 *
 *  \return    Platform_EOK on Success or error code
 */
Platform_STATUS mcaspConfig(void)
{
	Int status;
	uint8_t count;

	hMcaspDevTx  = NULL;
	hMcaspDevRx  = NULL;
	hMcaspTxChan = NULL;
	hMcaspRxChan = NULL;

	/* Initialize McASP HW details */
	McaspDevice_init();

	/* Initialize McASP Tx and Rx parameters */
	/* TODO:
	   It is assumed that McASP default parameters are changed by the
	   McASP driver based on the channel configurations IN/OUT and TDM/DIT.
	   Need to confirm while testing */
	mcaspTxParams = Mcasp_PARAMS;
	mcaspRxParams = Mcasp_PARAMS;

	/* Set the HW interrupt number */
	/* TODO: Need to confirm the interrupt number */
	mcaspTxParams.hwiNumber = 8;
	mcaspRxParams.hwiNumber = 8;

	mcaspTxParams.isDataBufferPayloadStructure = 1;

	/* Initialize eDMA handle */
	mcaspRxChanParam.edmaHandle = hEdma;
	mcaspTxChanParam.edmaHandle = hEdma;

	/* Initilize user data and channel status bits
	   TODO: Need to revisit data to be set for user data and channel
	   status here and while changing the DIT blocks */
	for(count = 0; count < 6; count++)
	{
		ditUserData.userDataLeft[count]  = 0;
		ditUserData.userDataRight[count] = 0;

		ditChStatus.chStatusLeft[count]  = 0;
		ditChStatus.chStatusRight[count] = 0;
	}

	ditPayload.chStat         = &ditChStatus;
	ditPayload.userData       = &ditUserData;
	ditPayload.writeDitParams = TRUE;

	pDitPayload = &ditPayload;

	/* Bind McASP1 for Tx */
	status = mcaspBindDev(&hMcaspDevTx, CSL_MCASP_1, &mcaspTxParams);
	if((status != MCASP_COMPLETED) || (hMcaspDevTx == NULL))
	{
		platform_write("mcaspBindDev for Tx Failed\n");
		return (Platform_EFAIL);
	}

	/* Bind McASP2 for Rx */
	status = mcaspBindDev(&hMcaspDevRx, CSL_MCASP_2, &mcaspRxParams);
	if((status != MCASP_COMPLETED) || (hMcaspDevRx == NULL))
	{
		platform_write("mcaspBindDev for Rx Failed\n");
		return (Platform_EFAIL);
	}

	/* Create McASP channel for Tx */
	status = mcaspCreateChan(&hMcaspTxChan, hMcaspDevTx,
	                         MCASP_OUTPUT,
	                         &mcaspTxChanParam,
	                         mcaspAppCallback, &txChanMode);
	if((status != MCASP_COMPLETED) || (hMcaspTxChan == NULL))
	{
		platform_write("mcaspCreateChan for Tx Failed\n");
		return (Platform_EFAIL);
	}

	/* Create McASP channel for Rx */
	status = mcaspCreateChan(&hMcaspRxChan, hMcaspDevRx,
	                         MCASP_INPUT,
	                         &mcaspRxChanParam,
	                         mcaspAppCallback, &rxChanMode);
	if((status != MCASP_COMPLETED) || (hMcaspRxChan == NULL))
	{
		platform_write("mcaspCreateChan for Rx Failed\n");
		return (Platform_EFAIL);
	}

	/* Initialize the buffers and submit for McASP Tx/Rx */
	if(initBuffers() != Platform_EOK)
	{
		platform_write("McASP Buffer Initialization Failed\n");
		return (Platform_EFAIL);
	}

	return (Platform_EOK);
}

/**
 *  \brief   Task to echo the input data to output
 *
 *  Waits for the McASP data transfer completion and copies the
 *  Rx data to Tx buffers
 *
 *  \return    Platform_EOK on Success or error code
 */
/*  */
Void Audio_echo_Task(void)
{
    int32_t count;

    Semaphore_Params_init(&params);

	/* Create semaphores to wait for buffer reclaiming */
    semR = Semaphore_create(0, &params, &eb);
    semT = Semaphore_create(0, &params, &eb);

    /* Forever loop to continuously receive and transmit audio data */
    while (1)
    {
    	if(gblErrFlag)
    	{
    		break;
		}

    	/* Reclaim full buffer from the input stream */
    	/* TODO: Need check Rx and Tx sync from two McASP instances during testing */
    	Semaphore_pend(semR, BIOS_WAIT_FOREVER);
    	Semaphore_pend(semT, BIOS_WAIT_FOREVER);

        /* Reclaim full buffer from the input stream */
    	gtxFrameIndexCount = txFrameIndex;
    	grxFrameIndexCount = rxFrameIndex;

		for(count = 0; count < 0; count++)
		{
			asm("* Comment to maintain loops through compiler optimization");
		}

        /* Reclaim empty buffer from the output stream to be reused           */

        /* Copy the receive information to the transmit buffer */
        Cache_inv(rxBuf[grxFrameIndexCount], (BUFSIZE * RX_NUM_SERIALIZER), Cache_Type_ALL, TRUE);
        memcpy(txBuf[gtxFrameIndexCount], rxBuf[grxFrameIndexCount], (BUFSIZE * RX_NUM_SERIALIZER));
        Cache_wbInv(txBuf[gtxFrameIndexCount], (BUFSIZE * RX_NUM_SERIALIZER), Cache_Type_ALL, TRUE);

    	pDitPayload->addr = (uint32_t*)txBuf[gtxFrameIndexCount];

        /* Issue full buffer to the output stream                             */
        /* TX frame processing */
		txFrame[gtxFrameIndexCount].cmd    = 0;
		txFrame[gtxFrameIndexCount].addr   = (void*)pDitPayload;
		txFrame[gtxFrameIndexCount].size   = (BUFSIZE * TX_NUM_SERIALIZER);
		txFrame[gtxFrameIndexCount].arg    = (uint32_t) mcaspTxChanArg;
		txFrame[gtxFrameIndexCount].status = 0;
		txFrame[gtxFrameIndexCount].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

		mcaspSubmitChan(hMcaspTxChan, &txFrame[gtxFrameIndexCount]);

        /* Issue an empty buffer to the input stream                          */
		rxFrame[grxFrameIndexCount].cmd    = 0;
		rxFrame[grxFrameIndexCount].addr   = (void*)rxBuf[grxFrameIndexCount];
		rxFrame[grxFrameIndexCount].size   = (BUFSIZE * RX_NUM_SERIALIZER);
		rxFrame[grxFrameIndexCount].arg    = (uint32_t) mcaspRxChanArg;
		rxFrame[grxFrameIndexCount].status = 0;
		rxFrame[grxFrameIndexCount].misc   = 1;   /* reserved - used in callback to indicate asynch packet */

        mcaspSubmitChan(hMcaspRxChan, &rxFrame[grxFrameIndexCount]);
	}
}

/* Nothing past this point */
