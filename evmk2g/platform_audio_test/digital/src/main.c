/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      main.c
 *
 * \brief     Audio DC digital test main file
 *
 */

#include "digital_test.h"

/**
 *  \brief    Initializes platform specific modules
 *
 *  This function initializes the modules like PLL, DDR, I2C etc
 *  which are required for audio processing. Need to call this function
 *  before doing any HW related operations.
 *
 *  \return    Platform_EOK on Success or error code
 */
Platform_STATUS initPlatform(void)
{
    platform_init_flags  init_flags;
    platform_init_config init_config;
	Platform_STATUS status;

	/* Set request to configure PLL, DDR and Time Stamp Counter */
	init_flags.pll  = 1;
	init_flags.ddr  = 1;
	init_flags.tcsl = 1;
	init_flags.phy  = 0;
	init_flags.ecc  = 0;

	/* PLL configuration shall be done based on default values */
    init_config.pllm       = 0;
    init_config.plld       = 0;
    init_config.postdiv    = 0;
    init_config.mastercore = 1;

	status = platform_init(&init_flags, &init_config);
    if (status != Platform_EOK)
    {
    	printf("Error in Platform Initialization\n");
    	return(status);
    }

	/* Configure platform log messages to standard printf */
    platform_write_configure(PLATFORM_WRITE_PRINTF);

    /* Initialize UART */
    platform_uart_init();
    platform_uart_set_baudrate(115200);

    return (status);
}

/**
 *  \brief    Audio analog test main function
 *
 *  \return    none
 */
void main (void)
{
	Platform_STATUS status;

	printf("Audio Daughter Card Digital Interface Test!\n");

	status = initPlatform();
	if(status != Platform_EOK)
	{
		printf("Platform Init Failed!\n");
		exit(1);
	}

	/* Configure eDMA module */
	status = eDmaConfig();
	if(status != Platform_EOK)
	{
		platform_write("eDMA Configuration Failed!\n");
		exit(1);
	}

	/* Initialize common audio configurations */
	status = platformAudioInit();
	if(status != Platform_EOK)
	{
		platform_write("Audio Init Failed!\n");
		exit(1);
	}

	/* Initialize Audio DIR module */
	status = audioDirConfig();
	if(status != Platform_EOK)
	{
		platform_write("Audio DIR Configuration Failed!\n");
		exit(1);
	}

	/* Initialize McASP module */  //TODO: Check if eDMA and McASP init should be in task context
	status = mcaspConfig();
	if(status != Platform_EOK)
	{
		platform_write("McASP Configuration Failed!\n");
		exit(1);
	}

	/* Start BIOS execution */
	BIOS_start();
}

/* Nothing past this point */
