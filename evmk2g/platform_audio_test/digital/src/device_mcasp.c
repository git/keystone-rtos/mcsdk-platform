/*
 * mcbsp_device.c
 *
 * This file contains MCBSP IP related EVM (platform) specific routines
 * implementation. This file contains the board specific code for enabling 
 * the use of mcbsp driver, and may contain related device pre-driver 
 * initialization routines. The file is provided as a sample configuration 
 * and should be modified by customers for their own platforms and 
 * configurations.
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/* MCBSP Driver Includes. */

/* CSL MCBSP Register Layer */
#include <ti/csl/cslr_mcasp.h>

/* CSL Chip Functional Layer */
// #include <ti/csl/csl_chip.h> // no functional CSL for Freon

#include <ti/csl/cslr_device.h>


/*============================================================================*/
/*                           IMPORTED VARIABLES                               */
/*============================================================================*/



/* ========================================================================== */
/*                           MODULE FUNCTIONS                                 */
/* ========================================================================== */
/**
 * \brief   Initializes McBSP driver's data structures
 *
 *          This function initializes the McBSP driver's data structures
 *          including instance objects and channel objects. This is the 
 *          MCBSP Driver Initialization API which needs to be invoked by 
 *          the users to initialize the MCBSP peripheral. This call is 
 *          *mandatory* and should be called before calling any of the 
 *          other driver API's.  This can be modified by customers for 
 *          their application and configuration.
 *
 * \return  None
 */
#include <mcasp_drv.h>
#include <mcasp_osal.h>
#include <include/McaspLocal.h>
#include "string.h"
extern Mcasp_HwInfo Mcasp_deviceInstInfo[CSL_MCASP_CNT];
extern Mcasp_Module_State Mcasp_module ;
extern Mcasp_Object Mcasp_Instances[CSL_MCASP_CNT];
extern Mcasp_TempBuffer Mcasp_muteBuf[CSL_MCASP_CNT];

void McaspDevice_init(void)

	{
	    Int i;

	    for (i = 0; i < CSL_MCASP_CNT; i++)
	    {
	        /* have to initialize  ally */
	        Mcasp_module.inUse[i] = 0;//FALSE;
	        memset((void *)&Mcasp_Instances[i], 0x0, sizeof(Mcasp_Object));

	        Mcasp_module.isrObject[i].isIsrRegistered = 0;//FALSE;
	        Mcasp_module.isrObject[i].chanEnabled[0] = 0;//FALSE;
	        Mcasp_module.isrObject[i].chanEnabled[1] = 0;//FALSE;
	        Mcasp_module.isrObject[i].instHandle = 0;//NULL
	        Mcasp_module.isrObject[i].isrSwiTaskHandle = 0;//NULL;

	        if (i == 0)
	        {
	//#if defined(CHIP_C6747) || defined (CHIP_OMAPL137)
	//            Mcasp_deviceInstInfo[i].ditSupport = FALSE;
	//#elif defined(CHIP_C6748) || defined (CHIP_OMAPL138)
	            Mcasp_deviceInstInfo[i].ditSupport = 1;//TRUE;
	//#endif
	            Mcasp_deviceInstInfo[i].baseAddress =
	                (CSL_McaspRegs *)CSL_MCASP_0_CFG_REGS;
	            Mcasp_deviceInstInfo[i].fifoAddress =
	                (CSL_AfifoRegs *)CSL_MCASP_0_FIFO_CFG_REGS;
	            Mcasp_deviceInstInfo[i].dataAddress =
	                (CSL_AdataRegs *)CSL_MCASP_0_SLV_REGS;
	            Mcasp_deviceInstInfo[i].numSerializers = 16u;
	//#if defined(CHIP_C6747) || defined (CHIP_OMAPL137)
	//            Mcasp_deviceInstInfo[i].cpuEventNumber =
	//                (Uint32)CSL_INTC_EVENTID_AXRINT;
	//#elif defined(CHIP_C6748) || defined (CHIP_OMAPL138)
	            Mcasp_deviceInstInfo[i].cpuEventNumber =
	                (Uint32)CSL_CIC_McASP_0_XINT;
	//#endif
	            Mcasp_deviceInstInfo[i].rxDmaEventNumber =
	                (Uint32)CSL_EDMACC_0_McASP_0_REVT;
	            Mcasp_deviceInstInfo[i].txDmaEventNumber =
	                (Uint32)CSL_EDMACC_0_McASP_0_XEVT;
	#ifndef BIOS_PWRM_ENABLE
	            Mcasp_deviceInstInfo[i].pwrmLpscId =
	                0;//(Uint32)CSL_PSC_MCASP0;
	#else
	            Mcasp_deviceInstInfo[i].pwrmLpscId =
	                (Uint32)CSL_LPSC_NUMBER_MCASP_0;
	#endif
	            Mcasp_deviceInstInfo[i].pscInstance =
	               0;// (Uint32)CSL_LPSC_INST_MCASP_0;
	        }
	//#if defined(CHIP_C6747) || defined (CHIP_OMAPL137)
	        else if (i == 1)
	        {
	            Mcasp_deviceInstInfo[i].ditSupport = 1;
	            Mcasp_deviceInstInfo[i].baseAddress =
	                (CSL_McaspRegs *)CSL_MCASP_1_CFG_REGS;
	            Mcasp_deviceInstInfo[i].fifoAddress =
	                (CSL_AfifoRegs *)CSL_MCASP_1_FIFO_CFG_REGS;
	            Mcasp_deviceInstInfo[i].dataAddress =
	                (CSL_AdataRegs *)CSL_MCASP_1_SLV_REGS;
	            Mcasp_deviceInstInfo[i].numSerializers = 10u;
	            Mcasp_deviceInstInfo[i].cpuEventNumber =
	                (Uint32)CSL_CIC_McASP_1_XINT;
	            Mcasp_deviceInstInfo[i].rxDmaEventNumber =
	                (Uint32)CSL_EDMACC_1_McASP_1_REVT;
	            Mcasp_deviceInstInfo[i].txDmaEventNumber =
	                (Uint32)CSL_EDMACC_1_McASP_1_XEVT;
	            Mcasp_deviceInstInfo[i].pwrmLpscId =
	           	                0;
	            		 Mcasp_deviceInstInfo[i].pscInstance =
	            			               0;
	        }
	        else if (i == 2)
	        {
	            Mcasp_deviceInstInfo[i].ditSupport = 1;
	            Mcasp_deviceInstInfo[i].baseAddress =
	                (CSL_McaspRegs *)CSL_MCASP_2_CFG_REGS;
	            Mcasp_deviceInstInfo[i].fifoAddress =
	                (CSL_AfifoRegs *)CSL_MCASP_2_FIFO_CFG_REGS;
	            Mcasp_deviceInstInfo[i].dataAddress =
	                (CSL_AdataRegs *)CSL_MCASP_2_SLV_REGS;
	            Mcasp_deviceInstInfo[i].numSerializers = 6u;
	            Mcasp_deviceInstInfo[i].cpuEventNumber =
	           	                (Uint32)CSL_CIC_McASP_2_XINT;
	           	            Mcasp_deviceInstInfo[i].rxDmaEventNumber =
	           	                (Uint32)CSL_EDMACC_1_McASP_2_REVT;
	           	            Mcasp_deviceInstInfo[i].txDmaEventNumber =
	           	                (Uint32)CSL_EDMACC_1_McASP_2_XEVT;
	           	            Mcasp_deviceInstInfo[i].pwrmLpscId =
	           	           	                0;
	           	            		 Mcasp_deviceInstInfo[i].pscInstance =
	           	            			               0;
	        }
	//#endif  /* defined(CHIP_C6747) || defined (CHIP_OMAPL137) */
	        else
	        {
	            /* do nothing                                                     */
	        }
	    }

	    /* intialise the loop job buffers and the mute buffers for all instances  */
	#ifdef Mcasp_LOOPJOB_ENABLED
	    memset((void *)Mcasp_loopDstBuf, 0x0,
	           sizeof(Mcasp_TempBuffer) * CSL_MCASP_CNT);
	    memset((void *)Mcasp_loopSrcBuf, 0x0,
	           sizeof(Mcasp_TempBuffer) * CSL_MCASP_CNT);
	#endif /* Mcasp_LOOPJOB_ENABLED */
	    memset((void *)Mcasp_muteBuf, 0x0,
	           sizeof(Mcasp_TempBuffer) * CSL_MCASP_CNT);
}

/* ========================================================================== */
/*                              END OF FILE                                   */
/* ========================================================================== */
