/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      audio_dc_config.c
 *
 * \brief     Configures Audio daughter card HW modules
 *
 */

#include "audio_dc_cfg.h"

/**
 *  \brief    Initializes DIR module
 *
 *  This function initializes and configures the DIR modules
 *  on audio daughter card
 *
 *  \return    Platform_EOK on Success or error code
 */
Platform_STATUS audioDirConfig(void)
{
	Platform_STATUS status;
	uint32_t        timeout;
	int8_t          fsout;

	status = platformAudioDirInit();
	if(status != Platform_EOK)
	{
		IFPRINT(platform_write("audioDirConfig : Audio DIR Configuration Failed!\n"));
		return (status);
	}

#ifdef CHECK_ERROR_STATUS
	/* DIR should be in PLL mode.
	   Wait for ERROR signal to be low as DIR is configured for
	   AUTO mode */
	timeout = ERROR_STATUS_WAIT_TIMEOUT;
	while (timeout)
	{
		if(!platformAudioDirGetErrStatus())
		{
			IFPRINT(platform_write("audioDirConfig : DIR in PLL Mode\n"));
			break;
		}

		IFPRINT(platform_write("audioDirConfig : Waiting for DIR to Enter PLL Mode...\n"));
		platform_delay(1000);
		timeout--;
	}

	if(!timeout)
	{
		IFPRINT(platform_write("audioDirConfig : DIR is not in PLL Mode!!\n"));
		return (Platform_EFAIL);
	}
#endif

	fsout = platformAudioDirGetFsOut();
	if(fsout == 2)
	{
		IFPRINT(platform_write("audioDirConfig : Out of Range Sampling Frequency\n"));
	}
	else if(fsout == 0)
	{
		IFPRINT(platform_write("audioDirConfig : Calculated Sampling Frequency Output is 43 kHz�45.2 kHz\n"));
	}
	else if(fsout == 1)
	{
		IFPRINT(platform_write("audioDirConfig : Calculated Sampling Frequency Output is 46.8 kHz�49.2 kHz\n"));
	}
	else if(fsout == 3)
	{
		IFPRINT(platform_write("audioDirConfig : Calculated Sampling Frequency Output is 31.2 kHz�32.8 kHz\n"));
	}
	else
	{
		IFPRINT(platform_write("audioDirConfig : Error in Reading FSOUT status \n"));
	}

 	return (status);
}

/* Nothing past this point */
