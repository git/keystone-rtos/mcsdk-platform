/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      mcasp_config.h
 *
 * \brief     McASP configuration header file
 *
 */

#ifndef _MCASP_CONFIG_H_
#define _MCASP_CONFIG_H_

#include "digital_test.h"

/*
 * Buffers placed in external memory are aligned on a 128 bytes boundary.
 * In addition, the buffer should be of a size multiple of 128 bytes for
 * the cache work optimally on the C6x.
 */

/* Number of bytes in DIT sub-frame */
#define DIT_SUBFRAME_SIZE  (4)
/* Number of bytes in a DIT frame */
#define DIT_FRAME_SIZE     (2 * DIT_SUBFRAME_SIZE)

/* Number of samples in a block of DIT data.
   192 frames, each frame having two sub-frames
   Each sub-frame having 32 bits of data */
#define BUFLEN                  (192 * DIT_FRAME_SIZE)
#define BUFALIGN                (128) /* Alignment of buffer for use of L2 cache */

/** Number of serializers configured for Rx */
#define RX_NUM_SERIALIZER       (1u)
/** Number of serializers configured for Tx */
#define TX_NUM_SERIALIZER       (1u)

/** Size of the McASP serializer buffers */
#define BUFSIZE                 (BUFLEN * sizeof(Ptr))

/** Num Bufs to be issued and reclaimed */
#define NUM_BUFS                2

/**
 * \brief McASP configurations for Tx - DIT
 */
/** Transmit format unit bit mask register default value */
#define MCASP_DIT_XMASK      (0xFFFFFF00)

/** Transmit bit stream format register defult value */
#define MCASP_DIT_XFMT       (  ( 0 << 16)   /* XDATDLY: Transmit with 0 bit delay */ \
                              | ( 0 << 15)   /* XRVRS  : LSB first ?? */              \
                              | ( 0 << 13)   /* XPAD   : Pad extra bits with 0 */     \
		                      | (15 << 4)    /* XSSZ   : 32-bit slot size */          \
                              | ( 0 << 3)    /* XBUSSEL: Writes from edma port */     \
                              | ( 2 << 0))    /* XROT   : Rotate right by 8 bits */   \

/** Transmit frame sync control register defult value */
#define MCASP_DIT_AFSXCTL    (  (384 << 7)    /* XMOD : 384-slot TDM */                       \
                              | (  0 << 4)    /* FXWID: Single bit frame sync */              \
                              | (  1 << 1)    /* FSXM : Internally-generated tx frame sync */ \
                              | (  0 << 0))   /* FSXP : Start on rising edge */

/** Transmit TDM time slot 0-31 register defult value */
#define MCASP_DIT_XTDM       (0xFFFFFFFF)

/** Transmitter interrupt control register defult value */
#define MCASP_DIT_XINTCTL    (0x00000007)

/** Transmitter status register defult value */
#define MCASP_DIT_XSTAT      (0x000001FF)

/** Transmitter DMA event control register defult value */
#define MCASP_DIT_XEVTCTL    (0x00000000)

/** Transmit clock control register defult value */
#define MCASP_DIT_ACLKXCTL   (  (0 << 7)    /* CLKXP  : TX on rising edge */            \
                              | (1 << 6)    /* ASYNC  : TX and RX are asynchronous  */  \
                              | (1 << 5)    /* CLKXM  : Internally generated ACLK */    \
                              | (0 << 0))   /* CLKXDIV: Divide by 1 - 128fs */

/** Transmit high-frequency clock control register defult value */
#define MCASP_DIT_AHCLKXCTL   (  (0 << 15)    /* HCLKXM  : Clock from AHCLKX pin */  \
                               | (0 << 14)    /* HCLKXP  : Rising edge */            \
                               | (0 << 0))    /* HCLKXDIV: AHCLKX = AUXCLK / 1 */

/** Transmit clock check control register defult value */
#define MCASP_DIT_XCLKCHK     (0x00000000)


/**
 * \brief McASP configurations for Rx - DIR
 */
/** Receive format unit bit mask register defult value */
#define MCASP_DIR_RMASK      (0xFFFFFF00)

/** Receive bit stream format register defult value */
#define MCASP_DIR_RFMT       (  ( 0 << 16)   /* RDATDLY: Transmit with 0 bit delay */ \
                              | ( 1 << 15)   /* RXRVRS  : MSB first */                \
                              | ( 0 << 13)   /* RPAD   : Pad extra bits with 0 */     \
		                      | (15 << 4)    /* RSSZ   : 32-bit slot size */          \
                              | ( 0 << 3)    /* RBUSSEL: Reads from edma port */      \
                              | ( 0 << 0))    /* RROT   : No rotation */

/** Receive frame sync control register defult value */
#define MCASP_DIR_AFSRCTL    (  (2 << 7)    /* RMOD : 2-slot TDM */                         \
                              | (1 << 4)    /* FRWID: Single word frame sync */             \
                              | (0 << 1)    /* FSRM : Externally-generated rx frame sync */ \
                              | (1 << 0))   /* FSRP : Start on falling edge */

/** Receive TDM time slot 0-31 register defult value */
#define MCASP_DIR_RTDM       (0x00000003)

/** Receive interrupt control register defult value */
#define MCASP_DIR_RINTCTL    (0x00000003)

/** Receive status register defult value */
#define MCASP_DIR_RSTAT      (0x000001FF)

/** Receive DMA event control register defult value */
#define MCASP_DIR_REVTCTL    (0x00000000)

/** Receive clock control register defult value */
#define MCASP_DIR_ACLKRCTL   (  (0 << 7)    /* CLKRP  : RX on falling edge */          \
                              | (0 << 5)    /* CLKRM  : Externally generated ACLK */  \
                              | (0 << 0))   /* CLKRDIV: NA */

/** Receive high-frequency clock control register defult value */
#define MCASP_DIR_AHCLKRCTL   (  (0 << 15)    /* HCLKRM  : Clock from AHCLKR pin */  \
                               | (0 << 14)    /* HCLKRP  : Rising edge */            \
                               | (0 << 0))    /* HCLKRDIV: AHCLKR = AUXCLK / 1 */

/** Receive clock check control register defult value */
#define MCASP_DIR_RCLKCHK     (0x00000000)

/**
 *  \brief   Configures McASP module and creates the channel
 *           for audio Tx and Rx
 *
 *  \return    Platform_EOK on Success or error code
 */
Platform_STATUS mcaspConfig(void);

#endif /* _MCASP_CONFIG_H_ */
