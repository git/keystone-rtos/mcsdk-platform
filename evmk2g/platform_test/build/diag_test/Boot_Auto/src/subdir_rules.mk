################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
src/audio_clk_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/audio_clk_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/audio_clk_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/dcan_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/dcan_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/dcan_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ddr_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/ddr_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/ddr_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/emmc_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/emmc_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/emmc_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/hdmi_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/hdmi_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/hdmi_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/i2c_eeprom_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/i2c_eeprom_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/i2c_eeprom_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/lcd_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/lcd_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/lcd_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/nand_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/nand_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/nand_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/platform_test_config.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/platform_test_config.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/platform_test_config.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/platforms_utils_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/platforms_utils_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/platforms_utils_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/qspi_flash_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/qspi_flash_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/qspi_flash_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/sd_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/sd_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/sd_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/spi_nor_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/spi_nor_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/spi_nor_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/touch_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/touch_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/touch_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/uart_test.obj: C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/src/uart_test.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"c:/ti/ccsv6/tools/compiler/c6000_7.4.4/bin/cl6x" -mv6600 --abi=eabi -O2 --include_path="c:/ti/ccsv6/tools/compiler/c6000_7.4.4/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/csl" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_lib/include" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform" --include_path="C:/ti/pdk_keystone2_3_02_00_00/packages/ti/platform/evmk2g/platform_test/include" --define=DEVICE_K2G --define=USE_SERIAL_CONSOLE --define=ENABLE_AUTO_RUN --display_error_number --diag_warning=225 --diag_wrap=off --preproc_with_compile --preproc_dependency="src/uart_test.pp" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


