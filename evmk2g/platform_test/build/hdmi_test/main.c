/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
*
* \file main.c
*
* \brief hdmi test main file.
*
******************************************************************************/

#include "stdio.h"
#include "hdmi_test.h"

/* Platform init default flags */
platform_init_flags gHdmiInitFlags =
{
	0,
	0,
	PLATFORM_INIT_FLAGS_TCSL,
	PLATFORM_INIT_FLAGS_PHY,
	PLATFORM_INIT_FLAGS_ECC
};

/* Platform init default configurations */
platform_init_config gHdmiInitConfig =
{
	PLATFORM_INIT_CONFIG_PLLM,
	PLATFORM_INIT_CONFIG_PLLD,
	PLATFORM_INIT_CONFIG_PREDIV,
	PLATFORM_INIT_CONFIG_POSTDIV,
	PLATFORM_INIT_CONFIG_MASTERCORE
};

/**
 *  \brief    Initializes platform test modules
 *
 *  \return    - Platform_EOK on Success or error code
 */
static Platform_STATUS platformInit(void)
{
	Platform_STATUS status = 0;

	/* Initialize platform HW modules */
#ifndef GALILEO_SIM_TEST
    status = platform_init(&gHdmiInitFlags, &gHdmiInitConfig);
#endif
    if(status != Platform_EOK)
    {
    	IFDEBUGPRINT(printf("Platform failed to initialize, errno = 0x%x \n", platform_errno));
    	return (status);
    }

	/* Initialize user interface module */
	/* Set output console to CCS */
	platform_write_configure(PLATFORM_WRITE_PRINTF);
	platform_read_configure(PLATFORM_READ_SCANF);

    return (status);
}

/**
 * \brief Invokes hdmi test functions
 *
 */
int main(void)
{
    TEST_STATUS     testStatus;
    Platform_STATUS status;
    hdmiTestArgs_t  testArgs;

    testArgs.autoRun = 1;

    /* Initialize Plarform */
    status = platformInit();
    if(status != Platform_EOK)
    {
    	IFDEBUGPRINT(printf("Platform Init Failed\n"));
    	return (-1);
    }

    /* Invoke HDMI Test */
    testStatus = hdmiTest(&testArgs);
    if(testStatus != TEST_PASS)
    {
    	return (-1);
    }

    return (0);
}

/* Nothing past this point */
