/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * \file  emmc_test.h
 *
 * \brief   This file contains structure, typedefs, functions and
 *          prototypes used for emmc test
 *
 *****************************************************************************/

#ifndef	_EMMC_TEST_H_
#define	_EMMC_TEST_H_

#include "platform_test.h"

#define EMMC_TEST_PATTERN	(0xAA)

#define EMMC_PAGE_OFFSET         (0x0)
#define EMMC_STARTSECTOR         (100)
#define EMMC_TESTSECTOR_COUNT    (512) //0x734000

#define EMMC_DATA_PATTERN_00           (0)
#define EMMC_DATA_PATTERN_FF           (1)
#define EMMC_DATA_PATTERN_AA     (2)
#define EMMC_DATA_PATTERN_55     (3)
#define EMMC_DATA_PATTERN_INC    (4)

#define EMMC_TESTSECTORS       (1)
#define EMMC_ERASE_START       (0)
#define EMMC_ERASE_END         (100)

/**
 * \brief This function performs emmc test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS emmcTest(void *testArgs);

#endif // _EMMC_TEST_H_

/* Nothing past this point */

