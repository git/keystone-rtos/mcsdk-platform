/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * \file  audio_clk_test.h
 *
 * \brief   This file contains structure, typedefs, functions and
 *          prototypes used for audio clock test
 *
 *****************************************************************************/

#ifndef    _AUDIO_CLK_TEST_H_
#define    _AUDIO_CLK_TEST_H_

#include "platform_test.h"
#include "platform_internal.h"

#define CS2000_REG_START (1)
#define CS2000_REG_END   (0x1E)

/** CS2000 device ID register address */
#define CS2000_DEVID_REG      (0x1)
/** CS2000 device control register address */
#define CS2000_DEVCTL_REG     (0x2)
/** CS2000 device configuration 1 register address */
#define CS2000_DEVCFG1_REG    (0x3)
/** CS2000 device configuration 2 register address */
#define CS2000_DEVCFG2_REG    (0x4)
/** CS2000 global configuration register address */
#define CS2000_GBLCFG_REG     (0x5)
/** CS2000 ratio configuration register start address */
#define CS2000_RATIO_CFG_REG_START (0x6)
/** CS2000 ratio configuration register end address */
#define CS2000_RATIO_CFG_REG_END   (0x15)
/** CS2000 function configuration register address */
#define CS2000_FUN_CFG_REG   (0x16)

/** 12.20 format ratio for output/input = 1 */
#define CS2000_RATIO_12p20_1  (0x100000)
/** 12.20 format ratio for output/input = 2 */
#define CS2000_RATIO_12p20_2  (0x200000)
/** 12.20 format ratio for output/input = 3 */
#define CS2000_RATIO_12p20_3  (0x300000)
/** 12.20 format ratio for output/input = 4 */
#define CS2000_RATIO_12p20_4  (0x400000)

/** 20.12 format ratio for output/input = 1 */
#define CS2000_RATIO_20p12_1  (0x1000)
/** 20.12 format ratio for output/input = 2 */
#define CS2000_RATIO_20p12_2  (0x2000)
/** 20.12 format ratio for output/input = 3 */
#define CS2000_RATIO_20p12_3  (0x3000)
/** 20.12 format ratio for output/input = 4 */
#define CS2000_RATIO_20p12_4  (0x4000)

/** Number of input data values for DAC8550 */
#define DAC8550_DATA_COUNT (3)

#define SPI3_SPI3SCS0_PADMUM   (86)
#define SPI3_SPI3SCS1_PADMUM   (87)
#define SPI3_SPI3CLK_PADMUM    (88)
#define SPI3_SPI3DIN_PADMUM    (89)
#define SPI3_SPI3DOUT_PADMUM   (90)

#define CPTS_TS_COMP_PADMUM   (126)

/**
 * \brief This function performs audio clock test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS audioClkTest(void *testArgs);

#endif // _AUDIO_CLK_TEST_H_

/* Nothing past this point */

