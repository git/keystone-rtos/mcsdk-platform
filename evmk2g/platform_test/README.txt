-------------
INTRODUCTION
-------------
This document provides the details of procedure to execute K2G EVM diagnostic tests


----------
EVM Setup
----------
Configure K2G EVM with below settings while executing the diagnostic tests 
unless a different configuration is recommended for a specific test execution 

 - J15 (SYSCLK selection) � Short pins 1 and 2
 - J46 (BMC 5v_EN) - Short pins 2 and 3
 - SW3 (Boot mode selection) � 0 (Sleep)
 - SW11 (COM8 buffer selection) � OFF
 - SW12 (UART mux) � OFF
 - SW13 (HDMI selection) � ON
 - SW14 (LCD selection) � ON
 - SW15 (Audio buffer selection) � ON
 

----------- 
JTAG Setup
-----------
Onboard XDS200 emulator is used for loading and executing the diagnostic tests on EVM. 
Power ON the EVM first and connect the USB cable between EVM JTAG port (XDS_USB) and 
test PC for proper functioning of JTAG connection.


---------------------
Target Configuration
---------------------
Use target configuration file for K2G (evmk2g\platform_test\target_config\evmk2g.ccxml) along with 
gel file evmk2g.gel for running the diagnostics tests

Diagnostic tests are desgined to be executed from C66x core. Target connection in the diagnostic
test procedure refers to C66x core.


---------------
Test Procedure
---------------

DDR3 Test
----------
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\ddr_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program ddr_test.out
- Verify the test output for test status

I2C EEPROM Test
---------------
- Connect BMC serial cable to BMC serial header (J6) and test PC
- Open serial console application (Ex: Teraterm) on test PC and connect to BMC COM port. 
  Configure the serial port to below values
  -- Baudrate � 115200
  -- Data length � 8 bit
  -- Parity � None
  -- Stop Bits � 1
  -- Flow control � None
- Connect K2G EVM and JTAG to test PC
- Type below command in BMC serial console
  -- wp off eeprom
- Build the project evmk2g\platform_test\build\i2c_eeprom_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program i2c_eeprom_test.out
- Verify the test output for test status

SPI NOR Test
-------------
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\spi_nor_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program spi_nor_test.out
- Verify the test output for test status

NAND Flash Test
---------------
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\nand_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program nand_test.out
- Verify the test output for test status

QSPI Flash Test
---------------
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\qspi_flash_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program qspi_flash_test.out
- Verify the test output for test status

eMMC Test
----------
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\emmc_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program emmc_test.out
- Verify the test output for test status

SD Test
--------
- Insert microSD card into SD card slot (J37) of the EVM
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\sd_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program sd_test.out
- Verify the test output for test status

SDIO Test
----------
- Make sure the below hardware modifications are done on the EVM to test SDIO
  -- Resistors RA6, R387 & R388 should be DNI and mount RA15, R713, R714 and R393    
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\sdio_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program sdio_test.out
- Verify the test output for test status

DCAN Test
----------
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\dcan_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program dcan_test.out
- Verify the data on DCAN Tx pins at J25 and J26. 1 KHz square wave should be observed on DCAN Tx pins.

HDMI Test
----------
- Connect HDMI monitor to HDMI OUT (J36) port of the EVM 
- Keep the switches SW13 and SW14 in ON state
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\hdmi_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program hdmi_test.out
- Verify the HDMI output
  Colors will be displayed on HDMI monitor in the below sequence
  -- Color bar, White, BLUE, GREEN, RED, PURPLE, PINK, BLACK and YELLOW 

Note: HDMI test runs at resolution of 640x480 make sure the HDMI monitor connected to EVM supports this resolution.  
  
LCD Test
---------
- Mount LCD panel to on the EVM  
- Keep the switches SW13 and SW14 in OFF state
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\lcd_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program lcd_test.out
- Verify the LCD output
  Colors will be displayed on LCD in the below sequence
  -- Color bar, White, BLUE, GREEN, RED, PURPLE, PINK, BLACK and YELLOW
  
Touch Test
-----------
- Mount LCD panel to on the EVM  
- Keep the switches SW13 and SW14 in OFF state
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\touch_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program touch_test.out
- Follow the instructions displayed on CCS console
  -- Touch the LCD panel and verify the touch locations displayed on console
  -- Press Y/y to continue the test and X/x to exit the test
  

Audio Clock Test
----------------
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\audio_clk_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program audio_clk_test.out
- CS2000 will be configured without digital PLL in first step. 
  Check the clock value at R407 after below messages are displayed in console. Clock value should be 12.288MHz.
  -- Running CS2000 Clock Config Test without Digital PLL
     Configuring Output/Input Ratio to 1
     Check PLL Output at CLK_OUT
     Waiting for 20 secs for Clock check to Complete...
- CS2000 will be configured in digital PLL mode in second step. 
  Check the clock value at R407 after below messages are displayed in console. Clock value should be ~2.5MHz. This completes CS2000 test
  -- Running CS2000 Test with Digital PLL
     Configuring Output/Input Ratio to 1
     Check PLL Output at CLK_OUT
     Enabling CLK_IN, at Rate 10 KHz
     Check Clock output and PLL Lock at AUX_OUT
- After completing the CS2000 test, DAC8550 test starts and test will pause for 10secs before start writing data input to DAC. 
  Get ready to check the DAC_VOUT at R486.
- Check DAC_VOUT when DAC data write is in progress which should change from 0 to 2.5V in constant steps.


Ethernet Test
--------------
- Connect K2G EVM and JTAG to test PC
- Launch K2G EVM target configuration file and connect the target
- Load the program evmk2g\platform_test\bin\ethernet_test.out
- Open CCS disassembly window (View->Disassembly)
- Search for symbol �Setup_Rx� and put a break point at start of Setup_Rx()
- Run the program until it hits the break point
- Run GEL script to enable PHY loopback mode
  -- Scripts->Tests->MDIO_Init
  -- Scripts->Tests->MDIO_Set_GB_Loopback
- Run the program till end and observe the test output in CCS Window. 
  Packet send and receive should be successful


PCIe Test
---------
PCIe test is designed to validate PCIe interface by running the test on two EVMs. 
One EVM shall be configured as root complex and other as end point. 

Follow below steps to run PCIe test. Test steps apply to both the EVMs unless it is explicitly mentioned.

- Set the SYSCLK selection jumper (J15) to short pins 2 and 3
- Connect customized PCIe loopback cable between PCIe connectors (J5) of two EVMs
- Power ON the EVM.
- Connect the USB cable between EVM JTAG port (XDS_USB) and test PC
- Launch K2G EVM target configuration file and connect the target
- Connect to the C66x core and load the program evmk2g\platform_test\bin\pcie_test.out
- On the EVM which is intended to work as root complex, open CCS expression window (View->Expressions)
- Add expression �PcieModeGbl� and change its value to �pcie_RC_MODE�
- Run the program on both the EVMs
- Check the test output on both the EVMs

Audio Codec Test
-----------------
- Connect male to male audio cable to LINE IN port on EVM and audio out of test PC
- Connect headphone or speakers to LINE OUT port of EVM
- Keep SW15 in ON state and SW11 in OFF state
- Connect K2G EVM and JTAG to test PC
- Launch K2G EVM target configuration file and connect the target
- Load the program evmk2g\platform_test\bin\aic3106_audio_codec_test.out
- Play audio on test PC
- Observe the audio output coming out of EVM LINE OUT
  -- Audio played on test PC should be coming out of EVM LINE OUT without any noise.


Menu Based Diagnostic Test Execution
-------------------------------------
Diagnostic tests can be executed by menu selection using integrated diagnostic test project.

Follow below steps to execute integrated diagnostic test
- Connect K2G EVM and JTAG to test PC
- Build the project evmk2g\platform_test\build\diag_test
- Launch K2G EVM target configuration file and connect the target
- Load and run the program diag_test.out
- Below menu will be displayed on CCS console

Diagnostic Tests		 Pass	  Fail
-----------------------		------	 ------
 0 - Auto Run All Tests       
 1 - Run All Memory Tests     
 2 - DDR3 Test                	   0	   0
 3 - NAND Flash Test          	   0	   0
 4 - QSPI Flash Test          	   0	   0
 5 - SPI NOR Flash Test       	   0	   0
 6 - I2C EEPROM Test          	   0	   0
 7 - eMMC Test                	   0	   0
 8 - uSD Test                 	   0	   0
 9 - PCIe Test                	   0	   0
10 - CAN Bus Test             	   0	   0
11 - MLB Test                 	   0	   0
12 - HDMI Test                	   0	   0
13 - LCD Test                 	   0	   0
14 - Touch Test               	   0	   0
15 - Ethernet Test            	   0	   0
16 - UART Test - 'DB9'        	   0	   0
17 - UART Test - 'UART to USB'	   0	   0
18 - UART Test - 'Serial Hdr' 	   0	   0
19 - COM8 Test                	   0	   0
20 - Audio Clock Test         	   0	   0
21 - Audio Test               	   0	   0
22 - Audio DC Test            	   0	   0

- Enter a test number in CCS console to run the test
- Follow the instructions displayed on CCS console and observe the test results

Note: Some of the tests executed by menu selection requires manual inputs.
        Follow the instructions to provide test inputs

        Diagnostic test menu and status messages can be routed to serial console instead of CCS console by
        compiling the diagnostic test project in 'Boot' build mode. This feature is not fully validated in the current version.
      
      Below tests are not supported by the current version of diagnostic tests even though they are available in menu
      - PCIe
      - MLB
      - Ethernet
      - COM8 test
      - Audio and Audio DC tests
      