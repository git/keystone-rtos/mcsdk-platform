/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
*
*  \file lcd_test.c
*
*  \brief This file contains lcd test functions.
*
******************************************************************************/

#include "lcd_test.h"
#include "platform_internal.h"

/** LCD Panel configuration for NHD 4.3 -480272EF-ATXL-CTP */
lcdCfg_t lcdCfg = {
        480,        /** LcdWidth    */
        272,        /** LcdHeight    */
        9000000,    /** LcdPclk      */
        41,         /** HsyncWidth   */
        2,          /** HFrontPorch  */
        2,          /** HBackPorch   */
        10,         /** VsyncWidth   */
        2,          /** VFrontPorch  */
        2,          /** VBackPorch   */
};

/**
 * \brief Function to display color and read user input or
 *        wait for some time after LCD display
 *
 *  \param    input [IN]   Flag to indicate wait for user input
 *  \param    color [IN]   Value of Color
 *  \param    name  [IN]   Name of the pattern displayed on LCD
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
static TEST_STATUS displayColor(uint8_t input, int color, char *name)
{
	uint8_t c = 0;

	if(input == FALSE)
	{
		platform_write("\rDisplaying %s... WAIT  Press 'y' if %s is displayed, any other key for failure: ", name, name);

		lcdColorDisplay(&lcdCfg, color);

		platform_read(&c, 1);
		if((c == 'y') || (c == 'Y'))
		{
			platform_write("\rDisplay %s           PASS\n", name);
		}
		else
		{
			platform_write("\rDisplay %s           FAIL\n", name);
			return (TEST_FAIL);
		}
	}
	else
	{
		platform_write("\rDisplaying %s...\n", name);
		lcdColorDisplay(&lcdCfg, color);
    	delay_secs(LCD_DISPLAY_DELAY);
	}

	return (TEST_PASS);
}

/**
 * \brief This function performs lcd initialization, panel initialization
 *        and DSS init
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS lcd_display_test(void *testArgs)
{
	lcdTestArgs_t *args = (lcdTestArgs_t *)testArgs;
    TEST_STATUS status = TEST_PASS;
    uint8_t c = 0;
    uint8_t ret;

	ret = pwmInit();
	if (ret)
	{
		platform_write("PWM initialization failed\n");
		return (TEST_FAIL);
	}

    status = lcdPanelInit(&lcdCfg);
    if (status != DSS_RET_OK)
    {
    	return (TEST_FAIL);
	}

#ifdef DISPLAY_LOGO
    lcdLogoDisplay();
#endif

	if(args->autoRun == FALSE)
	{
		platform_write("\rDisplaying Colorbar... WAIT  Press 'y' if Colorbar is displayed, any other key for failure: ");
	}
	else
	{
		platform_write("\rDisplaying Colorbar...\n");
	}

    lcdColorBarDisplay(&lcdCfg);

	if(args->autoRun == FALSE)
	{
		platform_read(&c, 1);
		if((c == 'y') || (c == 'Y'))
		{
			platform_write("\rDisplay Colorbar           PASS\n");
		}
		else
		{
			platform_write("\rDisplay Colorbar           FAIL\n");
			return (TEST_FAIL);
		}
	}
	else
	{
    	delay_secs(LCD_DISPLAY_DELAY);
	}

	if(displayColor(args->autoRun, LCD_WHITE, "WHITE"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, LCD_BLUE, "BLUE"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, LCD_GREEN, "GREEN"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, LCD_RED, "RED"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, LCD_PURPLE, "PURPLE"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, LCD_PINK, "PINK"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, LCD_BLACK, "BLACK"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, LCD_YELLOW, "YELLOW"))
	{
		return (TEST_FAIL);
	}

    return (TEST_PASS);
}

/**
 *
 * \brief    This function is used to test the LCD backlight by increasing and
 *           decreasing the brightness.
 *
 * No parameters.
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS lcd_backlight_test(void)
{
	int8_t  count;
	uint8_t ret;

	ret = pwmInit();
	if (ret)
	{
		platform_write("PWM initialization failed\n");
		return (TEST_FAIL);
	}

	/* Increase the LCD backlight brightness from 0 to 100% in steps of 5 */
	for (count = 0; count <= 20; count++)
	{
		ret = pwmSetDutyCycle((5 * count));
		if (ret)
		{
			return (TEST_FAIL);
		}

		/* Wait for some time */
		platform_delay(200000);
	}

	/* Decrease the LCD backlight brightness from 100 to 0% in steps of 10 */
	for (count = 20; count >= 0; count--)
	{
		ret = pwmSetDutyCycle((5 * count));
		if (ret)
		{
			return (TEST_FAIL);
		}

		/* Wait for some time */
		platform_delay(200000);
	}

    /* Set LCD backlight brightness to 50% */
	ret = pwmSetDutyCycle(50);
	if (ret)
	{
		return (TEST_FAIL);
	}

	return (TEST_PASS);
}

/**
 *  \brief    Executes LCD tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_lcd_test(void *testArgs)
{
	TEST_STATUS status = TEST_PASS;

	platform_write("\nRunning LCD Display Test\n");

	status = lcd_display_test(testArgs);
	if(status != TEST_PASS)
	{
		platform_write("\nLCD Display Test Failed\n");
		return (status);
	}

	platform_write("\nLCD Display Test Completed!\n");

	platform_write("\nRunning LCD Backlight Test\n");

	status = lcd_backlight_test();
	if(status != TEST_PASS)
	{
		platform_write("\nLCD Backlight Test Failed\n");
		return (status);
	}

	platform_write("\nLCD Backlight Test Completed!\n");

	return (status);
}

/**
 * \brief This function performs lcd test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS lcdTest(void *testArgs)
{
    TEST_STATUS testStatus;
	lcdTestArgs_t *args = (lcdTestArgs_t *)testArgs;

	platform_write("\n**********************\n");
	platform_write(  "       LCD Test       \n");
	platform_write(  "**********************\n");

	testStatus = run_lcd_test(args);
	if(testStatus != TEST_PASS)
	{
		platform_write("\nLCD Test Failed!\n");
	}
	else
	{
		//platform_write("\nLCD Test Passed!\n");
	}

	platform_write("\nLCD Tests Completed!!\n");
	platform_write("\n-----------------X-----------------\n\n\n");

    return(testStatus);

} // lcdTest

/* Nothing past this point */

