/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
*
*  \file  uart_test.c
*
*  \brief This file contains uart test functions.
*
******************************************************************************/

#include "uart_test.h"

/**
 *  \brief    Executes UART tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_uart_test(void *testArgs)
{
    TEST_STATUS status = TEST_PASS;
    uartTestArgs_t *args = (uartTestArgs_t *)testArgs;
    PLATFORM_UART_Params params;
    uint16_t    length;
    uint16_t    index;
    uint8_t     message[] = "\r\nThis is UART Diagnostic Test!\r\n";
    uint8_t     message1[] = "\r\nEnter 10 Characters Here: ";
    uint8_t     message2[] = "\r\nText Entered: ";
    uint8_t     buf[10];

	params.uart_port = args->uartPort;
	platform_uart_set_params(&params);

	/* Initialize UART */
	platform_uart_init();

    platform_uart_set_baudrate(args->baudRate);

    platform_write("\nOpen a Serial Port Console in the PC Connected to\n");
    platform_write("the Board Using UART and Set Its Baudrate to %d\n", args->baudRate);
    platform_write("You Should See Following Message --- %s", message);

    length = strlen((char *)message);
    for (index = 0; index < length; index++)
    {
        platform_uart_write(message[index]);
    }

    platform_write("\nType 10 characters in serial console\n");

    length = strlen((char *)message1);
    for (index = 0; index < length; index++)
    {
        platform_uart_write(message1[index]);
    }

    platform_write("\nCharacters Received from Console\n");
    for (index = 0; index < 10; index++)
    {
        platform_errno = PLATFORM_ERRNO_RESET;
        if (platform_uart_read(&buf[index], TIMEOUT_SECS) == Platform_EOK)
        {
            platform_write("Char %d = %c\n", index, buf[index]);
        }
        else
        {
            platform_write(" TimeOut Error : 0x%x\n",  platform_errno);
        }
    }

    if(platform_errno)
    {
        status = TEST_FAIL;
    }
    else
    {
        platform_write("\nSending the Characters Back to Console. Check the Console Output\n");

        length = strlen((char *)message2);
        for (index = 0; index < length; index++)
        {
            platform_uart_write(message2[index]);
        }

        for (index = 0; index < 10; index++)
        {
            platform_uart_write(buf[index]);
        }
    }

    return (status);
}

/**
 *  \brief    Configures GPIO to select 'DB9' or 'UART to USB' connection
 *
 *  \param    flag [IN]   GPIO state
 *                        0 - UART to USB
 *                        1 - DB9 Connector
 *
 */
static void uart_gpio_config(uint8_t flag)
{
    pinMuxSetMode(UART_SELECT_GPIO_PADMUM, PADCONFIG_MUX_MODE_QUATERNARY);
    gpioSetDirection(UART_SELECT_GPIO_PORT, UART_SELECT_GPIO_PIN, GPIO_OUT);

    if(flag == 0)
    {
    	gpioClearOutput(UART_SELECT_GPIO_PORT, UART_SELECT_GPIO_PIN);
    }
    else
    {
    	gpioSetOutput(UART_SELECT_GPIO_PORT, UART_SELECT_GPIO_PIN);
    }
}

/**
 * \brief This function performs uart test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS uartTest(void *testArgs)
{
    TEST_STATUS testStatus;
    uartTestArgs_t *args = (uartTestArgs_t *)testArgs;
    WRITE_info     setting;
    uint8_t        ch;

    /* Don't echo to the uart since we are testing on it */
    setting = platform_write_configure (PLATFORM_WRITE_PRINTF);

    platform_write("\n***********************\n");
    platform_write(  "       UART Test       \n");
    platform_write(  "***********************\n");

    if(args == NULL)
    {
        platform_write("Invalid Test Arguments!\n");
        platform_write("Aborting the Test!!\n");
        return (TEST_FAIL);
    }

    if(args->uartId == UART_ID_DB9)
    {
		platform_write("\nRunning UART DB9 Port Test\n");
		platform_write("Make Sure SW12 is OFF\n");
		platform_write("Connect Serial Cable to DB9 Connector\n");

		uart_gpio_config(UART_GPIO_STATE_DB9);
    }
    else if(args->uartId == UART_ID_UART2USB)
    {
		platform_write("\nRunning UART to USB Connection Test\n");
		platform_write("Make Sure SW12 is ON\n");
		platform_write("Connect USB Cable to mini USB Connector\n");

		uart_gpio_config(UART_GPIO_STATE_UART2USB);
	}
	else
	{
		platform_write("\nRunning UART Serial Header Test\n");
		platform_write("Connect Serial Cable to SoC Serial Header\n");
	}

	if(args->autoRun == FALSE)
	{
		fflush(stdin);
		platform_write("Press Any Key to Continue When Setup is Ready: ");
    	scanf("%c", &ch);
	}

    testStatus = run_uart_test(args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nUART Test Failed!\n");
    }
    else
    {
        //platform_write("\nUART Test Passed!\n");
    }

    platform_write("\nUART Tests Completed!!\n");
    platform_write("\n-----------------X-----------------\n\n\n");

    platform_write_configure (setting);

    return(testStatus);

} // uartTest

/* Nothing past this point */

