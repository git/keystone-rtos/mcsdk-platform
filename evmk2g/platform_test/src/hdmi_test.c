/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * \file  hdmi_test.c
 *
 * \brief This file contains hdmi test functions.
 *
 ******************************************************************************/

#include "hdmi_test.h"
#include "platform_internal.h"

#if 1
lcdCfg_t hdmiCfg = {
        640,        /** LcdWidth    */
        480,       /** LcdHeight    */
        24750000,  /** LcdPclk      */
        92,        /** HsyncWidth   */
		34,       /** HFrontPorch  */
        34,        /** HBackPorch   */
        3,        /** VsyncWidth   */
        3,        /** VFrontPorch  */
        42,        /** VBackPorch   */
};
#endif
#if 0
lcdCfg_t hdmiCfg = {
        640,        /** LcdWidth    */
        480,       /** LcdHeight    */
        27000000,  /** LcdPclk      */
        44,        /** HsyncWidth   */
        21,       /** HFrontPorch  */
        113,        /** HBackPorch   */
        3,        /** VsyncWidth   */
        10,        /** VFrontPorch  */
        30,        /** VBackPorch   */
};
#endif
#if 0
lcdCfg_t hdmiCfg = {
        1280,        /** LcdWidth    */
        720,       /** LcdHeight    */
        74250000,  /** LcdPclk      */
        81,        /** HsyncWidth   */
        73,        /** HFrontPorch  */
        217,        /** HBackPorch   */
		6,        /** VsyncWidth   */
		3,       /** VFrontPorch  */
		22,        /** VBackPorch   */
};
#endif

/**
 * \brief Function to display color and read user input or
 *        wait for some time after HDMI display
 *
 *  \param    input [IN]   Flag to indicate wait for user input
 *  \param    color [IN]   Value of Color
 *  \param    name  [IN]   Name of the pattern displayed on HDMI
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
static TEST_STATUS displayColor(uint8_t input, int color, char *name)
{
	uint8_t c = 0;

	if(input == FALSE)
	{
		platform_write("\rDisplaying %s... WAIT  Press 'y' if %s is displayed, any other key for failure: ", name, name);

		hdmiColorDisplay(&hdmiCfg, color);

		platform_read(&c, 1);
		if((c == 'y') || (c == 'Y'))
		{
			platform_write("\rDisplay %s           PASS\n", name);
		}
		else
		{
			platform_write("\rDisplay %s           FAIL\n", name);
			return (TEST_FAIL);
		}
	}
	else
	{
		platform_write("\rDisplaying %s...\n", name);
		hdmiColorDisplay(&hdmiCfg, color);
    	delay_secs(HDMI_DISPLAY_DELAY);
	}

	return (TEST_PASS);
}

/**
 *  \brief    This function performs HDMI, DSS initialization
 *            and verifies HDMI display
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_hdmi_test(void *testArgs)
{
	hdmiTestArgs_t *args = (hdmiTestArgs_t *)testArgs;
	TEST_STATUS status = TEST_PASS;
	uint8_t c = 0;

#if 0
	uint8_t edid[HDMI_EDID_MAX_LENGTH];
#endif
	uint8_t ver;

	/* HDMI i2c init */
	hdmiInit();

	platform_write("Running HDMI Device Detect Test\n");

	status = hdmiProbeChipVersion(&ver);
	if (status)
	{
		platform_write("ERROR: Reading HDMI version Id\n");
		platform_write("HDMI Device Detect Test Failed\n");
		goto err;
	}
	else
	{
		if (ver != HDMI_CHIPID_902x)
		{
			platform_write("Not a valid verId: 0x%x\n", ver);
			platform_write("HDMI Device Detect Test Failed\n");
			goto err;
		}
		else
		{
			platform_write("sil9022 HDMI Chip version ="
						   " %x\n", ver);
		}
	}

	platform_write("HDMI Device Detect Test Passed\n");

	status = hdmiConnect();
	if (status)
	{
		platform_write("Error in Enabling power for HDMI\n");
        goto err;
	}

	status = hdmiHwEnable();
	if (status)
	{
		platform_write("Error during enabling HDMI\n");
        goto err;
	}
#if 0
	status = hdmiReadEdid(edid, 3);
	if (status)
	{
		platform_write("ERROR: Reading HDMI EDID.\n");
        goto err;
	}
	else
	{
		platform_write("edid[0] = 0x%x edid[1] = 0x%x edid[2]"
					   " = 0x%x\n",edid[0], edid[1], edid[2]);
	}
#endif
	status = hdmiPanelInit(&hdmiCfg);
	if (status != DSS_RET_OK)
	{
		platform_write("ERROR: Initializing HDMI Panel\n");
        goto err;
	}

	if(args->autoRun == FALSE)
	{
		platform_write("\rDisplaying Colorbar... WAIT  Press 'y' if Colorbar is displayed, any other key for failure: ");
	}
	else
	{
		platform_write("\rDisplaying Colorbar...\n");
	}

	hdmiColorBarDisplay(&hdmiCfg);

	if(args->autoRun == FALSE)
	{
		platform_read(&c, 1);
		if((c == 'y') || (c == 'Y'))
		{
			platform_write("\rDisplay Colorbar           PASS\n");
		}
		else
		{
			platform_write("\rDisplay Colorbar           FAIL\n");
			return (TEST_FAIL);
		}
	}
	else
	{
    	delay_secs(HDMI_DISPLAY_DELAY);
	}

	if(displayColor(args->autoRun, HDMI_WHITE, "WHITE"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, HDMI_BLUE, "BLUE"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, HDMI_GREEN, "GREEN"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, HDMI_RED, "RED"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, HDMI_PURPLE, "PURPLE"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, HDMI_PINK, "PINK"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, HDMI_BLACK, "BLACK"))
	{
		return (TEST_FAIL);
	}

	if(displayColor(args->autoRun, HDMI_YELLOW, "YELLOW"))
	{
		return (TEST_FAIL);
	}

err :	return (status);
}

/**
 * \brief This function performs hdmi test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS hdmiTest(void *testArgs)
{
	TEST_STATUS testStatus = TEST_PASS;
	hdmiTestArgs_t *args = (hdmiTestArgs_t *)testArgs;

	platform_write("\n***********************\n");
	platform_write(  "       HDMI Test       \n");
	platform_write(  "***********************\n");

	testStatus = run_hdmi_test(args);
	if(testStatus != TEST_PASS)
	{
		platform_write("\nHDMI Test Failed!\n");
	}
	else
	{
		//platform_write("\nHDMI Test Passed!\n");
	}

	platform_write("\nHDMI Tests Completed!!\n");
	platform_write("\n-----------------X-----------------\n\n\n");

	return (testStatus);

} // hdmiTest

/* Nothing past this point */
