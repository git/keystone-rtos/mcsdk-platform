/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
*
* \file ddr_test.c
*
* \brief This file contains ddr test functions.
*
******************************************************************************/

#include "ddr_test.h"
#include "time.h"

/**
 * \brief This function compares the two memory regions
 *
 * \param	buf1  [IN]  - starting address of region1
 * \param	buf2  [IN]  - starting address of region2
 * \param	count [IN]  - size to test the memory
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
int16_t compareArea(uint32_t *bufa, uint32_t *bufb, uint16_t count)
{
	int32_t retVal = 0;
	uint32_t index;
	uint32_t *p1 = bufa;
	uint32_t *p2 = bufb;

	for (index = 0; index < count; index++, p1++, p2++)
	{
		if (*p1 != *p2)
		{
			platform_write("FAILURE: 0x%08lx != 0x%08lx at offset 0x%08lx.\n",
				           *p1, *p2, (index * sizeof(uint32_t)));
			retVal = -1;
		}
	}

	return (retVal);
}

/**
 * \brief This function performs the pattern test
 *
 * \param	buf1  [IN]  - starting address of region1
 * \param	buf2  [IN]  - starting address of region2
 * \param	count [IN]  - size to test the memory
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
int16_t patternTest(uint32_t *bufa, uint32_t *bufb, uint32_t count)
{
	uint32_t *p1 = bufa;
	uint32_t *p2 = bufb;
	uint32_t iteration;
	uint32_t pattern;
	uint32_t index;

	for (iteration = 0; iteration < 64; iteration++)
	{
		pattern = (iteration % 2) == 0 ? PATTERN : ~PATTERN;
		p1 = (uint32_t *) bufa;
		p2 = (uint32_t *) bufb;

		for (index = 0; index < count; index++)
		{
			if((index%STATUS_MSG_FREQ) == 0)
			{
				platform_write("patternTest: Iteration %d At Location 1 - 0x%X & Location 2 - 0x%X\n", iteration, p1, p2);
			}

			*p1++ = *p2++ = (index % 2) == 0 ? pattern : ~pattern;
		}

		if (compareArea(bufa, bufb, count))
		{
		    return (-1);
		}
	}

	return (0);
}

/**
 * \brief This function performs walking 0's test
 *
 * \param	buf1  [IN]  - starting address of region1
 * \param	buf2  [IN]  - starting address of region2
 * \param	count [IN]  -  size to test the memory
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
int16_t walking0s(uint32_t *bufa, uint32_t *bufb, uint32_t count)
{
	uint32_t *p1 = bufa;
	uint32_t *p2 = bufb;
	uint32_t shift;
	uint32_t index;

	for (shift = 0; shift < UL_LEN * 2; shift++)
	{
		p1 = (uint32_t *) bufa;
		p2 = (uint32_t *) bufb;

		for (index = 0; index < count; index++)
		{
			if((index%STATUS_MSG_FREQ) == 0)
			{
				if (shift < UL_LEN)
				{
					platform_write("walking0s: Value - 0x%X At Location 1 - 0x%X & Location 2 - 0x%X\n", (UL_ONEBITS ^ (ONE << shift)), p1, p2);
				}
				else
				{
					platform_write("walking0s: Value - 0x%X At Location 1 - 0x%X & Location 2 - 0x%X\n", (UL_ONEBITS ^ (ONE << (UL_LEN * 2 - shift - 1))), p1, p2);
				}
			}

			if (shift < UL_LEN)
			{ /* Walk it up. */
				//*p1++ = *p2++ = ONE << shift;
				*p1++ = *p2++ = UL_ONEBITS ^ (ONE << shift);
			}
			else
			{ /* Walk it back down. */
				//*p1++ = *p2++ = ONE << (UL_LEN * 2 - shift - 1);
				*p1++ = *p2++ = UL_ONEBITS ^ (ONE << (UL_LEN * 2 - shift - 1));
			}
		}

		if (compareArea(bufa, bufb, count))
		{
			return (-1);
		}
	}

	return (0);
}

/**
 * \brief This function performs walking 1's test
 *
 * \param	buf1  [IN] - starting address of region1
 * \param	buf2  [IN] - starting address of region2
 * \param	count [IN] - size  to test the memory
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
int16_t walking1s(uint32_t *bufa, uint32_t *bufb, uint32_t count)
{
	uint32_t *p1 = bufa;
	uint32_t *p2 = bufb;
	uint32_t shift;
	uint32_t index;

	for (shift = 0; shift < UL_LEN * 2; shift++)
	{
		p1 = (uint32_t *) bufa;
		p2 = (uint32_t *) bufb;

		for (index = 0; index < count; index++)
		{
			if((index%STATUS_MSG_FREQ) == 0)
			{
				if (shift < UL_LEN)
				{
					platform_write("walking1s: Value - 0x%X At Location 1 - 0x%X & Location 2 - 0x%X\n", (ONE << shift), p1, p2);
				}
				else
				{
					platform_write("walking1s: Value - 0x%X At Location 1 - 0x%X & Location 2 - 0x%X\n", (ONE << (UL_LEN * 2 - shift - 1)), p1, p2);
				}
			}

			if (shift < UL_LEN)
			{ /* Walk it up. */
				//*p1++ = *p2++ = UL_ONEBITS ^ (ONE << shift);
				*p1++ = *p2++ = (ONE << shift);
			}
			else
			{ /* Walk it back down. */
				//*p1++ = *p2++ = UL_ONEBITS ^ (ONE << (UL_LEN * 2 - shift - 1));
				*p1++ = *p2++ = (ONE << (UL_LEN * 2 - shift - 1));
			}
		}

		if (compareArea(bufa, bufb, count))
		{
			return (-1);
		}
	}

	return (0);
}

/**
 *  \brief    Executes DDR tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_ddr_test(void *testArgs)
{
	int32_t ret;
	TEST_STATUS status;
	uint32_t *region1, *region2;
	uint32_t count;
	ddrTestArgs_t *args = (ddrTestArgs_t *)testArgs;

	region1 = (uint32_t *)args->region1;
	region2 = (uint32_t *)args->region2;
	count   = args->count;

	platform_write("Walking 1s\n");

	ret = walking1s(region1, region2, count);
	if (ret == -1)
	{
		platform_write("Walking 1s failed\n");
		status = TEST_FAIL;
	}
	else
	{
		platform_write("Walking 1s success\n");
		status = TEST_PASS;
	}

	platform_write("Walking 0s\n");
	ret = walking0s(region1, region2, count);

	if (ret == -1)
	{
		platform_write("Walking 0s failed\n");
		status = TEST_FAIL;
	}
	else
	{
		platform_write("Walking 0s success\n");
		status = TEST_PASS;
	}

	platform_write("Pattern test\n");
	ret = patternTest(region1, region2, count);

	if (ret == -1)
	{
		platform_write("Pattern test failed\n");
		status = TEST_FAIL;
	}
	else
	{
		platform_write("Pattern test success\n");
		status = TEST_PASS;
	}

	return (status);
}

/**
 * \brief This function performs ddr test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS ddrMemTest (void *testArgs)
{
	TEST_STATUS   testStatus;
	uint32_t      size;
	uint32_t      randNum;
	uint8_t       wrongInput = 0;
	uint16_t      randFail = 0;
	ddrTestArgs_t *args = (ddrTestArgs_t *)testArgs;

	platform_write("\n**********************\n");
	platform_write(  "       DDR Test       \n");
	platform_write(  "**********************\n");

	if(args->autoRun == FALSE)
	{
		/* Read test inputs from user if needed */

		do
		{
			platform_write("\nSize in bytes(hex) (0x0 < (x) < 0x80000000):");

			if (scanf("%x", &size) != 1)
			{
				platform_write("Wrong input \n");
				wrongInput = 1;
				break;
			}

			platform_write("size = 0x%x\n",size);

		} while(size > 0x80000000);

		do
		{
			srand(time(NULL));
			randNum = rand();
			platform_write("rand =0x%x \n", randNum);

			args->region1 = DDR_START + randNum;
			args->region2 = DDR_1GB_START + randNum;

			platform_write("region1 = 0x%x\n", args->region1);
			platform_write("region2 = 0x%x\n", args->region2);

			if (randFail > RAND_RETRIES)
			{
				args->region1 = DDR_1GB_END - size/2 + 1;
				args->region2 = DDR_2GB_END - size/2 + 1;
			}

			if ((args->region1 + size/2) > DDR_1GB_START)
			{
				randFail++;
				continue;
			}
			else
			{
				randFail = 0;
				break;
			}

		} while(!wrongInput && !randFail);

		if(wrongInput)
		{
			platform_write("Wrong input Execute again\n");
			return (TEST_FAIL);
		}
		else
		{
			platform_write("DDR Testing - Size %d bytes\n", size);
		}

		args->count = size/8;
	}

	testStatus = run_ddr_test(args);
	if(testStatus != TEST_PASS)
	{
		platform_write("\nDDR Test Failed!\n");
	}
	else
	{
		platform_write("\nDDR Test Passed!\n");
	}

	platform_write("\nDDR Tests Completed!!\n");
	platform_write("\n-----------------X-----------------\n\n\n");

	return (testStatus);
}

/* Nothing past this point */

