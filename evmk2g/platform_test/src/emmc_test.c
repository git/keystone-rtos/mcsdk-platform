/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
*
* \file emmc_test.c
*
* \brief This file contains emmc test functions.
*
******************************************************************************/

#include "emmc_test.h"
#include "platform_internal.h"

//TODO: Need to update the test to use platform_device_xxx APIs after bring-up
uint8_t emmcWriteBuf[EMMC_TESTSECTORS * SECTOR_SIZE];
uint8_t emmcReadBuf[EMMC_TESTSECTORS * SECTOR_SIZE];
uint8_t emmcSaveBuf[EMMC_TESTSECTORS * SECTOR_SIZE];

/**
 * \brief This function performs emmc read/write test
 *
 * \param mmcCard  - MMC handle
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
static TEST_STATUS emmc_access_test (mmchsCardInfo *mmcCard)
{
    int32_t retVal;
    int32_t block = EMMC_STARTSECTOR;
    uint32_t index;

    for(block = EMMC_STARTSECTOR; block < (EMMC_TESTSECTOR_COUNT + EMMC_STARTSECTOR);
        block += EMMC_TESTSECTORS)
    {
        memset (emmcWriteBuf, EMMC_TEST_PATTERN,
                sizeof(uint8_t) * (EMMC_TESTSECTORS * SECTOR_SIZE));
        memset (emmcReadBuf, 0x00, sizeof(uint8_t) * (EMMC_TESTSECTORS * SECTOR_SIZE));

#ifdef ENABLE_MEMORY_BACKUP
        platform_write ("Reading the Current Contents of 0x%x to emmcSaveBuf\n", block);
        retVal = mmchsBlockRead (mmcCard, block,
                                 EMMC_TESTSECTORS, emmcSaveBuf);
        if (retVal != MMCHS_RET_OK)
        {
            platform_write ("\tUnable to Read Block data at 0x%x\n",block);
            return (TEST_FAIL);
        }
#endif

        platform_write ("\teMMC: Writing a pattern to Block %d....", block);
        retVal = mmchsBlockWrite (mmcCard, block,
        		                  EMMC_TESTSECTORS, emmcWriteBuf);

        if (retVal != MMCHS_RET_OK)
        {
            platform_write ("Unable to Write Sector at 0x%x\n",block);
            return (TEST_FAIL);
        }

        platform_write ("Reading back the pattern to emmcReadBuf\n");
        retVal = mmchsBlockRead (mmcCard, block,
                                 EMMC_TESTSECTORS, emmcReadBuf);
        if (retVal != MMCHS_RET_OK)
        {
            platform_write ("\tUnable to Read Block data at 0x%x\n",block);
            return (TEST_FAIL);
        }

#ifdef ENABLE_MEMORY_BACKUP
        platform_write ("Writing back the original contents from emmcSaveBuf\n");
        retVal = mmchsBlockWrite (mmcCard, block, \
                                  EMMC_TESTSECTORS, emmcSaveBuf);

        if (retVal != MMCHS_RET_OK)
        {
            platform_write ("Unable to Write Sector at 0x%x\n",block);
            return (TEST_FAIL);
        }
#endif

         for ( index = 0 ; index < (EMMC_TESTSECTORS * SECTOR_SIZE) ; index++ )
         {
             if (emmcWriteBuf[index] != emmcReadBuf[index])
             {
                 platform_write ("Data comparison failed at %d\n",index);
                 return (TEST_FAIL);
             }
         }
         block = EMMC_TESTSECTOR_COUNT + EMMC_STARTSECTOR;
    }

    return (TEST_PASS);
}

/**
 * \brief This function performs emmc erase test
 *
 * \param mmcCard  - MMC handle
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
static TEST_STATUS emmc_erase_test (mmchsCardInfo *mmcCard)
{
    int32_t retVal;
    int32_t block = EMMC_ERASE_START;
    uint32_t index;

    memset (emmcWriteBuf, EMMC_TEST_PATTERN,
            sizeof(uint8_t) * (EMMC_TESTSECTORS * SECTOR_SIZE));
    memset (emmcReadBuf, 0x00, sizeof(uint8_t) * (EMMC_TESTSECTORS * SECTOR_SIZE));

#ifdef ENABLE_MEMORY_BACKUP
    platform_write ("Reading the current contents of 0x%x to emmcSaveBuf\n", block);
    retVal = mmchsBlockRead (mmcCard, block,
                             EMMC_TESTSECTORS, emmcSaveBuf);
    if (retVal != MMCHS_RET_OK)
    {
        platform_write ("\tUnable to Read Block data at 0x%x\n",block);
        return (TEST_FAIL);
    }
#endif

    platform_write ("\teMMC: Writing a pattern to Block %d....", block);
    retVal = mmchsBlockWrite (mmcCard, block,
                             EMMC_TESTSECTORS, emmcWriteBuf);

    if (retVal != MMCHS_RET_OK)
    {
        platform_write ("Unable to Write Sector at 0x%x\n",block);
        return (TEST_FAIL);
    }

    platform_write ("Reading back the pattern 0XAA to emmcReadBuf\n");
    retVal = mmchsBlockRead (mmcCard, block,
                             EMMC_TESTSECTORS, emmcReadBuf);

    if (retVal != MMCHS_RET_OK)
    {
        platform_write ("\tUnable to Read Block data at 0x%x\n",block);
        return (TEST_FAIL);
    }

    platform_write ("Erase the blocks from %d to %d\n", EMMC_ERASE_START, EMMC_ERASE_END);
    retVal = mmchsBlockErase (mmcCard, EMMC_ERASE_START, EMMC_ERASE_END);
    if (retVal != MMCHS_RET_OK)
    {
    	platform_write ("\tBlock Erase Failed\n");
        return (TEST_FAIL);
    }

    platform_delay(10000);

    retVal = mmchsBlockRead (mmcCard, block,
                             EMMC_TESTSECTORS, emmcReadBuf);
    if (retVal != MMCHS_RET_OK)
    {
    	platform_write ("\tRead after Erase Failed\n");
        return (TEST_FAIL);
    }

#ifdef ENABLE_MEMORY_BACKUP
    platform_write ("Writing back the original contents from emmcSaveBuf\n");
    retVal = mmchsBlockWrite (mmcCard, block, \
                              EMMC_TESTSECTORS, emmcSaveBuf);

    if (retVal != MMCHS_RET_OK)
    {
        platform_write ("Unable to Write Sector at 0x%x\n",block);
        return (TEST_FAIL);
    }
#endif

	platform_write ("Compare if the erased blocks are 0 from %d to %d\n", EMMC_ERASE_START, EMMC_ERASE_END);

	for ( index = 0 ; index < (EMMC_TESTSECTORS * SECTOR_SIZE) ; index++ )
	{
		if (emmcReadBuf[index] != 0)
		{
			platform_write ("Data comparison failed at %d\n",index);
			return (TEST_FAIL);
		}
	}

	platform_write ("Data comparison passed !!\n",index);

	return (TEST_PASS);
}

/**
 *  \brief    Executes eMMC tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_emmc_test(void *testArgs)
{
    mmchsInfo* mmcInfo;
    mmchsCardInfo *mmcCard;
    TEST_STATUS retVal = TEST_PASS;

	platform_write("\nRunning eMMC Chip Detect Test\n");

    mmcInfo = mmchsInit(MMCHS_MMC_INST);
    if ((mmcInfo == NULL) || (mmcInfo->card == NULL))
    {
        platform_write ("eMMC Test: Error Initializing the eMMC Card \n");
        return (TEST_FAIL);
    }

    mmcCard = mmcInfo->card;
    if (mmcCard->cardType == MMCHS_MMC)
    {
        platform_write ("\teMMC Chip Identified\n");
    }
    else
    {
        platform_write ("\teMMC Chip not Identified\n");
        platform_write("\neMMC Chip Detect Test Failed!\n");
        return (TEST_FAIL);
    }

	platform_write ("\teMMC Card Info:\n");
    mmchsPrintCardInfo (mmcCard);

    platform_write("\neMMC Chip Detect Test Passed!\n");

    platform_write("\nRunning eMMC Memory Access Test\n");

	retVal = emmc_access_test(mmcCard);
	if(retVal != TEST_PASS)
	{
		platform_write("\neMMC Memory Access Test Failed!\n");
		return (retVal);
	}

	platform_write("\neMMC Memory Access Test Passed!\n");

	platform_write("\nRunning eMMC Block Erase Test\n");
	retVal = emmc_erase_test(mmcCard);
	if(retVal != TEST_PASS)
	{
		platform_write("\neMMC Block Earse Test Failed!\n");
		return (retVal);
	}

	platform_write("\neMMC Block Earse Test Passed!\n");

    return (retVal);
}

/**
 * \brief This function performs emmc test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS emmcTest(void *testArgs)
{
    TEST_STATUS testStatus;
	emmcTestArgs_t *args = (emmcTestArgs_t *)testArgs;

	platform_write("\n***********************\n");
	platform_write(  "       eMMC Test       \n");
	platform_write(  "***********************\n");

	if(args->autoRun == FALSE)
	{
		/* Read test inputs from user if needed */
	}

	testStatus = run_emmc_test(args);
	if(testStatus != TEST_PASS)
	{
		platform_write("\neMMC Test Failed!\n");
	}
	else
	{
		platform_write("\neMMC Test Passed!\n");
	}

	platform_write("\neMMC Tests Completed!!\n");
	platform_write("\n-----------------X-----------------\n\n\n");

    return(testStatus);

} // emmcTest

/* Nothing past this point */

