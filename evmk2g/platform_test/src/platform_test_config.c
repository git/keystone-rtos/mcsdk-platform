/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 *  \file   platform_test_config.c
 *
 *  \brief  This file contains global configurations for diagnostic tests
 *
 */

#include "platform_test.h"
#include "ddr_test.h"
#include "dcan_test.h"
#include "sd_test.h"
#include "emmc_test.h"
#include "uart_test.h"
#include "touch_test.h"
#include "qspi_flash_test.h"
#include "spi_nor_test.h"
#include "i2c_eeprom_test.h"
#include "nand_test.h"
#include "hdmi_test.h"
#include "lcd_test.h"
#include "audio_clk_test.h"

/* Default DDR test arguments */
ddrTestArgs_t gDdrTestArgs =
{
	PLATFORM_TEST_AUTO_RUN,    // Flag to enable/disable test auto run
	0x80000000,                // Address of DDR region 1
	0xC0000000,                // Address of DDR region 2
	0x10000000                 // Number of DDR test locations
};

/* Default NAND flash test arguments */
nandTestArgs_t gNandTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,
    0,                        // NAND flash block number to execute read/write
    BYTES_PER_PAGE,           // Length in bytes to read/write from NAND flash
    PLATFORM_DEVID_MT29F2G16ABAFA,  // NAND flash device ID
    NAND_FLASH_TEST_PATTERN1        // Test Pattern

};

/* Default QSPI flash test arguments */
qspiFlashTestArgs_t gQspiFlashTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,
    0,                        // QSPI flash sector number to execute read/write
    QSPI_FLASH_PAGE_SIZE,     // Length in bytes to read/write from QSPI flash
    PLATFORM_DEVID_QSPIFLASH_S25FL512S,  // QSPI flash device ID
    QSPI_FLASH_TEST_PATTERN1             // Test Pattern
};

/* Default SPI NOR flash test arguments */
spiNorTestArgs_t gSpiNorTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,   // Flag to enable/disable test auto run
    0,                        // NOR flash sector number to execute read/write
    SPI_NOR_SECTOR_SIZE,      // Length in bytes to read/write from NOR flash
    PLATFORM_DEVID_NORN25Q128A13ESF40F,  // NOR flash device ID
    NOR_FLASH_TEST_PATTERN1              // Test Pattern
};

/* Default I2C EEPROM test arguments */
i2cEepromTestArgs_t gI2cEepromTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,  // Auto run selection flag
    0,                       // EEPROM offset to execute read/write
    64,                      // Length in bytes to read/write from EEPROM
    0,                       // EEPROM I2C port number
    PLATFORM_DEVID_EEPROM50  // Slave address of I2C device
};

/* Default EMMC test arguments */
emmcTestArgs_t gEmmcTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,
};

/* Default SD test arguments */
sdTestArgs_t gSdTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,
};

/* Default PCIe test arguments */
pcieTestArgs_t gPcieTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,
};

/* Default DCAN test arguments */
dcanTestArgs_t gDcanTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,
};

/* Default HDMI test arguments */
hdmiTestArgs_t gHdmiTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,
};

/* Default LCD test arguments */
lcdTestArgs_t gLcdTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,
};

/* Default touch test arguments */
touchTestArgs_t gTouchTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,
};

/* Default Ethernet test arguments */
ethernetTestArgs_t gEthernetTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,
};

/* Default UART test arguments for DB9 port */
uartTestArgs_t gUartTestArgs_db9 =
{
    1,  // Flag to enable/disable test auto run
    115200,                  // UART baudrate to run the test
    PLATFORM_UART_PORT_0,    // UART port number
    UART_ID_DB9              // UART ID - DB9
};

/* Default UART test arguments for UART to USB port */
uartTestArgs_t gUartTestArgs_uart2usb =
{
    1,  // Flag to enable/disable test auto run
    115200,                  // UART baudrate to run the test
    PLATFORM_UART_PORT_0,    // UART port number
    UART_ID_UART2USB         // UART ID - UART to USB
};

/* Default UART test arguments for serial header port */
uartTestArgs_t gUartTestArgs_serhead =
{
    1,  // Flag to enable/disable test auto run
    115200,                  // UART baudrate to run the test
    PLATFORM_UART_PORT_2,    // UART port number
    UART_ID_SERIAL_HEADER    // UART ID - 4 pin serial header
};

/* Default Audio clock test arguments */
audioClkTestArgs_t gAudioClkTestArgs =
{
    PLATFORM_TEST_AUTO_RUN,  // Flag to enable/disable test auto run
    PLATFORM_TEST_SYS_CLK,   // CPU clock value
    10,                      // CPTS TS comparator clock rate
    0xFFFF,                  // Number of TS comparator output clock
    I2C_PORT_1,              // CS2000 I2C port number
    0x4F,                    // I2C slave address for CS2000
    SPI3,                    // DAC 8550 SPI port number
    0,                       // DAC 8550 SPI chip select
    SPI_MAX_FREQ             // SPI clock value
};

/* Platform diagnostic test table */
diagTestTable_t gDiagTestTable[PLATFORM_TEST_COUNT] =
{
	{"Auto Run All Tests", NULL, NULL, 0, 0},
	{"Run All Memory Tests", NULL, NULL, 0, 0},
	{"DDR3 Test", ddrMemTest, &gDdrTestArgs, 0, 0},
	{"NAND Flash Test", nandTest, &gNandTestArgs, 0, 0},
	{"QSPI Flash Test", qspiFlashTest, &gQspiFlashTestArgs, 0, 0},
	{"SPI NOR Flash Test", spiNorTest, &gSpiNorTestArgs, 0, 0},
	{"I2C EEPROM Test", i2cEepromTest, &gI2cEepromTestArgs, 0, 0},
	{"eMMC Test", emmcTest, &gEmmcTestArgs, 0, 0},
	{"uSD Test", sdTest, &gSdTestArgs, 0, 0},
	{"PCIe Test", run_external_tests, &gPcieTestArgs, 0, 0},
	{"CAN Bus Test", dcanTest, &gDcanTestArgs, 0, 0},
	{"MLB Test", run_external_tests, NULL, 0, 0},
	{"Audio Clock Test", audioClkTest, &gAudioClkTestArgs, 0, 0},
	{"HDMI Test", hdmiTest, &gHdmiTestArgs, 0, 0},
	{"LCD Test", lcdTest, &gLcdTestArgs, 0, 0},
	{"Touch Test", touchTest, &gTouchTestArgs, 0, 0},
	{"Ethernet Test", run_external_tests, &gEthernetTestArgs, 0, 0},
	{"UART Test - 'DB9'", uartTest, &gUartTestArgs_db9, 0, 0},
	{"UART Test - 'UART to USB'", uartTest, &gUartTestArgs_uart2usb, 0, 0},
	{"UART Test - 'Serial Hdr'", uartTest, &gUartTestArgs_serhead, 0, 0},
	{"COM8 Test", run_external_tests, NULL, 0, 0},
	{"Audio Test", run_external_tests, NULL, 0, 0},
	{"Audio DC Test", run_external_tests, NULL, 0, 0},
};

/* Nothing past this point */
