/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
*
*  \file  spi_nor_test.c
*
*  \brief This file contains spi nor test functions.
*
******************************************************************************/

#include "spi_nor_test.h"

/**
 *  \brief    Executes SPI NOR flash chip detect test
 *
 *  \param    p_device [IN]   NOR flash device handle
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS spi_nor_chip_detect_test(PLATFORM_DEVICE_info *p_device,
                                            void *testArgs)
{
    spiNorTestArgs_t *args = (spiNorTestArgs_t *)testArgs;

    p_device = platform_device_open(args->devId, 0);
    if (p_device == NULL)
    {
       platform_write("SPI NOR Flash Test: Could Not Open NOR Device; errno = 0x%x \n", platform_errno);
       return (TEST_FAIL);
    }

    platform_write("Device Id - 0x%X\n", p_device->device_id);
    platform_write("Manufacturer Id - 0x%X\n", p_device->manufacturer_id);
    platform_write("Device Width - %d\n", p_device->width);
    platform_write("Block Count - %d\n", p_device->block_count);
    platform_write("Page Count - %d\n", p_device->page_count);
    platform_write("Page Size - %d\n", p_device->page_size);

    platform_device_close(p_device->handle);

    return (TEST_PASS);
}

/**
 *  \brief    Executes SPI NOR flash memory read/write test
 *
 *  \param    p_device [IN]   NOR flash device handle
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS spi_nor_memory_access_test(PLATFORM_DEVICE_info *p_device,
                                              void *testArgs)
{
    spiNorTestArgs_t *args = (spiNorTestArgs_t *)testArgs;
    Platform_STATUS  status;
    TEST_STATUS      testStatus = TEST_FAIL;

    uint32_t address;
    uint8_t  *norWrBuf;
    uint8_t  *norRdBuf;
#ifdef ENABLE_MEMORY_BACKUP
    uint8_t  *norBkpBuf;
#endif

    norWrBuf = malloc(args->blockLen);
    norRdBuf = malloc(args->blockLen);

    if((norWrBuf == NULL) ||
       (norRdBuf == NULL))
    {
       platform_write("SPI NOR Flash Test: Buffer Allocation Failed!\n");
       return (TEST_FAIL);
    }

#ifdef ENABLE_MEMORY_BACKUP
    norBkpBuf = malloc(args->blockLen);
    if(norBkpBuf == NULL)
    {
        platform_write("SPI NOR Flash Test: Buffer Allocation Failed!\n");
        free(norWrBuf);
        free(norRdBuf);
        return (TEST_FAIL);
    }
#endif

    /* Initialize the test pattern */
    memset(norWrBuf, args->testPattern, args->blockLen);
    memset(norRdBuf, 0, args->blockLen);

    address = ((args->sectNum) * (SPI_NOR_SECTOR_SIZE));

#ifdef ENABLE_MEMORY_BACKUP
    status = platform_device_read(p_device->handle, address,
                                  norBkpBuf, args->blockLen);
    if(status != Platform_EOK)
    {
        platform_write("SPI NOR Flash Test: Read Failed  errno = 0x%x\n", platform_errno);
        goto free;
    }
#endif

    status = platform_device_erase_block (p_device->handle, args->sectNum);
    if(status != Platform_EOK)
    {
        platform_write("SPI NOR Flash Test: Erase Failed for Sector %d  errno = 0x%x\n",
                       args->sectNum, platform_errno);
    }

    status = platform_device_write(p_device->handle, address,
                                   norWrBuf, args->blockLen);
    if(status != Platform_EOK)
    {
        platform_write("SPI NOR Flash Test: Write Test Data Failed  errno = 0x%x\n", platform_errno);
        goto free;
    }

    status = platform_device_read(p_device->handle, address,
                                  norRdBuf, args->blockLen);
    if(status != Platform_EOK)
    {
        platform_write("SPI NOR Flash Test: Read Test Data Failed  errno = 0x%x\n", platform_errno);
        goto free;
    }

    if (memcmp(norWrBuf, norRdBuf, args->blockLen) != 0)
    {
        platform_write("SPI NOR Flash Test: Data Verification Failed\n");
        testStatus = TEST_FAIL;
    }
    else
    {
        platform_write("SPI NOR Flash Test: Data Verification Passed\n");
        testStatus = TEST_PASS;
    }

#ifdef ENABLE_MEMORY_BACKUP
    status = platform_device_erase_block (p_device->handle, args->sectNum);
    if(status != Platform_EOK)
    {
        platform_write("SPI NOR Flash Test: Erase Failed  errno = 0x%x\n",
                       platform_errno);
        testStatus = TEST_FAIL;
    }

    status = platform_device_write(p_device->handle, address,
                                   norBkpBuf, args->blockLen);
    if(status != Platform_EOK)
    {
        platform_write("SPI NOR Flash Test: Write Back Original Data Failed  errno = 0x%x\n",
                       platform_errno);
        testStatus = TEST_FAIL;
    }
#endif

free:
    free(norWrBuf);
    free(norRdBuf);
#ifdef ENABLE_MEMORY_BACKUP
    free(norBkpBuf);
#endif

    return (testStatus);
}

/**
 *  \brief    Executes SPI NOR flash memory erase test
 *
 *  \param    p_device [IN]   NOR flash device handle
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS spi_nor_erase_test(PLATFORM_DEVICE_info *p_device,
                                      void *testArgs)
{
    spiNorTestArgs_t *args = (spiNorTestArgs_t *)testArgs;
    Platform_STATUS  status;
    TEST_STATUS      testStatus = TEST_FAIL;

    uint32_t index;
    uint32_t address;
    uint8_t  *norRdBuf;
#ifdef ENABLE_MEMORY_BACKUP
    uint8_t  *norBkpBuf;
#endif

    norRdBuf = malloc(SPI_NOR_SECTOR_SIZE);
    if(norRdBuf == NULL)
    {
       platform_write("SPI NOR Flash Test: Buffer Allocation Failed!\n");
       return (testStatus);
    }

#ifdef ENABLE_MEMORY_BACKUP
    norBkpBuf = malloc(SPI_NOR_SECTOR_SIZE);
    if(norBkpBuf == NULL)
    {
        platform_write("SPI NOR Flash Test: Buffer Allocation Failed!\n");
        free(norRdBuf);
        return (testStatus);
    }
#endif

    memset(norRdBuf, 0, SPI_NOR_SECTOR_SIZE);

    address = ((args->sectNum) * (SPI_NOR_SECTOR_SIZE));

#ifdef ENABLE_MEMORY_BACKUP
    status = platform_device_read(p_device->handle, address,
                                  norBkpBuf, SPI_NOR_SECTOR_SIZE);
    if(status != Platform_EOK)
    {
        platform_write("SPI NOR Flash Test: Read Failed  errno = 0x%x\n", platform_errno);
        goto free;
    }
#endif

    status = platform_device_erase_block (p_device->handle, args->sectNum);
    if(status != Platform_EOK)
    {
        platform_write("SPI NOR Flash Test: Erase Failed for Sector %d  errno = 0x%x\n",
                        args->sectNum, platform_errno);
        goto free;
    }

    status = platform_device_read(p_device->handle, address,
                                  norRdBuf, SPI_NOR_SECTOR_SIZE);
    if(status != Platform_EOK)
    {
        platform_write("SPI NOR Flash Test: Read Test Data Failed  errno = 0x%x\n", platform_errno);
        goto free;
    }

    for (index = 0; index < SPI_NOR_SECTOR_SIZE; index++)
    {
        if(norRdBuf[index] != 0xFF)
        {
            platform_write("SPI NOR Flash Test: Erase Data Verification Failed\n");
            testStatus = TEST_FAIL;
            break;
        }
    }

    if(index == SPI_NOR_SECTOR_SIZE)
    {
        platform_write("SPI NOR Flash Test: Erase Data Verification Passed\n");
        testStatus = TEST_PASS;
    }

#ifdef ENABLE_MEMORY_BACKUP
    status = platform_device_write(p_device->handle, address,
                                   norBkpBuf, SPI_NOR_SECTOR_SIZE);
    if(status != Platform_EOK)
    {
        platform_write("SPI NOR Flash Test: Write Back Original Data Failed  errno = 0x%x\n",
                       platform_errno);
        testStatus = TEST_FAIL;
    }
#endif

free:
    free(norRdBuf);
#ifdef ENABLE_MEMORY_BACKUP
    free(norBkpBuf);
#endif

    return (testStatus);
}

/**
 *  \brief    Executes SPI NOR flash tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_spi_nor_test(void *testArgs)
{
    PLATFORM_DEVICE_info *p_device = NULL;
    spiNorTestArgs_t     *args = (spiNorTestArgs_t *)testArgs;
    TEST_STATUS          testStatus = TEST_FAIL;

    platform_write("\nRunning SPI NOR Flash Chip Detect Test\n");
    testStatus = spi_nor_chip_detect_test(p_device, args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nSPI NOR Flash Chip Detect Test Failed\n");
        goto TEST_END;
    }
    else
    {
        platform_write("\nSPI NOR Flash Chip Detect Test Passed\n");
    }

    p_device = platform_device_open(args->devId, 0);
    if (p_device == NULL)
    {
       platform_write("SPI NOR Flash Test: Could Not Open NOR Device; errno = 0x%x \n",
                       platform_errno);
       return (TEST_FAIL);
    }

    platform_write("\nRunning SPI NOR Flash Block Erase Test\n");
    testStatus = spi_nor_erase_test(p_device, args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nSPI NOR Flash Block Erase Test Failed\n");
        goto TEST_END;
    }
    else
    {
        platform_write("\nSPI NOR Flash Block Erase Test Passed\n");
    }

    platform_write("\nRunning SPI NOR Flash Memory Access Test - Test Pattern 1\n");
    args->testPattern = NOR_FLASH_TEST_PATTERN1;
    testStatus = spi_nor_memory_access_test(p_device, args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nSPI NOR Flash Memory Access Test Failed\n");
        goto TEST_END;
    }
    else
    {
        platform_write("\nSPI NOR Flash Memory Access Test Passed\n");
    }

    platform_write("\nRunning SPI NOR Flash Memory Access Test - Test Pattern 2\n");
    args->testPattern = NOR_FLASH_TEST_PATTERN2;
    testStatus = spi_nor_memory_access_test(p_device, args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nSPI NOR Flash Memory Access Test Failed\n");
        goto TEST_END;
    }
    else
    {
        platform_write("\nSPI NOR Flash Memory Access Test Passed\n");
    }

TEST_END:
    platform_device_close(p_device->handle);

    return (testStatus);
}

/**
 * \brief This function performs spi nor test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS spiNorTest(void *testArgs)
{
    TEST_STATUS testStatus;

    spiNorTestArgs_t *args = (spiNorTestArgs_t *)testArgs;

    platform_write("\n********************************\n");
    platform_write(  "       SPI NOR Flash Test       \n");
    platform_write(  "********************************\n");

    if(args == NULL)
    {
        platform_write("Invalid Test Arguments!\n");
        platform_write("Aborting the Test!!\n");
        return (TEST_FAIL);
    }

    if(args->autoRun == FALSE)
    {
        /* Read test inputs from user if needed */
    }

    testStatus = run_spi_nor_test(args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nSPI NOR Flash Test Failed!\n");
    }
    else
    {
        platform_write("\nSPI NOR Flash Test Passed!\n");
    }

    platform_write("\nSPI NOR Flash Tests Completed!!\n");
    platform_write("\n-----------------X-----------------\n\n\n");

    return (testStatus);

} // spiNorTest

/* Nothing past this point */

