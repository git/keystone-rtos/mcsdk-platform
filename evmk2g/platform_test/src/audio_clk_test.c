/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 *  \file  audio_clk_test.c
 *
 *  \brief This file contains functions for CS2000 and DAC8550 test.
 *
 ******************************************************************************/

#include <csl_cpts.h>
#include <csl_cpsw.h>
#include "audio_clk_test.h"

int32_t cptsTsCmpNudge[10] = { -2, 1, 1, 0, 5, -2, -2, 1, -2, 0};
uint32_t dbgTsCmpTimestamp[1000];

int16_t dacData[DAC8550_DATA_COUNT] = {0x8000, 0, 0x7FFF};

static uint32_t data1_reg_val;
static uint32_t spi_base_addr [CSL_SPI_CNT]  = {CSL_SPI_0_SLV_REGS,
                                                    CSL_SPI_1_SLV_REGS,
                                                    CSL_SPI_2_SLV_REGS,
                                                    CSL_SPI_3_SLV_REGS};
/**
 * \brief Configures SPI3 pin muxing
 *
 * \return None
 *
 */
static void spi_pinmux_config(void)
{
    pinMuxSetMode(SPI3_SPI3SCS0_PADMUM, PADCONFIG_MUX_MODE_SECONDARY);
    pinMuxSetMode(SPI3_SPI3CLK_PADMUM, PADCONFIG_MUX_MODE_SECONDARY);
    pinMuxSetMode(SPI3_SPI3DIN_PADMUM, PADCONFIG_MUX_MODE_SECONDARY);
    pinMuxSetMode(SPI3_SPI3DOUT_PADMUM, PADCONFIG_MUX_MODE_SECONDARY);
}

/**
 * \brief Configures SPI controller for DAC8550 communication
 *
 * \param spiportnumber [IN] - SPI port number
 * \param cs            [IN] - SPI bus chip select
 * \param freq          [IN] - SPI bus frequency
 *
 * \return None
 *
 */
static void spi_config(uint8_t spiportnumber, uint32_t cs, uint32_t freq)
{
	uint32_t scalar;

    /* Enable the SPI hardware */
    SPI_SPIGCR0 = CSL_SPI_SPIGCR0_RESET_IN_RESET;
    platform_delay (2000);
    SPI_SPIGCR0 = CSL_SPI_SPIGCR0_RESET_OUT_OF_RESET;

    /* Set master mode, powered up and not activated */
    SPI_SPIGCR1 =   (CSL_SPI_SPIGCR1_MASTER_MASTER << CSL_SPI_SPIGCR1_MASTER_SHIFT)   |
                    (CSL_SPI_SPIGCR1_CLKMOD_INTERNAL << CSL_SPI_SPIGCR1_CLKMOD_SHIFT);


    /* CS0, CS1, CLK, Slave in and Slave out are functional pins */
    if (cs == 0) {
        SPI_SPIPC0 =    (CSL_SPI_SPIPC0_SCS0FUN0_SPI << CSL_SPI_SPIPC0_SCS0FUN0_SHIFT) |
                        (CSL_SPI_SPIPC0_CLKFUN_SPI << CSL_SPI_SPIPC0_CLKFUN_SHIFT)     |
                        (CSL_SPI_SPIPC0_SIMOFUN_SPI << CSL_SPI_SPIPC0_SIMOFUN_SHIFT)   |
                        (CSL_SPI_SPIPC0_SOMIFUN_SPI << CSL_SPI_SPIPC0_SOMIFUN_SHIFT);
    } else if (cs == 1) {
        SPI_SPIPC0 =    ((CSL_SPI_SPIPC0_SCS0FUN1_SPI << CSL_SPI_SPIPC0_SCS0FUN1_SHIFT) |
                        (CSL_SPI_SPIPC0_CLKFUN_SPI << CSL_SPI_SPIPC0_CLKFUN_SHIFT)     |
                        (CSL_SPI_SPIPC0_SIMOFUN_SPI << CSL_SPI_SPIPC0_SIMOFUN_SHIFT)   |
                        (CSL_SPI_SPIPC0_SOMIFUN_SPI << CSL_SPI_SPIPC0_SOMIFUN_SHIFT)) & 0xFFFF;
    }

    /* setup format */
    scalar = ((SPI_MODULE_CLK / freq) - 1 ) & 0xFF;

    if ( cs == 0) {
        SPI_SPIFMT0 =   (8 << CSL_SPI_SPIFMT_CHARLEN_SHIFT)               |
                        (scalar << CSL_SPI_SPIFMT_PRESCALE_SHIFT)                      |
                        (CSL_SPI_SPIFMT_PHASE_NO_DELAY << CSL_SPI_SPIFMT_PHASE_SHIFT)     |
//                        (CSL_SPI_SPIFMT_POLARITY_LOW << CSL_SPI_SPIFMT_POLARITY_SHIFT) |
                        (CSL_SPI_SPIFMT_POLARITY_HIGH << CSL_SPI_SPIFMT_POLARITY_SHIFT) |
                        (CSL_SPI_SPIFMT_SHIFTDIR_MSB << CSL_SPI_SPIFMT_SHIFTDIR_SHIFT);
    }else if ( cs == 1) {
        SPI_SPIFMT0 =   (16 << CSL_SPI_SPIFMT_CHARLEN_SHIFT)               |
                        (scalar << CSL_SPI_SPIFMT_PRESCALE_SHIFT)                      |
                        (CSL_SPI_SPIFMT_PHASE_DELAY << CSL_SPI_SPIFMT_PHASE_SHIFT)     |
                        (CSL_SPI_SPIFMT_POLARITY_LOW << CSL_SPI_SPIFMT_POLARITY_SHIFT) |
                        (CSL_SPI_SPIFMT_SHIFTDIR_MSB << CSL_SPI_SPIFMT_SHIFTDIR_SHIFT);
    }

    /* hold cs active at end of transfer until explicitly de-asserted */
    data1_reg_val = (CSL_SPI_SPIDAT1_CSHOLD_ENABLE << CSL_SPI_SPIDAT1_CSHOLD_SHIFT) |
                    (0x02 << CSL_SPI_SPIDAT1_CSNR_SHIFT);
     if (cs == 0) {
         SPI_SPIDAT1 =   (CSL_SPI_SPIDAT1_CSHOLD_ENABLE << CSL_SPI_SPIDAT1_CSHOLD_SHIFT) |
                         (0x02 << CSL_SPI_SPIDAT1_CSNR_SHIFT);
     }

    /* no interrupts */
    SPI_SPIINT0 = CSL_SPI_SPIINT0_RESETVAL;
    SPI_SPILVL  = CSL_SPI_SPILVL_RESETVAL;

    /* enable SPI */
    SPI_SPIGCR1 |= ( CSL_SPI_SPIGCR1_ENABLE_ENABLE << CSL_SPI_SPIGCR1_ENABLE_SHIFT );
}

/**
 * \brief Writes Data to SPI
 *
 * \param spiportnumber [IN] - SPI port number
 * \param data          [IN] - Data buffer
 * \param nbytes        [IN] - Data length in bytes
 *
 * \return None
 *
 */
static void spi_data_write(uint8_t spiportnumber, uint8_t *data, uint32_t nbytes)
{
	uint32_t index;
	uint8_t terminate = 1;

    for (index = 0; index < nbytes; index++)
    {
        /* Wait untill TX buffer is not full */
        while( SPI_SPIBUF & CSL_SPI_SPIBUF_TXFULL_MASK );

        /* Set the TX data to SPIDAT1 */
        data1_reg_val &= ~0xFFFF;
		data1_reg_val |= *data & 0xFF;
		data++;

        /* Write to SPIDAT1 */
        if((index == (nbytes -1)) && (terminate))
        {
            /* Release the CS at the end of the transfer when terminate flag is TRUE */
            SPI_SPIDAT1 = data1_reg_val & ~(CSL_SPI_SPIDAT1_CSHOLD_ENABLE << CSL_SPI_SPIDAT1_CSHOLD_SHIFT);
        }
        else
        {
            SPI_SPIDAT1 = data1_reg_val;
        }
    }
}

/**
 * \brief Configures CPTS pin as GPIO and toggles at given rate
 *
 * \param rate       [IN] - Pulse rate in KHz (250KHz max)
 * \param pulseCount [IN] - Pulse count
 *
 * \return None
 *
 */
void cpts_gpio_toggle(uint32_t rate, uint32_t pulseCount)
{
	uint32_t index;

	pinMuxSetMode(CPTS_TS_COMP_PADMUM, PADCONFIG_MUX_MODE_QUATERNARY);

    gpioSetDirection(GPIO_PORT_1, 55, GPIO_OUT);

    gpioClearOutput(GPIO_PORT_1, 55);

    for (index = 0; index < pulseCount; index++)
    {
    	gpioClearOutput(GPIO_PORT_1, 55);
    	platform_delay(500 / rate);
    	gpioSetOutput(GPIO_PORT_1, 55);
    	platform_delay(500 / rate);
    }
}

#if 0

/**
 * \brief Configure CPTS module
 *
 * \param toggle [IN] - Toggle configure
 *                      1: Toggle mode
 *                      0: Non-Toggle mode
 *
 * \return None
 *
 */
static void configCpts(uint32_t toggle)
{
    CSL_CPTS_CONTROL    ctrl;
    uint32_t            refClockSelect = 0;

    memset(&ctrl, 0, sizeof(CSL_CPTS_CONTROL));

    ctrl.cptsEn = 1;
    ctrl.tsCompToggle = toggle;
    ctrl.ts64bMode = 1;

    CSL_CPTS_disableCpts();

    CSL_CPTS_setRFTCLKSelectReg (refClockSelect);

    CSL_CPTS_setCntlReg(&ctrl);
}

/**
 * \brief Controls CPTS module
 *
 * \param sysClk       [IN] - CPU clock value
 * \param cptsCmpRate  [IN] - CPTS comparator rate
 * \param tsCmpOutClks [IN] - TS comparator output clocks count
 *
 * \return None
 *
 */
static void cptsTsCmpCtrl(uint32_t sysClk,
                          uint32_t cptsCmpRate,
                          uint32_t tsCmpOutClks)
{
    CSL_CPTS_EVENTINFO  cptsEventInfo1;
    CSL_CPTS_EVENTINFO  cptsEventInfo2;
    uint32_t        index1;
    uint32_t        index2;
    uint32_t    startTime;
    uint32_t    cptsClkRate;
    uint32_t    cptsTsCmpPeriod;
    uint32_t    corePeriod;
    uint32_t    tsNextHi;
    uint32_t    tsNextLo;
    uint32_t    tsNextLoTmp;

    corePeriod = sysClk/cptsCmpRate;

    /* Configure the CPTS in non-toggle mode */
    configCpts(0);

    /* Measure CPTS period  */
    CSL_CPTS_popEvent();
    CSL_CPTS_TSEventPush();
    startTime = CSL_chipReadTSCL();

    while (!CSL_CPTS_isRawInterruptStatusBitSet());
    CSL_CPTS_getEventInfo(&cptsEventInfo1);
    CSL_CPTS_popEvent();

    platform_delaycycles(startTime + sysClk/100);

    CSL_CPTS_TSEventPush();

    while (!CSL_CPTS_isRawInterruptStatusBitSet());

    CSL_CPTS_getEventInfo(&cptsEventInfo2);
    CSL_CPTS_popEvent();

    cptsClkRate = cptsEventInfo2.timeStamp - cptsEventInfo1.timeStamp;

    /* Truncate the last two digits and convert to clock rate per second */
    cptsClkRate = cptsClkRate/(100 * 10000);

    platform_write ("Audio Clock Test: CPTS Clock Rate = %d per Seconds\n", cptsClkRate);


    /* Program CPTS in non-toggle mode, generate TS_COMP output clocks */
    cptsTsCmpPeriod = cptsClkRate/cptsCmpRate;

#ifdef ENABLE_CPTS_NON_TOGGLE_MODE
    platform_write ("Audio Clock Test: CPTS Non-toggle mode - Generate %d TS_CMP Output Pulse with Period = %d\n",
                    tsCmpOutClks, cptsTsCmpPeriod);

    /* Find the start time */
    CSL_CPTS_popEvent();
    CSL_CPTS_TSEventPush();
    startTime = CSL_chipReadTSCL();

    while (!CSL_CPTS_isRawInterruptStatusBitSet());

    CSL_CPTS_getEventInfo(&cptsEventInfo1);
    CSL_CPTS_popEvent();

    /* Generate the first pulse after half of the expected duration */
    tsNextHi = cptsEventInfo1.timeStampHi;
    tsNextLo = cptsEventInfo1.timeStamp + cptsTsCmpPeriod/2;

    if(tsNextLo < cptsEventInfo1.timeStamp)
    {
        tsNextHi++;
    }

    CSL_CPTS_setTSCompVal(tsNextLo, tsNextHi, cptsTsCmpPeriod/2);

    for (index1 = 0, index2 = 0; index1 < tsCmpOutClks; index1++)
    {
        platform_delaycycles(startTime + corePeriod);
        startTime += corePeriod;

        CSL_CPTS_getEventInfo(&cptsEventInfo1);
        CSL_CPTS_popEvent();

        tsNextLoTmp = tsNextLo + cptsTsCmpPeriod;

        if(tsNextLoTmp < tsNextLo)
        {
            tsNextHi++;
        }

        tsNextLo = tsNextLoTmp;
        CSL_CPTS_setTSCompVal(tsNextLo, tsNextHi, cptsTsCmpPeriod/2);

        dbgTsCmpTimestamp[index2++] = cptsEventInfo1.timeStamp;

        if(index2 >= 1000)
        {
            index2 = 0;
        }
    }
#else
    /* Program CPTS in toggle mode, generate TS_COMP output clocks with periodic Nudge updates*/
    platform_write ("Audio Clock Test: CPTS toggle mode - Generate %d TS_CMP Output Pulse with Period = %d and per-determined nudge\n",
                    tsCmpOutClks, cptsTsCmpPeriod);

    configCpts(1);

    /* Find the start time */
    CSL_CPTS_popEvent();
    CSL_CPTS_TSEventPush();
    startTime = CSL_chipReadTSCL();

    while (!CSL_CPTS_isRawInterruptStatusBitSet());

    CSL_CPTS_getEventInfo(&cptsEventInfo1);
    CSL_CPTS_popEvent();

    /* Generate the first pulse after half of the expected duration */
    tsNextHi = cptsEventInfo1.timeStampHi;
    tsNextLo = cptsEventInfo1.timeStamp + cptsTsCmpPeriod/2;

    if(tsNextLo < cptsEventInfo1.timeStamp)
    {
        tsNextHi++;
    }

    CSL_CPTS_setTSCompVal(tsNextLo, tsNextHi, cptsTsCmpPeriod/2);

    for (index1 = 0; index1 < tsCmpOutClks; index1++)
    {
        platform_delaycycles(startTime + corePeriod);
        startTime += corePeriod;

        CSL_CPTS_setTSCompNudge(cptsTsCmpNudge[index1 % 10]);
    }
#endif /* #ifdef ENABLE_CPTS_NON_TOGGLE_MODE */

}
#endif

/**
 *  \brief    Writes a 8-bit value to CS2000 register
 *
 *  \param    testArgs [IN]   Test arguments
 *  \param    regAddr  [IN]   Register Address
 *  \param    data     [IN]   Data to write
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS cs2000_reg_write(void *testArgs, uint8_t regAddr,
                                    uint8_t data)
{
    I2C_RET retVal;
    audioClkTestArgs_t *args = (audioClkTestArgs_t *)testArgs;
    uint8_t wrBuf[2];

    wrBuf[0] = regAddr;
    wrBuf[1] = data;

    retVal = i2cWrite (args->i2cPortNum, args->i2cSlaveAddr, wrBuf,
                       2, I2C_RELEASE_BUS);
    if(retVal != I2C_RET_OK)
    {
        platform_write ("Audio Clock Test: CS2000 Register Write Failed, Err No - %d\n", retVal);
        return(TEST_FAIL);
    }

    return(TEST_PASS);
}

/**
 *  \brief    Reads a 8-bit value from CS2000 register
 *
 *  \param    testArgs [IN]   Test arguments
 *  \param    regAddr  [IN]   Register Address
 *  \param    data     [OUT]  Buffer to store data read
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS cs2000_reg_read(void *testArgs, uint8_t regAddr, uint8_t *data)
{
    I2C_RET retVal;
    audioClkTestArgs_t *args = (audioClkTestArgs_t *)testArgs;

    retVal = i2cRead (args->i2cPortNum, args->i2cSlaveAddr, data,
                      regAddr, 1, 1);
    if(retVal != I2C_RET_OK)
    {
        platform_write ("Audio Clock Test: CS2000 Register Read Failed, Err No - %d\n", retVal);
        return(TEST_FAIL);
    }

    return(TEST_PASS);
}

/**
 *  \brief    Function to read all the CS2000 clock register values
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS cs2000_reg_dump(void *testArgs)
{
	uint16_t index;
	uint8_t  regVal;
	TEST_STATUS testStatus = TEST_PASS;

	platform_write ("\n\n***************************\n");

	for (index = CS2000_REG_START; index <= CS2000_REG_END; index++)
	{
	    testStatus = cs2000_reg_read(testArgs, index, &regVal);
	    if(testStatus != TEST_PASS)
	    {
	        platform_write ("Audio Clock Test: CS2000 Reg Dump Failed\n");
	        return(testStatus);
	    }

	    platform_write ("Reg : %d;    Value : %d\n", index, regVal);
	}

	return(testStatus);
}

/**
 *  \brief    Executes CS2000 clock device detect test
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS cs2000_detect_test(void *testArgs)
{
    uint8_t data;
    I2C_RET retVal;
    TEST_STATUS testStatus;
    audioClkTestArgs_t *args = (audioClkTestArgs_t *)testArgs;

    retVal = i2cProbe(args->i2cPortNum, args->i2cSlaveAddr, 0, &data, 1);
    if(retVal != I2C_RET_OK)
    {
        platform_write ("Audio Clock Test: CS2000 Device Detection Failed\n");
        return(TEST_FAIL);
    }

    platform_write ("Audio Clock Test: CS2000 Device Detection Successful\n");

    /* Read Device Id register */
    testStatus = cs2000_reg_read (args, CS2000_DEVID_REG, &data);
    if(testStatus != TEST_PASS)
    {
        platform_write ("Audio Clock Test: CS2000 Device ID Read Failed\n");
        return(testStatus);
    }

    platform_write ("Audio Clock Test: CS2000 Device ID - 0x%x\n",
                     ((data >> 3) & 0x1F));
    platform_write ("Audio Clock Test: CS2000 Device Revision - 0x%x\n",
                     (data & 0x7));

    return(TEST_PASS);
}

/**
 *  \brief    Configures CS2000
 *
 *  \param    testArgs [IN]   Test arguments
 *  \param    pllMode  [IN]   Flag to control digital PLL mode
 *                             0 - Disable digital PLL
 *                             1 - Enable digital PLL
 *  \param    ratio    [IN]   Ratio value
 *  \param    auxOut   [IN]   Auxiliary output source selection
 *                             0 - REF_CLK will be routed to AUX_OUT
 *                             1 - CLK_IN will be routed to AUX_OUT
 *                             2 - CLK_OUT will be routed to AUX_OUT
 *                             3 - PLL Lock status will be routed to AUX_OUT
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS cs2000_device_config(void *testArgs, uint8_t pllMode,
                                        uint32_t ratio, uint8_t auxOut)
{
    uint8_t count;
    uint8_t value;
    TEST_STATUS testStatus = TEST_PASS;
    audioClkTestArgs_t *args = (audioClkTestArgs_t *)testArgs;

    /* Set freeze and EnDevCfg2 */
    testStatus = cs2000_reg_write(args, CS2000_GBLCFG_REG, 0x9);
    if(testStatus != TEST_PASS)
    {
        platform_write ("Audio Clock Test: CS2000 Global Config Failed\n");
        return (testStatus);
    }

    /* Configure reference clock input divider (divide by 1) */
    testStatus = cs2000_reg_write(args, CS2000_FUN_CFG_REG, 0x10);
    if(testStatus != TEST_PASS)
    {
        platform_write ("Audio Clock Test: CS2000 Ref Clock Input Divider Config Failed\n");
        return (testStatus);
    }

	/* Configure ratio selection (Ratio 0) and auxiliary output selection */
	value  = 1;  // Enables EnDevCfg1
	value |= (auxOut << 1);
	testStatus = cs2000_reg_write(args, CS2000_DEVCFG1_REG, value);
	if(testStatus != TEST_PASS)
	{
		platform_write ("Audio Clock Test: CS2000 Ratio Selection Failed\n");
		return (testStatus);
	}

    if(pllMode == 0)  //PLL mode disabled
    {
        /* Configure Fractional N source */
        testStatus = cs2000_reg_write(args, CS2000_DEVCFG2_REG, 0);
        if(testStatus != TEST_PASS)
        {
            platform_write ("Audio Clock Test: CS2000 Fractional N Source Selection Failed\n");
            return (testStatus);
        }

    	/* Configure Ratio 0 register */
    	for(count = 0; count < 4; count++)
    	{
    		/* Writing ratio with MSB at lower address */
    		value = (ratio >> (24 - 8*count)) & 0xFF;
    		testStatus = cs2000_reg_write(args, (CS2000_RATIO_CFG_REG_START +
    									  count), value);
    		if(testStatus != TEST_PASS)
    		{
    			platform_write ("Audio Clock Test: CS2000 Ratio Config Failed\n");
    			return (testStatus);
    		}
    	}
    }
    else //PLL mode enabled
    {
        /* Configure ratio selection (Ratio 0) and Fractional N source */
        testStatus = cs2000_reg_write(args, CS2000_DEVCFG2_REG, 0x1);
        if(testStatus != TEST_PASS)
        {
            platform_write ("Audio Clock Test: CS2000 Fractional N Source Selection Failed\n");
            return (testStatus);
        }
    }

    /* Clear freeze for register configurations to take effect */
    testStatus = cs2000_reg_write(args, CS2000_GBLCFG_REG, 0x1);
    if(testStatus != TEST_PASS)
    {
        platform_write ("Audio Clock Test: CS2000 Global Config Failed\n");
        return (testStatus);
    }

    //cs2000_reg_dump(args);

    return (testStatus);
}

/**
 *  \brief    Executes CS2000 clock tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_cs2000_clk_test(void *testArgs)
{
    TEST_STATUS testStatus = TEST_PASS;
    audioClkTestArgs_t *args = (audioClkTestArgs_t *)testArgs;
    uint8_t data;

    platform_write("\nRunning CS2000 Device Detect Test\n");

    pinMuxSetMode(CPTS_TS_COMP_PADMUM, PADCONFIG_MUX_MODE_QUINARY);

    testStatus = cs2000_detect_test(args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nCS2000 Device Detect Test Failed!\n");
        return (TEST_FAIL);
    }
    else
    {
        platform_write("\nCS2000 Device Detect Test Passed!\n");
    }

    platform_write("\nRunning CS2000 Clock Config Test without Digital PLL \n");
    platform_write("\nConfiguring Output/Input Ratio to 1\n");
    platform_write("\nCheck PLL Output at CLK_OUT\n");

    testStatus = cs2000_device_config(args, 0, CS2000_RATIO_12p20_1, 0);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nCS2000 Device Configuration Failed!\n");
        return (TEST_FAIL);
    }

	if(args->autoRun == FALSE)
	{
    	platform_write("\nEnter Any Key to Contiue:");
    	platform_read(&data, 1);
	}
	else
	{
	    platform_write("\nWaiting for 20 secs for Clock check to Complete...\n");
	    delay_secs(20);
	}

    platform_write("\nCS2000 Clock Config Test without Digital PLL is Completed\n");

    platform_write("\nRunning CS2000 Test with Digital PLL \n");
    platform_write("\nConfiguring Output/Input Ratio to 1\n");
    platform_write("\nCheck PLL Output at CLK_OUT\n");

    testStatus = cs2000_device_config(args, 1, CS2000_RATIO_20p12_1, 0x3);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nCS2000 Device Configuration Failed!\n");
        return (TEST_FAIL);
    }

    platform_write("\nEnabling CLK_IN, at Rate %ld KHz\n", args->cptsCmpRate);
    platform_write("\nCheck Clock output and PLL Lock at AUX_OUT\n");
    //cptsTsCmpCtrl(args->sysClk, args->cptsCmpRate, args->tsCmpOutClks);
    cpts_gpio_toggle(args->cptsCmpRate, args->tsCmpOutClks);

#if 0
    /* Read PLL Lock state */
    testStatus = cs2000_reg_read(args, CS2000_DEVCTL_REG, &data);
    if(testStatus != TEST_PASS)
    {
        platform_write ("Audio Clock Test: PLL Lock Read Failed\n");
        return(testStatus);
    }

    platform_write("PLL Lock State Value: %d\n", ((data >> 7) & 0x1));
#endif

    platform_write("\nCS2000 Clock Config Test with Digital PLL is Completed\n");

	if(args->autoRun == FALSE)
	{
    	platform_write("\nEnter Any Key to Contiue:");
    	platform_read(&data, 1);
	}

    return (testStatus);
}

/**
 *  \brief    Executes DAC8550 tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_dac8550_clk_test(void *testArgs)
{
    audioClkTestArgs_t *args = (audioClkTestArgs_t *)testArgs;
    int16_t dataVal;
    uint16_t increment;
    uint8_t data[3];
    uint8_t input;

    /* Enable SPI3 lines */
    spi_pinmux_config();

    /* Configure the SPI controller */
    spi_config(args->spiPortNum, args->spiCs, args->spiClk);

    platform_write ("\nWriting Data to DAC 8550\n");
    platform_write ("Check DAC DAC_VOUT Value which should increment from 0V to 2.5V\n");

	if(args->autoRun == FALSE)
	{
		platform_write("\nEnter Any Key to Contiue When Ready for Checking DAC_VOUT:");
		platform_read(&input, 1);
	}
	else
	{
		platform_write("\nWaiting for 10 secs Before Writing DAC Data...\n");
		delay_secs(10);
	}

    platform_write ("Data Write in Progress...\n");

    increment = 0x200;
    data[0] = 0;
    for(dataVal = -32678; (dataVal + increment) < 32767; dataVal += increment)
    {
    	data[1] = (dataVal >> 8) & 0xFF;
    	data[2] = dataVal & 0xFF;

        spi_data_write(args->spiPortNum, data, 3);

        platform_delay(50000);
    }

    platform_write ("DAC Data Write Completed!\n");

    return (TEST_PASS);
}

/**
 * \brief This function performs audio clock test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS audioClkTest(void *testArgs)
{
    TEST_STATUS testStatus = TEST_PASS;
    audioClkTestArgs_t *args = (audioClkTestArgs_t *)testArgs;

    platform_write("\n********************************\n");
    platform_write(  "        Audio Clock Test        \n");
    platform_write(  "********************************\n");

    platform_write("\nRunning CS2000 Clock Test\n");

    testStatus = run_cs2000_clk_test(args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nCS2000 Clock Test Failed!\n");
    }
    else
    {
        //platform_write("\nCS2000 Clock Test Passed!\n");
    }

    platform_write("\nCS2000 Clock Test Completed\n\n");

    platform_write("\n\nRunning DAC8550 Test\n");

    testStatus = run_dac8550_clk_test(args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nDAC8550 Test Failed!\n");
    }
    else
    {
        //platform_write("\nDAC8550 Test Passed!\n");
    }

    platform_write("\nAudio Clock Tests Completed!!\n");
    platform_write("\n-----------------X-----------------\n\n\n");

    return (testStatus);

} // audioClkTest

/* Nothing past this point */

