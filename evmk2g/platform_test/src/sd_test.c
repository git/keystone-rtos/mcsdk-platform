/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
*
* \file  sd_test.c
*
* \brief This file contains sd card test functions.
*
******************************************************************************/

#include "sd_test.h"
#include "platform_internal.h"

//TODO: Need to update the test to use platform_device_xxx APIs after bring-up
uint8_t sdWriteBuf[SECTOR_SIZE];
uint8_t sdReadBuf[SECTOR_SIZE];
uint8_t sdSaveBuf[SECTOR_SIZE];

/**
 * \brief This function performs write and readback the test pattern
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
static TEST_STATUS sd_memory_access_test (mmchsCardInfo *mmcCard)
{
    int32_t retVal;
    int32_t block = 2;

    memset (sdWriteBuf, SD_TEST_PATTERN, sizeof(sdWriteBuf));
    memset (sdReadBuf, 0x00, sizeof(sdReadBuf));

#ifdef ENABLE_MEMORY_BACKUP
    retVal = mmchsBlockRead (mmcCard, block, 1,
                             sdSaveBuf);
    if (retVal != TEST_PASS)
    {
        platform_write ("\tUnable to Read Block data\n");
        return (TEST_FAIL);
    }
#endif

    platform_write ("\tSD: Writing a pattern to Block %d....", block);
    retVal = mmchsBlockWrite (mmcCard, block, 1,
                              sdWriteBuf);
    if (retVal == TEST_PASS)
    {
        platform_write ("Write test passed\n");
    }
    else
    {
        platform_write ("Unable to Write Sector\n");
        return (TEST_FAIL);
    }
    platform_delay(10000);
    platform_write ("\n\tSD: Reading from Block %d...", block);
    retVal = mmchsBlockRead (mmcCard, block, 1,
                             sdReadBuf);
    if (retVal == TEST_PASS)
    {
        platform_write ("Read test passed\n");
    }
    else
    {
        platform_write ("Unable to Read Sector\n");
        return (TEST_FAIL);
    }

#ifdef ENABLE_MEMORY_BACKUP
    retVal = mmchsBlockWrite (mmcCard, block, 1,
                              sdSaveBuf);
    if (retVal != TEST_PASS)
    {
        platform_write ("\tUnable to restore the original contents of the SD card\n");
        return (TEST_FAIL);
    }
#endif

    if (memcmp(sdWriteBuf, sdReadBuf, SECTOR_SIZE) == 0)
    {
        platform_write ("\tVerify data...Passed\n");
        return (TEST_PASS);
    }
    else
    {
        platform_write ("\tVerify data...Failed\n");
        return (TEST_FAIL);
    }
}

/**
 * \brief This function performs SD erase test
 *
 * \param mmcCard  - MMC handle
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
static TEST_STATUS sd_erase_test (mmchsCardInfo *mmcCard)
{
    int32_t retVal;
    int32_t block = 1;
    uint32_t index;

    memset (sdWriteBuf, SD_TEST_PATTERN, sizeof(sdWriteBuf));
    memset (sdReadBuf, 0x00, sizeof(sdReadBuf));

#ifdef ENABLE_MEMORY_BACKUP
    platform_write ("Reading the current contents of 0x%x to sdSaveBuf\n", block);
    retVal = mmchsBlockRead (mmcCard, block, 1, sdSaveBuf);
    if (retVal != MMCHS_RET_OK)
    {
        platform_write ("\tUnable to Read Block data at 0x%x\n",block);
        return (TEST_FAIL);
    }
#endif

    platform_write ("\tSD: Writing a pattern to Block %d....", block);

    retVal = mmchsBlockWrite (mmcCard, block, 1, sdWriteBuf);
    if (retVal != MMCHS_RET_OK)
    {
        platform_write ("Unable to Write Sector at 0x%x\n",block);
        return (TEST_FAIL);
    }
    platform_delay(10000);

    platform_write ("\n\tReading back the pattern 0XAA to sdReadBuf\n");
    retVal = mmchsBlockRead (mmcCard, block, 1, sdReadBuf);
    if (retVal != MMCHS_RET_OK)
    {
        platform_write ("\tUnable to Read Block data at 0x%x\n",block);
        return (TEST_FAIL);
    }
    platform_delay(10000);
    platform_write ("Erase the block 0x%x \n", block);
    retVal = mmchsBlockErase (mmcCard, block, block);
    if (retVal != MMCHS_RET_OK)
    {
		platform_write ("\tBlock Erase Failed\n");
        return (TEST_FAIL);
    }

    platform_delay(10000);
    platform_delay(10000);
    platform_delay(10000);
    retVal = mmchsBlockRead (mmcCard, block, 1, sdReadBuf);
    if (retVal != MMCHS_RET_OK)
    {
        return (TEST_FAIL);
    }

#ifdef ENABLE_MEMORY_BACKUP
    platform_write ("Writing back the original contents from sdSaveBuf\n");
    retVal = mmchsBlockWrite (mmcCard, block, 1, sdSaveBuf);
    if (retVal != MMCHS_RET_OK)
    {
        platform_write ("Unable to Write Sector at 0x%x\n",block);
        return (TEST_FAIL);
    }
#endif

	platform_write ("Compare if the erased blocks are 0x00\n");

	for ( index = 0 ; index < (SECTOR_SIZE) ; index++ )
	{
		if (sdReadBuf[index] != 0x00)
		{
			platform_write ("Data comparison failed at %d\n",index);
			return (TEST_FAIL);
		}
	}

	platform_write ("Data comparison passed !!\n",index);

	return (TEST_PASS);
}

/**
 * \brief This function performs SD test
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
static TEST_STATUS run_sd_test(void *testArgs)
{
    mmchsInfo* mmcInfo;
    mmchsCardInfo *mmcCard;
    TEST_STATUS retVal;

    platform_write("\nRunning SD Card Detect Test\n");

    mmcInfo = mmchsInit(MMCHS_SD_CARD_INST);
    if ((mmcInfo == NULL) || (mmcInfo->card == NULL))
    {
        platform_write("testSD: Error initializing SD Card \n");
        return (TEST_FAIL);
    }

    mmcCard = mmcInfo->card;

    if (mmcCard->cardType == MMCHS_SD_CARD)
    {
		platform_write ("\tSD Card Identified\n");
    }
    else
    {
		platform_write ("\tSD Card not Identified\n");
        platform_write("\nSD Card Detect Test Failed!\n");
        return (TEST_FAIL);
	}

	platform_write ("\tSD Card Info:\n");
    mmchsPrintCardInfo(mmcCard);

    platform_write("\nSD Card Detect Test Passed!\n");

	platform_write("\nRunning SD Memory Access Test\n");
    retVal = sd_memory_access_test(mmcCard);
	if(retVal != TEST_PASS)
	{
		platform_write("\nSD Memory Access Test Failed!\n");
		return (retVal);
	}

	platform_write("\nSD Memory Access Test Passed!\n");

	platform_write("\nRunning SD Block Erase Test\n");
	retVal = sd_erase_test(mmcCard);
	if(retVal != TEST_PASS)
	{
		platform_write("\nSD Block Earse Test Failed!\n");
		return (retVal);
	}

	platform_write("\nSD Block Earse Test Passed!\n");

    return (retVal);
}

/**
 * \brief This function performs SD test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS sdTest(void *testArgs)
{
    TEST_STATUS testStatus;
	sdTestArgs_t *args = (sdTestArgs_t *)testArgs;

	platform_write("\n*********************\n");
	platform_write(  "       SD Test       \n");
	platform_write(  "*********************\n");

	if(args->autoRun == FALSE)
	{
		/* Read test inputs from user if needed */
	}

	testStatus = run_sd_test(args);
	if(testStatus != TEST_PASS)
	{
		platform_write("\nSD Test Failed!\n");
	}
	else
	{
		platform_write("\nSD Test Passed!\n");
	}

	platform_write("\nSD Tests Completed!!\n");
	platform_write("\n-----------------X-----------------\n\n\n");

    return(testStatus);

} // sdTest

/* Nothing past this point */

