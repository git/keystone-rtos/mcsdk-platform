/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
*
* \file i2c_eeprom_test.c
*
* \brief This file contains I2C EEPROM test functions.
*
******************************************************************************/

#include "i2c_eeprom_test.h"

/**
 *  \brief    Verifies I2C-EEPROM device connection
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS test_eeprom_connection(void *testArgs)
{
	I2C_RET retVal;
	uint8_t data;
	i2cEepromTestArgs_t *args = (i2cEepromTestArgs_t *)testArgs;

    retVal = i2cProbe(args->i2cPortNum, args->slaveAddr, 0, &data, 1);
    if(retVal != I2C_RET_OK)
    {
        platform_write ("I2C EEPROM Test: EEPROM Device Detection Failed\n");
        return(TEST_FAIL);
    }

    platform_write ("EEPROM Device Detection Successful\n");

    return (TEST_PASS);
}

/**
 *  \brief    Opens EEPROM device and performs read/write operations
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS test_eeprom_access(void *testArgs)
{
    PLATFORM_DEVICE_info *p_device;
    i2cEepromTestArgs_t *args = (i2cEepromTestArgs_t *)testArgs;
    Platform_STATUS status;
    TEST_STATUS testStatus = TEST_PASS;
    uint16_t index;
    uint8_t  *i2cWrBuf;
    uint8_t  *i2cRdBuf;
#ifdef ENABLE_MEMORY_BACKUP
    uint8_t  *i2cBkpBuf;
#endif

    p_device = platform_device_open(args->slaveAddr, 0);
    if (p_device == NULL)
    {
       platform_write("I2C EEPROM Test: Could not open EEPROM Device %x errno = 0x%x \n",
                      args->slaveAddr, platform_errno);
       return (TEST_FAIL);
    }

    i2cWrBuf  = malloc(args->blockLen);
    i2cRdBuf  = malloc(args->blockLen);

    if((i2cWrBuf == NULL) ||
       (i2cRdBuf == NULL))
    {
       platform_write("I2C EEPROM Test: Buffer Allocation Failed!\n");
       return (TEST_FAIL);
    }

#ifdef ENABLE_MEMORY_BACKUP
    i2cBkpBuf = malloc(args->blockLen);
    if(i2cBkpBuf == NULL)
    {
       platform_write("I2C EEPROM Test: Buffer Allocation Failed!\n");
       return (TEST_FAIL);
    }
#endif

    /* Initialize write buffer */
    for (index = 0; index < args->blockLen; index++)
    {
        i2cWrBuf[index] = index & 0xFF;
        //i2cWrBuf[index] = 0xAA;
        //i2cWrBuf[index] = 0x55;
    }

#ifdef ENABLE_MEMORY_BACKUP
    status = platform_device_read(p_device->handle, args->offset, i2cBkpBuf,
                                  args->blockLen);
    if(status != Platform_EOK)
    {
        platform_write("I2C EEPROM Test: EEPROM Read Failed, errno = 0x%x\n",
                        platform_errno);
        testStatus = TEST_FAIL;
        goto free;
    }
#endif

    status = platform_device_write(p_device->handle, args->offset,
                                   i2cWrBuf, args->blockLen);
    if(status != Platform_EOK)
    {
        platform_write("I2C EEPROM Test: EEPROM Write Failed, errno = 0x%x\n",
                       platform_errno);
        testStatus = TEST_FAIL;
        goto free;
    }

    status = platform_device_read(p_device->handle, args->offset,
                                  i2cRdBuf, args->blockLen);
    if(status != Platform_EOK)
    {
        platform_write("I2C EEPROM Test: EEPROM Read Failed, errno = 0x%x\n",
                        platform_errno);
        testStatus = TEST_FAIL;
        goto free;
    }

    if (memcmp(i2cWrBuf, i2cRdBuf, args->blockLen) != 0)
    {
        platform_write("I2C EEPROM Test: Data Verification Failed\n");
        testStatus = TEST_FAIL;
        goto free;
    }
    else
    {
        testStatus = TEST_PASS;
        platform_write("I2C EEPROM Test: Data Verification Passed\n");
    }

#ifdef ENABLE_MEMORY_BACKUP
    status = platform_device_write(p_device->handle, args->offset,
                                   i2cBkpBuf, args->blockLen);
    if(status != Platform_EOK)
    {
        platform_write("I2C EEPROM Test: Write Back Original Data Failed, errno = 0x%x\n", platform_errno);
        testStatus = TEST_FAIL;
    }
#endif

free:
    platform_device_close(p_device->handle);
    free(i2cWrBuf);
    free(i2cRdBuf);
#ifdef ENABLE_MEMORY_BACKUP
    free(i2cBkpBuf);
#endif

    return (testStatus);
}

/**
 *  \brief    Executes I2C EEPROM tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_i2c_eeprom_test(void *testArgs)
{
    i2cEepromTestArgs_t *args = (i2cEepromTestArgs_t *)testArgs;
    TEST_STATUS status = TEST_PASS;

    platform_write("\n\nTest for EEPROM at Address 0x%x\n", args->slaveAddr);

    platform_write("\nEEPROM Device Identification Test\n");
    status = test_eeprom_connection(args);
    if(status != TEST_PASS)
    {
        platform_write("EEPROM Device Identification Test at Address 0x%x Failed\n",
                       args->slaveAddr);
    }

    platform_write("\nEEPROM Access Test\n");
    status = test_eeprom_access(args);
    if(status != TEST_PASS)
    {
        platform_write("EEPROM Access Test at Address 0x%x Failed\n",
                       args->slaveAddr);
    }

    platform_write("\nTest for EEPROM at Address 0x%x Completed\n", args->slaveAddr);

    return (status);
}

/**
 * \brief This function performs I2C EEPROM test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS i2cEepromTest(void *testArgs)
{
    TEST_STATUS testStatus;
    i2cEepromTestArgs_t *args = (i2cEepromTestArgs_t *)testArgs;

    platform_write("\n*****************************\n");
    platform_write(  "       I2C EEPROM Test       \n");
    platform_write(  "*****************************\n");

    if(args == NULL)
    {
        platform_write("Invalid Test Arguments!\n");
        platform_write("Aborting the Test!!\n");
        return (TEST_FAIL);
    }

    if(args->autoRun == FALSE)
    {
        /* Read test inputs from user if needed */
    }

    args->slaveAddr = PLATFORM_DEVID_EEPROM50;
    testStatus = run_i2c_eeprom_test(args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nI2C EEPROM Test Failed!\n");
    }
    else
    {
        args->slaveAddr = PLATFORM_DEVID_EEPROM51;
        testStatus = run_i2c_eeprom_test(args);
        if(testStatus != TEST_PASS)
        {
            platform_write("\nI2C EEPROM Test Failed!\n");
        }
        else
        {
            platform_write("\nI2C EEPROM Test Passed!\n");
        }
    }

    platform_write("\nI2C EEPROM Tests Completed!!\n");
    platform_write("\n-----------------X-----------------\n\n\n");

    return(testStatus);

} // i2cEepromTest

/* Nothing past this point */

