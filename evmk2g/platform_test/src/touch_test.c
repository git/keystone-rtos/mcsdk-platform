/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 *  \file  touch_test.c
 *
 *  \brief This file contains touch test functions.
 *
 ******************************************************************************/

#include "touch_test.h"
#include "lcd_test.h"
#include "platform_internal.h"

uint16_t touchData[5][DIMENSIONS];

/** LCD Panel configuration for NHD 4.3 -480272EF-ATXL-CTP */
lcdCfg_t touchLcdCfg = {
        480,        /** LcdWidth    */
        272,        /** LcdHeight    */
        9000000,    /** LcdPclk      */
        41,         /** HsyncWidth   */
        2,          /** HFrontPorch  */
        2,          /** HBackPorch   */
        10,         /** VsyncWidth   */
        2,          /** VFrontPorch  */
        3,          /** VBackPorch   */
};

/**
 *
 * \brief     Function to get touch data.
 *
 * \param     none
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
static int8_t detect_touch_points(void)
{
	uint8_t fTouch;
	uint8_t c = 0;
	uint8_t val = 0;
	int8_t ret;
	uint8_t i, x, y;

	memset(touchData, 0x0, sizeof(touchData));

	ret = touchSetup();
	if (ret)
	{
		platform_write("Failed to do Touch Setup\n");
		return (TEST_FAIL);
	}
	else
		platform_write("Touch Setup done\n");


	printf("Please touch with single/multiple fingers on the touchscreen\n");

	while(1)
	{
		fTouch = touchGpioPinRead();
		if (fTouch)
		{
			platform_delay(WAIT_DELAY/2);
			/* Get number of touches */
			ret = touchRead(TD_STATUS, &val);
			val &= 0x7;
		    /* Get and save touch data */
			ret = touchGetAllData(touchData, val);
			if (ret)
			{
				platform_write("Failed to Get All Touch Data\n");
				return (TEST_FAIL);
			}
			for (i=0, x=0, y=1;i<val;i++)
				printf("Touch point %d X-Loc: %d Y-Loc:%d \n", (i+1), touchData[i][x], touchData[i][y]);

			printf("Press X/x for exiting the test\n");
			printf("Press Y/y to continue\n");
			platform_read(&c, 1);
			if((c == 'x') || (c == 'X'))
			{
				platform_write("Exit...\n");
				break;
			}
			if((c == 'y') || (c == 'Y'))
			{
				platform_write("Continue...\n");
			}
		}
	}

	return (TEST_PASS);
}

static TEST_STATUS test_touchscreen_connection(void)
{
	I2C_RET retVal;
	uint8_t data;

	retVal = i2cProbe(TOUCH_I2C_INSTANCE, TOUCH_SLAVE_ADDR, 0, &data, 1);
    if(retVal != I2C_RET_OK)
    {
        printf ("Touch Device Detection Failed\n");
        return(TEST_FAIL);
    }
    printf ("Touch Device Detection Successful\n");
    return (TEST_PASS);
}
/**
 *  \brief    Executes Touch tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_touch_test(void *testArgs)
{
	TEST_STATUS status;
	DSS_RET  retVal;
	uint8_t ret;

	status = TEST_PASS;

	touchInit();
    ret = pwmInit();
	if (ret)
	{
		platform_write("PWM initialization failed\n");
		return (TEST_FAIL);
	}

    retVal = lcdPanelInit(&touchLcdCfg);
    if (retVal != DSS_RET_OK)
    {
		platform_write("Display Initialization Failed\n");
    	return (TEST_FAIL);
	}

    lcdColorDisplay(&touchLcdCfg, LCD_WHITE);

	platform_write("Running Touch Device Detect Test\n");

	status = test_touchscreen_connection();
	if (status)
	{
	    platform_write("Touch Device Detect Test Failed\n");
		return (status);
	}

	platform_write("Touch Device Detect Test Passed\n");

	platform_write("Touch Point Read Test\n");

	status = detect_touch_points();
	if (status)
	{
		platform_write("Touch Point Read Test Failed\n");
		return (status);
	}

	platform_write("Touch Point Read Test Completed\n");

	return (status);
}

/**
 * \brief    This function performs touch test.
 *
 * \param    testArgs  - Test arguments.
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS touchTest(void *testArgs)
{
	TEST_STATUS testStatus;
	touchTestArgs_t *args = (touchTestArgs_t *)testArgs;

	platform_write("\n************************\n");
	platform_write(  "       Touch Test       \n");
	platform_write(  "************************\n");

	testStatus = run_touch_test(args);
	{
		if(testStatus != TEST_PASS)
		{
			platform_write("\nTouch Test Failed!\n");
		}
		else
		{
			//platform_write("\nTouch Test Passed!\n");
		}
	}

	platform_write("\nTouch Tests Completed!!\n");
	platform_write("\n-----------------X-----------------\n\n\n");

	return (testStatus);

} // touchTest

/* Nothing past this point */
