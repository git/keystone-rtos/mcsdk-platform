/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
*
*  \file sdio_test.c
*
*  \brief This file contains SDIO test functions.
*
******************************************************************************/

#include "sdio_test.h"
#include "platform_internal.h"

#define PIN_NUM_148  (148)


void sdioPinConf()
{
    uint16_t ret;

    platform_write("NOTE!! Make sure the hardware modification is done on this EVM to test the SDIO\n");
    platform_write("SDIO is muxed with eMMC and by default eMMC is enabled. To use SDIO the following \n");
    platform_write("hardware modification is required : (Refer to eMMC page of Schematics) \n");
    platform_write("IF COM8 TO BE USED RESISTOR RA6, R387 & R388 SHOULD BE DNI AND MOUNT RA15, R713, \n");
    platform_write("R714 and R393 SHOULD BE MOUNTED\n\n\n");

    ret = pinMuxSetMode(PIN_NUM_148, PADCONFIG_MUX_MODE_QUATERNARY);
    if(ret)
    {
        platform_write("ERROR:In configuring the %d pin\n",PIN_NUM_148);
    }

    /* Set the direction */
    ret = gpioSetDirection(GPIO_PORT_0, GPIO_PIN_117, GPIO_OUT);
    if(ret)
    {
        platform_write("Failed to set the direction of %d pin\n",GPIO_PIN_117);
    }

    /* Clear the wlan_trans_en pin*/
    ret = gpioClearOutput( GPIO_PORT_0, GPIO_PIN_117);
    if(ret)
    {

        platform_write("Error in clearing the output of %d pin\n", GPIO_PIN_117);
        goto err;
    }

    /* Enable WLAN EN pin from IO Expander */
     ret = i2cIoExpanderWritePin((I2cIoExpPins)IO_EXP_SOC_WLAN_EN,
                                  I2C_IO_EXP_PIN_HIGH);


err: return;
}
/**
 * \brief This function performs sdio test
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */

static TEST_STATUS testSDIO()
{
    mmchsInfo* mmcInfo;
    mmchsCardInfo *mmcCard;

    sdioPinConf();
    mmcInfo = mmchsInit(MMCHS_MMC_INST);
    if (mmcInfo == NULL)
    {
        platform_write("testSD: Error initializing SDIO \n");
        return TEST_FAIL;
    }

    mmcCard = mmcInfo->card;

    if (mmcCard == NULL)
    {
        platform_write("testSD: Error initializing SDIO \n");
        return TEST_FAIL;
    }

    if (mmcCard->cardType == MMCHS_SDIO)
    {
        platform_write ("SDIO card Detected\n");
    }
    return TEST_PASS;
}

/**
 * \brief This function performs SDIO test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS sdioTest(void *testArgs)
{
    TEST_STATUS testStatus;

    platform_write("\n*********************\n");
    platform_write(  "       SDIO Test       \n");
    platform_write(  "*********************\n");

    testStatus = testSDIO();

    if(testStatus != TEST_PASS)
    {
        platform_write("\nSDIO Test Failed!\n");
    }
    else
    {
        platform_write("\nSDIO Test Passed!\n");
    }

    platform_write("\nSDIO Tests Completed!!\n");
    platform_write("\n-----------------X-----------------\n\n\n");

    return(testStatus);
} // sdioTest

/* Nothing past this point */

