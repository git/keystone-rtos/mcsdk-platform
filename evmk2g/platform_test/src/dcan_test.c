/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 *  \file dcan_test.c
 *
 *  \brief This file contains dcan test functions.
 *
 ******************************************************************************/

#include "dcan_test.h"
#include "platform_internal.h"

/**
 *  \brief    Executes CAN bus tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_dcan_test(void *testArgs)
{
	int32_t index;
    TEST_STATUS testStatus = TEST_PASS;
    int16_t     status;

    status = TEST_PASS;

    platform_write("Toggling DCAN Pins. Check the Signals\n");

    dcanInit();

    for(index = 0; index < COUNT; index++)
    {
    	status = dcanTogglePins(DELAY_TIME);
        if(status)
        {
            platform_write("DCAN GPIO Toggle Failed");
            testStatus = TEST_FAIL;
            break;
        }
    }

    return (testStatus);
}

/**
 * \brief This function performs dcan test
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS dcanTest(void *testArgs)
{
    TEST_STATUS testStatus;
 	dcanTestArgs_t *args = (dcanTestArgs_t *)testArgs;

	platform_write("\n**************************\n");
	platform_write(  "       CAN Bus Test       \n");
	platform_write(  "**************************\n");

 	testStatus = run_dcan_test(args);
 	if(testStatus != TEST_PASS)
 	{
 		platform_write("\nCAN Bus Test Failed!\n");
 	}
 	else
 	{
 		platform_write("\nCAN Bus Test Passed!\n");
 	}

	platform_write("\nCAN Bus Tests Completed!!\n");
	platform_write("\n-----------------X-----------------\n\n\n");

    return(testStatus);
}

/* Nothing past this point */
