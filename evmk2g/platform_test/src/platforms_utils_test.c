/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *  \file    platforms_utils_test.c
 *
 *  \brief  This file contains the platform test functions
 *
 *****************************************************************************/

#include "platform_test.h"

extern diagTestTable_t gDiagTestTable[PLATFORM_TEST_COUNT];

uint8_t testInput[10];
TEST_STATUS testStatus;

/**
 *  \brief    Displays HW board information
 *
 *  \return - None
 */
static void displayBoardInfo(void)
{
	platform_write("\n\nHardware Info\n");
	platform_write("--------------\n");
	platform_write("Header:        0xEE3355AA\n");
	platform_write("Board Id:      K2G EVM\n");
	platform_write("Version:       1.0B\n");
	platform_write("Serial Number: 19154P540001\n");
	platform_write("Configuration: XXX\n");
	platform_write("---------------------------------------------------------------------");
	platform_write("\n");
}

/**
 *  \brief    Displays diagnostic test menu
 *
 *  \return   - None
 */
static void displayMenu(void)
{
	uint16_t    count;

    platform_write("\n\n");
	platform_write("*************************************************************************\n");
	platform_write("                       K2G EVM HW Diagnostic Tests                       \n");
	platform_write("*************************************************************************");

	/* Display board information */
	displayBoardInfo();

	platform_write("\n\n");
	platform_write("Diagnostic Tests");
	platform_write("\t\t Pass");
	platform_write("\t  Fail\n");
	platform_write("-----------------------\t\t------\t ------\n");

	/* Display Test Menu */
	for (count = 0; count < 2; count++)
	{
		platform_write("%2d - ", count);
		platform_write("%-25s", (const char*)gDiagTestTable[count].testName);
		platform_write("\n");
	}

	for (; count < PLATFORM_TEST_COUNT; count++)
	{
		platform_write("%2d - ", count);
		platform_write("%-25s", (const char*)gDiagTestTable[count].testName);
		platform_write("\t%  4d", gDiagTestTable[count].pass);
		platform_write("\t%   4d", gDiagTestTable[count].fail);
		platform_write("\n");
	}

#if 0
	platform_write("\n");
	platform_write("Other\n");
	platform_write("-----------------------\n");
	platform_write("%2d - Get Info\n", PLATFORM_TEST_GET_INFO);
	platform_write("%2d - ID Programming\n", PLATFORM_TEST_ID_PROG);
#endif

	platform_write("\n");
	platform_write("q - Quit\n");

	platform_write("\nEnter Desired Option:");
}

/**
 *  \brief    Executes all the diagnostic tests
 *
 *  \return   None
 */
static void run_all_tests(void)
{
	uint16_t index;

	platform_write("\nAuto Running All the Diagnostic Tests!\n\n\n");

	for (index = PLATFORM_TEST_START; index < PLATFORM_TEST_COUNT; index++)
	{
		if(gDiagTestTable[index].testFunction != NULL)
		{
			if(gDiagTestTable[index].testFunction(gDiagTestTable[index].args))
			{
				gDiagTestTable[index].fail++;
			}
			else
			{
				gDiagTestTable[index].pass++;
			}
		}
		else
		{
			platform_write("\n\nInvalid Test Handle for %s\n",
			               (const char*)gDiagTestTable[index].testName);
		}
	}

	platform_write("\nDiagnostic Tests Completed!!\n\n");
}

/**
 *  \brief    Executes all memory tests
 *
 *  \return   None
 */
static void run_memory_tests(void)
{
	uint16_t index;

	platform_write("\nRunning All the Memory Diagnostic Tests!\n");

	for (index = PLATFORM_MEM_TEST_START; index < PLATFORM_MEM_TEST_COUNT; index++)
	{
		if(gDiagTestTable[index].testFunction != NULL)
		{
			if(gDiagTestTable[index].testFunction(gDiagTestTable[index].args))
			{
				gDiagTestTable[index].fail++;
			}
			else
			{
				gDiagTestTable[index].pass++;
			}
		}
		else
		{
			platform_write("\n\nInvalid Test Handle for %s\n",
			               (const char*)gDiagTestTable[index].testName);
		}
	}

	platform_write("\nMemory Diagnostic Tests Completed!!\n");
}

/**
 *  \brief    Function to perform ID programming
 *
 *  \return   None
 */
static void id_program(void)
{
    platform_write("\n\n\n\n\n\n\n");
    platform_write(  "**************************************************************\n");
    platform_write(  "                    K2G EVM ID Programming                    \n");
    platform_write(  "**************************************************************\n\n\n");
    platform_write(  "    Option                                                    \n");
    platform_write(  "    --------------------------                                \n");
    platform_write(  "    1 - Update Header                                         \n");
    platform_write(  "    2 - Update Board Name                                     \n");
    platform_write(  "    3 - Update Version Number                                 \n");
    platform_write(  "    4 - Update Serial Number                                  \n");
    platform_write(  "    5 - Update Config                                         \n\n");
    platform_write(  "    q - Quit                                                  \n\n");
    platform_write(  "  Enter desired option:  ");
    platform_read(testInput, 3);

    //TODO: Implement ID programming interfaces
}

/**
 *  \brief    Gets the platform information
 *
 *  \return   NONE
 */
static void get_info(void)
{
	/* TODO: This function is for getting the device details available
	   on K2G EVM. Full functionality TBD */
	platform_write("\nRequest for Get Platform Information\n");
}

/**
 *  \brief    Diagnostic test main function
 *
 *  \return   None
 */
void main (void)
{
	Platform_STATUS status;
	uint8_t         exitTest;
	uint8_t         inputErr;
	uint32_t        input;

	/* Initialize the platform */
	status = initPlatform();
	if(status == Platform_EOK)
	{
		exitTest = 0;
		inputErr = 0;

		/* Display menu */
		displayMenu();

		while(!exitTest)
		{
			/* Wait for user input */
			platform_read(testInput, 3);

			/* Check user input */
			if((testInput[0] == 'q') || (testInput[0] == 'Q'))
			{
				exitTest = 1;
				platform_write("\n\nExiting the Diagnostic Tests!!\n");
				break;
			}
			else
			{
				input = strtoul((const char*)testInput, NULL, 10);

				switch(input)
				{
					case PLATFORM_TEST_AUTO_ALL:
						run_all_tests();
						break;

					case PLATFORM_TEST_AUTO_MEM:
						run_memory_tests();
						break;

					case PLATFORM_TEST_GET_INFO:
						get_info();
						break;

					case PLATFORM_TEST_ID_PROG:
						id_program();
						break;

					default:
						if(input < PLATFORM_TEST_COUNT)
						{
							if(gDiagTestTable[input].testFunction != NULL)
							{
								if(gDiagTestTable[input].testFunction(gDiagTestTable[input].args))
								{
									gDiagTestTable[input].fail++;
								}
								else
								{
									gDiagTestTable[input].pass++;
								}
							}
							else
							{
								platform_write("\n\nInvalid Test Handle!!\n");
							}
						}
						else
						{
							inputErr = 1;
						}

						break;
				}

				if(inputErr)
				{
					inputErr = 0;
					/* Invalid input. Get the desired option again and continue */
					platform_write("\n\nInvalid Input!\n");
					platform_write("Please Enter Valid Input\n");
					platform_write("\nEnter Desired Option:");
					continue;
				}
			}

			/* Display menu */
			displayMenu();
		}
	}
	else
	{
		IFDEBUGPRINT(printf("Platform Initialization Failed!\n"));
	}
}

/**
 *  \brief    Executes all the diagnostic tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 *  \return   TEST_STATUS
 */
TEST_STATUS run_external_tests(void *testArgs)
{
	TEST_STATUS status = TEST_PASS;

	/* TODO:

	   This function is to execute the diagnostic tests which are not part of
	   platform library.

	   Execution of this function depends on how the drivers/libraries
	   which are not platform library are integrated with diagnostic test suit.
    */

	platform_write("\nThis Test is Not Supported in this Version of Diagnostic Tests!!\n");
	//platform_write("\nRunning External Tests!\n");

	//platform_write("\nRunning External Tests Completed!!\n");

	return (status);
}

/* Nothing past this point */
