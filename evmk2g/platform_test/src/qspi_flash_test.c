/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *
 * \file  qspi_flash_test.c
 *
 * \brief This file contains qspi flash test functions.
 *
 ******************************************************************************/

#include "qspi_flash_test.h"
#include "platform_internal.h"

//#define QSPI_BASIC_TESTING 1

#define QSPI_DEVICE_ID 0x220
#define QSPI_MANF_ID   0x01
/**
 *  \brief    Executes QSPI flash chip detect test
 *
 *  \param    p_device [IN]   QSPI flash device handle
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS qspi_flash_chip_detect_test(PLATFORM_DEVICE_info *p_device,
                                               void *testArgs)
{
    qspiFlashTestArgs_t *args = (qspiFlashTestArgs_t *)testArgs;

    p_device = platform_device_open(args->devId, 0);
    if (p_device == NULL)
    {
       platform_write("QSPI Flash Test: Could Not Open QSPI Flash Device; errno = 0x%x \n", platform_errno);
       return (TEST_FAIL);
    }

    platform_write("Device Id - 0x%x\n", p_device->device_id);
    platform_write("Manufacturer Id - %d\n", p_device->manufacturer_id);
    platform_write("Device Width - %d\n", p_device->width);
    platform_write("Block Count - %d\n", p_device->block_count);
    platform_write("Page Count - %d\n", p_device->page_count);
    platform_write("Page Size - %d\n", p_device->page_size);

    platform_device_close(p_device->handle);

    if ((p_device->device_id != QSPI_DEVICE_ID) || (p_device->manufacturer_id != QSPI_MANF_ID))
        return TEST_FAIL;
    return (TEST_PASS);
}

/**
 *  \brief    Executes QSPI flash memory read/write test
 *
 *  \param    p_device [IN]   QSPI flash device handle
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS qspi_flash_memory_access_test(PLATFORM_DEVICE_info *p_device,
                                                 void *testArgs)
{
    qspiFlashTestArgs_t *args = (qspiFlashTestArgs_t *)testArgs;
    Platform_STATUS  status;
    TEST_STATUS      testStatus = TEST_FAIL;

    uint32_t address;
    uint8_t  *qspiWrBuf;
    uint8_t  *qspiRdBuf;
#ifdef ENABLE_MEMORY_BACKUP
    uint8_t  *qspiBkpBuf;
#endif

    qspiWrBuf = malloc(args->blockLen);
    qspiRdBuf = malloc(args->blockLen);

    if((qspiWrBuf == NULL) ||
       (qspiRdBuf == NULL))
    {
       platform_write("QSPI Flash Test: Buffer Allocation Failed!\n");
       return (TEST_FAIL);
    }

#ifdef ENABLE_MEMORY_BACKUP
    qspiBkpBuf = malloc(args->blockLen);
    if(qspiBkpBuf == NULL)
    {
        platform_write("QSPI Flash Test: Buffer Allocation Failed!\n");
        free(qspiWrBuf);
        free(qspiRdBuf);
        return (TEST_FAIL);
    }
#endif

    /* Initialize the test pattern */
    memset(qspiWrBuf, args->testPattern, args->blockLen);
    memset(qspiRdBuf, 0, args->blockLen);

    address = ((args->sectNum) * (QSPI_FLASH_SECTOR_SIZE));
#ifdef ENABLE_MEMORY_BACKUP
    status = platform_device_read(p_device->handle, address,
                                  qspiBkpBuf, args->blockLen);
    if(status != Platform_EOK)
    {
        platform_write("QSPI Flash Test: Read Failed  errno = 0x%x\n", platform_errno);
        goto free;
    }
#endif

#ifdef QSPI_BASIC_TESTING
    status = qspiFlashErase(p_device, args->sectNum);
#else
    status = platform_device_erase_block (p_device->handle, args->sectNum);
#endif
    if(status != Platform_EOK)
    {
        platform_write("QSPI Flash Test: Erase Failed for Sector %d  errno = 0x%x\n",
                       args->sectNum, platform_errno);
    }

#ifdef QSPI_BASIC_TESTING
    status = qspiFlashWrite(p_device, address,args->blockLen, qspiWrBuf, 0);

#else
    status = platform_device_write(p_device->handle, address,
                                       qspiWrBuf, args->blockLen);
#endif
    if(status != Platform_EOK)
    {
        platform_write("QSPI Flash Test: Write Test Data Failed  errno = 0x%x\n", platform_errno);
        goto free;
    }
#ifdef QSPI_BASIC_TESTING
    status = qspiFlashRead(p_device, address,args->blockLen, qspiRdBuf, 0);
#else
    status = platform_device_read(p_device->handle, address,
                                  qspiRdBuf, args->blockLen);
#endif
    if(status != Platform_EOK)
    {
        platform_write("QSPI Flash Test: Read Test Data Failed  errno = 0x%x\n", platform_errno);
        goto free;
    }

    if (memcmp(qspiWrBuf, qspiRdBuf, args->blockLen) != 0)
    {
        platform_write("QSPI Flash Test: Data Verification Failed\n");
        testStatus = TEST_FAIL;
    }
    else
    {
        platform_write("QSPI Flash Test: Data Verification Passed\n");
        testStatus = TEST_PASS;
    }

#ifdef ENABLE_MEMORY_BACKUP
    status = platform_device_erase_block (p_device->handle, args->sectNum);
    if(status != Platform_EOK)
    {
        platform_write("QSPI Flash Test: Erase Failed  errno = 0x%x\n",
                       platform_errno);
        testStatus = TEST_FAIL;
    }

    status = platform_device_write(p_device->handle, address,
                                   qspiBkpBuf, args->blockLen);
    if(status != Platform_EOK)
    {
        platform_write("QSPI Flash Test: Write Back Original Data Failed  errno = 0x%x\n",
                       platform_errno);
        testStatus = TEST_FAIL;
    }
#endif

free:
    free(qspiWrBuf);
    free(qspiRdBuf);
#ifdef ENABLE_MEMORY_BACKUP
    free(qspiBkpBuf);
#endif

    return (testStatus);
}

#ifdef QSPI_BASIC_TESTING
/**
 *  \brief    Executes QSPI flash memory erase test
 *
 *  \param    p_device [IN]   QSPI flash device handle
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS qspi_flash_erase_test(PLATFORM_DEVICE_info *p_device,
                                         void *testArgs)
{
    qspiFlashTestArgs_t *args = (qspiFlashTestArgs_t *)testArgs;
    Platform_STATUS  status;
    TEST_STATUS      testStatus = TEST_FAIL;

    uint32_t index;
    uint32_t address;
    uint8_t  *qspiRdBuf;
#ifdef ENABLE_MEMORY_BACKUP
    uint8_t  *qspiBkpBuf;
#endif

    qspiRdBuf = malloc(QSPI_FLASH_SECTOR_SIZE);
    if(qspiRdBuf == NULL)
    {
       platform_write("QSPI Flash Test: Buffer Allocation Failed!\n");
       return (testStatus);
    }

#ifdef ENABLE_MEMORY_BACKUP
    qspiBkpBuf = malloc(QSPI_FLASH_SECTOR_SIZE);
    if(qspiBkpBuf == NULL)
    {
        platform_write("QSPI Flash Test: Buffer Allocation Failed!\n");
        free(qspiRdBuf);
        return (testStatus);
    }
#endif

    memset(qspiRdBuf, 0, QSPI_FLASH_SECTOR_SIZE);

    address = ((args->sectNum) * (QSPI_FLASH_SECTOR_SIZE));
    platform_write("QSPI Flash Test: Erasing sector 0x%x address : 0x%x\n", args->sectNum, address);
#ifdef ENABLE_MEMORY_BACKUP
    status = platform_device_read(p_device->handle, address,
                                  qspiBkpBuf, QSPI_FLASH_SECTOR_SIZE);
    if(status != Platform_EOK)
    {
        platform_write("QSPI Flash Test: Read Failed  errno = 0x%x\n", platform_errno);
        goto free;
    }
#endif

    status = platform_device_erase_block (p_device->handle, args->sectNum);
    if(status != Platform_EOK)
    {
        platform_write("QSPI Flash Test: Erase Failed for Sector %d  errno = 0x%x\n",
                        args->sectNum, platform_errno);
        goto free;
    }

    status = platform_device_read(p_device->handle, address,
                                  qspiRdBuf, QSPI_FLASH_SECTOR_SIZE);
    if(status != Platform_EOK)
    {
        platform_write("QSPI Flash Test: Read Test Data Failed  errno = 0x%x\n", platform_errno);
        goto free;
    }

    for (index = 0; index < QSPI_FLASH_SECTOR_SIZE; index++)
    {
        if(qspiRdBuf[index] != 0xFF)
        {
            platform_write("QSPI Flash Test: Erase Data Verification Failed at indext 0x%x\n",index);
            testStatus = TEST_FAIL;
            break;
        }
    }

    if(index == QSPI_FLASH_SECTOR_SIZE)
    {
        platform_write("QSPI Flash Test: Erase Data Verification Passed\n");
        testStatus = TEST_PASS;
    }

#ifdef ENABLE_MEMORY_BACKUP
    status = platform_device_write(p_device->handle, address,
                                   qspiBkpBuf, QSPI_FLASH_SECTOR_SIZE);
    if(status != Platform_EOK)
    {
        platform_write("QSPI Flash Test: Write Back Original Data Failed  errno = 0x%x\n",
                       platform_errno);
        testStatus = TEST_FAIL;
    }
#endif

free:
    free(qspiRdBuf);
#ifdef ENABLE_MEMORY_BACKUP
    free(qspiBkpBuf);
#endif

    return (testStatus);
}
#endif
/**
 *  \brief    Executes QSPI flash tests
 *
 *  \param    testArgs [IN]   Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 */
static TEST_STATUS run_qspi_flash_test(void *testArgs)
{
    PLATFORM_DEVICE_info *p_device = NULL;
    qspiFlashTestArgs_t  *args = (qspiFlashTestArgs_t *)testArgs;
    TEST_STATUS          testStatus = TEST_FAIL;

    platform_write("\nRunning QSPI Flash Chip Detect Test\n");
    testStatus = qspi_flash_chip_detect_test(p_device, args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nQSPI Flash Chip Detect Test Failed\n");
        goto TEST_END;
    }
    else
    {
        platform_write("\nQSPI Flash Chip Detect Test Passed\n");
    }

    p_device = platform_device_open(args->devId, 0);
    if (p_device == NULL)
    {
       platform_write("QSPI Flash Test: Could Not Open QSPI Flash Device; errno = 0x%x \n",
                       platform_errno);
       return (TEST_FAIL);
    }

#ifdef QSPI_BASIC_TESTING
    platform_write("\nRunning QSPI Flash Block Erase Test\n");
    testStatus = qspi_flash_erase_test(p_device, args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nQSPI Flash Block Erase Test Failed\n");
        goto TEST_END;
    }
    else
    {
        platform_write("\nQSPI Flash Block Erase Test Passed\n");
    }
#endif
    platform_write("\nRunning QSPI Flash Memory Access Test - Test Pattern 1\n");
    args->testPattern = QSPI_FLASH_TEST_PATTERN1;
    testStatus = qspi_flash_memory_access_test(p_device, args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nQSPI Flash Memory Access Test Failed\n");
        goto TEST_END;
    }
    else
    {
        platform_write("\nQSPI Flash Memory Access Test Passed\n");
    }
#ifndef QSPI_BASIC_TESTING
    platform_write("\nRunning QSPI Flash Memory Access Test - Test Pattern 2\n");
    args->sectNum = 2;
    args->testPattern = QSPI_FLASH_TEST_PATTERN2;
    testStatus = qspi_flash_memory_access_test(p_device, args);
    if(testStatus != TEST_PASS)
    {
        platform_write("\nQSPI Flash Memory Access Test Failed\n");
        goto TEST_END;
    }
    else
    {
        platform_write("\nQSPI Flash Memory Access Test Passed\n");
    }
#endif
TEST_END:
    platform_device_close(p_device->handle);

    return (testStatus);
}

/**
 * \brief This function performs qspi flash
 *
 * \param testArgs  - Test arguments
 *
 * \return
 * \n      TEST_PASS  - Test Passed
 * \n      TEST_FAIL  - Test Failed
 *
 */
TEST_STATUS qspiFlashTest(void *testArgs)
{
	TEST_STATUS testStatus = TEST_PASS;
	qspiFlashTestArgs_t *args = (qspiFlashTestArgs_t *)testArgs;

	platform_write("\n******************************\n");
	platform_write(  "       QSPI Flash Test        \n");
	platform_write(  "******************************\n");

    if(args == NULL)
    {
        platform_write("Invalid Test Arguments!\n");
        platform_write("Aborting the Test!!\n");
        return (TEST_FAIL);
    }

	if(args->autoRun == FALSE)
	{
		/* Read test inputs from user if needed */
	}

	testStatus = run_qspi_flash_test(args);
	if(testStatus != TEST_PASS)
	{
		platform_write("\nQSPI Flash Test Failed!\n");
	}
	else
	{
		platform_write("\nQSPI Flash Test Passed!\n");
	}

	platform_write("\nQSPI Flash Tests Completed!!\n");
	platform_write("\n-----------------X-----------------\n\n\n");

	return (testStatus);

} // qspiFlashTest

/* Nothing past this point */
