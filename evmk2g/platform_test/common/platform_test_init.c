/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *  \file    platforms_test_init.c
 *
 *  \brief  This file contains the platform test initialization functions
 *
 *****************************************************************************/

#include "platform_test.h"
#include "platform_internal.h"

/* Platform init default flags */
platform_init_flags gInitFlags =
{
	PLATFORM_INIT_FLAGS_PLL,
	PLATFORM_INIT_FLAGS_DDR,
	PLATFORM_INIT_FLAGS_TCSL,
	PLATFORM_INIT_FLAGS_PHY,
	PLATFORM_INIT_FLAGS_ECC
};

/* Platform init default configurations */
platform_init_config gInitConfig =
{
	PLATFORM_INIT_CONFIG_PLLM,
	PLATFORM_INIT_CONFIG_PLLD,
	PLATFORM_INIT_CONFIG_PREDIV,
	PLATFORM_INIT_CONFIG_POSTDIV,
	PLATFORM_INIT_CONFIG_MASTERCORE
};

/**
 *  \brief    Initializes user interface
 *
 *  User input for the diagnostic tests can be taken from
 *  serial console or CCS console. This function configures
 *  the user interface based on the selected input.
 *
 *  Below are settings needs to be used on host machine in case
 *  of serial console user interface
 *   Baud Rate    - 115200
 *   Data width   - 8 bit
 *   Parity       - None
 *   Stop Bits    - 1
 *   Flow Control - None
 *
 *  \return    - Platform_EOK on Success or error code
 */
static Platform_STATUS initUserIf(void)
{
	Platform_STATUS status = Platform_EOK;

#ifdef USE_SERIAL_CONSOLE
	PLATFORM_UART_Params params;
	uint8_t uartId;

	params.uart_port = PLATFORM_UART_PORT_0;

	uartId = UART_ID_DB9;
	//uartId = UART_ID_UART2USB;

	if(params.uart_port == PLATFORM_UART_PORT_0)
	{
		pinMuxSetMode(125, PADCONFIG_MUX_MODE_QUATERNARY);
		gpioSetDirection(GPIO_PORT_1, 54, GPIO_OUT);

		if(uartId == UART_ID_UART2USB)
		{
			gpioClearOutput(GPIO_PORT_1, 54);
		}
		else
		{
			gpioSetOutput(GPIO_PORT_1, 54);
		}
	}

    platform_uart_set_params(&params);

	/* Set output console to serial port */
	platform_write_configure(PLATFORM_WRITE_UART);
	platform_read_configure(PLATFORM_READ_UART);

	/* Initialize UART */
	status = platform_uart_init();
	if(status == Platform_EOK)
	{
		status = platform_uart_set_baudrate(PLATFORM_TEST_UART_BAUD);
	}
#else
	/* Set output console to CCS */
	platform_write_configure(PLATFORM_WRITE_PRINTF);
	platform_read_configure(PLATFORM_READ_SCANF);
#endif

	return (status);
}

/**
 *  \brief    Initializes platform test modules
 *
 *  \return    - Platform_EOK on Success or error code
 */
Platform_STATUS initPlatform(void)
{
	Platform_STATUS status = 0;

	/* Initialize platform HW modules */
#ifndef GALILEO_SIM_TEST
    status = platform_init(&gInitFlags, &gInitConfig);
#endif
    if(status != Platform_EOK)
    {
    	IFDEBUGPRINT(printf("Platform failed to initialize, errno = 0x%x \n", platform_errno));
    	return (status);
    }

	/* Initialize user interface module */
    status = initUserIf();
    if(status != Platform_EOK)
    {
    	platform_write("User Interface Init Failed\n");
    	return (status);
    }

    return (status);
}

/**
 *  \brief    Function to generate delay in secs
 *
 *  \return    - None
 */
void delay_secs(uint32_t secs)
{
	uint32_t index;

	for (index = 0; index < secs; index++)
	{
		platform_delay(1000000);
	}

}

/* OSAL functions for Platform Library */
uint8_t *Osal_platformMalloc (uint32_t num_bytes, uint32_t alignment)
{
	return malloc(num_bytes);
}

void Osal_platformFree (uint8_t *dataPtr, uint32_t num_bytes)
{
    /* Free up the memory */
    if (dataPtr)
    {
        free(dataPtr);
    }
}

void Osal_platformSpiCsEnter(void)
{
    /* Get the hardware semaphore.
     *
     * Acquire Multi core CPPI synchronization lock
     */
    while ((CSL_semAcquireDirect (PLATFORM_SPI_HW_SEM)) == 0);

    return;
}

void Osal_platformSpiCsExit (void)
{
    /* Release the hardware semaphore
     *
     * Release multi-core lock.
     */
    CSL_semReleaseSemaphore (PLATFORM_SPI_HW_SEM);

    return;
}

/* Nothing past this point */
