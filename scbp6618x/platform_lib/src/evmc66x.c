/******************************************************************************
 * Copyright (c) 2010-2011 Texas Instruments Incorporated - http://www.ti.com
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *****************************************************************************/

/******************************************************************************
 *
 * File	Name:       evm66x.c
 *
 * Description: This contains   TMS320TCI6618 specific functions.
 * 
 ******************************************************************************/
 
/************************
 * Include Files
 ************************/
#include "platform_internal.h"

 /**
  *  Handle to access BOOTCFG registers.
  */
#define hPscCfg     ((CSL_PscRegs*)CSL_PSC_REGS)
 
 /**
 @}
 */
/* Boot Cfg Registers */
#define   MAINPLLCTL0_REG       (*((volatile uint32_t *) 0x02620328))
#define   MAINPLLCTL1_REG       (*((volatile uint32_t *) 0x0262032C))
#define   DDR3PLLCTL0_REG       (*((volatile uint32_t *) 0x02620330))
#define   DDR3PLLCTL1_REG       (*((volatile uint32_t *) 0x02620334))

/* PA PLL Registers */
#define   PAPLLCTL0_REG         (*((volatile uint32_t *) 0x02620338))
#define   PAPLLCTL1_REG         (*((volatile uint32_t *) 0x0262033C))

/*PLL controller registers*/
#define   PLLCTL_REG            (*((volatile uint32_t *) 0x02310100))
#define   SECCTL_REG            (*((volatile uint32_t *) 0x02310108))
#define   PLLM_REG              (*((volatile uint32_t *) 0x02310110))
#define   PLLCMD_REG            (*((volatile uint32_t *) 0x02310138))
#define   PLLSTAT_REG           (*((volatile uint32_t *) 0x0231013C))
#define   PLLDIV2_REG           (*((volatile uint32_t *) 0x0231011C))
#define   PLLDIV5_REG           (*((volatile uint32_t *) 0x02310164))
#define   PLLDIV8_REG           (*((volatile uint32_t *) 0x02310170))
#define   PREDIV_REG            (*((volatile uint32_t *) 0x02310114))


CSL_Status CorePllcHwSetup (
    PllcHwSetup          *hwSetup
)
{
    CSL_Status       status = CSL_SOK;
    volatile uint32_t i, loopCount;
    uint32_t ctl,secctl, pllstatus;

    /* Unlock the Boot Config */
    CSL_BootCfgUnlockKicker();

    /*In PLLCTL, write PLLENSRC = 0 (enable PLLEN bit)*/    
    /* Program pllen=0 (pll bypass), pllrst=1 (reset pll), pllsrc = 0 */

    ctl         = PLLCTL_REG;
    ctl        &= ~(0x00000031);
    PLLCTL_REG = ctl;

    /* Enable secondary controller pll bypass */
    secctl     = SECCTL_REG;
    secctl     |= 1 << 23;
    SECCTL_REG = secctl;


/* PLL WORK AROUND FOR THE SI BUG */
    /* Reset the PLL, wait at least 5 us, release reset */
    ctl = ctl | 2;
    PLLCTL_REG = ctl;

    loopCount = 50000;
    while (loopCount--) {
        asm("   NOP");
    }

    ctl = ctl & ~2;
    PLLCTL_REG = ctl;

    loopCount = 50000;
    while (loopCount--) {
        asm("   NOP");
    }

    
    /* Reset the PLL */
      ctl = ctl | (1 << 3);
      PLLCTL_REG = ctl;
/* WORK AROUND ENDS */

    /* Enable the pll divider */
    secctl = SECCTL_REG;
    secctl |= (1 << 16);
    secctl |= (1 << 19);
    SECCTL_REG = secctl;

    MAINPLLCTL0_REG  = hwSetup->preDiv & 0x3f; /* PLLD[5:0] */
    MAINPLLCTL0_REG |= (((hwSetup->pllM) >> 6) & 0x7f) << 12; /* PLLM[12:6] */
    PLLM_REG   = hwSetup->pllM & 0x3f; /* PLLM[5:0] */

    MAINPLLCTL0_REG      |= (((hwSetup->pllM+1)>>1)-1) << 24;  /* BWADJ bits */

    /*If necessary program PLLDIV1n.  Note that you must aplly the GO operation
     to change these dividers to new ratios*/
     if (hwSetup->divEnable & PLLC_DIVEN_PLLDIV2) {
        PLLDIV2_REG = (hwSetup->pllDiv2) | 0x00008000;        
     }
     if (hwSetup->divEnable & PLLC_DIVEN_PLLDIV5) {
        PLLDIV5_REG = (hwSetup->pllDiv5) | 0x00008000;
     }
     if (hwSetup->divEnable & PLLC_DIVEN_PLLDIV8) {
        PLLDIV8_REG = (hwSetup->pllDiv8) | 0x00008000;
     }

    /*Set GOSET bit in PLLCMD to initiate the GO operation to change the divide
    values and align the SYSCLKs as programmed */
    PLLCMD_REG |= 0x00000001;

    loopCount = 2000;
    while (loopCount--) {
        asm("   NOP");
    }

    /* set pllrst to 0 to deassert pll reset */
    ctl = ctl & ~(1<<3);
    PLLCTL_REG = ctl;


    /* wait for the pll to lock, but don't trap if lock is never read */
    for (i = 0; i < 100; i++)  {
        loopCount = 2000;
        while (loopCount--) {
            asm("   NOP");
        }
      pllstatus = PLLSTAT_REG;
      if ( (pllstatus & 0x1) != 0 )
        break;
    }

    /* Clear the secondary controller bypass bit */
    secctl = secctl & ~(1<<23);
    SECCTL_REG = secctl;
    

    /* Set pllen to 1 to enable pll mode */
    ctl = ctl | 0x1;
    PLLCTL_REG = ctl;

    loopCount = 8000;
    while (loopCount--) {
        asm("   NOP");
    }
    
    CSL_BootCfgLockKicker();
    
    return status;
}

CSL_Status CorePllcGetHwSetup (
    PllcHwSetup             *hwSetup
)
{
    CSL_Status       status   = CSL_SOK;

    /* Unlock the Boot Config */
    CSL_BootCfgUnlockKicker();

    hwSetup->divEnable = 0;

    hwSetup->pllM       = (PLLM_REG & 0x3F);
    hwSetup->preDiv     = PREDIV_REG;
    hwSetup->pllDiv2    = PLLDIV2_REG ;
    hwSetup->pllDiv5    = PLLDIV5_REG;
    hwSetup->pllDiv8    = PLLDIV8_REG;

    /* Make sure no GO operation is pending*/
    while((PLLSTAT_REG) & 0x00000001);    
    
    CSL_BootCfgLockKicker();

    return status;
}

#define BWADJ_BIT_SHIFT     24
#define BYPASS_BIT_SHIFT    23
#define CLKF_BIT_SHIFT      6
#define PA_PLL_BYPASS_MASK (1 << BYPASS_BIT_SHIFT)    /* Tells whether the PA PLL is in BYPASS mode or not */
#define PA_PLL_BWADJ_MASK (1 << BWADJ_BIT_SHIFT) /* Tells the bandwidth adjust value for the PA PLL */
#define PA_PLL_CLKOD_MASK  (0x00780000) /* Tells the output divider value for the PA PLL */
#define PA_PLL_CLKF_MASK   (0x0007FFC0) /* Tells the multiplier value for the PA PLL */
#define PA_PLL_CLKR_MASK   (0x0000003F) /* Tells the divider value for the PA PLL */

#define ENSAT_BIT_SHIFT 6
#define RESET_BIT_SHIFT 14
#define PA_PLL_ENSAT_MASK (1 << ENSAT_BIT_SHIFT) /* Tells the configuration of the ENSAT bit */
#define PA_PLL_RESET_MASK (1 << RESET_BIT_SHIFT) /* Tells the configuration of the RESET bit */

#if 0
#define PA_PLLM_VAL        (204)
#define PA_PLLD_VAL        (11)

/* Set the desired PA PLL configuration -- assumes 122.88 MHz input clock, should yield 1049.6 MHz output 
   (122.88 * (204+1))/(2*(11+1)) = 1049.6 MHz */
#else
#define PA_PLLM_VAL        (16)
#define PA_PLLD_VAL        (0)

/* Set the desired PA PLL configuration -- assumes 122.88 MHz input clock, should yield 1044.48 MHz output 
   (122.88 * (16+1))/(2*(0+1)) = 1044.48 MHz */
#endif

CSL_Status 
SetPaPllConfig
(
    void
) 
{
    CSL_Status  status   = CSL_SOK;
    uint32_t    passclksel;
    uint32_t    papllctl0val = PAPLLCTL0_REG;
    uint32_t    papllbypass = 0;
    uint32_t    papllclkf, papllclkd, papllbwadj; 
	uint32_t    loopCount;

    passclksel  = CSL_BootCfgGetPARefClockSelect();
    if (passclksel == 0) 
    {
       IFPRINT(platform_write("WARNING: SYSCLK/ALTCORECLK is the input to the PA PLL.\n"));
    }
  
    papllclkf   = (2 * (PA_PLLM_VAL + 1)) - 1;	/* 122.8 * (17) / (2) = 1044 MHz */
    papllclkd   = PA_PLLD_VAL + 1;
    papllbwadj = (papllclkf + 1) / 2 - 1;
    
    papllctl0val &= (~PA_PLL_BYPASS_MASK);	/* clear bypass bit         */
    papllctl0val &= (~PA_PLL_CLKF_MASK);    /* clear multiplier value   */
    papllctl0val &= (~PA_PLL_CLKR_MASK);    /* clear divider value      */
    papllctl0val &= (~PA_PLL_BWADJ_MASK);   /* clear bwadj value        */

    /* set value of PAPLLCTL0_REG register */
    papllctl0val |= ((papllbwadj<<BWADJ_BIT_SHIFT) | (papllbypass<<BYPASS_BIT_SHIFT) | (papllclkf<<CLKF_BIT_SHIFT) | (papllclkd));	
    
    /* Unlock the Boot Config */
    CSL_BootCfgUnlockKicker();

    PAPLLCTL1_REG |= PA_PLL_ENSAT_MASK;	/* Set ensat to 1 */
    PAPLLCTL1_REG |= (PA_PLL_PLLSEL_MASK | PA_PLL_RESET_MASK); 	/* Set RESET bit, pllsel BIT before programming PLLCTL0 */
    
    /* Set pll3 */
    PAPLLCTL0_REG = papllctl0val;
    
    /* Wait for PLL to properly reset (128 CLKIN1 cycles) */
    loopCount = 1000;
    while (loopCount--) 
    {
        asm("   NOP");
    }

    PAPLLCTL1_REG &= ~(PA_PLL_RESET_MASK); /* Clear RESET bit   */
    
    /* Wait for PLL to lock (2000 CLKIN1 cycles) */
    loopCount = 5000;
    while (loopCount--) 
    {
        asm("   NOP");
    }
    
    /* Lock Chip Level Registers */
    CSL_BootCfgLockKicker();

    return status;
}

/***************************************************************************************
 * FUNCTION PURPOSE: Power up all the power domains
 ***************************************************************************************
 * DESCRIPTION: this function powers up the PA subsystem domains
 ***************************************************************************************/
void PowerUpDomains (void)
{


    /* PASS power domain is turned OFF by default. It needs to be turned on before doing any 
     * PASS device register access. This not required for the simulator. */

    /* Set PASS Power domain to ON */        
    CSL_PSC_enablePowerDomain (CSL_PSC_PD_ALWAYSON);

    /* Enable the clocks for PASS modules */
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_EMIF4F, PSC_MODSTATE_ENABLE);

    /* Start the state transition */
    CSL_PSC_startStateTransition (CSL_PSC_PD_ALWAYSON);

    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone (CSL_PSC_PD_ALWAYSON));


    /* PASS power domain is turned OFF by default. It needs to be turned on before doing any 
     * PASS device register access. This not required for the simulator. */

    /* Set PASS Power domain to ON */        
    CSL_PSC_enablePowerDomain (CSL_PSC_PD_PASS);

    /* Enable the clocks for PASS modules */
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_PKTPROC, PSC_MODSTATE_ENABLE);
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_CPGMAC,  PSC_MODSTATE_ENABLE);
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_Crypto,  PSC_MODSTATE_ENABLE);

    /* Start the state transition */
    CSL_PSC_startStateTransition (CSL_PSC_PD_PASS);

    /* Wait until the state transition process is completed. */
    while (!CSL_PSC_isStateTransitionDone (CSL_PSC_PD_PASS));
}

CSL_Status SetDDR3PllConfig() {

  CSL_Status       status   = CSL_SOK;
  Int32            loopCount;

  /* Unlock the Boot Config */
  CSL_BootCfgUnlockKicker();

  DDR3PLLCTL0_REG = 0x0A0804C0;

  /* Wait for PLL to lock */
  loopCount = 4000;
  while (loopCount--) {
      asm("   NOP");
  }

  /* Lock Chip Level Registers */
  CSL_BootCfgLockKicker();

  return status;
}


/*--------------------------------------------------------------*/
/* xmc_setup()                                                  */
/* XMC MPAX register setting to access DDR3 config space        */
/*--------------------------------------------------------------*/
void xmc_setup()
{  
    /* mapping for ddr emif registers XMPAX*2 */
    CSL_XMC_XMPAXL    mpaxl;
    CSL_XMC_XMPAXH    mpaxh;

    /* base addr + seg size (64KB)*/	//"1B"-->"B" by xj */
    mpaxh.bAddr     = (0x2100000B >> 12);
    mpaxh.segSize   = (0x2100000B & 0x0000001F);

    /* replacement addr + perm*/
    mpaxl.rAddr     = 0x100000;
    mpaxl.sr        = 1;
    mpaxl.sw        = 1;
    mpaxl.sx        = 1;
    mpaxl.ur        = 1;
    mpaxl.uw        = 1;
    mpaxl.ux        = 1;

    /* set the xmpax for index2 */
    CSL_XMC_setXMPAXH(2, &mpaxh);
    CSL_XMC_setXMPAXL(2, &mpaxl);    
}

/* Set the desired DDR3 configuration -- assumes 66.67 MHz DDR3 clock input */
CSL_Status DDR3Init() {

    CSL_Status              status   = CSL_SOK;    
    volatile unsigned int loopCount;
    uint32_t ddr3config, ddrPhyCtrl;
    uint8_t ddrPHYReadLatency;
    EMIF4F_TIMING1_CONFIG sdram_tim1;
    EMIF4F_TIMING2_CONFIG sdram_tim2;
    EMIF4F_TIMING3_CONFIG sdram_tim3;
    EMIF4F_OUTPUT_IMP_CONFIG    zqcfg;
    EMIF4F_PWR_MGMT_CONFIG      pwrmgmtcfg;
    EMIF4F_SDRAM_CONFIG         sdramcfg;
    

    CSL_BootCfgUnlockKicker();        

   /***************** 2.2 DDR3 PLL Configuration ************/
    DDR3PLLCTL1_REG      |= 0x00000040;      //Set ENSAT bit = 1
    DDR3PLLCTL1_REG      |= 0x00002000;      //Set RESET bit = 1
    DDR3PLLCTL0_REG       = 0x090804C0;       //Configure CLKR, CLKF, CLKOD, BWADJ;

    /* Wait for 5us min reset to complete  */
    platform_delaycycles(50000);
    DDR3PLLCTL1_REG      &= ~(0x00002000);   //Clear RESET bit

   /* Wait for PLL to lock = min 500 ref clock cycles. 
      With refclk = 100MHz, = 5000 ns = 5us */
    platform_delaycycles(50000);

    /**************** 3.0 Leveling Register Configuration ********************/
    /* Using partial automatic leveling due to errata */

    /**************** 3.2 Invert Clock Out ********************/
    CSL_BootCfgGetDDRConfig(0, &ddr3config);
    ddr3config &= ~(0x007FE000);  // clear ctrl_slave_ratio field
    CSL_BootCfgSetDDRConfig(0, ddr3config);

    CSL_BootCfgGetDDRConfig(0, &ddr3config);
    ddr3config |= 0x00200000;     // set ctrl_slave_ratio to 0x100
    CSL_BootCfgSetDDRConfig(0, ddr3config);

    CSL_BootCfgGetDDRConfig(12, &ddr3config);
    ddr3config |= 0x08000000;    // Set invert_clkout = 1
    CSL_BootCfgSetDDRConfig(12, ddr3config);

    CSL_BootCfgGetDDRConfig(0, &ddr3config);
    ddr3config |= 0xF;            // set dll_lock_diff to 15
    CSL_BootCfgSetDDRConfig(0, ddr3config);

    CSL_BootCfgGetDDRConfig(23, &ddr3config);
    ddr3config |= 0x00000200;    //Set bit 9 = 1 to use forced ratio leveling for read DQS
    CSL_BootCfgSetDDRConfig(23, ddr3config);

    /*Values with invertclkout = 1 */
    /**************** 3.3+3.4 Partial Automatic Leveling ********************/
    ddr3config = 0x0000005E	; CSL_BootCfgSetDDRConfig(2,  ddr3config);
    ddr3config = 0x0000005E;  CSL_BootCfgSetDDRConfig(3,  ddr3config);
    ddr3config = 0x0000005E;  CSL_BootCfgSetDDRConfig(4,  ddr3config);
    ddr3config = 0x00000051;  CSL_BootCfgSetDDRConfig(5,  ddr3config);
    ddr3config = 0x00000038;  CSL_BootCfgSetDDRConfig(6,  ddr3config);
    ddr3config = 0x0000003A;  CSL_BootCfgSetDDRConfig(7,  ddr3config);
    ddr3config = 0x00000024;  CSL_BootCfgSetDDRConfig(8,  ddr3config);
    ddr3config = 0x00000020;  CSL_BootCfgSetDDRConfig(9,  ddr3config);
    ddr3config = 0x00000044;  CSL_BootCfgSetDDRConfig(10, ddr3config);

    ddr3config = 0x000000DD;  CSL_BootCfgSetDDRConfig(14,  ddr3config);
    ddr3config = 0x000000DD;  CSL_BootCfgSetDDRConfig(15,  ddr3config);
    ddr3config = 0x000000BE;  CSL_BootCfgSetDDRConfig(16,  ddr3config);
    ddr3config = 0x000000CA;  CSL_BootCfgSetDDRConfig(17,  ddr3config);
    ddr3config = 0x000000A9;  CSL_BootCfgSetDDRConfig(18,  ddr3config);
    ddr3config = 0x000000A7;  CSL_BootCfgSetDDRConfig(19,  ddr3config);
    ddr3config = 0x0000009E;  CSL_BootCfgSetDDRConfig(20,  ddr3config);
    ddr3config = 0x000000A1;  CSL_BootCfgSetDDRConfig(21,  ddr3config);
    ddr3config = 0x000000BA;  CSL_BootCfgSetDDRConfig(22,  ddr3config);
    
    /*Do a PHY reset. Toggle DDR_PHY_CTRL_1 bit 15 0->1->0 */
    CSL_EMIF4F_GetPhyControl(&ddrPhyCtrl, &ddrPHYReadLatency);
    ddrPhyCtrl &= ~(0x00000400);
    CSL_EMIF4F_SetPhyControl(ddrPhyCtrl,  ddrPHYReadLatency);

    CSL_EMIF4F_GetPhyControl(&ddrPhyCtrl, &ddrPHYReadLatency);
    ddrPhyCtrl |= (0x00000400);
    CSL_EMIF4F_SetPhyControl(ddrPhyCtrl,  ddrPHYReadLatency);

    CSL_EMIF4F_GetPhyControl(&ddrPhyCtrl, &ddrPHYReadLatency);
    ddrPhyCtrl &= ~(0x00000400);
    CSL_EMIF4F_SetPhyControl(ddrPhyCtrl,  ddrPHYReadLatency);
    
/*
    hEmif->DDR_PHY_CTRL_1  &= ~(0x00008000);
    hEmif->DDR_PHY_CTRL_1  |= (0x00008000);
    hEmif->DDR_PHY_CTRL_1  &= ~(0x00008000);
*/    

    /***************** 2.3 Basic Controller and DRAM configuration ************/
    /* enable configuration */
    /*    hEmif->SDRAM_REF_CTRL    = 0x00005162; */
    CSL_EMIF4F_EnableInitRefresh();
    CSL_EMIF4F_SetRefreshRate(0x5162);

/*    hEmif->SDRAM_TIM_1   = 0x1113783C; */

    sdram_tim1.t_wtr    = 4;
    sdram_tim1.t_rrd    = 7;
    sdram_tim1.t_rc     = 0x20;
    sdram_tim1.t_ras    = 0x17;
    sdram_tim1.t_wr     = 9;
    sdram_tim1.t_rcd    = 8;
    sdram_tim1.t_rp     = 8;
    CSL_EMIF4F_SetTiming1Config(&sdram_tim1);

/*    hEmif->SDRAM_TIM_2   = 0x304F7FE3; */
    sdram_tim2.t_cke    = 3;
    sdram_tim2.t_rtp    = 4;
    sdram_tim2.t_xsrd   = 0x1FF;
    sdram_tim2.t_xsnr   = 0x04F;
    sdram_tim2.t_xp     = 3;
    sdram_tim2.t_odt    = 0;
    CSL_EMIF4F_SetTiming2Config (&sdram_tim2);

/*    hEmif->SDRAM_TIM_3   = 0x559F849F; */
    sdram_tim3.t_rasMax     = 0xF;
    sdram_tim3.t_rfc        = 0x049;
    sdram_tim3.t_tdqsckmax  = 0;
    sdram_tim3.zq_zqcs      = 0x3F;
    sdram_tim3.t_ckesr      = 4;
    sdram_tim3.t_csta       = 0x5;
    sdram_tim3.t_pdll_ul    = 0x5;
    CSL_EMIF4F_SetTiming3Config (&sdram_tim3);    

/*    hEmif->DDR_PHY_CTRL_1   = 0x0010010F; */
    ddrPHYReadLatency   = 0x0F;
    ddrPhyCtrl          = (0x08008);
    CSL_EMIF4F_SetPhyControl(ddrPhyCtrl,  ddrPHYReadLatency);

/*    hEmif->ZQ_CONFIG        = 0x70073214; */
    zqcfg.zqRefInterval     = 0x3214;
    zqcfg.zqZQCLMult        = 3;
    zqcfg.zqZQCLInterval    = 1;
    zqcfg.zqSFEXITEn        = 1;
    zqcfg.zqDualCSEn        = 1;
    zqcfg.zqCS0En           = 1;
    zqcfg.zqCS1En           = 0;
    CSL_EMIF4F_SetOutputImpedanceConfig(&zqcfg);

/*    hEmif->PWR_MGMT_CTRL    = 0x0; */
    pwrmgmtcfg.csTime           = 0;
    pwrmgmtcfg.srTime           = 0;
    pwrmgmtcfg.lpMode           = 0;
    pwrmgmtcfg.dpdEnable        = 0;
    pwrmgmtcfg.pdTime           = 0;
    CSL_EMIF4F_SetPowerMgmtConfig  (&pwrmgmtcfg);
 
    /* New value with DYN_ODT disabled and SDRAM_DRIVE = RZQ/7 */ 
/*    hEmif->SDRAM_CONFIG     = 0x63062A32; */
    CSL_EMIF4F_GetSDRAMConfig (&sdramcfg);
    sdramcfg.pageSize           = 2;
    sdramcfg.eBank              = 0;
    sdramcfg.iBank              = 3;
    sdramcfg.rowSize            = 4;
    sdramcfg.CASLatency         = 10;
    sdramcfg.narrowMode         = 0;
    sdramcfg.CASWriteLat        = 2;
    sdramcfg.SDRAMDrive         = 1;
    sdramcfg.disableDLL         = 0;
    sdramcfg.dynODT             = 0;
    sdramcfg.ddrDDQS            = 0;
    sdramcfg.ddrTerm            = 3;
    sdramcfg.iBankPos           = 0;
    sdramcfg.type               = 3;
    CSL_EMIF4F_SetSDRAMConfig (&sdramcfg);

    /* Refresh rate = (7.8*666MHz] */
    /*    hEmif->SDRAM_REF_CTRL   = 0x00001450;     */
    CSL_EMIF4F_SetRefreshRate(0x00001450);

    /* enable full leveling */
/*    hEmif->RDWR_LVL_RMP_CTRL      =  0x80000000; */
    CSL_EMIF4F_SetLevelingRampControlInfo(1, 0, 0, 0, 0);

    /* Trigger full leveling - This ignores read DQS leveling result and uses ratio forced value */
    /*    hEmif->RDWR_LVL_CTRL          =  0x80000000; */
    CSL_EMIF4F_SetLevelingControlInfo(1, 0, 0, 0, 0);

    /************************************************************
      Wait for min 1048576 DDR clock cycles for leveling to complete 
        = 1048576 * 1.5ns = 1572864ns = 1.57ms.
      Actual time = ~10-15 ms 
     **************************************************************/
	platform_delaycycles(1600000);

    /* Lock Chip Level Registers */
    CSL_BootCfgLockKicker();
    
    return (status);
    
}

/* Nothing past this point */


