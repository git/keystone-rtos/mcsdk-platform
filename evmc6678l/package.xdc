/******************************************************************************
 * FILE PURPOSE: Package specification file 
 ******************************************************************************
 * FILE NAME: package.xdc
 *
 * DESCRIPTION: 
 *  This file contains the package specification for the platform library
 *
 * Copyright (C) 2011, Texas Instruments, Inc.
 *****************************************************************************/

requires ti.csl;

package ti.platform.evmc6678l[1, 0, 0, 0] {
    module Settings;
}

