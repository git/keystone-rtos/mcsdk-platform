init_pll   = 1
init_ddr   = 1
init_uart   = 1
init_tcsl   = 1
init_phy    = 0

print_info            = 1
print_current_core_id = 1
test_uart             = 1
run_external_memory_test = 1
run_internal_memory_test = 1

init_config_pll1_pllm     = 0
init_config_uart_baudrate = 115200

ext_mem_test_base_addr = 0x80000000
ext_mem_test_length    = 0x1fffffff
int_mem_test_core_id   = 2
